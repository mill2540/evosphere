# Getting Started

## Installing Requirments

1. Install [Unity](https://unity3d.com)
2. Install [nodejs and npm](https://www.npmjs.com/get-npm)
3. To use the prebuilt build scripts, you will want to have [GNU Make](https://www.gnu.org/software/make/) installed. At the moment, this only matters with linux, and possibly Mac

## Getting the Source Code
```bash
git clone git@bitbucket.org:mill2540/evosphere.git
cd evosphere
```

## Build and Launch the UI
```bash
# Assumes that you are the the evosphere directory from above
cd evosphere-ui
npm install # Installs all the npm packages needed to build EvoSphere's UI
npm run-script build # Compile EvoSphere's UI
npm start # If Everything went well, this should launch the UI!
```

## Build the EvoSphere engine
```bash
# Assumes that you are the the evosphere root directory
make # At the moment, this is only known to work on linux
```



