﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannotDeserializeTypeException : Exception
{
    public readonly Type type;

    public CannotDeserializeTypeException(Type type, string message) : base(string.Format("cannot deserialize {0} because {1]", type.FullName, message))
    {
        this.type = type;
    }
}

public interface ConfigFor<out T>
{
    T FromRepresentation();
}

public interface UnityConfigFor<out T> where T : MonoBehaviour
{
    T FromRepresentationUnity(GameObject go);
}

public class ConfigDataBase
{
    public readonly Dictionary<string, object> parameters = new Dictionary<string, object>();

    public object Get(string name) { return parameters[name]; }
    public void Set(string name, object value) { parameters[name] = value; }

    public static object CreateConfigData(Type type)
    {
        if (type.IsSubclassOf(typeof(MonoBehaviour)))
        {
            return Activator.CreateInstance(typeof(UnityConfigData<>).MakeGenericType(type));
        }
        else if (type.GetConstructor(Type.EmptyTypes) != null)
        {
            return Activator.CreateInstance(typeof(ConfigData<>).MakeGenericType(type));
        }
        else
        {
            // TODO: Add support for static deserializsers & using constructors as deserializers 
            throw new CannotDeserializeTypeException(type, "type is not a MonoBehavior and does not have a public parameterless constructor");
        }
    }
}

public class ConfigData<T> : ConfigDataBase, ConfigFor<T> where T : new()
{
    public ConfigData() { }

    public T FromRepresentation()
    {
        // var meta = TypeMeta.MetaDB.Get(typeof(T));
        T instance = new T();
        // meta.Deserializer.InvokeWithNamedParameters(instance, parameters);
        return instance;
    }
}

public class UnityConfigData<T> : ConfigDataBase, UnityConfigFor<T> where T : MonoBehaviour
{
    public UnityConfigData() { }

    public T FromRepresentationUnity(GameObject go)
    {
        // var meta = TypeMeta.MetaDB.Get(typeof(T));
        var instance = go.AddComponent<T>();
        // meta.Deserializer.InvokeWithNamedParameters(instance, parameters);
        return instance;
    }
}