﻿using System;
using YamlDotNet.Core.Tokens;

namespace Config
{
    /// <summary>
    /// A ValueAttribute is an attribute that can be put on a field or property
    /// to indicate that it should be configurable.
    /// </summary>
    /// <example>
    /// <code>
    /// [Register]
    /// public class Foo {
    /// 	[Config.Value] // Now the configuration engine knowns that Foo has a configurable value named bar
    ///     public Bar bar;
    /// }
    /// </code>
    /// </example>
    [AttributeUsage(AttributeTargets.Field |
                    AttributeTargets.Property | AttributeTargets.Parameter,
                    AllowMultiple = false)]
    public class ValueAttribute : Attribute
    {
        /// The name of this property, AS IT WILL APPEAR IN THE CONFIG FILE
        public string Name { get; private set; }

        public ValueAttribute(string name = null)
        {
            Name = name;
        }
    }

}