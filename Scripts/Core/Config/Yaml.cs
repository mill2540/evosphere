﻿using System;
using System.IO;
using YamlDotNet.RepresentationModel;


namespace Config
{
	public class UnexpectedYamlNode : Exception
	{
		public Type Type { get; private set; }
		public YamlNodeType ExpectedYaml { get; private set; }
		public YamlNode GotYaml { get; private set; }

		public UnexpectedYamlNode(Type type, YamlNodeType expected, YamlNode got)
			: base(string.Format(
				"In {0}, expected {1} but got {2}.",
				type.Name,
				expected,
				got.NodeType))
		{
			Type = type;
			ExpectedYaml = expected;
			GotYaml = got;
		}
	}

	public class InvalidYamlNode : Exception
	{
		public Type Type { get; private set; }
		public YamlNodeType Node { get; private set; }

		public InvalidYamlNode(Type type, YamlNodeType node)
			: base(string.Format(
				"Cannot create {0} from node of type {1}.",
				type.Name,
				node))
		{
			Type = type;
			Node = node;
		}
	}

	/// <summary>
	/// A utility class which makes loading yaml files better.
	/// </summary>
	public static class Yaml
	{
		public static YamlNode LoadFromReader(TextReader reader)
		{
			var yamlStream = new YamlStream();
			yamlStream.Load(reader);
			return yamlStream.Documents[0].RootNode;
		}

		public static YamlNode LoadFromString(string yaml)
		{
			using (var reader = new StringReader(yaml))
			{
				return LoadFromReader(reader);
			}
		}
	}
}
