using System.Collections;
using UnityEngine;

public interface IComponentFactory
{
    AbstractOrganismComponent Build(GameObject target);
}

[QueryableName(QueryName.FromType)]
public abstract class AbstractOrganismComponent : Queryable
{
    public abstract AbstractOrganismComponent AddTo(GameObject target);
    public abstract AbstractOrganismComponent Inherit(AbstractOrganismComponent parent);

    public virtual IEnumerator Build()
    {
        yield break;
    }
    
    public virtual void StartRunning() {}
    

    public virtual void Kill()
    {
    }

    public abstract void UpdateComponent(float dt, float[] state);
    public abstract void Pause(bool paused);

    public abstract void PrepInspection();
}