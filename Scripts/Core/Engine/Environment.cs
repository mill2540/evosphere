using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public abstract class EnvironmentComponent : Queryable
{
    public Environment Environment { get; private set;  }

    public void Init(Environment environment)
    {
        Environment = environment;
    }

    public abstract string Pause(bool state);
}

[Doc("Each environment may have several components, such as populations of organisms or groups of obstacles.")]
public interface IEnvironmentComponentFactory
{
    EnvironmentComponent Build(GameObject go);
}

public interface IEnvironmentFactory
{
    Environment Build(GameObject target);
}

[QueryableName(QueryName.FromType)]
public abstract class Environment : Queryable
{
    public Experiment Experiment { get; private set; }

    private List<EnvironmentComponent> components;

    public void Init(List<EnvironmentComponent> components)
    {
        this.components = components;
        foreach (var component in components)
        {
            component.Init(this);
        }
    }

    protected override IEnumerable<Queryable> ToSearch()
    {
        return components.Cast<Queryable>();
    }

    public Environment SetExperiment(Experiment experiment)
    {
        Experiment = experiment;
        return this;
    }

    public string Pause(bool state)
    {
        return components.Select(component => component.Pause(state)).FirstOrDefault(error => error != null);
    }
    

    public abstract void Place(GameObject target);
    public abstract void Remove(GameObject target);
}
