using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using UnityEngine;

public interface IExperimentFactory
{
    Experiment Build();
}

[Doc("An experiment factory is the starting point for everything.")]
[Register]
public class ExperimentFactory : IExperimentFactory
{
    [Doc("Defines an environment to run the experiment in. There are (will be) several different kinds of environments.")]
    [Config.Value] public IEnvironmentFactory Environment;

    public Experiment Build()
    {
        var go = new GameObject();
        var experiment = go.AddComponent<Experiment>();
        experiment.SetEnvironment(Environment.Build(go));
        return experiment;
    }
}

[QueryableName("Experiment")]
public class Experiment : Queryable
{
    public EvoSphereInstance Instance;
    public Environment Environment { get; private set; }

    [Expose]
    public string Pause(bool state)
    {
        return Environment.Pause(state);
    }

    protected override IEnumerable<Queryable> ToSearch()
    {
        yield return Environment;
    }

    public Experiment SetEnvironment(Environment environment)
    {
        Environment = environment;
        return this;
    }
}