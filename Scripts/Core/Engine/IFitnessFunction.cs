public interface IFitnessFunctionFactory
{
    IFitnessFunction Build();
}

public interface IFitnessFunction
{
    void Init(Organism target);

    float CalculateNow(Organism target);

    IFitnessFunction Reset();
}
