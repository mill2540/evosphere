﻿//using System;
//using System.Collections;
//using System.Collections.Generic;
//using System.Linq;
//using YamlDotNet.RepresentationModel;
//using System.IO;
//using System.Net;
//using System.Net.Mime;
//using System.Net.Sockets;
//using System.Runtime.Remoting.Messaging;
//using Newtonsoft.Json;
//using UnityEngine;
//using Newtonsoft.Json.Linq;
//using UnityEngine.Rendering;
//
//public class JS : Queryable
//{
//#if UNITY_EDITOR
//    private HttpListener server = null;
//    private IAsyncResult handle;
//#endif
//
//    public JObject LoadExperimentFromJson(string jsonDesc)
//    {
//        return LoadExperimentFromJsonObject(JObject.Parse(jsonDesc));
//    }
//
//    public JObject LoadExperimentFromJsonObject(JObject json)
//    {
//        var experiment = (IExperimentFactory) Values.ValueFactory.FromJson(json).AsObject();
//        experiment.Build();
//
//        return new JObject();
//    }
//
//    public JObject JsonToYaml(string jsonString)
//    {
//        var json = JObject.Parse(jsonString);
//        var value = Values.ValueFactory.FromJson(json);
//
//        var stream = new YamlStream(new YamlDocument(Values.ValueFactory.AsYamlRoot(value)));
//        using (var writer = new StringWriter())
//        {
//            stream.Save(writer, false);
//            var result = new JObject();
//            result["value"] = new JValue(writer.ToString());
//            return result;
//        }
//    }
//
//    public JObject YamlToJson(string yamlString)
//    {
//        using (var reader = new StringReader(yamlString))
//        {
//            var stream = new YamlStream();
//            stream.Load(reader);
//
//            var value = Values.ValueFactory.FromYaml(null, stream.Documents[0].RootNode).Value;
//            return value.AsJson();
//        }
//    }
//
//    public JObject GetTypeMetaInterface(string typeName)
//    {
//        var type = Type.GetType(typeName);
//        return GetTypeMeta(type);
//    }
//
//    public static JObject GetTypeMeta(Type type)
//    {
//        return TypeMeta.MetaDB.Get(type).AsJson();
//    }
//
//    public void JSReturn(string id, string args)
//    {
//        Application.ExternalCall("unityCallReturning", id, args);
//    }
//
//    public void JSError(string id, string error)
//    {
//        Application.ExternalCall("unityCallError", id, error);
//    }
//
//    public void UnityCall(string jsonCallSpec)
//    {
//        var callSpec = JObject.Parse(jsonCallSpec);
//        var id = callSpec["id"].ToString();
//        var function = callSpec["function"].ToString();
//        var args = callSpec["args"];
//
//        var method = GetType().GetMethod(function);
//        if (method == null)
//        {
//            JSError(id, string.Format("No such method as \"{0}\"", function));
//            return;
//        }
//
//        var ret = method.Invoke(this, args.Select(arg => arg.ToString()).ToArray());
//        if (ret == null)
//        {
//            JSReturn(id, null);
//        }
//        if (ret.GetType() != typeof(void))
//        {
//            JSReturn(id, ret.ToString());
//        }
//    }
//
//    [Expose]
//    public void HelloWorld()
//    {
//    }
//
//    [Expose]
//    public int HelloWorld(int x)
//    {
//        return x;
//    }
//
//    [Expose]
//    public int HelloWorld(int x, int y, string z = "Hello")
//    {
//        return y;
//    }
//
//    public void Start()
//    {
//
//
//#if UNITY_EDITOR
//        var addressString = File.ReadAllText("./default").Split(':');
//        var ip = addressString[0];
//        var port = int.Parse(addressString[1]);
//#endif
//
//#if UNITY_STANDALONE
//        server = new HttpListener();
//        server.Prefixes.Add("http://localhost:5050/");
//
//        server.Start();
//        handle = server.BeginGetContext(new AsyncCallback(GetContext), server);
//#endif
//
//
//#if UNITY_STANDALONE && !UNITY_EDITOR
//        using (var reader = new StreamReader("./run_config.yaml"))
//        {
//            var stream = new YamlStream();
//            stream.Load(reader);
//
//            var root = stream.Documents[0].RootNode as YamlMappingNode;
//            
//            if (root == null)
//            {
//                Debug.LogError("Unexpected root in config document");
//                Application.Quit();
//                return;
//            }
//
//            // TODO: we will want to setup TCP/files/streams here...
//            var configFile = root["config_file"] as YamlScalarNode;
//
//            if (configFile == null)
//            {
//                Debug.LogError("config_file is not a string!");
//                Application.Quit();
//                return;
//            }
//            
//            var defaultConfig = File.ReadAllText(configFile.Value);
//            LoadExperimentFromJsonObject(YamlToJson(defaultConfig));
//            return;
//        }
//        Debug.LogError("Could not open config file!");
//
//#endif
//
//#if UNITY_WEBGL && !UNITY_EDITOR
//        Application.ExternalCall("UnityReady");
//#endif
//    }
//
//#if UNITY_STANDALONE
//    private void GetContext(IAsyncResult result)
//    {
//        var listener = (HttpListener) result.AsyncState;
//        var context = listener.EndGetContext(result);
//        var request = context.Request;
//        var response = context.Response;
//
//        using (var responseStream = response.OutputStream)
//        using (var responseWriter = new StreamWriter(responseStream))
//        {
//            responseWriter.Write("Hello World!");
//        }
//
//        Debug.Log(request.Url);
//    }
//
//    private void Update()
//    {
//        if (handle != null)
//        {
//            handle.AsyncWaitHandle.WaitOne(1);
//        }
//    }
//
//    private void OnDestroy()
//    {
//        server.Stop();
//    }
//#endif
//}