using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using UnityEngine;

public delegate void OrganismFinished(Organism child);

public enum OrganismPhase
{
    Preinherit,
    Inheriting,
    Inherited,
    Building,
    Ready,
    Running,
    Dead
}

[Register]
[Doc("Defines a organism. An organism is the reproductive unit in EvoSphere.")]
public class Organism : Queryable, IComparable<Organism>
{
    public Environment Environment { get; private set; }

    private IFitnessFunction fitnessFunction;

    [Expose]
    public float Fitness { get; private set; }

    [Expose]
    public QueryableId Parent { get; private set; }

    [Expose]
    public OrganismPhase Phase { get; private set; }

    private List<AbstractOrganismComponent> components;
    private float[] state;

    [Deserializer]
    public void Deserialize(
        [Doc("The number of communication channels that are available in this organism")]
        [Config.Value] int StateCount,

        [Doc("The fitness function that will be used to evaluate this oranism")]
        [Config.Value] IFitnessFunctionFactory FitnessFunction,

        [Doc("The subcomponents (organs?) of the organism")]
        [Config.Value] UnityConfigFor<AbstractOrganismComponent>[] Components)
    {
        InitState(StateCount)
            .SetFitness(FitnessFunction.Build())
            .AddComponents(Components.Select(component =>
            {
                return component.FromRepresentationUnity(gameObject);
            }))
            .Inherit(null);
    }

    public void Init(Environment environment)
    {
        Environment = environment;
        Environment.Place(gameObject);
    }

    protected override void OnAwake()
    {
        Phase = OrganismPhase.Preinherit;
    }

    public Organism SetFitness(IFitnessFunction fitnessFunction)
    {
        this.fitnessFunction = fitnessFunction;
        return this;
    }

    public Organism InitState(int size)
    {
        state = new float[size];
        return this;
    }

    public Organism AddComponents(IEnumerable<AbstractOrganismComponent> components)
    {
        if (this.components == null)
        {
            this.components = new List<AbstractOrganismComponent>();
        }

        this.components.AddRange(components);
        return this;
    }

    public Organism Replicate()
    {
        var obj = new GameObject();

        var child = obj.AddComponent<Organism>();
        child.Parent = QueryableId;
        return child.Inherit(this);
    }

    public Organism Inherit(Organism parent)
    {
        Debug.Assert(Phase == OrganismPhase.Preinherit, "Cannot inherit on organism that has already been inherited");

        Phase = OrganismPhase.Inheriting;
        // Inherit the components from the parent
        if (parent != null)
        {
            InitState(parent.state.Length);
            SetFitness(parent.fitnessFunction.Reset());

            components = parent.components.Select(component =>
                component.AddTo(gameObject).Inherit(component)
            ).ToList();
        }
        else
        {
            // Assume that the factory added all the components properly
            components.ForEach(component => component.Inherit(null));
        }
        Phase = OrganismPhase.Inherited;

        return this;
    }

    public IEnumerator Build(OrganismFinished onFinished)
    {
        Debug.Assert(Phase == OrganismPhase.Inherited, "Inherit has not completed");

        Phase = OrganismPhase.Building;
        foreach (var component in components)
        {
            yield return StartCoroutine(component.Build());
        }

        Phase = OrganismPhase.Ready;
        onFinished(this);
        fitnessFunction.Init(this);
    }

    public IEnumerator Build(Action onFinished)
    {
        return Build(_ => onFinished());
    }

    public void StartRunning()
    {
        Debug.Assert(Phase == OrganismPhase.Ready, "Organism is not Ready");
        foreach (var component in components)
        {
            component.StartRunning();
        }

        Phase = OrganismPhase.Running;
    }

    public void Kill()
    {
        Debug.Assert(Phase == OrganismPhase.Running, "Cannot kill organism that is not running");
        Phase = OrganismPhase.Dead;
        components.ForEach(component => component.Kill());
        Environment.Remove(gameObject);
    }

    public void UpdateOrganism(float dt)
    {
        foreach (var component in components)
        {
            component.UpdateComponent(dt, state);
        }
        Fitness = fitnessFunction.CalculateNow(this);
    }

    private void Update()
    {
        if (Phase == OrganismPhase.Running)
        {
            UpdateOrganism(Time.deltaTime);
        }
    }

    public int CompareTo(Organism other)
    {
        return Fitness.CompareTo(other.Fitness);
    }

    public void Pause(bool paused)
    {
        Debug.Assert(Phase == OrganismPhase.Running, "Cannot pause organism that is not running");
        foreach (var component in this.components) component.Pause(paused);
        enabled = !paused;
    }

    public Organism GetInspectable()
    {
        var inspectable = Instantiate(gameObject);

        foreach (var component in inspectable.GetComponentsInChildren<AbstractOrganismComponent>())
        {
            component.PrepInspection();
        }

        return inspectable.GetComponent<Organism>();
    }

    protected override IEnumerable<Queryable> ToSearch()
    {
        return components.Cast<Queryable>();
    }
}