using UnityEngine;

public abstract class OrganismComponent<TD> : AbstractOrganismComponent where TD : OrganismComponent<TD>
{
    public override AbstractOrganismComponent AddTo(GameObject target)
    {
        return target.AddComponent<TD>();
    }

    public abstract void Inherit(TD parent);

    public override AbstractOrganismComponent Inherit(AbstractOrganismComponent parent)
    {
        Inherit(parent as TD);
        return this;
    }
}