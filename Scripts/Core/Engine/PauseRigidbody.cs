﻿using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class PauseRigidbody : MonoBehaviour
{
    private Vector3 angularVelocity;
    private Vector3 velocity;
    private Rigidbody rigidbody;

    private Rigidbody Rigidbody
    {
        get { return rigidbody ?? (rigidbody = GetComponent<Rigidbody>()); }
    }

    private void ApplyPause()
    {
        velocity = Rigidbody.velocity;
        angularVelocity = Rigidbody.angularVelocity;
        Rigidbody.isKinematic = true;
    }

    private void RemovePause()
    {
        Rigidbody.velocity = velocity;
        Rigidbody.angularVelocity = angularVelocity;
        Rigidbody.isKinematic = false;
        Rigidbody.WakeUp();
    }

    private void Pause(bool paused)
    {
        if(paused) ApplyPause();
        else RemovePause();
    }

    public static void Pause(Component target, bool paused)
    {
        foreach (var rb in target.GetComponentsInChildren<Rigidbody>())
        {
            var pr = rb.GetComponent<PauseRigidbody>() ?? rb.gameObject.AddComponent<PauseRigidbody>();
            pr.Pause(paused);
        }
    }
    
}