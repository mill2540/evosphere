using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using JetBrains.Annotations;
using UnityEngine;

public interface ISelectorProvider
{
    ISelector Init(List<Organism> population);
}

public interface ISelector
{
    Organism Select();
}

[Register]
[Doc("Adds a homogeneous population to an evironment. It contains a collection of organisms, all of whom are of the same type.")]
public class HomogeneousPopulationFactory : IEnvironmentComponentFactory
{
    [Config.Value] public int PopulationSize;
    [Config.Value] public ISelectorProvider SelectorProvider;
    [Config.Value] public UnityConfigFor<Organism> Organism;
    [Config.Value] public float GenerationLength;

    public EnvironmentComponent Build(GameObject go)
    {
        return go.AddComponent<Population>()
            .SetSelector(SelectorProvider)
            .SetGenerationLength(GenerationLength)
            .AddOrganisms(Enumerable.Range(0, PopulationSize)
                .Select(_ => Organism.FromRepresentationUnity(new GameObject())));
    }
}

[Register]
[Doc("Adds a population to an evironment. It contains a collection of organisms, of several different types.")]
public class PopulationFactory : IEnvironmentComponentFactory
{
    [Config.Value] public int PopulationSize;
    [Config.Value] public ISelectorProvider SelectorProvider;
    [Config.Value] public UnityConfigFor<Organism>[] Organisms;
    [Config.Value] public float GenerationLength;

    public EnvironmentComponent Build(GameObject go)
    {
        return go.AddComponent<Population>()
            .SetSelector(SelectorProvider)
            .SetGenerationLength(GenerationLength)
            .AddOrganisms(Enumerable.Range(0, PopulationSize)
                .Select(_ => BuildOrganism(new GameObject())));
    }

    private Organism BuildOrganism(GameObject go)
    {
        var idx = (int) (UnityEngine.Random.value * Organisms.Length);
        return Organisms[idx].FromRepresentationUnity(go);
    }

}

[QueryableName("Population")]
public class Population : EnvironmentComponent
{
    private readonly List<Organism> population = new List<Organism>();
    private ISelectorProvider selectorProvider;
    private bool paused;
    private bool running;

    public new class Events
    {
        public static readonly string GenerationStarting = "GenerationStarting";
        public static readonly string GenerationEnding = "GenerationEnding";
    }

    protected override void OnAwake()
    {
        Fires(Queryable.Events.HierarchyUpdated);
        Fires(Events.GenerationStarting);
        Fires(Events.GenerationEnding);
    }

    [Expose] private float generationLength;
    [Expose] private float generationLengthRemaining;
    [Expose] private bool populationRunning = false;

    [Expose]
    public int GenerationCount { get; private set; }

    [Expose]
    public int PopulationSize
    {
        get { return population.Count; }
    }

    public Population SetGenerationLength(float generationLength)
    {
        this.generationLength = generationLength;
        return this;
    }

    public Population SetSelector(ISelectorProvider selectorProvider)
    {
        this.selectorProvider = selectorProvider;
        return this;
    }

    public Population AddOrganisms(IEnumerable<Organism> organisms)
    {
        population.AddRange(organisms);
        Fire(Queryable.Events.HierarchyUpdated);
        return this;
    }

    private void ReplicatePopulation()
    {
        population.ForEach(member => member.Kill());
        var count = population.Count;
        var nextPopulation = new List<Organism> { Capacity = count };

        var selector = selectorProvider.Init(population);
        while (nextPopulation.Count < count)
        {
            var child = selector.Select().Replicate();
            nextPopulation.Add(child);
        }

        population.ForEach(member => Destroy(member.gameObject));
        population.Clear();
        AddOrganisms(nextPopulation);

        Resources.UnloadUnusedAssets();
    }

    private void StartBuildingPopulation()
    {
        enabled = false;
        var unbuilt = population.Count;

        OrganismFinished finished = organism =>
        {
            --unbuilt;
            if (unbuilt == 0)
            {
                FinishedBuildingPopulation();
            }
        };

        foreach (var member in population)
        {
            member.Init(Environment);
            member.transform.SetParent(transform);
            // TODO: Handle timeouts?
            StartCoroutine(member.Build(finished));
        }
    }

    private void FinishedBuildingPopulation()
    {
        population.ForEach(member => member.StartRunning());
        Fire(Events.GenerationStarting);
        enabled = true;
        running = true;
    }

    private void Update()
    {
        if (generationLengthRemaining > 0)
        {
            generationLengthRemaining -= Time.deltaTime;
            return;
        }

        running = false;

        generationLengthRemaining = generationLength;
        // Start the next generation
        ++GenerationCount;
        Fire(Events.GenerationEnding);
        ReplicatePopulation();
        StartBuildingPopulation();
    }

    private void Start()
    {
        StartBuildingPopulation();
        generationLengthRemaining = generationLength;
    }

    protected override IEnumerable<Queryable> ToSearch()
    {
        return population.Cast<Queryable>();
    }

    public override string Pause(bool state)
    {
        paused = state;
        if (running)
        {
            foreach (var member in population)
            {
                member.Pause(state);
            }
            enabled = !paused;
            return null;
        }
        return "Cannot pause a population when it is not running.";
    }
}