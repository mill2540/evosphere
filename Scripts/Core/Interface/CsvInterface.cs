﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Config;
using Newtonsoft.Json.Linq;
using UnityEngine;

[Register]
public class CsvInterfaceFactory : IInterfaceFactory
{
    [Value] public TargetedQuery Index;
    [Value] public MultiTargetedQuery Watch;
    [Value] public TargetedQuery? On;
    [Value] public float? Period;
    [Value] public string Output;

    public Interface Build(GameObject go)
    {
        return go.AddComponent<CsvInterface>().Init(Index, Watch, On, Period, new StreamWriter(Output, false));
    }
}

public class CsvInterface : Interface
{
    private TargetedQuery index;
    private MultiTargetedQuery watch;
    private TargetedQuery? on;
    private float? period;
    private StreamWriter output;

    public CsvInterface Init(TargetedQuery index, MultiTargetedQuery watch, TargetedQuery? on, float? period,
        StreamWriter output)
    {
        this.index = index;
        this.watch = watch;
        this.on = on;
        this.period = period;
        this.output = output;

        return this;
    }


    protected override void OnStart()
    {
        if (period != null)
        {
            StartCoroutine(EveryPeriod((float)period));
        }
        ScheduleOnEvent();


        output.Write("\"Name\",\"{0}\"", index.Target);
        foreach (var target in watch.Targets) output.Write(",\"{0}\"", target);
        output.WriteLine();
        output.Flush();
    }

    private void ScheduleOnEvent()
    {
        if (on == null) return;

        var onQuery = (TargetedQuery)@on;
        Query(onQuery.Query).Subscribe(onQuery.Target, RunQuery);
    }

    private IEnumerator EveryPeriod(float period)
    {
        yield return new WaitForSeconds(period);
        RunQuery();
    }

    private void RunQuery(Queryable q = null)
    {
        ScheduleOnEvent();

        var index = Query(this.index.Query).Call(this.index.Target).First();
        var indexValue = index.ValueAsJson();

        foreach (var responder in Query(watch.Query))
        {
            output.Write("\"{0}\",\"{1}\"", responder.QueryableName, indexValue);

            foreach (var target in watch.Targets)
            {
                foreach (var response in responder.Call(target))
                {
                    output.Write(",\"{0}\"", response.ValueAsJson());
                }
            }
            output.WriteLine();
        }
        output.Flush();
    }

    private void OnDisable()
    {
        if (output != null) output.Close();
        StopAllCoroutines();
    }
}