using System.Collections.Generic;
using System.IO;
using GUI;
using Newtonsoft.Json.Linq;
using UnityEngine;
using Values;
using YamlDotNet.RepresentationModel;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class EvoSphereInstance : MonoBehaviour
{
    public Experiment Experiment { get; private set; }
    private readonly List<Interface> interfaces = new List<Interface>();
    public CameraSelect mainCamera;
    public InspectorCamera inspectorCamera;
    public GameObject[] uiViews;

#if UNITY_EDITOR
    [SerializeField]
    private string settingsFile;
#endif

    private void Awake()
    {
        gameObject.AddComponent<StdOut>();
    }

    private void Start()
    {
        interfaces.AddRange(GetComponents<Interface>());
#if UNITY_EDITOR

        if (settingsFile == null || !File.Exists(settingsFile))
        {
            settingsFile = EditorUtility.OpenFilePanel("Select Settings File", "", "yaml");
        }
        LoadSettingsFromFile(settingsFile);

#elif UNITY_STANDALONE
        string cli = System.Environment.GetEnvironmentVariable("EVOSPHERE_CONFIG");
        string stdOut = System.Environment.GetEnvironmentVariable("EVOSPHERE_STDOUT_FILE");
        if (stdOut == null) stdOut = "stdout." + System.Diagnostics.Process.GetCurrentProcess().Id;
        StdOut.Instance.Init(stdOut);
        StdOut.Instance.WriteLine(System.Diagnostics.Process.GetCurrentProcess().Id);
        StdOut.Instance.WriteLine("Loading config:");
        StdOut.Instance.WriteLine(cli);

        LoadSettingsFromString(cli);
#elif UNITY_WEBGL
        foreach (var canvas in uiViews)
        {
            Destroy(canvas);
        }
        interfaces.Add(gameObject.AddComponent<WebGLInterface>());
#endif

        // Ensure that the correct camera is used by default
        mainCamera.GetComponent<Camera>().enabled = true;
        inspectorCamera.enabled = false;

        foreach (var @interface in interfaces) @interface.Started(this, Experiment);
    }

    public void LoadSettingsFromFile(string path)
    {
        using(var reader = new StreamReader(path))
        {
            var stream = new YamlStream();
            stream.Load(reader);

            LoadSettingsFromDocument(stream.Documents[0]);
        }
    }

    public void LoadSettingsFromString(string settings)
    {
        var document = new YamlDocument(settings);
        YamlScalarNode root;
        if ((root = document.RootNode as YamlScalarNode) != null)
        {
            LoadSettingsFromFile(root.Value);
        }
        else
        {
            LoadSettingsFromDocument(document);
        }
    }

    public void LoadSettingsFromDocument(YamlDocument document)
    {
        YamlMappingNode root;
        if ((root = document.RootNode as YamlMappingNode) == null)
        {
            Debug.LogError("Unexpected root in config document");
            Application.Quit();
            return;
        }
        Debug.Log(root.ToString());

        LoadInterfaceConfigs(root);

        LoadExperimentConfig(root);
    }

    public void LoadInterfaceConfigs(YamlMappingNode root)
    {
        YamlMappingNode interfaceConfigs;
        if (!root.Children.ContainsKey("Interfaces") ||
            (interfaceConfigs = root.Children["Interfaces"] as YamlMappingNode) == null)
            return;

        foreach (var interfaceConfig in interfaceConfigs)
        {
            YamlScalarNode interfaceName;
            if ((interfaceName = interfaceConfig.Key as YamlScalarNode) == null)
            {
                Debug.LogError("Invalid interface name " + interfaceConfig.Key);
                Application.Quit();
                return;
            }

            var interfaceFactoryConfig = interfaceConfig.Value;

            var @interface = LoadFromYaml<UnityConfigFor<Interface>>(interfaceFactoryConfig).FromRepresentationUnity(gameObject);
            @interface.QueryableName = interfaceName.Value;
            interfaces.Add(@interface);
        }
    }

    public IExperimentFactory LoadExperimentFactory(YamlNode node)
    {
        var experimentConfigPath = node as YamlScalarNode;
        if (experimentConfigPath != null)
        {
            return LoadExperimentFromFile(experimentConfigPath.Value);
        }

        var experimentConfig = node as YamlMappingNode;
        if (experimentConfig != null)
        {
            return LoadFromYaml<IExperimentFactory>(experimentConfig);
        }

        Debug.LogError("Not a valid experiment config: " + node);
        Application.Quit();
        return null;
    }

    public void LoadExperimentConfig(YamlMappingNode root)
    {
        IExperimentFactory experimentFactory;

        if (root.Children.ContainsKey("Experiment"))
        {
            experimentFactory = LoadExperimentFactory(root["Experiment"]);
        }
        else
        {
            Debug.LogWarning("No experiment configured.");
            return;
        }

        Experiment = experimentFactory.Build();
        Experiment.Instance = this;
    }

    public void LoadExperimentFromJson(JObject json)
    {
        var experiment = (IExperimentFactory) ValueFactory.FromJson(json).AsObject();
        Experiment = experiment.Build();
        Experiment.Instance = this;
    }

    public void UnloadExperiment()
    {
        Destroy(Experiment);
        foreach (var @interface in interfaces) @interface.Exited();
    }

    public IExperimentFactory LoadExperimentFromFile(string path)
    {
        if (!File.Exists(path))
        {
#if UNITY_EDITOR
            EditorUtility.DisplayDialog(
                "\"" + path + "\" is not a valid path",
                "Select a valid experiement configuration", "Ok");
            path = EditorUtility.OpenFilePanel("Select a valid experiement configuration", "", "yaml");
            if (path == null) return null;
            return LoadExperimentFromFile(path);
#else
            Debug.LogError(path + " does not exist.");
            Application.Quit();
            return null;
#endif
        }

        using(var reader = new StreamReader(path))
        {
            var stream = new YamlStream();
            stream.Load(reader);

            if (stream.Documents.Count != 0)
                return LoadFromYaml<IExperimentFactory>(stream.Documents[0].RootNode);

            Debug.LogError("Environment config is empty");
            Application.Quit();
            return null;
        }
    }

    public T LoadFromYaml<T>(YamlNode config) where T : class
    {
        var loadResult = ValueFactory.FromYaml(typeof(T), config);
        Debug.Log(loadResult);
        var buildResult = loadResult.AsObject();
        T experimentFactory = null;
        if ((experimentFactory = buildResult as T) != null) return experimentFactory;
        Debug.LogError("Expected a " + typeof(T).Name + ", but got a " + buildResult.GetType().Name);
        Application.Quit();
        return null;
    }

    public string Pause(bool paused)
    {
        return Experiment != null ? Experiment.Pause(paused) : null;
    }

    public JObject EnterInspector()
    {
        if (mainCamera.Selection != null)
        {
            Pause(true);
            mainCamera.GetComponent<Camera>().enabled = false;
            mainCamera.enabled = false;
            return inspectorCamera.Enter(mainCamera.Selection.GetInspectable());
        }
        return new JObject();
    }

    public void ExitInspector()
    {
        inspectorCamera.Exit();
        Pause(false);

        mainCamera.GetComponent<Camera>().enabled = true;
        mainCamera.enabled = true;
    }
}