using UnityEngine;

public interface IInterfaceFactory
{
    Interface Build(GameObject go);
}