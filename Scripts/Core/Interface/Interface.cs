﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;
using YamlDotNet.RepresentationModel;

public class Interface : Queryable
{
    public EvoSphereInstance Instance { get; private set; }
    public Experiment Experiment { get; private set; }

    public new class Events : Queryable.Events
    {
        public static readonly string SelectionChanged = "SelectionChanged";
    }

    public void Started(EvoSphereInstance instance, Experiment experiment)
    {
        Instance = instance;
        instance.mainCamera.OnSelection += FireSelection;

        Experiment = experiment;

        Fires(Events.SelectionChanged);

        OnStart();
    }

    protected virtual void OnStart() { }

    public void Exited()
    {
        if (Instance != null)
        {
            Instance.mainCamera.OnSelection -= FireSelection;
            Instance = null;
        }

        if (Experiment != null)
        {
            Experiment = null;
        }

        OnExit();
    }

    protected virtual void OnExit() { }

    protected override IEnumerable<Queryable> ToSearch()
    {
        if (Experiment != null) yield return Experiment;
    }

    [Expose]
    public void Pause(bool paused)
    {
        Instance.Pause(paused);
    }

    [Expose]
    public JObject EnterInspector()
    {
        return Instance.EnterInspector();
    }

    [Expose]
    public void ExitInspector()
    {
        Instance.ExitInspector();
    }

    [Expose]
    public void EnterInspectorView(string inspector, string view)
    {
        Instance.inspectorCamera.Enter(inspector, view);
    }

    //    [Expose]
    //    public void Select(string id)
    //    {
    //        var targets = Query("#" + id).Selected;
    //        if (targets.Count == 1)
    //        {
    //            var target = targets[0] as Organism;
    //            if (target != null)
    //            {
    //                
    //            }
    //        }
    //    }

    [Expose] public QueryableId? SelectedId;

    public void FireSelection(Organism target)
    {
        if (target != null) SelectedId = target.QueryableId;
        else SelectedId = null;

        Fire(Events.SelectionChanged);
    }
}