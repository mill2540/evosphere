﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using TypeMeta;
using Values;
using YamlDotNet.RepresentationModel;

[Serializable]
public struct JsonResponse : IAsJson
{
    public string Id;
    public CallResult[] Responses;

    public JsonResponse(string id, IEnumerable<CallResult> responses)
    {
        Id = id;
        Responses = responses.ToArray();
    }

    public JToken AsJson()
    {
        var obj = new JObject();

        obj["Id"] = Id;
        var responses = new JArray();
        foreach (var response in Responses) responses.Add(response.AsJson());
        obj["Responses"] = responses;

        return obj;
    }
}

public struct JsonRequest
{
    public readonly string Id;
    public readonly TargetedQuery Query;
    public readonly JToken[] Arguments;

    public JsonRequest(string id, TargetedQuery query, JToken[] arguments)
    {
        Id = id;
        Query = query;
        Arguments = arguments;
    }

    public JsonResponse Call(Queryable target)
    {
        return new JsonResponse(Id, target.Query(Query.Query).Call(Query.Target, Arguments));
    }
}

public abstract class JsonInterface : Interface
{
    public JToken HandleJson(JObject json)
    {
        return HandleJson(TargetedQuery.Parse(json["Query"].ToString()), json);
    }

    public JToken HandleJson(TargetedQuery query, JObject json)
    {
        var request = new JsonRequest(
            json["Id"].ToString(),
            query,
            json["Arguments"].ToArray());

        return request.Call(this).AsJson();
    }

    // [Expose]
    // public Meta GetTypeInfo(string typeName)
    // {
    //     var type = Type.GetType(typeName);
    //     var meta = MetaDB.Get(type);

    //     return meta;
    // }

    [Expose]
    public string JsonToYaml(string jsonString)
    {
        var json = JObject.Parse(jsonString);
        var value = Values.ValueFactory.FromJson(json);

        var stream = new YamlStream(new YamlDocument(Values.ValueFactory.AsYamlRoot(value)));
        using(var writer = new StringWriter())
        {
            stream.Save(writer, false);

            return writer.ToString();
        }
    }

    [Expose]
    public IValue YamlToJson(string yamlString)
    {
        using(var reader = new StringReader(yamlString))
        {
            var stream = new YamlStream();
            stream.Load(reader);

            var value = Values.ValueFactory.FromYaml(null, stream.Documents[0].RootNode);
            return value;
        }
    }

    [Expose]
    public string ExperimentId
    {
        get
        {
            var experiment = GetComponentInParent<EvoSphereInstance>().Experiment;
            return experiment != null ? experiment.QueryableId.ToString() : null;
        }
    }

    [Expose]
    public string LoadExperimentFromJson(string experimentJson)
    {
        var experiment = JObject.Parse(experimentJson);
        GetComponentInParent<EvoSphereInstance>().LoadExperimentFromJson(experiment);
        return GetComponentInChildren<EvoSphereInstance>().Experiment.QueryableId.ToString();
    }
}