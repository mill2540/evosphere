﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;

public class StdOut : MonoBehaviour
{

    private StreamWriter writer;
    private string path;

    public static StdOut Instance { get; private set; }

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void OnDestroy()
    {
        if (writer != null)
        {
            writer.Close();
            // Make sure that the writer gets properly cleaned up
            writer.Dispose();
        }

        Application.logMessageReceived -= CatchLogMessages;
    }

    public void Init(string path)
    {
        this.path = path;
        using (File.Create(path)) ;

        writer = new StreamWriter(this.path, false)
        {
            AutoFlush = true,
        };

        Application.logMessageReceived += CatchLogMessages;

    }

    private void CatchLogMessages(string logString, string stackTrace, LogType type)
    {
        WriteLine(logString);
        if (stackTrace != null && stackTrace != "" && (type == LogType.Error || type == LogType.Exception || type == LogType.Assert))
        {
            Write(stackTrace);
        }
    }

    public void Flush()
    {
        writer.Flush();
    }

    public void Write(bool value)
    {
        writer.Write(value);
    }

    public void Write(char value)
    {
        writer.Write(value);
    }

    public void Write(char[] value)
    {
        writer.Write(value);
    }

    public void Write(char[] value, int begin, int end)
    {
        writer.Write(value, begin, end);
    }

    public void Write(decimal value)
    {
        writer.Write(value);
    }

    public void Write(double value)
    {
        writer.Write(value);
    }

    public void Write(int value)
    {
        writer.Write(value);
    }

    public void Write(long value)
    {
        writer.Write(value);
    }

    public void Write(Object value)
    {
        writer.Write(value);
    }

    public void Write(float value)
    {
        writer.Write(value);
    }

    public void Write(string value)
    {
        writer.Write(value);
    }

    public void Write(string value, object arg0)
    {
        writer.Write(value, arg0);
    }

    public void Write(string value, object arg0, object arg1)
    {
        writer.Write(value, arg0, arg1);
    }

    public void Write(string value, object arg0, object arg1, object arg2)
    {
        writer.Write(value, arg0, arg1, arg2);
    }

    public void Write(string value, object[] args)
    {
        writer.Write(value, args);
    }

    public void Write(uint value)
    {
        writer.Write(value);
    }

    public void Write(ulong value)
    {
        writer.Write(value);
    }

    public void WriteLine()
    {
        writer.WriteLine();
    }

    public void WriteLine(bool value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(char value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(char[] value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(char[] value, int begin, int end)
    {
        writer.WriteLine(value, begin, end);
    }

    public void WriteLine(decimal value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(double value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(int value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(long value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(object value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(float value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(string value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(string value, object arg0)
    {
        writer.WriteLine(value, arg0);
    }

    public void WriteLine(string value, object arg0, object arg1)
    {
        writer.WriteLine(value, arg0, arg1);
    }

    public void WriteLine(string value, object arg0, object arg1, object arg2)
    {
        writer.WriteLine(value, arg0, arg1, arg2);
    }

    public void WriteLine(string value, object[] args)
    {
        writer.WriteLine(value, args);
    }

    public void WriteLine(uint value)
    {
        writer.WriteLine(value);
    }

    public void WriteLine(ulong value)
    {
        writer.WriteLine(value);
    }
}
