﻿using System.Runtime.InteropServices;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;

[Register]
public class WebGLInterfaceFactory : IInterfaceFactory
{
    public Interface Build(GameObject go)
    {
        return go.AddComponent<WebGLInterface>();
    }
}

public class WebGLInterface : JsonInterface
{
    // See Pluings/webgl.jslib
    [DllImport("__Internal")]
    private static extern void WebGLUnityReady(string name);

    [DllImport("__Internal")]
    private static extern void WebGLUnityEvent(string e);

    [DllImport("__Internal")]
    private static extern void WebGLUnityResponse(string r);

    protected override void OnAwake()
    {
        WebGLUnityReady(gameObject.name);

#if UNITY_WEBGL
        // WebGLInput.captureAllKeyboardInput = false;
#endif
    }

    [Expose]
    public void Subscribe(string queryStr, string id)
    {
        var query = TargetedQuery.Parse(queryStr);

        Query(query.Query).Subscribe(query.Target, queryable =>
        {
            var response = new JObject();
            response["subscriber"] = id;
            response["queryable"] = queryable.QueryableId.ToString();
            WebGLUnityEvent(response.ToString());
        });
    }

    [Expose]
    public void UnloadExperiment()
    {
        if (Instance != null)
        {
            Instance.UnloadExperiment();
        }

    }

    public void ExternalHandleWebGLRequest(string jsonString)
    {
        var json = JObject.Parse(jsonString);

        var response = HandleJson(json);
        WebGLUnityResponse(response.ToString());
    }
}