﻿#if !UNITY_WEBGL

using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;
using WebSocketSharp;
using WebSocketSharp.Server;

[Register]
public class WebSocketInterface : JsonInterface
{
    [SerializeField]
    private string url;

    // It turns out that unity won't let us do most stuff from a thread that is not the main thread.
    // Generally, this is fine because it means there's a lot of ways we can't shoot ourselves in the foot.
    // However, it creates a problem here, because the websocket library uses a threadpool, so we can't perform 
    // operations from the websocket client threads.
    // 
    // We solve this using this collection of queues. 
    // Basically, we give each client a queue it can pass requests into.
    // The we can communicate with the socket through the queues 
    private ReaderWriterLockSlim connectionsLock = new ReaderWriterLockSlim();
    private List<Connection> connections = new List<Connection>();

    private void Connect(Socket socket)
    {
        connectionsLock.EnterReadLock();
        try
        {
            // Try to find a connection that isn't already connected to anything,
            // and if we do, connect to it
            foreach (var connection in connections)
            {
                if (connection.ConnectIfNotConnected(socket))
                {
                    return;
                }
            }
        }
        finally { connectionsLock.ExitReadLock(); }

        connectionsLock.EnterWriteLock();
        try
        {
            // We failed to find one, so we have to create a new one
            this.connections.Add(new Connection());
            this.connections[this.connections.Count - 1].Connect(socket);
        }
        finally { connectionsLock.ExitWriteLock(); }
    }

    class Connection : IDisposable
    {
        public ConcurrentQueue<JObject> fromSocket;
        public ReaderWriterLockSlim connectionLock;
        public Socket socket;

        public Connection()
        {
            this.connectionLock = new ReaderWriterLockSlim();
            this.socket = null;
            this.fromSocket = new ConcurrentQueue<JObject>();
        }

        public void SendToUnity(JObject o)
        {
            this.fromSocket.Enqueue(o);
        }

        public void SendToSocket(JToken t)
        {
            // We have to lock this, because if someone tried to change which 
            // socket we were connected to while we were doing this it would be bad
            this.connectionLock.EnterReadLock();
            try
            {
                // Hopefully this is threadsafe
                socket.SendJSON(t);
            }
            finally
            {
                this.connectionLock.ExitReadLock();
            }
        }

        public bool ConnectIfNotConnected(Socket socket)
        {

            this.connectionLock.EnterReadLock();
            try
            {
                if (this.socket != null) return false;
            }
            finally
            {
                this.connectionLock.ExitReadLock();
            }

            Connect(socket);

            return true;
        }

        public void Connect(Socket socket)
        {
            // First, we make sure that no one else is messing with this connection right now
            this.connectionLock.EnterReadLock();
            try
            {
                // Then, we have to disconnect from whomever we're currently connected to
                if (this.socket != null)
                {
                    this.socket.Connect(null);
                }

                // But if we are supposed to connect to someone, we need to do that...
                if (socket != null)
                {
                    // So connect the new socket to ourself
                    socket.Connect(this);
                }
            }
            finally
            {
                this.connectionLock.ExitReadLock();
            }

            this.connectionLock.EnterWriteLock();
            try
            {
                // And connect ourself to the new socket, 
                // or null of the new socket is null
                this.socket = socket;
            }
            finally
            {
                this.connectionLock.ExitWriteLock();
            }
        }

        public void Dispose()
        {
            ((IDisposable) connectionLock).Dispose();
        }
    }

    private WebSocketServer server;

    class Socket : WebSocketBehavior
    {
        public Connection connection;
        public object socketLock;
        public WebSocketInterface owner;

        public Socket() { this.socketLock = new object(); }

        public void Connect(Connection connection)
        {
            lock(this.socketLock)
            {
                this.connection = connection;
            }
        }

        public void SendJSON(JToken t)
        {
            lock(socketLock)
            {
                this.Send(t.ToString());
            }
        }

        protected override void OnOpen()
        {
            owner.EvoSphereLog(string.Format("New connection from {0}", this.Context.Origin));
            try
            {
                var obj = new JObject();
                obj["callTarget"] = owner.QueryableId.ToString();
                Debug.Log(obj);
                SendJSON(obj);
            }
            catch (Exception ex)
            {
                owner.EvoSphereLogError(ex);
            }

        }

        protected override void OnClose(CloseEventArgs e)
        {
            Debug.Log(string.Format("Closing {0}", this.Context.Origin));
            // Disconnect from our connection to the main thread, so that it can be returned to the pool
            this.connection.Connect(null);
        }

        protected override void OnMessage(MessageEventArgs e)
        {
            owner.EvoSphereLog(string.Format("Data from {0}", e.Data));
            try
            {
                var json = JObject.Parse(e.Data);
                this.connection.SendToUnity(json);
            }
            catch (Exception ex)
            {
                owner.EvoSphereLogError(ex);
            }
        }
    }

    [Deserializer]
    public void Load(
        [Doc("The URL to start this server on")] string url = "ws://127.0.0.1:4646")
    {
        this.url = url;
    }

    protected override void OnStart()
    {
        server = new WebSocketServer(this.url);
        server.AddWebSocketService<Socket>("/", (socket) =>
        {
            socket.owner = this;
            // Connect to the new socket
            this.Connect(socket);
        });

        StartCoroutine(CheckStatus());

        server.ReuseAddress = true;
        server.Start();
    }

    System.Collections.IEnumerator CheckStatus()
    {
        EvoSphereLog("Waiting for server...");
        while (server == null || !server.IsListening)
        {
            yield return null;
        }
        EvoSphereLog(string.Format("Server connected at {0}:{1}", server.Address, server.Port));
    }

    bool PumpMessage()
    {
        // Make sure that no-one else is listening for messages
        connectionsLock.EnterReadLock();
        try
        {
            // Find some connection that we can pull stuff out of
            foreach (var connection in connections)
            {
                JObject o = null;
                if (connection.fromSocket.TryDequeue(out o))
                {
                    var value = HandleJson(o);
                    connection.SendToSocket(value);
                    return true;
                }
            }

        }
        finally { connectionsLock.ExitReadLock(); }

        return false;
    }

    void Update()
    {
        if (server != null && server.IsListening)
        {
            // We want to handle a reasonable number of messags each frame
            var now = DateTime.Now;

            var maxTime = Math.Min(1.0 / 3, Math.Max(Time.maximumDeltaTime, Time.deltaTime));
            while (PumpMessage() && (DateTime.Now - now).TotalSeconds <= maxTime)
            {

            }
        }
    }

    protected override void OnExit()
    {
        if (server != null)
        {
            EvoSphereLog(string.Format("Shutting down server {0}:{1}", server.Address, server.Port));
            server.Stop();
        }

        connections.ForEach(connection => connection.Dispose());
        connectionsLock.Dispose();
    }

    // [Expose]
    // public void Subscribe(string queryStr, string id)
    // {
    //     var query = TargetedQuery.Parse(queryStr);

    //     Query(query.Query).Subscribe(query.Target, queryable =>
    //     {
    //         var response = new JObject();
    //         response["subscriber"] = id;
    //         response["queryable"] = queryable.QueryableId;
    //         // We should be doing something to inform the subscriber with the given id that something has happend
    //         // We'll need a target callback here, or some form of managed queue...
    //         // TODO: Respond
    //     });
    // }

    [Expose]
    public void UnloadExperiment()
    {
        if (Instance != null)
        {
            Instance.UnloadExperiment();
        }

    }
}

#endif