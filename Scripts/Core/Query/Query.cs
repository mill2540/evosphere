using System;
using System.Linq;
using System.Runtime.Serialization;
using JetBrains.Annotations;

public class QueryParseException : Exception
{
    public QueryParseException()
    {
    }

    protected QueryParseException([NotNull] SerializationInfo info, StreamingContext context) : base(info, context)
    {
    }

    public QueryParseException(string message) : base(message)
    {
    }

    public QueryParseException(string message, Exception innerException) : base(message, innerException)
    {
    }
}

public class TargetedQueryParseException : Exception
{
    public TargetedQueryParseException()
    {
    }

    protected TargetedQueryParseException([NotNull] SerializationInfo info, StreamingContext context) : base(info,
        context)
    {
    }

    public TargetedQueryParseException(string message) : base(message)
    {
    }

    public TargetedQueryParseException(string message, Exception innerException) : base(message, innerException)
    {
    }
}
public class IllegialCharactersInQueryException : Exception
{
    public IllegialCharactersInQueryException()
    {
    }

    protected IllegialCharactersInQueryException([NotNull] SerializationInfo info, StreamingContext context) : base(info,
        context)
    {
    }

    public IllegialCharactersInQueryException(string message) : base(message)
    {
    }

    public IllegialCharactersInQueryException(string message, Exception innerException) : base(message, innerException)
    {
    }
}

static class QueryUtils
{
    public static readonly char PATH_SEPARATOR = '/';
    public static readonly char DASH = '-';
    public static readonly char START_ID = '$';
    public static readonly char START_TARGET = '.';
    public static readonly char OPEN_TARGET_LIST = '(';
    public static readonly char CLOSE_TARGET_LIST = ')';
    public static readonly char TARGET_LIST_DELIMITER = ',';
    public static readonly char TYPE_HINT = ':';
    public static readonly char ANY = '*';

    public static void CatchIllegalCharacters(string query)
    {
        foreach (char c in query)
        {
            if (!Char.IsLetterOrDigit(c) &&
            !Char.IsWhiteSpace(c) &&
            c != DASH &&
            c != PATH_SEPARATOR &&
            c != START_ID &&
            c != START_TARGET &&
            c != OPEN_TARGET_LIST &&
            c != CLOSE_TARGET_LIST &&
            c != TARGET_LIST_DELIMITER &&
            c != TYPE_HINT &&
            c != ANY)
            {
                throw new IllegialCharactersInQueryException(string.Format("{0} cannot contain the '{1}' character", query, (int)c));
            }
        }
    }

}


public class IdQuery
{
    public readonly QueryableId? Id;
    public readonly Query Start;

    public IdQuery(QueryableId? id, Query start)
    {
        Id = id;
        Start = start;
    }

    public static IdQuery Parse(string parse)
    {
        QueryUtils.CatchIllegalCharacters(parse);
        // We want to split an id query into one of two cases. It either starts
        // with an id or it does not. If it does not, then we can simply hand
        // parsing off to the Query class. Otherwise, we need to to some more
        // processing before we are ready to do that
        if (parse.Length == 0 || parse[0] != QueryUtils.START_ID) return new IdQuery(null, Query.Parse(parse));

        // The query has the form #id/...
        // Split the query into the id part (ie #id) and the path part (ie /...)
        var segments = parse.Split(new[] { QueryUtils.PATH_SEPARATOR }, 2);
        // The id is the first segment
        var id = Guid.Parse(segments[0].Substring(1));
        // The path part may or may not exists.
        Query start = null;
        if (segments.Length >= 2)
        {
            // If it does, parse it
            start = Query.Parse(segments[1]);
        }

        return new IdQuery(new QueryableId(id), start);
    }

    public override string ToString()
    {
        var str = "";
        if (Id != null)
        {
            str += QueryUtils.START_ID + Id.ToString();
        }

        str += Start.ToString();
        return str;
    }
}


public class Query
{
    public readonly string Name;
    public string Type;
    public Query Next;


    public Query(string name = null, string type = null, Query next = null)
    {
        Name = name;
        Type = type;
        Next = next;
    }

    public static Query Parse(string parse)
    {
        QueryUtils.CatchIllegalCharacters(parse);
        // Handle null arguments, because the world is a dangerous place
        if (parse == null) throw new ArgumentNullException();
        // throw out any whitespace at the ends
        parse = parse.Trim();
        // You are not allowed to start with /, as that would be ambiguous 
        if (parse.Length > 0 && parse[0] == QueryUtils.PATH_SEPARATOR) throw new QueryParseException(string.Format("Queries cannot start with a '{0}'", QueryUtils.PATH_SEPARATOR));

        Query root = null;
        var current = root;

        // For each segment in the path (ie, segment0/segment1/segment2/.../segmentN)...
        foreach (var segment in parse.Split(QueryUtils.PATH_SEPARATOR))
        {
            // TODO: this should probably be split into a ParseSegment(segment) function

            // Split at '!'. The syntax is Name!Type
            var nameType = segment.Split(QueryUtils.TYPE_HINT);
            string name = null;
            string type = null;

            if (nameType.Length == 1 || nameType.Length == 2)
            {
                // The name comes first always
                name = nameType[0];

                // Then, if there was a second part, it was the type. 
                if (nameType.Length == 2)
                    type = nameType[1];
            }
            else
            {
                // Not a valid length. Someone either did segment//segment or segment/a!b!c/segment
                throw new QueryParseException("Invalid query element \"" + segment + "\"");
            }

            // Construct the next query
            var next = new Query(name, type);

            // If we are at the beginning of the query, then this next query is now the current and first query
            if (current == null)
            {
                root = current = next;
            }
            else
            {
                // Otherwise, the current query becomes the last query, and the next query becomes the current query
                current.Next = next;
                current = next;
            }
        }

        // If we failed to build anything then bail out
        if (root == null)
        {
            throw new QueryParseException("Invalid query \"" + parse + "\"");
        }

        // Otherwise, everything is good and we can return!
        return root;
    }

    public override string ToString()
    {
        var repr = Name;
        if (Type != null) repr += QueryUtils.TYPE_HINT + Type;
        if (Next != null) repr += QueryUtils.PATH_SEPARATOR + Next.ToString();

        return repr;
    }
}

public struct MultiTargetedQuery
{
    public readonly IdQuery Query;
    public readonly string[] Targets;

    public MultiTargetedQuery(IdQuery query, string[] targets)
    {
        Query = query;
        Targets = targets;
    }

    public static MultiTargetedQuery Parse(string parse)
    {
        QueryUtils.CatchIllegalCharacters(parse);
        var segments = parse.Split(QueryUtils.START_TARGET);
        if (segments.Length != 2)
            throw new TargetedQueryParseException("Targeted queries must be of the form query.targets, not \"" + parse +
                                                  "\"");

        var targetsString = segments[1].Trim();
        string[] targets;

        if (targetsString.Length > 0 && targetsString.First() == QueryUtils.OPEN_TARGET_LIST && targetsString.Last() == QueryUtils.CLOSE_TARGET_LIST)
        {
            targets = targetsString.Substring(1, targetsString.Length - 2).Split(QueryUtils.TARGET_LIST_DELIMITER).Select(target => target.Trim())
                .ToArray();
        }
        else if (targetsString.Contains(QueryUtils.OPEN_TARGET_LIST) || targetsString.Contains(QueryUtils.CLOSE_TARGET_LIST) || targetsString.Contains(QueryUtils.TARGET_LIST_DELIMITER))
        {
            throw new TargetedQueryParseException(
                "The target of targeted queries must be of the forms \"target\" or (target, target, ...), not \"" +
                parse +
                "\"");
        }
        else
        {
            targets = new[] { targetsString };
        }

        return new MultiTargetedQuery(IdQuery.Parse(segments[0]), targets);
    }

    public override string ToString()
    {
        return Query.ToString() + QueryUtils.START_TARGET + QueryUtils.OPEN_TARGET_LIST + string.Join(QueryUtils.TARGET_LIST_DELIMITER.ToString(), Targets) + QueryUtils.CLOSE_TARGET_LIST;
    }
}

public struct TargetedQuery
{
    public readonly IdQuery Query;
    public readonly string Target;

    public TargetedQuery(IdQuery query, string target)
    {
        Query = query;
        Target = target;
    }

    public static TargetedQuery Parse(string parse)
    {
        QueryUtils.CatchIllegalCharacters(parse);
        var segments = parse.Split(QueryUtils.START_TARGET);
        if (segments.Length != 2)
            throw new TargetedQueryParseException("Targeted queries must be of the form query.target, not \"" + parse +
                                                  "\"");

        return new TargetedQuery(IdQuery.Parse(segments[0]), segments[1]);
    }
    public override string ToString()
    {
        return Query.ToString() + QueryUtils.START_TARGET + Target;
    }
}