﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using TypeMeta;
using UnityEngine;

/// Utility interface which can be used to enable easy serialization. Necessary
/// because the Newtonsoft JSON automatic serialization fails when in the WebGL
/// build
public interface IAsJson
{
    JToken AsJson();
}

/// TODO: is this even used anywhere?
public class JsonAsJson : IAsJson
{
    private readonly JToken json;

    public JsonAsJson(JToken json)
    {
        this.json = json;
    }

    public JToken AsJson()
    {
        return json;
    }
}

/// This is returned by the queryable interface for call results. It provides a
/// way to be notified of errors and exceptions when they are thrown, and allows
/// for nice interop with the UI.
public struct CallResult : IAsJson
{
    public readonly string Id;
    public readonly JToken Value;
    public readonly JToken Error;

    public CallResult(string id, JToken value = null, JToken error = null)
    {
        Id = id;
        Value = value;
        Error = error;
    }

    public JToken ValueAsJson()
    {
        return Error ?? Value;
    }

    public JToken AsJson()
    {
        var obj = new JObject();
        obj["Id"] = Id;
        obj["Value"] = Value;
        obj["Error"] = Error;

        return obj;
    }
}

/// A metadata class describing an exported member of a queryable class
public struct Export : IEquatable<Export>, IAsJson
{
    public readonly string Name;
    public readonly Type ReturnType;
    public readonly Type[] Parameters;

    public Export(string name, Type returnType, params Type[] parameters)
    {
        Name = name;
        ReturnType = returnType;
        Parameters = parameters;
    }

    public JToken AsJson()
    {
        var obj = new JObject();
        obj["Name"] = Name;

        var parameters = new JArray();
        foreach (var param in Parameters) parameters.Add(param.AssemblyQualifiedName);
        obj["Parameters"] = parameters;

        if (ReturnType != null)
            obj["ReturnType"] = ReturnType.AssemblyQualifiedName;

        return obj;
    }

    public bool Equals(Export other)
    {
        return string.Equals(Name, other.Name) && ReturnType == other.ReturnType &&
            Parameters.SequenceEqual(other.Parameters);
    }

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is Export && Equals((Export) obj);
    }

    public override int GetHashCode()
    {
        unchecked
        {
            var hashCode = (Name != null ? Name.GetHashCode() : 0);
            hashCode = (hashCode * 397) ^ (ReturnType != null ? ReturnType.GetHashCode() : 0);

            return Parameters.Aggregate(hashCode,
                (current, param) => (current * 397) ^ (param != null ? param.GetHashCode() : 0));
        }
    }

    public static bool operator ==(Export left, Export right)
    {
        return left.Equals(right);
    }

    public static bool operator !=(Export left, Export right)
    {
        return !left.Equals(right);
    }
}

/// TODO: this may not be necessary
internal class ExportComp : EqualityComparer<Export>
{
    public override bool Equals(Export x, Export y)
    {
        return x == y;
    }

    public override int GetHashCode(Export obj)
    {
        return obj.GetHashCode();
    }
}

public interface IQueryResults
{
    IEnumerable<Export> GetExports();
    IEnumerable<CallResult> Call(string memberName, params JToken[] args);
    void Subscribe(string eventName, Action<Queryable> subscriber);
}

/// <summary>
/// Creates an abstraction wrapping a set of queryable results. This allows
/// actions to be preformed in aggregate on all the results of a query.
/// <summary>
public class QueryAll : IQueryResults, IEnumerable<Queryable>
{
    /// All the components which matched the query
    public readonly List<Queryable> Selected;
    /// Caches the union of all the exports of all the selected components
    private HashSet<Export> exports;

    public QueryAll(List<Queryable> selected)
    {
        Selected = selected;
    }

    /// <summary> 
    /// Get all the exports that can be called on these results. Notice that
    /// this is the union, not the intersection of exports for all components
    /// selected 
    /// </summary>
    public IEnumerable<Export> GetExports()
    {
        if (exports != null) return exports;

        exports = new HashSet<Export>(Selected.SelectMany(select => select.GetExports()), new ExportComp());

        return exports;
    }

    /// <summary>
    /// Calls the given member of EVERY selected component. This means that in
    /// cases where the results are heterogenous, this may results in errors
    /// being returned by some components.
    /// </summary>
    public IEnumerable<CallResult> Call(string memberName, params JToken[] args)
    {
        return Selected.SelectMany(item => item.Call(memberName, args));
    }

    /// <summary>
    /// Subscribes to the event for every selected component. This should be
    /// used with caution, as it could have a significant performace cost in
    /// some cases.
    /// </summary>
    public void Subscribe(string eventName, Action<Queryable> subscriber)
    {
        foreach (var item in Selected)
        {
            item.Subscribe(eventName, subscriber);
        }
    }

    public IEnumerator<Queryable> GetEnumerator()
    {
        return Selected.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
}

/// <summary>
/// Representation of a given root component and its immediate children.
/// <summary> 
public struct Hierarchy : IAsJson
{
    /// The id of this component
    public readonly QueryableId Id;
    /// The name of this component
    public readonly string Name;
    /// The type of this component
    public readonly Type Type;

    /// The Ids of this component's children AT THE TIME THIS Hierarchy was requested
    public readonly QueryableId[] Children;
    /// The exported members of this component
    public readonly Export[] Exports;

    public Hierarchy(QueryableId id, string name, Type type, QueryableId[] children, Export[] exports)
    {
        Id = id;
        Name = name;
        Type = type;
        Children = children;
        Exports = exports;
    }

    private void PrettyImpl(TextWriter writer, string indentStr, bool root, string indent)
    {
        writer.Write(indent);
        if (!root) writer.Write("+ ");
        writer.WriteLine(Name + ":" + Type.Name);

        indent += indentStr;

        foreach (var child in Children)
        {
            writer.Write(indent + child);
        }
    }

    public void PrettyPrint(TextWriter writer, string indentStr = "  ")
    {
        PrettyImpl(writer, indentStr, true, "");
    }

    public string PrettyPrint(string indentStr = "  ")
    {
        using(var writer = new StringWriter())
        {
            PrettyImpl(writer, indentStr, true, "");
            writer.Flush();
            return writer.ToString();
        }
    }

    public JToken AsJson()
    {
        var obj = new JObject();

        obj["Id"] = Id.ToString();
        obj["Name"] = Name;
        obj["Type"] = Type.AssemblyQualifiedName;

        var children = new JArray();
        foreach (var child in Children) children.Add(child.ToString());
        obj["Children"] = children;

        var exports = new JArray();
        foreach (var export in Exports) exports.Add(export.AsJson());
        obj["Exports"] = exports;

        return obj;
    }
}

public struct QueryableId
{
    private readonly Guid id;

    public QueryableId(Guid id)
    {
        this.id = id;
    }

    public static QueryableId New()
    {
        return new QueryableId(Guid.NewGuid());
    }

    public override bool Equals(object obj)
    {
        var other = obj as QueryableId?;
        return other != null && ((QueryableId) other).id == id;
    }

    public override int GetHashCode()
    {
        return id.GetHashCode();
    }

    public override string ToString()
    {
        return "$" + id.ToString();
    }
}

/// <summary>
/// This is the most fundamental component of the EvoSphere engine. Basically,
/// it allows the user to ask components of a running simulation to do things or
/// to answer questions. For example, it allows things like
/// <code>Experiment.Call("environment/my-child-id:Replicate");</code>
/// This functionality provides the basis for easy and generic integration into
/// UIs.
/// </summary>
/// One of the most important things that Queryables do is allow the user to get
/// access to objects hidden deeper in the hierarchy of the experiment. Also, by
/// using the [Expose] attribute, users can mark custom functions as queryable,
/// and, if those functions meet certain requirements, they will just work.
public class Queryable : MonoBehaviour, IQueryResults
{
    /// This is the unique ID associated with this organism during this run
    public readonly QueryableId QueryableId = QueryableId.New();

    // Global index cache. Allows for O(1) lookup with IDs
    // TODO: this will sometimes use a lot of memory (although, really, even for
    // a simulation with on the order of 10k components, this shouldn't take too
    // much space). A good fix would be to use long instead of string, which is
    // a *much* more compact way to store strings
    private static readonly Dictionary<QueryableId, Queryable> index = new Dictionary<QueryableId, Queryable>();
    /// All the events which this Queryable is able to trigger
    private readonly HashSet<string> events = new HashSet<string>();

    /// All the listeners subscribed to events on this queryable
    private readonly Dictionary<string, List<Action<Queryable>>> subscriptions =
        new Dictionary<string, List<Action<Queryable>>>();

    /// It is good practice (but not mandatory as such) to extend this class when adding new events.
    /// TODO: use reflection so that this becomes identical to registering events...
    public class Events
    {
        public static readonly string HierarchyUpdated = "HierarchyUpdated";
    }

    /// The name of this queryable. Can be used in some contexts to make
    /// queries. However, note that names are not (TODO: really?) guaranteed to
    /// be unique, and where possible Ids should be used instead, as they will
    /// run much faster.
    [Expose]
    public string QueryableName { get; set; }

    protected void Awake()
    {
        // Setup the default name, if one has not already been set.
        QueryableNameAttribute.InitName(this);

        // Also, put ourself into the cache for fast lookup later.
        index[QueryableId] = this;
        // Maybe this is needed, I am not sure.
        // It is intend to be a workaround for the fact that 
        // If a child class defines Awake, it will hide this unless they call super.Awake().
        OnAwake();
    }

    /// Override this instead of Awake
    protected virtual void OnAwake() { }

    private void OnDestroy()
    {
        // Make sure that we are removed from the cache to prevent blowup and erroneous Queries
        index.Remove(QueryableId);
    }

    /// This should be overridden in anycase where the hierarchy of the
    /// experiment does not match the hierarchy of objects in unity. By default,
    /// the children of this object are considered to be any direct children of
    /// its gameobject which are Queryable.
    protected virtual IEnumerable<Queryable> ToSearch()
    {
        return
        from Transform child in transform
        select child.GetComponent<Queryable>();
    }

    /// Gets hierarchy information from this component. Is generally useful for
    /// experiment agnostic UIs and other interfaces where the structure of the
    /// experiment is not known or fixed. 
    [Expose]
    public Hierarchy GetHierarchy()
    {
        return new Hierarchy(QueryableId, QueryableName, GetType(),
            ToSearch().Where(child => child != null).Select(child => child.QueryableId).ToArray(),
            GetExports().ToArray());
    }

    /// Handles lookups based on Queries when the ID is not given
    private IEnumerable<Queryable> QueryItems(Query query)
    {
        // TODO: this is some of the logic I am most unsure of, and most needs testing

        // TODO: this will be *very* slow sometimes.
        if (query == null)
            yield break;

        // Are there any more segments left in the query?
        var finished = query.Next == null;

        // We will also search children to see if they match the rest of the query
        var descend = query.Name == "**";

        // **: match any child or this component, regardless of ID or name
        // * : match this component regardless of ID or name
        var matchesAny = descend || query.Name == "*";

        // If there not wildcards and the name does not match then we can exit
        if (!matchesAny && query.Name != QueryableName) yield break;

        // So, now we know that we have wildcards, or the name matches.

        // However, this query segment might also contain a type filter. 
        // If it does, make sure that the typenames match.
        if (query.Type == null || query.Type == GetType().Name)
        {
            // Yep, they did. If this is the last segment in the query, then we are done! Hurray!
            if (finished) yield return this;

            // Otherwise, continue searching through the children of this component
        }
        else
        {
            // The type filter did *not* pass. This query segment does *not* match this component, and we can bail out
            yield break;
        }

        // Pass the search to the children to see if any of them match the remainder of the query
        if (!finished)
        {
            // Loop through all the living children and flatten the result
            foreach (var item in ToSearch().Where(item => item != null)
                    .SelectMany(item => item.QueryItems(query.Next)))
            {
                yield return item;
            }
        }

        // If we are not using the descend wildcard, then we have matched everything we can, and are done
        if (!descend) yield break;

        // Ok, so now we know that we have to match the original query against the children too.

        // We are descending, so we need to check all the children to see if any of them match the current query
        // Same drill as above, make sure that dead children are not searched and flatten the result
        foreach (var item in ToSearch().Where(item => item != null)
                .SelectMany(item => item.QueryItems(query)))
        {
            yield return item;
        }
    }

    // This is the main interface that Queryable uses to resolve queries. It handles the branching logic for IDs vs no id queries. 
    private IEnumerable<Queryable> QueryItems(IdQuery query)
    {
        // The target is the root component where the query will start being resolved from
        var target = this;
        // If this query starts with an ID, then resolve the id
        if (query.Id != null)
        {
            // Try and match the id in the id index.
            // If this fails, bail out and no matches are returned
            if (!index.TryGetValue((QueryableId) query.Id, out target)) yield break;
        }

        // If the query is of the form #guid/tail then resolve the tail
        if (query.Start == null)
        {
            // There is no tail, so we are done
            yield return target;
        }
        else
        {
            // Run the rest of the query from above
            foreach (var result in target.QueryItems(query.Start))
            {
                yield return result;
            }
        }
    }

    public QueryAll Query(string query)
    {
        return Query(IdQuery.Parse(query));
    }

    /// Runs a query against this component
    public QueryAll Query(IdQuery query)
    {
        return new QueryAll(new HashSet<Queryable>(QueryItems(query)).ToList());
    }

    /// Discover all the exported members of this element
    /// TODO: this should be static and cached
    public IEnumerable<Export> GetExports()
    {
        throw new InvalidOperationException("Not Implemented");
        /// Invoke the meta utility to get a nicer representation
        // var meta = MetaDB.Get(GetType());

        // /// Check for any instance methods which have the [Export] attribute
        // foreach (var method in meta.InstanceMethods.SelectMany(group => group.Methods)
        //     .Where(method => method.IsExposed))
        // {
        //     // Fix the void return type
        //     var returnType = method.Info.ReturnType;
        //     if (returnType == typeof(void)) returnType = null;

        //     yield return new Export(method.Info.Name, returnType,
        //         method.Parameters.Select(param => param.ParameterType).ToArray());
        // }

        // /// Read through all the exposed properties and fields
        // foreach (var property in meta.ExposedMembers)
        // {
        //     // TODO: check for getter/setter as appropriate on properties
        //     // TODO: check for readyonly on fields
        //     yield return new Export(property.Name, property.Expects);
        //     yield return new Export(property.Name, null, property.Expects);
        // }

    }

    private static JToken ToJson(Type expected, object value)
    {
        throw new InvalidOperationException("Not Implemented");
        // var type = value != null ? value.GetType() : expected;

        // var token = value as JToken;
        // if (token != null) return token;

        // var asJson = value as IAsJson;
        // if (asJson != null) return asJson.AsJson();

        // if (type.IsEnum || MetaDB.Get(type).IsStringable)
        //     return value != null ? value.ToString() : "";

    }

    /// <summary>
    /// Allows calling of exposed members and properties/fields. This, used in
    /// conjunction with the GetExports and GetHierarchy functions forms the
    /// basis of generic reflective UIs.
    /// </summary>
    public IEnumerable<CallResult> Call(string memberName, params JToken[] args)
    {
        throw new InvalidOperationException("Not Implemented");
        // Grab the util info
        // // var meta = MetaDB.Get(GetType());

        // // MethodGroup<MethodMeta> methods;
        // // // If a method named memberName exists, call it
        // // if (meta.TryGetInstanceMethod(memberName, out methods))
        // // {
        // //     // Try each of the methods with the given name
        // //     foreach (var method in methods.Methods)
        // //     {
        // //         // If the method is missing the Expose attribute, skip it
        // //         if (!method.Info.IsDefined(typeof(ExposeAttribute), true)) continue;
        // //         // If the methods has a different number of arguments than we have been given, skip it
        // //         if (!method.Accepts(args.Length)) continue;

        // //         var tmp = method;
        // //         // Attempt to deserialize the arguments we have been given into the arguments that this method is expecting
        // //         var deserializedArgs = args.Select((arg, i) => arg.ToObject(tmp.Parameters[i].ParameterType)).ToArray();

        // //         object result = null;
        // //         Exception exception = null;
        // //         try
        // //         {
        // //             // Try to run the method
        // //             result = method.Invoke(this, deserializedArgs);
        // //         }
        // //         catch (Exception ex)
        // //         {
        // //             // If any error occurs, catch it and remember it
        // //             exception = ex;
        // //         }

        // //         // An exception happend, so tell the user what it was and bail out
        // //         if (exception != null)
        // //         {
        // //             yield return new CallResult(QueryableName, error : exception.ToString());

        // //             yield break;
        // //         }

        // //         // No exception happen, and the method has a void return type, so we can just return 
        // //         if (method.Info.ReturnType == typeof(void))
        // //         {
        // //             yield return new CallResult(QueryableName);
        // //             yield break;
        // //         }

        // //         // The mehtod has a non-null return type, so convert the return to JSON and return it
        // //         // Unless we can't convert it to JSON...
        // //         var json = ToJson(method.Info.ReturnType, result);
        // //         if (json != null)
        // //         {
        // //             yield return new CallResult(QueryableName, json);
        // //             yield break;
        // //         }

        // //         // In which case we have an error and exit
        // //         yield return new CallResult(QueryableName,
        // //             error : method.Info.ReturnType.Name + " cannot be converted to json in calling " + memberName +
        // //             " for " + memberName + "(" +
        // //             string.Join(", ", args.Select(arg => arg.ToString()).ToArray()) + ")");

        // //         yield break;
        // //     }

        // //     // We didn't find ANY functions which could plausibly be the one we want, so exit with an error message

        // //     yield return new CallResult(QueryableName,
        // //         error: "No such overload of " + memberName + " for " + memberName + "(" +
        // //         string.Join(",", args.Select(arg => arg.ToString()).ToArray()) + ")");
        // //     yield break;
        // }

        // PropertyLikeMeta property;
        // // Otherwise, no method named memberName exists, check for properties/fields
        // if (meta.TryGetExposedMember(memberName, out property))
        // {
        //     // If there are no arguments, then we assume that the user is
        //     // expecting this to act like a getter
        //     if (args.Length == 0)
        //     {
        //         // Get the value of the property
        //         var value = property.Get(this);
        //         // Attempt to convert it to JSON
        //         var json = ToJson(property.Expects, value);

        //         if (json != null) yield return new CallResult(QueryableName, json);
        //         else // Fail with a message
        //             yield return new CallResult(QueryableName,
        //                 error: "Value of type " + property.Expects.Name + " of property " + memberName +
        //                 " cannot be converted to json.");

        //         yield break;
        //     }

        //     // Otherwise, they must want to act like a setter
        //     if (args.Length == 1 && property.Expects.IsInstanceOfType(args[0]))
        //     {
        //         // Set the property
        //         property.Set(this, args[0]);

        //         yield return new CallResult(QueryableName);
        //         yield break;
        //     }

        //     // More than 2 arguments to a property does not make sense, so fail and exit
        //     yield return new CallResult(QueryableName,
        //         error: "Properties cannot be set using " + memberName + " the arguments (" +
        //         string.Join(",", args.Select(arg => arg.ToString()).ToArray()) + ")");
        //     yield break;
        // }

        // yield return new CallResult(QueryableName, error: "No such member as " + memberName);
    }

    /// <summary>
    /// Allows observers to subscribe to events triggered by this queryable. Not
    /// that if the subscriber wants to remain subscribed they must resubscribe
    /// each time the event is fired.
    /// </summary>
    public void Subscribe(string eventName, Action<Queryable> subscriber)
    {
        // If there is no such event, exit
        if (!events.Contains(eventName)) return;

        // If no one is already subscribed to this event, we have to create a new list of subscribers
        List<Action<Queryable>> subscribers;
        if (!subscriptions.TryGetValue(eventName, out subscribers))
        {
            subscribers = new List<Action<Queryable>>();
            subscriptions.Add(eventName, subscribers);
        }

        // Join the list of subscribers
        subscribers.Add(subscriber);
    }

    /// <summary>
    /// Fire an event attached to this queryable.
    /// </summary>
    public void Fire(string eventName)
    {
        if (!events.Contains(eventName)) return;

        // If no-one is already subscribed to the event, exit
        List<Action<Queryable>> subscribers;
        if (!subscriptions.TryGetValue(eventName, out subscribers)) return;

        // Collect all the subscribers to the event, and then unsubscribe them
        var current = new List<Action<Queryable>>();
        current.AddRange(subscribers);
        subscribers.Clear();

        // notify each of the subscribers
        foreach (var subscriber in current) subscriber(this);
    }

    // TODO: replace with reflection against the <code>Events</code> or somethings
    public void Fires(string eventName)
    {
        events.Add(eventName);
    }

    public void EvoSphereLog(object message)
    {
        Debug.Log(string.Format("[{0} id={1}]\t{2}", QueryableName, QueryableId, message));
    }

    public void EvoSphereLogError(object message)
    {
        Debug.LogError(string.Format("[{0} id={1}]\t{2}", QueryableName, QueryableId, message));
    }

    public void EvoSphereLogWarning(object message)
    {
        Debug.LogWarning(string.Format("[{0} id={1}]\t{2}", QueryableName, QueryableId, message));
    }
}