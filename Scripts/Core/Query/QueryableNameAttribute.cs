﻿using System;

public enum QueryName
{
    FromType,
    FromId
}


[AttributeUsage(AttributeTargets.Class)]
public class QueryableNameAttribute : Attribute
{
    public readonly QueryName? QueryNameSource = null;
    public readonly string Name = null;

    public QueryableNameAttribute(QueryName name)
    {
        QueryNameSource = name;
    }

    public QueryableNameAttribute(string name)
    {
        Name = name;
    }

    public static void InitName(Queryable queryable)
    {
        var type = queryable.GetType();
        if (type.IsDefined(typeof(QueryableNameAttribute), true))
        {
            var nameAttr = (QueryableNameAttribute)type.GetCustomAttributes(typeof(QueryableNameAttribute), true)[0];
            if (nameAttr.QueryNameSource == QueryName.FromType)
            {
                queryable.QueryableName = type.Name;
                return;
            }
            if (nameAttr.Name != null)
            {
                queryable.QueryableName = nameAttr.Name;
                return;
            }
        }

        queryable.QueryableName = queryable.QueryableId.ToString();
    }
}