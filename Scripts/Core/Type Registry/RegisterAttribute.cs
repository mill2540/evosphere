﻿using System;

[AttributeUsage(AttributeTargets.Class |
                AttributeTargets.Enum |
                AttributeTargets.Struct)]
public sealed class RegisterAttribute : Attribute
{
    public string Name { get; private set; }
    public bool StripFactory { get; private set; }

    public RegisterAttribute(string name = null, bool stripFactory = true)
    {
        Name = name;
        StripFactory = stripFactory;
    }
}