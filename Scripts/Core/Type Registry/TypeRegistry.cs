﻿using System;
using System.Collections.Generic;
using System.Reflection;
using EvoSphere.Core.Reflection;
using UnityEngine;

public class DuplicateNameException : Exception
{
    public Type Original { get; private set; }
    public Type Impostor { get; private set; }
    public string Name { get; private set; }

    public DuplicateNameException(Type original, Type impostor, string name) : base("The type " + impostor.Name + " wants to be known as \"" + name + "\" but the type " +
        original.Name + " is already registered with that name")
    {
        Original = Original;
        Impostor = impostor;
        Name = name;
    }
}

public class UnregisteredNameException : Exception
{
    public string Name { get; private set; }

    public UnregisteredNameException(string name) : base("No type named \"" + name + "\" has been registered. Maybe you need to add an external assembly?")
    {
        Name = name;
    }
}

public class UnregisteredTypeException : Exception
{
    public Type Unregistered;

    public UnregisteredTypeException(Type unregistered) : base("The type " + unregistered.Name +
        " has not been registered. Maybe you need to add a Register annotation?")
    {
        Unregistered = unregistered;
    }
}

/// Provides an index of human friendly names which can be used with the
/// serialization system. Also, any type which is registered is assumed to have
/// configurable members.
public static class TypeRegistry
{
    // Index of assemblies that we have scanned. Prevents duplication of effort
    private static readonly HashSet<Assembly> scannedAssemblies = new HashSet<Assembly>();
    // The actual registry of types. TODO: just how big does this get, and is it a problem?
    private static Dictionary<string, Type> registry;

    // Wrapper for the registry, with some niceness added
    private static Dictionary<string, Type> Registry
    {
        get
        {
            // If the registry has been created, return it
            if (registry != null) return registry;

            // Otherwise, initialize it and can the current assembly
            registry = new Dictionary<string, Type>();
            ScanAssembly(Assembly.GetExecutingAssembly());
            return registry;
        }
    }

    /// Provides an collection over all the types which have been registered
    public static Dictionary<string, Type>.ValueCollection Registered
    {
        get { return Registry.Values; }
    }

    /// <summary> Scans through all the types in a assembly, and finds all the registered types. </summary>
    public static void ScanAssembly(Assembly assembly, bool rescan = false)
    {
        // By default, don't scan assemblies we have already scanned 
        if (!rescan && scannedAssemblies.Contains(assembly)) return;
        scannedAssemblies.Add(assembly);

        // Look through all the types in the assembly and find the ones which are registered
        foreach (var type in ReflectionUtils
                .FindTypesWithAttributes(
                    assembly,
                    new [] { typeof(RegisterAttribute) },
                    false))
        {
            type.GetRegistryName();
        }
    }

    /// <summary> Attempts to find a type by its registered name. </summary>
    public static Type GetTypeByName(string name)
    {
        // Check to see if the type is in the registry
        if (Registry.ContainsKey(name)) return Registry[name];
        // If a type is an array type, the name might have the form RegisteredType[]. Array types are not put into the 
        // registry, but we can still figure out what their type is here.

        // First, if the type is not an array, then we can't determine its type at all.
        if (!name.EndsWith("[]")) throw new UnregisteredNameException(name);

        // So, it is an array. Pop the array brackets off the end of the name, and search for that type. If it is found, 
        // then we can wrap it back into an array type using MakeArrayType()
        return GetTypeByName(name.Substring(0, name.Length - 2)).MakeArrayType();
    }

    public static bool IsRegistered(this Type type)
    {
        return GetRegistryName(type) != null;
    }

    /// <summary> Get the registered name of a type. </summary>
    public static string GetRegistryName(this Type type)
    {
        // If the type is an array, we want to be able to say that it's named Foo[].
        if (type.IsArray)
        {
            // Recursion here to handle nested arrays
            return type.GetElementType().GetRegistryName() + "[]";
        }

        // Grab the [Register] attribute 
        var register = (RegisterAttribute) Attribute.GetCustomAttribute(type, typeof(RegisterAttribute), false);
        if (register == null) return null; // Not registered, bail out

        var name = type.Name;
        if (register.Name != null)
        {
            name = register.Name;
        }
        else
        {
            // Some cosmetic touches
            if (register.StripFactory && (name.EndsWith("Factory") || name.EndsWith("factory")))
            {
                name = name.Substring(0, name.Length - "Factory".Length);
            }
            if (name.StartsWith("|") && !(name.Length >= 2 && char.IsLower(name[1])) && type.IsInterface)
            {
                name = name.Substring(1);
            }
        }

        // Make sure that the registry will still be sane after doing the insertion
        if (!Registry.ContainsKey(name)) Registry.Add(name, type);
        else if (Registry[name] != type)
        {
            throw new DuplicateNameException(Registry[name], type, name);
        }

        return name;
    }

    public static string GetRegistryNameRequired(this Type type)
    {
        var name = type.GetRegistryName();
        if (name == null) throw new UnregisteredTypeException(type);
        return name;
    }
}