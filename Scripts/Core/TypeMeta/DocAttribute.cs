﻿using System;
using System.Reflection;

[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct | AttributeTargets.Interface | AttributeTargets.Field | AttributeTargets.Property | AttributeTargets.Parameter)]
public class DocAttribute : Attribute
{
    public readonly string Documentation;

    public DocAttribute(string documentation)
    {
        Documentation = documentation;
    }

    public static string GetDocs(Type type)
    {
        var docs = GetCustomAttribute(type, typeof(DocAttribute), true);
        if (docs != null)
        {
            return ((DocAttribute)docs).Documentation;
        }
        return null;
    }

    public static string GetDocs(MemberInfo type)
    {
        var docs = GetCustomAttribute(type, typeof(DocAttribute), true);
        if (docs != null)
        {
            return ((DocAttribute)docs).Documentation;
        }
        return null;
    }

    public static string GetDocs(PropertyInfo type)
    {
        var docs = GetCustomAttribute(type, typeof(DocAttribute), true);
        if (docs != null)
        {
            return ((DocAttribute)docs).Documentation;
        }
        return null;
    }
}