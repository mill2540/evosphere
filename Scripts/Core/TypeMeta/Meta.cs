﻿// using System;
// using System.Collections;
// using System.Collections.Generic;
// using System.Linq;
// using System.Reflection;
// using Newtonsoft.Json.Linq;
// using UnityEngine;

// namespace TypeMeta
// {

//     public class MaleformedTypeException : Exception
//     {
//         public readonly Type Type;
//         public MaleformedTypeException(Type t) : base() { Type = t; }
//         public MaleformedTypeException(Type t, string message) : base(string.Format("[{0}] {1}", t.AssemblyQualifiedName, message)) { Type = t; }
//         public MaleformedTypeException(Type t, string message, Exception inner) : base(string.Format("[{0}] {1}", t.AssemblyQualifiedName, message), inner) { Type = t; }
//     }

//     /// <summary>
//     ///  Provides metadata about C# types, and generally acts as a wrapper for the C# reflection system which is somewhat complex
//     /// </summary>
//     public class Meta : IAsJson
//     {
//         /// <summary>
//         ///  Simply returns the C# type this meta wraps
//         /// </summary>
//         public Type Type { get; private set; }

//         public Type DeserializerTarget
//         {
//             get
//             {
//                 // var configFor = Type.GetGenericInterface(typeof(ConfigFor<>));
//                 // if (configFor != null)
//                 // {
//                 //     return configFor.GetGenericArguments()[0];
//                 // }

//                 // var unityConfigFor = Type.GetGenericInterface(typeof(UnityConfigFor<>));
//                 // if (unityConfigFor != null)
//                 // {
//                 //     return unityConfigFor.GetGenericArguments()[0];
//                 // }

//                 return Type;
//             }
//         }

//         public MethodMeta Deserializer { get; private set; }

//         public MethodGroup<ConstructorMeta> Constructors { get; private set; }

//         private readonly Dictionary<string, MethodGroup<MethodMeta>> instanceMethods;
//         private readonly Dictionary<string, MethodGroup<StaticMethodMeta>> staticMethods;

//         public IEnumerable<MethodGroup<MethodMeta>> InstanceMethods
//         {
//             get { return instanceMethods.Values; }
//         }

//         public IEnumerable<MethodGroup<StaticMethodMeta>> StaticMethods
//         {
//             get { return staticMethods.Values; }
//         }

//         private readonly Dictionary<string, PropertyLikeMeta> configMembers;

//         private readonly Dictionary<string, PropertyLikeMeta> exposedMembers;

//         public IEnumerable<PropertyLikeMeta> ExposedMembers
//         {
//             get { return exposedMembers.Values; }
//         }

//         public static readonly List<Type> StringableTypes = new List<Type>
//         {
//             typeof(DateTime),
//             typeof(bool),
//             typeof(byte),
//             typeof(sbyte),
//             typeof(char),
//             typeof(decimal),
//             typeof(double),
//             typeof(float),
//             typeof(int),
//             typeof(uint),
//             typeof(long),
//             typeof(ulong),
//             typeof(short),
//             typeof(ushort),
//             typeof(string),
//             typeof(Query),
//             typeof(MultiTargetedQuery),
//             typeof(TargetedQuery)
//         };

//         public bool IsEnum
//         {
//             get { return Type.IsEnum; }
//         }

//         public bool IsConstructible
//         {
//             get { return Constructors.HasMethods() && !Type.IsRegistered() && !IsStringable && !IsEnum; }
//         }

//         public bool IsArray
//         {
//             get { return Type.IsArray || typeof(IList).IsAssignableFrom(Type); }
//         }

//         public bool IsStringable
//         {
//             get
//             {
//                 var nullable = Nullable.GetUnderlyingType(Type);

//                 return StringableTypes.Contains(Type) || (nullable != null && MetaDB.Get(nullable).IsStringable);
//             }
//         }

//         private List<Type> assignableFrom;

//         /// <summary>
//         /// All the types which can be assigned to this type. 
//         /// </summary>
//         public IEnumerable<Type> AssignableFrom
//         {
//             get
//             {
//                 if (assignableFrom != null) return assignableFrom;

//                 // TODO: Add cacheing here...
//                 // May not be needed though, as JS will also do cacheing
//                 var assignableFromTmp = new HashSet<Type> { Type };

//                 // Search through all types. Notice that we are using WebSafeIsAssignableFrom, because apparently AssignableFrom has problems when used from WebGL? I am not positive that this is still nessesary
//                 foreach (var accepted in ReflectionUtils.FindTypes(Type.WebSafeIsAssignableFrom))
//                 {
//                     assignableFromTmp.Add(accepted);
//                 }

//                 assignableFromTmp.RemoveWhere(t => t.IsAbstract || t.IsInterface);

//                 assignableFrom = assignableFromTmp.ToList();

//                 return assignableFrom;
//             }
//         }

//         private Dictionary<string, PropertyLikeMeta> GetProperties<T>(BindingFlags flags = BindingFlags.Instance |
//             BindingFlags.Public |
//             BindingFlags.NonPublic,
//             Func<T, string> getName = null) where T : Attribute
//         {
//             getName = getName ?? (attr => null);

//             return Type
//                 .GetFields(flags)
//                 .Select(field => new
//                 {
//                     field,
//                     attr = Attribute.GetCustomAttribute(field, typeof(T)) as T
//                 })
//                 .Where(fieldInfo => fieldInfo.attr != null)
//                 .Select(fieldInfo => (PropertyLikeMeta) new FieldMeta(fieldInfo.field, getName(fieldInfo.attr)))
//                 .Concat(Type
//                     .GetProperties(flags)
//                     .Select(property => new
//                     {
//                         property,
//                         attr = Attribute.GetCustomAttribute(property,
//                             typeof(T)) as T
//                     })
//                     .Where(propertyInfo => propertyInfo.attr != null)
//                     .Select(propertyInfo => (PropertyLikeMeta) new PropertyMeta(propertyInfo.property,
//                         getName(propertyInfo.attr))))
//                 .ToDictionary(field => field.Name);
//         }

//         public Meta(Type type)
//         {
//             Type = type;

//             instanceMethods = new Dictionary<string, MethodGroup<MethodMeta>>();
//             staticMethods = new Dictionary<string, MethodGroup<StaticMethodMeta>>();

//             foreach (var method in Type.GetMethods(BindingFlags.Public | BindingFlags.Instance))
//             {
//                 if (!instanceMethods.ContainsKey(method.Name))
//                 {
//                     instanceMethods[method.Name] = new MethodGroup<MethodMeta>(Type, method.Name);
//                 }
//                 var methodMeta = new MethodMeta(method);
//                 instanceMethods[method.Name].AddMethod(methodMeta);

//                 if (methodMeta.IsDeserializer)
//                 {
//                     Deserializer = methodMeta;
//                 }
//             }

//             foreach (var method in Type.GetMethods(BindingFlags.Public | BindingFlags.Static))
//             {
//                 if (!staticMethods.ContainsKey(method.Name))
//                 {
//                     staticMethods[method.Name] = new MethodGroup<StaticMethodMeta>(Type, method.Name);
//                 }
//                 var staticMethodMeta = new StaticMethodMeta(method);
//                 staticMethods[method.Name].AddMethod(staticMethodMeta);

//                 if (staticMethodMeta.IsDeserializer)
//                 {
//                     Deserializer = staticMethodMeta;
//                 }
//             }

//             Constructors = new MethodGroup<ConstructorMeta>(
//                 Type, "Ctor",
//                 type.GetConstructors().Select(constructor => new ConstructorMeta(constructor)).ToList()
//             );

//             if (Deserializer == null)
//             {
//                 configMembers = GetProperties<Config.ValueAttribute>(getName: a => a.Name);
//             }
//             else
//             {
//                 configMembers = Deserializer.Parameters.ToDictionary(parameter => parameter.Name, parameter => (PropertyLikeMeta) new ParameterPropertyMeta(parameter, null));
//             }

//             exposedMembers = GetProperties<ExposeAttribute>(getName: a => a.Name);
//         }

//         public PropertyLikeMeta GetConfigMember(string name)
//         {
//             PropertyLikeMeta value;
//             return configMembers.TryGetValue(name, out value) ? value : null;
//         }

//         public PropertyLikeMeta GetExposedMember(string name)
//         {
//             return exposedMembers[name];
//         }

//         public bool TryGetExposedMember(string name, out PropertyLikeMeta result)
//         {
//             return exposedMembers.TryGetValue(name, out result);
//         }

//         public IEnumerable<PropertyLikeMeta> GetConfigMembers()
//         {
//             return configMembers.Values;
//         }

//         public MethodGroup<MethodMeta> GetInstanceMethod(string name)
//         {
//             return instanceMethods[name];
//         }

//         public bool TryGetInstanceMethod(string name, out MethodGroup<MethodMeta> result)
//         {
//             return instanceMethods.TryGetValue(name, out result);
//         }

//         public MethodGroup<StaticMethodMeta> GetStaticMethod(string name)
//         {
//             return staticMethods[name];
//         }

//         private JObject AsArrayInfo()
//         {
//             // Assume this is an array type
//             var arrayInfo = new JObject();

//             arrayInfo["elementType"] = ReflectionUtils.GetListElement(Type).AssemblyQualifiedName;

//             return arrayInfo;
//         }

//         private JObject AsConstructedInfo()
//         {
//             JObject info = new JObject();
//             var constructors = new JArray();

//             foreach (var constructor in Constructors)
//             {
//                 var args = new JArray();
//                 foreach (var param in constructor.Parameters)
//                 {
//                     var paramObj = new JObject();
//                     paramObj["name"] = param.Name;
//                     paramObj["type"] = param.ParameterType.AssemblyQualifiedName;
//                     paramObj["optional"] = param.IsOptional;
//                     args.Add(paramObj);
//                 }
//                 constructors.Add(args);
//             }
//             info["constructedInfo"]["constructors"] = constructors;

//             return info;
//         }

//         public JToken AsJson()
//         {
//             var ret = new JObject();
//             ret["name"] = Type.AssemblyQualifiedName;
//             ret["shortName"] = Type.Name;
//             ret["documentation"] = DocAttribute.GetDocs(Type);
//             ret["deserializerTargetType"] = DeserializerTarget.AssemblyQualifiedName;

//             // Don't bother to send this info for
//             // an abstract class, we will never use it
//             if (!Type.IsAbstract)
//             {
//                 ret["arrayInfo"] = null;
//                 ret["mapInfo"] = null;
//                 ret["constructedInfo"] = null;
//                 ret["registeredInfo"] = null;
//                 ret["enumInfo"] = null;

//                 ret["isStringable"] = IsStringable;
//                 ret["isConstructable"] = IsConstructible;
//                 ret["isRegistered"] = Type.IsRegistered() && !IsArray && !IsEnum;
//                 ret["isArray"] = IsArray;
//                 ret["isMap"] = typeof(IDictionary).IsAssignableFrom(Type);
//                 ret["isEnum"] = IsEnum;

//                 if (Type.IsEnum)
//                 {
//                     var variants = new JArray();
//                     foreach (var variant in Enum.GetNames(Type))
//                     {
//                         variants.Add(variant);
//                     }
//                     ret["enumInfo"] = new JObject();
//                     ret["enumInfo"]["variants"] = variants;
//                 }
//                 else if (Type.IsArray)
//                 {
//                     ret["arrayInfo"] = new JObject();
//                     ret["arrayInfo"]["element"] = ReflectionUtils.GetListElement(Type).AssemblyQualifiedName;
//                 }
//                 else if (typeof(IDictionary).IsAssignableFrom(Type))
//                 {
//                     var desc = ReflectionUtils.GetMapDesc(Type);
//                     ret["mapInfo"] = new JObject();
//                     ret["mapInfo"]["key"] = desc.Key.AssemblyQualifiedName;
//                     ret["mapInfo"]["value"] = desc.Value.AssemblyQualifiedName;
//                 }
//                 else if (IsConstructible) { }
//                 else if (Type.IsRegistered())
//                 {
//                     var members = new JObject();
//                     foreach (var member in GetConfigMembers())
//                     {
//                         var memberObj = new JObject();
//                         memberObj["type"] = member.Expects.AssemblyQualifiedName;
//                         memberObj["documentation"] = member.Documentation;
//                         members[member.Name] = memberObj;
//                     }

//                     ret["registeredInfo"] = new JObject();
//                     ret["registeredInfo"]["members"] = members;
//                 }
//             }

//             var accepts = new JArray();
//             foreach (var accept in AssignableFrom) accepts.Add(accept.AssemblyQualifiedName);

//             ret["accepts"] = accepts;

//             return ret;
//         }
//     }
// }