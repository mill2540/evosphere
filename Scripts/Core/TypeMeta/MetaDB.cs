﻿// using System;
// using System.Collections.Generic;

// namespace TypeMeta
// {
//     public static class MetaDB
//     {
//         // TODO: this may need to be size limited
//         private static Dictionary<Type, Meta> cache =
//             new Dictionary<Type, Meta>();

//         public static Meta Get(Type type)
//         {
//             if (type == null) throw new ArgumentNullException();

//             if (!cache.ContainsKey(type))
//             {
//                 cache[type] = new Meta(type);
//             }
//             return cache[type];
//         }
//     }
// }