﻿// using System;
// using System.Collections.Generic;
// using System.Collections;
// using System.Linq;
// using UnityEngine;

// namespace TypeMeta
// {
//     public class AmbiguousMethodMatchException : Exception
//     {
//         public Type Containing { get; private set; }
//         public string Name { get; private set; }
//         public ICollection<Type> Parameters { get; private set; }

//         public AmbiguousMethodMatchException(Type containing, string name, ICollection<Type> parameters)
//             : base(string.Format("There are multiple matches for {0}.{1}({2}).", containing.Name,
//                 name, string.Join(", ", parameters.Select(param => param.Name).ToArray())))
//         {
//             Containing = containing;
//             Name = name;
//             Parameters = parameters;
//         }
//     }

//     public class InvalidArgumentsException : Exception
//     {
//         public Type Containing { get; private set; }
//         public string Name { get; private set; }
//         public ICollection<Type> Parameters { get; private set; }

//         public InvalidArgumentsException(Type containing, string name, ICollection<Type> parameters)
//             : base(string.Format("{0}.{1}({2}) does not exist or does not take the given arguments.", containing.Name,
//                 name, string.Join(", ", parameters.Select(param => param.Name).ToArray())))
//         {
//             Containing = containing;
//             Name = name;
//             Parameters = parameters;
//         }
//     }

//     public class MethodGroup<M> : IEnumerable<M> where M : class, ICallableMeta
//     {
//         public Type Containing { get; private set; }
//         public string Name { get; private set; }
//         private readonly List<M> methods;

//         public ICollection<M> Methods
//         {
//             get { return methods; }
//         }

//         public MethodGroup(Type containing, string name, List<M> methods = null)
//         {
//             Containing = containing;
//             Name = name;
//             this.methods = methods ?? new List<M>();
//         }

//         public void AddMethod(M method)
//         {
//             methods.Add(method);
//         }

//         public M GetMethod(ICollection<Type> parameters, bool allowNullsForValues = false)
//         {
//             // TODO: 1) throw our own custom exception instead
//             // TODO: 2) this will always run in O(n) time. Can we do better?
//             var results = methods.FindAll(method => method.Accepts(parameters, allowNullsForValues));
//             switch (results.Count)
//             {
//                 case 0:
//                     throw new InvalidArgumentsException(Containing, Name, parameters);
//                 case 1:
//                     return results[0];
//                 default:
//                     throw new AmbiguousMethodMatchException(Containing, Name, parameters);
//             }
//         }

//         public M GetMethodSafe(ICollection<Type> parameters, bool allowNullsForValues = false)
//         {
//             // TODO: 1) throw our own custom exception instead
//             // TODO: 2) this will always run in O(n) time. Can we do better?
//             var results = methods.FindAll(method => method.Accepts(parameters, allowNullsForValues));
//             return results.Count == 1 ? results[0] : null;
//         }

//         public IEnumerator<M> GetEnumerator()
//         {
//             return methods.GetEnumerator();
//         }

//         IEnumerator IEnumerable.GetEnumerator()
//         {
//             return methods.GetEnumerator();
//         }

//         public bool HasMethods()
//         {
//             return methods.Count != 0;
//         }
//     }
// }