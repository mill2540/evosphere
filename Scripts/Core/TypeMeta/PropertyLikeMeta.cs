﻿using System;
using System.Reflection;
using UnityEngine;

namespace TypeMeta
{
    public abstract class PropertyLikeMeta
    {
        public Type Expects { get; private set; }
        public string Documentation { get; private set; }
        public string Name { get; private set; }

        protected PropertyLikeMeta(Type type, string name, string documentation)
        {
            Expects = type;
            Name = name;
            Documentation = documentation;
        }

        public bool IsAssignableFrom(Type value)
        {
            return Expects.IsAssignableFrom(value);
        }

        public abstract bool IsAssignable { get; }

        public abstract void Set(object instance, object value);
        public abstract object Get(object instance);
    }

    public sealed class PropertyMeta : PropertyLikeMeta
    {
        public PropertyInfo PropertyInfo { get; private set; }

        public PropertyMeta(PropertyInfo info, string name = null)
            : base(info.PropertyType, name ?? info.Name, DocAttribute.GetDocs(info))
        {
            PropertyInfo = info;
        }

        public override object Get(object instance)
        {
            return PropertyInfo.GetValue(instance, new object[0]);
        }

        public override bool IsAssignable
        {
            get { return PropertyInfo.CanWrite; }
        }

        public override void Set(object instance, object value)
        {
            PropertyInfo.SetValue(instance, value, new object[0]);
        }
    }

    public class FieldMeta : PropertyLikeMeta
    {
        public FieldInfo FieldInfo { get; private set; }

        public FieldMeta(FieldInfo info, string name)
            : base(info.FieldType, name ?? info.Name, DocAttribute.GetDocs(info))
        {
            FieldInfo = info;
        }

        public override object Get(object instance)
        {
            return FieldInfo.GetValue(instance);
        }

        public override bool IsAssignable
        {
            get { return FieldInfo.IsInitOnly; }
        }

        public override void Set(object instance, object value)
        {
            FieldInfo.SetValue(instance, value);
        }
    }

    public class ParameterPropertyMeta : PropertyLikeMeta
    {
        public ParameterInfo ParameterInfo { get; private set; }

        public ParameterPropertyMeta(ParameterInfo info, string name)
            : base(info.ParameterType, name ?? info.Name, "TODO")
        {
            ParameterInfo = info;
        }

        public override object Get(object instance)
        {
            var configFor = (ConfigDataBase)instance;
            return configFor.Get(Name);
        }

        public override bool IsAssignable
        {
            get { return true; }
        }

        public override void Set(object instance, object value)
        {
            var configFor = (ConfigDataBase)instance;
            configFor.Set(Name, value);
        }
    }
}