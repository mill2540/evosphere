﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Serialization;
using Newtonsoft.Json.Linq;
using UnityEngine;
using YamlDotNet.RepresentationModel;

namespace Values
{

    public class ValueArrayMemberException : ValueException
    {
        public int Index { get; private set; }

        public ValueArrayMemberException(int index, ValueException inner) : base(string.Format("Exception at index {0}:\n{1}", index, inner.ToString()), inner) { Index = index; }
    }
    public class ArrayValueFactory : IValueFactory<IValue>
    {
        public IValue FromObject(Type type, object o)
        {
            var e = ((IEnumerable) o).GetEnumerator();
            var children = new List<IValue>();
            while (e.MoveNext())
            {
                children.Add(ValueFactory.FromObject(e.Current));
            }

            return new ArrayValue(type, children);
        }

        public IValue FromJson(Type type, JToken json)
        {
            return new ArrayValue(type, json["values"]
                .Children()
                .Select(item => ValueFactory.FromJson((JObject) item))
                .ToList());
        }

        public IValue FromYaml(Type type, YamlNode yaml)
        {
            throw new InvalidOperationException("Not Implemented");
            // var seq = (YamlSequenceNode)yaml;
            // var elementType = ReflectionUtils.GetListElement(type);

            // var elements = new List<IValue>();
            // int idx = 0;
            // foreach (var child in seq)
            // {
            //     try
            //     {

            //         var result = ValueFactory.FromYaml(elementType, child);
            //         elements.Add(result);
            //     }
            //     catch (ValueException ex)
            //     {
            //         throw new ValueArrayMemberException(idx, ex);
            //     }
            //     ++idx;
            // }

            // return new ArrayValue(type, elements);
        }
    }

    public class ArrayValue : IValue
    {
        private readonly Type type;
        private readonly List<IValue> elements;

        public ArrayValue(Type type, List<IValue> elements)
        {
            this.type = type;
            this.elements = elements;
        }

        public Type GetValueType()
        {
            return type;
        }

        public JToken AsJson()
        {
            var valuesJson = new JArray();
            foreach (var element in elements)
            {
                valuesJson.Add(element.AsJson());
            }

            var json = new JObject();
            json["type"] = type.AssemblyQualifiedName;
            json["values"] = valuesJson;

            return json;
        }

        public YamlNode AsYaml()
        {

            throw new InvalidOperationException("Not Implemented");
            // var yaml = new YamlSequenceNode();

            // var meta = TypeMeta.MetaDB.Get(type);
            // var elementType = ReflectionUtils.GetListElement(type);
            // foreach (var value in elements)
            // {
            //     var valueYaml = value.AsYaml().SetType(value.GetValueType(), elementType);
            //     yaml.Add(valueYaml);
            // }

            // return yaml;
        }

        public object AsObject()
        {

            throw new InvalidOperationException("Not Implemented");
            // if (type.IsArray)
            // {
            //     var elementType = ReflectionUtils.GetListElement(type);
            //     var value = Array.CreateInstance(elementType, elements.Count);

            //     for (var i = 0; i < elements.Count; ++i)
            //     {
            //         var elementValue = elements[i].AsObject();
            //         value.SetValue(elementValue, new int[] { i });
            //     }

            //     return value;
            // }

            // var obj = Activator.CreateInstance(type);
            // var list = (IList) obj;

            // foreach (var value in elements)
            // {
            //     list.Add(value);
            // }

            // return obj;
        }
    }
}