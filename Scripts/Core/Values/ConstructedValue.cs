﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using YamlDotNet.RepresentationModel;

namespace Values
{
    public class ValueConstructorNotFoundException : ValueException
    {
        public Type Type { get; private set; }

        public ValueConstructorNotFoundException(Type type) : base(string.Format("No matching constructor found for {0}", type.FullName)) { }
    }

    public class ConstructedValueFactory : IValueFactory<IValue>
    {
        public IValue FromObject(Type type, object o)
        {
            throw new NotSupportedException("Constucted Types cannot be serialized");
        }

        public IValue FromJson(Type type, JToken json)
        {
            return new ConstructedValue(type, json["parameters"]
                .Children()
                .Select(item => ValueFactory.FromJson((JObject) item))
                .ToList());
        }

        private static IValue LoadFromParameters(Type type, IList<YamlNode> yamlParameters)
        {

            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);

            // var valueParameters = new List<IValue>();

            // // For each constructor, try to load its paramaters from the yaml
            // foreach (var constructor in meta.Constructors)
            // {
            //     valueParameters.Clear();

            //     // If we are given more parameters than the constructor can take, then this constructor is not valid.
            //     // TODO: Handle params arguments...
            //     if (yamlParameters.Count > constructor.Parameters.Length) continue;

            //     var success = true;
            //     for (var i = 0; i < yamlParameters.Count; ++i)
            //     {
            //         try
            //         {
            //             // Attempt to load the given yaml argument as the expected parameter type
            //             var result = ValueFactory.FromYaml(constructor.Parameters[i].ParameterType,
            //                 yamlParameters[i]);
            //             valueParameters.Add(result);
            //         }
            //         catch (ValueException)
            //         {
            //             success = false;
            //             break;
            //         }
            //     }

            //     // The parameter was loaded successfully, so return this constructor.
            //     // TODO: deal with abiguous matches.
            //     if (success)
            //     {
            //         return new ConstructedValue(type, valueParameters);
            //     }
            // }

            // throw new ValueConstructorNotFoundException(type);
        }

        public IValue FromYaml(Type type, YamlNode yaml)
        {
            var yamlSequence = yaml as YamlSequenceNode;
            return LoadFromParameters(type, yamlSequence != null ? yamlSequence.Children : new List<YamlNode> { yaml });
        }
    }

    public class ConstructedValue : IValue
    {
        private readonly Type type;
        private readonly List<IValue> parameters;

        public ConstructedValue(Type type, List<IValue> parameters)
        {
            this.type = type;
            this.parameters = parameters;
        }

        public Type GetValueType()
        {
            return type;
        }

        public JToken AsJson()
        {
            var parametersJson = new JArray();
            foreach (var value in parameters)
            {
                parametersJson.Add(value.AsJson());
            }

            var json = new JObject();
            json["type"] = type.AssemblyQualifiedName;
            json["parameters"] = parametersJson;

            return json;
        }

        public YamlNode AsYaml()
        {

            throw new InvalidOperationException("Not Implemented");
            // var yaml = new YamlSequenceNode();

            // var elementType = ReflectionUtils.GetListElement(type);
            // foreach (var value in parameters)
            // {
            //     var valueYaml = value.AsYaml().SetType(value.GetValueType(), elementType);
            //     yaml.Add(valueYaml);
            // }
            // return yaml;
        }

        public object AsObject()
        {

            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);
            // var constructor = meta
            //     .Constructors
            //     .GetMethod(parameters.Select(val => val.GetValueType()).ToArray());

            // return constructor.Construct(parameters.Select(val => val.AsObject()).ToArray());
        }
    }
}