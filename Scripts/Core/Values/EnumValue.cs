﻿using System;
using Newtonsoft.Json.Linq;
using YamlDotNet.RepresentationModel;

namespace Values
{
    public class EnumValueFactory : IValueFactory<IValue>
    {
        public IValue FromObject(Type type, object o)
        {
            return new EnumValue(type, Enum.GetName(type, o));
        }

        public IValue FromJson(Type type, JToken json)
        {
            return new EnumValue(type, json["variant"].ToString());
        }

        public IValue FromYaml(Type type, YamlNode yaml)
        {
            var scalar = (YamlScalarNode)yaml;
            // TODO: validate scalar here
            return new EnumValue(type, scalar.Value);
        }
    }

    public class EnumValue : IValue
    {
        private readonly Type type;
        private readonly string variant;

        public EnumValue(Type type, string variant)
        {
            this.type = type;
            this.variant = variant;
        }

        public Type GetValueType() { return type; }

        public JToken AsJson()
        {
            var json = new JObject();
            json["type"] = type.AssemblyQualifiedName;
            json["variant"] = variant;
            return json;
        }

        public YamlNode AsYaml()
        {
            var yaml = new YamlScalarNode { Value = variant };
            return yaml;
        }

        public object AsObject()
        {
            return Enum.Parse(type, variant);
        }
    }
}
