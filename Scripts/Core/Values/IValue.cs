﻿using System;
using Config;
using Newtonsoft.Json.Linq;
using TypeMeta;
using YamlDotNet.RepresentationModel;

namespace Values
{
    public class ValueException : Exception
    {
        public ValueException() { }
        public ValueException(string message) : base(message) { }
        public ValueException(string message, Exception inner) : base(message) { }
    }

    public class ValueScalarConversionException : ValueException
    {

        public string Scalar { get; private set; }
        public Type Type { get; private set; }

        public ValueScalarConversionException(string scalar, Type type) : base(string.Format(
            "Cannot convert \"{0}\" to object of type {1}",
            scalar,
            type.FullName)) { Scalar = scalar; Type = type; }
    }

    public class CannotDeserializeValueException : ValueException
    {
        public Type Type { get; private set; }

        public CannotDeserializeValueException(Type type) : base(string.Format(
            "Cannot deserialize to {0}",
            type.FullName)) { Type = type; }
    }

    public interface IValue : IAsJson
    {
        Type GetValueType();

        YamlNode AsYaml();

        object AsObject();
    }

    public interface IValueFactory<T> where T : IValue
    {
        T FromObject(Type type, object o);
        T FromJson(Type type, JToken json);
        T FromYaml(Type type, YamlNode yaml);
    }

    /// <summary>
    /// Handles serializing and deserializing values into JSON, YAML and C# objects
    /// </summary>
    public static class ValueFactory
    {
        public static readonly RegisteredValueFactory ObjectValueFactory = new RegisteredValueFactory();
        public static readonly StringValueFactory StringValueFactory = new StringValueFactory();
        public static readonly ArrayValueFactory ArrayValueFactory = new ArrayValueFactory();
        public static readonly ConstructedValueFactory ConstructedValueFactory = new ConstructedValueFactory();
        public static readonly EnumValueFactory EnumValueFactory = new EnumValueFactory();

        /// Turn JSON into a C# object
        public static object Deserialize(Type type, JToken json)
        {
            throw new InvalidOperationException("Not Implemented");
            // var meta = MetaDB.Get(type);
            // // If they type can be represented as a string, and we have been passed a string, unstringify it
            // if (meta.IsStringable && json is JValue)
            // {
            //     return StringValueFactory.FromJson(type, json).AsObject();
            // }

            // // Mostly just deligate to the values system
            // return FromJson((JObject)json).AsObject();
        }

        public static IValue FromObject(object o)
        {
            throw new InvalidOperationException("Not Implemented");
            // var type = o.GetType();
            // var meta = MetaDB.Get(type);

            // if (meta.IsConstructible)
            // {
            //     return ConstructedValueFactory.FromObject(type, o);
            // }

            // if (type.IsEnum)
            // {
            //     return EnumValueFactory.FromObject(type, o);
            // }

            // if (meta.IsStringable)
            // {
            //     return StringValueFactory.FromObject(type, o);
            // }

            // if (meta.IsArray)
            // {
            //     return ArrayValueFactory.FromObject(type, o);
            // }

            // if (type.IsRegistered())
            // {
            //     return ObjectValueFactory.FromObject(type, o);
            // }

            // return null;
        }

        /// <summary>
        /// Convert JSON into a IValue
        /// </summary>
        /// <param name="json"></param>
        /// <returns></returns>
        public static IValue FromJson(JObject json)
        {
            throw new InvalidOperationException("Not Implemented");
            // var type = Type.GetType(json["type"].ToString());
            // var meta = MetaDB.Get(type);

            // if (meta.IsConstructible)
            // {
            //     return ConstructedValueFactory.FromJson(type, json);
            // }

            // if (type.IsEnum)
            // {
            //     return EnumValueFactory.FromJson(type, json);
            // }

            // JToken value;
            // if (json.TryGetValue("value", out value))
            // {
            //     if (meta.IsStringable)
            //     {
            //         return StringValueFactory.FromJson(type, json);
            //     }

            //     return null;
            // }

            // if (meta.IsArray)
            // {
            //     return ArrayValueFactory.FromJson(type, json);
            // }

            // if (type.IsRegistered())
            // {
            //     return ObjectValueFactory.FromJson(type, json);
            // }

            // return null;
        }

        public static YamlNode AsYamlRoot(IValue value)
        {
            var yaml = value.AsYaml();
            yaml.SetType(value.GetValueType(), null);
            return yaml;
        }

        public static IValue FromYaml(Type expected, YamlNode yaml)
        {
            throw new InvalidOperationException("Not Implemented");
            // var type = GetTypeFromTag(expected, yaml);
            // var meta = MetaDB.Get(type);

            // if (meta.IsConstructible)
            // {
            //     return ConstructedValueFactory.FromYaml(type, yaml);
            // }

            // if (type.IsEnum)
            // {
            //     return EnumValueFactory.FromYaml(type, yaml);
            // }

            // var scalar = yaml as YamlScalarNode;
            // if (scalar != null)
            // {
            //     if (meta.IsStringable)
            //     {
            //         return StringValueFactory.FromYaml(type, yaml);
            //     }

            //     if (scalar.Value == "")
            //     {
            //         return null;
            //     }
            // }

            // if (meta.IsArray)
            // {
            //     return ArrayValueFactory.FromYaml(type, yaml);
            // }

            // if (type.IsRegistered())
            // {
            //     return ObjectValueFactory.FromYaml(type, yaml);
            // }

            // throw new CannotDeserializeValueException(type);
        }

        public static Type GetTypeFromTag(Type type, YamlNode node)
        {
            if (string.IsNullOrEmpty(node.Tag)) return type;

            var name = node.Tag.Substring(1);
            return TypeRegistry.GetTypeByName(name);
        }
    }

    public static class ValuesExt
    {
        public static YamlNode SetType(this YamlNode yaml, Type type, Type expected)
        {
            if (type.IsRegistered() && type != expected)
            {
                yaml.Tag = "!" + type.GetRegistryName();
            }

            return yaml;
        }
    }
}