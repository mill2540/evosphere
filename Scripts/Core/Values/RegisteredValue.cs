﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;
using YamlDotNet.RepresentationModel;

namespace Values
{

    public class ValueMemberDoesNotExistException : ValueException
    {
        public Type Type { get; private set; }
        public string Name { get; private set; }

        public ValueMemberDoesNotExistException(Type type, string name) : base(string.Format("No member named {0} exists in {1}.", name, type.Name))
        {
            Type = type;
            Name = name;
        }
        public ValueMemberDoesNotExistException(Type type, string name, Exception inner) : base(string.Format("No member named {0} exists in {1}.", name, type.Name), inner)
        {
            Type = type;
            Name = name;
        }
    }

    public class ValueMemberException : ValueException
    {
        public string Member { get; private set; }

        public ValueMemberException(string member, ValueException inner) : base(string.Format("Exception in {0}:\n{1}", member, inner.ToString()), inner) { }
    }

    public class RegisteredValueFactory : IValueFactory<IValue>
    {
        public IValue FromObject(Type type, object o)
        {
            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);
            // return new RegisteredValue(type, meta.GetConfigMembers()
            //     .ToDictionary(
            //         prop => prop.Name,
            //         prop => ValueFactory.FromObject(prop.Get(o))));
        }

        public IValue FromJson(Type type, JToken json)
        {
            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);
            // return new RegisteredValue(type, json["members"]
            //     .Children()
            //     .Cast<JProperty>()
            //     .ToDictionary(
            //         prop => prop.Name,
            //         prop => ValueFactory.FromJson((JObject) prop.Value)));
        }

        public IValue FromYaml(Type type, YamlNode yaml)
        {
            throw new InvalidOperationException("Not Implemented");
            // var map = (YamlMappingNode) yaml;

            // var meta = TypeMeta.MetaDB.Get(type);

            // var members = new Dictionary<string, IValue>();
            // foreach (var child in map)
            // {
            //     var name = child.Key.ToString();

            //     var member = meta.GetConfigMember(name);
            //     if (member == null)
            //     {
            //         throw new ValueMemberDoesNotExistException(type, name);
            //     }

            //     try
            //     {
            //         var result = ValueFactory.FromYaml(member.Expects, child.Value);
            //         members.Add(name, result);
            //     }
            //     catch (ValueException ex)
            //     {
            //         throw new ValueMemberException(name, ex);
            //     }
            // }

            // return new RegisteredValue(type, members);
        }
    }

    public class RegisteredValue : IValue
    {
        private Type type;
        private Dictionary<string, IValue> members;

        public RegisteredValue(Type type, Dictionary<string, IValue> members)
        {
            this.type = type;
            this.members = members;
        }

        public Type GetValueType()
        {
            return type;
        }

        public JToken AsJson()
        {
            var membersJson = new JObject();
            foreach (var member in members)
            {
                membersJson[member.Key] = member.Value.AsJson();
            }

            var json = new JObject();
            json["type"] = type.AssemblyQualifiedName;
            json["members"] = membersJson;

            return json;
        }

        public YamlNode AsYaml()
        {
            throw new InvalidOperationException("Not Implemented");
            // var yaml = new YamlMappingNode();

            // var meta = TypeMeta.MetaDB.Get(type);
            // foreach (var member in meta.GetConfigMembers())
            // {
            //     if (!members.ContainsKey(member.Name)) continue;

            //     var memberValue = members[member.Name];
            //     var value = memberValue.AsYaml().SetType(memberValue.GetValueType(), member.Expects);
            //     yaml.Add(new YamlScalarNode(member.Name), value);
            // }

            // return yaml;
        }

        public object AsObject()
        {
            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);
            // var obj = meta.Deserializer == null ? Activator.CreateInstance(type) : ConfigDataBase.CreateConfigData(type);

            // foreach (var member in members)
            // {
            //     meta.GetConfigMember(member.Key)
            //         .Set(obj, member.Value.AsObject());
            // }

            // return obj;
        }

        public override string ToString()
        {
            return string.Format("RegisteredValue({0})", GetValueType().FullName);
        }
    }
}