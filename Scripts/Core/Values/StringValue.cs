﻿using System;
using System.Text.RegularExpressions;
using Newtonsoft.Json.Linq;
using YamlDotNet.RepresentationModel;

namespace Values
{
    public class StringValueFactory : IValueFactory<IValue>
    {
        public IValue FromObject(Type type, object o)
        {
            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);
            // if (type == typeof(DateTime) || type == typeof(bool) || type == typeof(byte) || type == typeof(sbyte) ||
            //     type == typeof(char) || type == typeof(decimal) || type == typeof(double) || type == typeof(float) ||
            //     type == typeof(int) || type == typeof(uint) || type == typeof(long) || type == typeof(ulong) ||
            //     type == typeof(short) || type == typeof(ushort) || type == typeof(Query) ||
            //     type == typeof(TargetedQuery) || type == typeof(MultiTargetedQuery))
            // {
            //     return new StringValue(type, o.ToString());
            // }

            // if (type == typeof(string))
            // {
            //     return new StringValue(type, (string)o);
            // }

            // var nullable = Nullable.GetUnderlyingType(type);
            // // If the value is not nullable, then we really don't know what to do with it
            // if (nullable == null) throw new NotImplementedException();

            // return o == null ? new StringValue(type, null) : FromObject(nullable, o);
        }

        public IValue FromJson(Type type, JToken json)
        {
            return new StringValue(type, json["value"].ToString());
        }

        public IValue FromYaml(Type type, YamlNode yaml)
        {
            return new StringValue(type, yaml.ToString());
        }
    }

    public class StringValue : IValue
    {
        private readonly Type type;
        private readonly string value;

        public StringValue(Type type, string value)
        {
            this.type = type;
            this.value = value;
        }

        public JToken AsJson()
        {
            var obj = new JObject();
            obj["type"] = type.AssemblyQualifiedName;
            obj["value"] = value;
            return obj;
        }

        public YamlNode AsYaml()
        {
            return new YamlScalarNode(value);
        }

        public object AsObject()
        {
            throw new InvalidOperationException("Not Implemented");
            // var meta = TypeMeta.MetaDB.Get(type);
            // if (type == typeof(DateTime))
            // {
            //     return DateTime.Parse(value);
            // }

            // if (type == typeof(bool))
            // {
            //     return bool.Parse(value);
            // }

            // if (type == typeof(byte))
            // {
            //     return byte.Parse(value);
            // }

            // if (type == typeof(sbyte))
            // {
            //     return sbyte.Parse(value);
            // }

            // if (type == typeof(char))
            // {
            //     return char.Parse(value);
            // }

            // if (type == typeof(decimal))
            // {
            //     return decimal.Parse(value);
            // }

            // if (type == typeof(double))
            // {
            //     return double.Parse(value);
            // }

            // if (type == typeof(float))
            // {
            //     return float.Parse(value);
            // }

            // if (type == typeof(int))
            // {
            //     return int.Parse(value);
            // }

            // if (type == typeof(uint))
            // {
            //     return uint.Parse(value);
            // }

            // if (type == typeof(long))
            // {
            //     return long.Parse(value);
            // }

            // if (type == typeof(ulong))
            // {
            //     return ulong.Parse(value);
            // }

            // if (type == typeof(short))
            // {
            //     return short.Parse(value);
            // }

            // if (type == typeof(ushort))
            // {
            //     return ushort.Parse(value);
            // }

            // if (type == typeof(string))
            // {
            //     return value;
            // }

            // if (type == typeof(Query))
            // {
            //     return Query.Parse(value);
            // }

            // if (type == typeof(TargetedQuery))
            // {
            //     return TargetedQuery.Parse(value);
            // }

            // if (type == typeof(MultiTargetedQuery))
            // {
            //     return MultiTargetedQuery.Parse(value);
            // }

            // var nullable = Nullable.GetUnderlyingType(type);
            // if (nullable == null) throw new NotImplementedException();
            // if (value == "" || value == "null")
            // {
            //     return null;
            // }

            // return new StringValue(nullable, value).AsObject();
        }

        public Type GetValueType()
        {
            return type;
        }
    }
}