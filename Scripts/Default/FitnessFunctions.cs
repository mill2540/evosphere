using System.Linq;
using UnityEngine;

[Register]
[Doc("This fitness function counts how far the organism traveles from it's starting point.")]
public class DistanceFromDropFactory : IFitnessFunctionFactory
{
    public IFitnessFunction Build()
    {
        return new DistanceFromDrop();
    }
}

public class DistanceFromDrop : IFitnessFunction
{
    private Organism target;
    private Vector3 start;

    public void Init(Organism target)
    {
        this.target = target;
        start = CalculateCenter();
    }

    public IFitnessFunction Reset()
    {
        return new DistanceFromDrop();
    }

    private Vector3 CalculateCenter()
    {
        var children = target.GetComponentsInChildren<Rigidbody>();
        var center = children.Aggregate(Vector3.zero, (current, child) => current + child.transform.position);

        if (children.Length != 0)
        {
            center /= children.Length;
        }
        else
        {
            center = start;
        }

        return center;
    }


    public float CalculateNow(Organism target)
    {
        var current = CalculateCenter();
        return (current - start).magnitude;
    }
}

