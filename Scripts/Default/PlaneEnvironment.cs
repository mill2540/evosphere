using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Doc("A simple vector in the x-y plane with integer coordinates")]
public struct Vector
{
    public int X;
    public int Y;

    public Vector(int x, int y)
    {
        X = x;
        Y = y;
    }

    public Vector(int v)
    {
        X = v;
        Y = v;
    }
}

[Register]
[Doc("This is a simple environment. It consists of a single plane, which edges so that organisms do not fall off.")]
public class PlaneEnvironmentFactory : IEnvironmentFactory
{
    [Doc("Organisms are laid out in a grid. This defines dimensions of the grid.")]
    [Config.Value] public Vector Grid;

    [Doc("Organisms are laid out in a grid. This defines the spacing between organisms in the grid")]
    [Config.Value] public Vector Spacing;

    [Doc("How high above the plane organisms are started from. Note that some organisms may teleport to the ground.")]
    [Config.Value] public float DropHeight;

    [Doc("The components (elements?) of the environment")]
    [Config.Value] public IEnvironmentComponentFactory[] Components;

    public Environment Build(GameObject target)
    {
        return target.AddComponent<PlaneEnvironment>().Init(Grid, Spacing, DropHeight,
            Components.Select(component => component.Build(target)).ToList());
    }
}

internal class Position
{
    public Vector3 position;
    public GameObject content;
}

public class PlaneEnvironment : Environment
{
    private int placed = 0;
    private List<Position> grid;
    private Vector3 gridSize;
    private const int BorderWidth = 32;

    public PlaneEnvironment Init(Vector grid, Vector spacing, float dropHeight,
        List<EnvironmentComponent> components)
    {
        base.Init(components);

        this.grid = new List<Position>()
        {
            Capacity = grid.X * grid.X
        };

        gridSize = new Vector2(grid.X * spacing.X, grid.Y * spacing.Y);
        for (var x = 0; x < grid.X; ++x)
        {
            for (var y = 0; y < grid.Y; ++y)
            {
                this.grid.Add(new Position
                {
                    position = Vector3.up * dropHeight + Vector3.right * ((x + 1) * spacing.X) +
                               Vector3.forward * ((y + 1) * spacing.Y + 16),
                    content = null
                });
            }
        }

        return this;
    }

    private void Start()
    {
        var prefab = Resources.Load<GameObject>("PlaneEnvironmentTerrain");
        var terrain = Instantiate(prefab);
        terrain.transform.SetParent(transform);

        var extents = terrain.GetComponent<TerrainCollider>().bounds.extents;
        var cameraPosition = Vector3.up * 10 + Vector3.right * extents.x + Vector3.forward * BorderWidth;
        Camera.main.transform.position = cameraPosition;

        Camera.main.transform.LookAt(extents);

        foreach (var g in grid)
        {
            g.position += cameraPosition - gridSize / 2;
        }

        grid = grid.OrderBy((position) => (position.position - cameraPosition).sqrMagnitude).ToList();

    }

    public override void Place(GameObject target)
    {
        var pos = grid.SkipWhile(position => position.content != null).First();

        target.transform.position = pos.position;
        pos.content = target;
    }

    public override void Remove(GameObject target)
    {
        var pos = grid.Find(position => position.content == target);
        pos.content = null;
    }
}