using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[Register]
[Doc("Roulette selection algorithm")]
public class RouletteSelectorProvider : ISelectorProvider {
    public ISelector Init(List<Organism> population)
    {
        return new RouletteSelector(population);
    }
}


public class RouletteSelector : ISelector
{
    private class Normalized
    {
        public Organism organism;
        public float fitness;

        public static implicit operator Organism(Normalized normalized)
        {
            return normalized.organism;
        }
    }

    private readonly List<Normalized> population;
    private float total;

    public RouletteSelector(ICollection<Organism> population)
    {
        var min = population.Min(member => member.Fitness);
        var mean = 0.0f;

        this.population = population.Select(member =>
        {
            mean += member.Fitness;
            var normal = member.Fitness + min;
            total += normal;

            return new Normalized
            {
                organism = member,
                fitness = normal
            };
        }).ToList();
    }

    public Organism Select()
    {
        var target = Random.value * total;

        var selected = population.SkipWhile(organism =>
        {
            target -= organism.fitness;
            return target > 0;
        }).First();

        return selected;
    }
}
