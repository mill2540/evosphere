using System.Linq;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Camera))]
public class CameraSelect : MonoBehaviour
{
    private bool rotated = false;
    private bool selected = false;
    private bool buttonDown = false;
    private Camera cam;

    [SerializeField] private EvoSphereInstance instance;
    [SerializeField] private float targetDistance;
    [SerializeField] private float springStrength;
    [SerializeField] private float rotationSpringStrength;
    [SerializeField] private float speed;

    public delegate void SelectionListener(Organism target);

    public event SelectionListener OnSelection;

    private Organism selection;

    public Organism Selection
    {
        get { return selection; }
        set
        {
            if (OnSelection != null)
            {
                OnSelection(value);
            }
            selection = value;
        }
    }

    private Vector3 initialPosition;
    private Quaternion initialRotation;

    private void Awake()
    {
        cam = GetComponent<Camera>();
    }

    // Update is called once per frame
    private void Update()
    {
        if (Input.GetMouseButton(0) && !EventSystem.current.IsPointerOverGameObject())
        {
            if (!buttonDown)
            {
                buttonDown = true;

                var ray = cam.ScreenPointToRay(Input.mousePosition);
                Selection = null;

                RaycastHit hit;
                if (Physics.Raycast(ray, out hit))
                {
                    Selection = hit.collider.GetComponentInParent<Organism>();
                    if (!selected)
                    {
                        initialPosition = transform.position;
                        initialRotation = transform.rotation;
                    }
                    selected = true;

                    rotated = false;
                }
            }
        }
        else
        {
            buttonDown = false;
        }

        float radius;
        var position = transform.position;
        Vector3 target;
        Quaternion targetRotation;

        if (Selection != null)
        {
            var renderers = Selection.GetComponentsInChildren<Renderer>();
            var tmp = renderers.Aggregate(Vector3.zero, (current, r) => current + r.transform.position);
            tmp /= renderers.Length;

            var bounds = new Bounds(tmp, Vector3.zero);
            foreach (var r in renderers)
            {
                bounds.Encapsulate(r.bounds);
            }
            radius = bounds.extents.magnitude;

            target = bounds.center;
            targetRotation = Quaternion.LookRotation(target - position);
        }
        else
        {
            target = initialPosition;
            targetRotation = initialRotation;
            radius = 0;
        }

        if (selected)
        {
            var rotation = transform.rotation.eulerAngles;
            var deltaRotation = (targetRotation.eulerAngles - rotation).magnitude;

            transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation,
                rotationSpringStrength * Mathf.Min(deltaRotation, 10));

            if (deltaRotation <= 10)
            {
                rotated = true;
            }

            if (rotated)
            {
                target.y = position.y;
                var delta = (target - position);
                var distance = delta.magnitude;
                var targetPosition = (distance - (targetDistance + radius)) * delta.normalized * springStrength;

                transform.position += targetPosition;
            }
        }

        if (Input.GetKey(KeyCode.RightArrow))
        {
            transform.position += transform.right * speed;
        }

        if (Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position -= transform.right * speed;
        }

        if (Input.GetKey(KeyCode.UpArrow))
        {
            transform.position += transform.forward * speed;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            transform.position -= transform.forward * speed;
        }
    }
}