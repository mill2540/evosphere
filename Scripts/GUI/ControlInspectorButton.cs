﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GUI
{
    [RequireComponent(typeof(Button))]
    public class ControlInspectorButton : UIBehaviour
    {
        [SerializeField] private EvoSphereInstance _interface;
        [SerializeField] private bool entersInspector;

        protected override void Awake()
        {
            GetComponent<Button>().onClick.AddListener(Transition);
        }

        private void Transition()
        {
            if (entersInspector) _interface.EnterInspector();
            else _interface.ExitInspector();
        }
    }
}