﻿using UnityEngine;

namespace GUI
{
    public abstract class Inspectable : MonoBehaviour
    {
        public abstract string Name { get; }
        public abstract string[] Views { get; }

        protected abstract void EnterView(string view);
        
        public void Enter(string view) {Reset(); if(view != null) EnterView(view);}
        public abstract void Reset();
    }
}