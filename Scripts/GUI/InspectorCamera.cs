﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GUI
{
    [RequireComponent(typeof(Camera))]
    public class InspectorCamera : MonoBehaviour
    {
        private Vector3 rotationSpeed;
        private Vector3 initialPosition;
        private Quaternion initialRotation;

        private Vector3 mouse;
        public Rigidbody Handle;

        public GameObject InspectableUI;
        public GameObject ViewsContainer;
        public Text ViewLabel;
        public Button ViewButton;

        public Organism Target { get; private set; }

        private float minZoom;
        private float maxZoom;
        private float targetZoom;
        private float currentZoom;
        private readonly Dictionary<string, Inspectable> inspectors = new Dictionary<string, Inspectable>();

        private void Awake()
        {
            initialPosition = transform.position;
            initialRotation = transform.rotation;
            if(InspectableUI) InspectableUI.SetActive(false);
        }

        public JObject Enter(Organism target)
        {
            if(InspectableUI) InspectableUI.SetActive(true);
            transform.position = initialPosition;
            transform.rotation = initialRotation;
            Handle.transform.position = Vector3.zero;
            Handle.transform.rotation = Quaternion.identity;

            enabled = GetComponent<Camera>().enabled = true;
            Handle.transform.localPosition = Vector3.zero;

            Target = target;
            Target.transform.SetParent(Handle.transform, false);
            Target.transform.localPosition = Vector3.zero;

            minZoom = maxZoom = targetZoom = currentZoom = 10;
            var center = Vector3.zero;
            var count = 0;
            center = Target.GetComponentsInChildren<Renderer>().Aggregate(center, (current, r) =>
            {
                ++count;
                return current + r.bounds.center;
            });


            if (count != 0)
            {
                center /= count;

                var bounds = new Bounds {center = center};
                foreach (var r in Target.GetComponentsInChildren<Renderer>())
                {
                    bounds.Encapsulate(r.bounds);
                }
                minZoom = bounds.size.magnitude;
                maxZoom = minZoom * 3;

                targetZoom = currentZoom = (maxZoom - minZoom) / 2 + minZoom;
            }
            Target.transform.localPosition = Target.transform.position - center;

            Handle.transform.position = initialPosition + transform.forward * currentZoom;
            transform.LookAt(Handle.transform.position);
            
            ClearViews();

            inspectors.Clear();
            var inspectorsJson = new JObject();
            foreach (var inspectable in Target.GetComponentsInChildren<Inspectable>())
            {
                if (ViewsContainer && inspectable.Views.Length != 0)
                {
                    AddViewPanel(inspectable);

                    AddView(inspectable, "Reset", null);
                    foreach (var view in inspectable.Views)
                    {
                        AddView(inspectable, view, view);
                    }
                }
                
                inspectors[inspectable.Name] = inspectable;
                var views = new JArray();
                foreach (var view in inspectable.Views)
                {
                    views.Add(view);
                }
                inspectorsJson[inspectable.Name] = views;
            }
            
            return inspectorsJson;
        }

        private void AddViewPanel(Inspectable inspectable)
        {   
            var label = Instantiate(ViewLabel);
            label.text = inspectable.Name;
            label.transform.SetParent(ViewsContainer.transform, false);
        }

        private void AddView(Inspectable inspectable, string label, string view)
        {
            var viewButton = Instantiate(ViewButton);
            viewButton.GetComponentInChildren<Text>().text = label;
            viewButton.transform.SetParent(ViewsContainer.transform, false);
                        
            viewButton.onClick.AddListener(() => Enter(inspectable.Name, view));
        }

        private void ClearViews()
        {
            if (!ViewsContainer) return;
            
            foreach (var button in ViewsContainer.GetComponentsInChildren<Button>())
            {
                Destroy(button.gameObject);
            }
            
            foreach (var label in ViewsContainer.GetComponentsInChildren<Text>())
            {
                Destroy(label.gameObject);
            }
        }

        public void Enter(string inspector, string view)
        {
            inspectors[inspector].Enter(view);
        }

        public void Exit()
        {
            if(InspectableUI) InspectableUI.SetActive(false);
            enabled = GetComponent<Camera>().enabled = false;
            if (Target != null) Destroy(Target.gameObject);
        }

        public void Update()
        {
            var dzoom = 0f;
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                
                if (Input.GetMouseButton(0))
                {
                    var delta = mouse - Input.mousePosition;

                    delta = delta.normalized * Mathf.Sqrt(delta.magnitude);
                    rotationSpeed.x -= delta.y;
                    rotationSpeed.y += delta.x;
                }

                dzoom = Input.mouseScrollDelta.y;
            }
            
            Handle.transform.Rotate(rotationSpeed, Space.World);

            rotationSpeed *= 0.9f;

            targetZoom = Mathf.Max(Mathf.Min(targetZoom + dzoom, maxZoom), minZoom);

            currentZoom += (targetZoom - currentZoom) * 0.1f;

            Handle.transform.position = initialPosition + transform.forward * currentZoom;
            transform.LookAt(Handle.transform.position);

            mouse = Input.mousePosition;
        }
    }
}