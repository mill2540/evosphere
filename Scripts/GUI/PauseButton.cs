﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace GUI
{
    [RequireComponent(typeof(Button))]
    public class PauseButton : UIBehaviour
    {
        [SerializeField] private EvoSphereInstance _interface;
        private Button _button;
        private bool paused = false;

        protected override void Awake()
        {
            _button = GetComponent<Button>();
            _button.onClick.AddListener(TogglePaused);
            _button.GetComponentInChildren<Text>().text = "Pause";
        }

        private void TogglePaused()
        {
            paused = !paused;
            _button.GetComponentInChildren<Text>().text = paused ? "Play" : "Pause";

            _interface.Pause(paused);
        }
    }
}