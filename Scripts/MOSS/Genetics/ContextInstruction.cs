﻿using MOSS.Genetics.Instructions;

namespace MOSS.Genetics
{
    public abstract class ContextInstruction : Instruction<byte>
    {
        public Context Context { get; private set; }

        public ContextInstruction InitContext(Context context)
        {
            Context = context;
            return this;
        }
    }
}