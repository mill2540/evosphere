﻿using System;
using UnityEngine;

namespace MOSS.Genetics
{
    public abstract class Instruction<T> where T: IComparable<T>
    {
        public enum Direction {
            Backward = -1,
            Forward  =  1, 
        }

        /// <summary>
        /// Resets the state of this instruction so that it can be run in the forward direction.
        /// </summary>
        public abstract void Setup(Direction direction);

        /// <summary>
        /// Apply this instruction. Any objects created by this instruction should be added to parent.
        /// </summary>
        /// <param name="direction">The direction that the genome is being parsed in</param>
        /// <param name="genome">The genome class</param>
        /// <param name="parent">The parent object.</param>
        /// <returns>True if and only if this command has finished executing.</returns>
        public abstract bool Run(Direction direction, MossGenome genome, GameObject parent);
    }
}
