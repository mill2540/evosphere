﻿using UnityEngine;

namespace MOSS.Genetics.Instructions
{
	public class AddBlock : ContextInstruction
	{

		public byte BlockID { get; private set; }
		private readonly Vector3 translation;
		private readonly Vector3 rotation;
		private GameObject target;

		public AddBlock(byte blockID, Vector3 translation, Vector3 rotation)
		{
			BlockID = blockID;
			this.translation = translation;
			this.rotation = rotation;
		}

		public override void Setup(Direction direction)
		{
			// Nothing to do here
		}

		public override bool Run(Direction direction, MossGenome genome, GameObject parent)
		{
			if (direction == Direction.Forward)
			{
				if (target == null)
					target = genome.AddChild(parent,
						BlockID,
						Context.Translation + Context.Rotation * translation,
						Context.Rotation * Quaternion.Euler(rotation), true);
			}
			else
			{
				if (target == null) return true;
			
				Object.Destroy(target);
				target = null;
			}

			return true;
		}
	}
}