﻿using System.Collections.Generic;
using UnityEngine;

namespace MOSS.Genetics.Instructions
{
	public class Context : ContextInstruction
	{
		public List<ContextInstruction> Instructions { get; private set; }

		public int Passes { get; private set; }

		private readonly Quaternion initialRotation = Quaternion.identity;
		private readonly Quaternion rotationStep;
		private Quaternion currentRotation = Quaternion.identity;
		private readonly Vector3 translation;

		public ContextInstruction CurrentInstruction { get; private set; }

		public Quaternion Rotation
		{
			get
			{
				var parentRotation = Quaternion.identity;
				if (Context != null) parentRotation = Context.Rotation;
				return parentRotation * currentRotation;
			}
		}

		public Vector3 Translation
		{
			get
			{
				var parentTranslation = new Vector3();
				if (Context != null) parentTranslation = Context.Translation;
				return parentTranslation + Rotation * translation;
			}
		}

		private int pass;
		private int index;

		private bool ran; // Tracks if the current instruction has been run (if it needs to be Reset)

		public Context(int passes, Vector3 rotationStep, Vector3 translation, List<ContextInstruction> instructions)
		{
			Passes = passes;
			this.rotationStep = Quaternion.Euler(rotationStep);
			this.translation = translation;

			Instructions = instructions;
			foreach (var instruction in instructions) instruction.InitContext(this);
			CurrentInstruction = null;
		}

		public override void Setup(Direction direction)
		{
			SetCurrentInstruction(this);
			if (direction == Direction.Forward)
			{
				pass = 0;
				index = 0;
			}
			else
			{
				pass = Passes - 1;
				index = Instructions.Count - 1;
			}

			ran = false;
			currentRotation = initialRotation;
		}

		private void SetCurrentInstruction(ContextInstruction current)
		{
			if (Context != null) Context.SetCurrentInstruction(current);
			CurrentInstruction = current;
		}

		public override bool Run(Direction direction, MossGenome genome, GameObject parent)
		{
			if (Instructions.Count == 0) return true;

			if (!ran)
			{
				CurrentInstruction = Instructions[index];
				CurrentInstruction.Setup(direction);
				ran = true;
			}

			// Run the current instruction until it is finished
			if (Instructions[index].Run(direction, genome, parent))
			{
				ran = false;
				// The current instruction has finished, move to the next one
				index += (int)direction;

				// We are at the end of the instruciton sequence, wrap around
				if (index >= Instructions.Count)
				{
					currentRotation *= rotationStep;
					++pass;
					index = 0;
				}
				else if (index < 0)
				{
					currentRotation *= Quaternion.Inverse(rotationStep);

					--pass;
					index = Instructions.Count - 1;
				}
			}


			return pass >= Passes || Passes < 0;
		}
	}
}
