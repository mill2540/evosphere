﻿using System.Collections.Generic;
using MOSS.Genetics.Instructions;

namespace MOSS.Genetics.Parsers
{
	public class BlockParser : IParser<byte, ContextInstruction>
	{
		public ContextInstruction Parse(IParser<byte, ContextInstruction> parent, IEnumerator<byte> genome)
		{
			var blockID = genome.Read();
			var translation = genome.ReadVec();
			var rotation = genome
				.ReadN(3).Remap(0, 360).Snap(90).ToVectors().UnwrapHead();

			if (blockID == null || translation == null || rotation == null) return null;

			return new AddBlock(blockID.Value,
				MossDefaults.SnapTranslation(translation.Value, 0, 255),
				rotation.Value);
		}
	}
}