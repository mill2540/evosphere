﻿using System.Collections.Generic;
using MOSS.Genetics.Instructions;

namespace MOSS.Genetics.Parsers
{
	public class ContextParser : IParser<byte, ContextInstruction>
	{
		public int MaxLength
		{
			get;
			private set;
		}

		public int MaxPasses
		{
			get;
			private set;
		}

		public ContextParser(int maxLength, int maxPasses)
		{
			MaxLength = maxLength;
			MaxPasses = maxPasses;
		}


		public ContextInstruction Parse(IParser<byte, ContextInstruction> parent, IEnumerator<byte> genome)
		{
			var length = genome.Read() % MaxLength;
			var passes = genome.Read() % MaxPasses;
			var rotationStep = genome.ReadN(3).Remap(0, 360).Snap(90).ToVectors().UnwrapHead();
			var translation = genome.ReadN(3).Remap(-1.5f, 1.5f).Snap(1).ToVectors().UnwrapHead();

			if (length == null || passes == null || rotationStep == null || translation == null) return null;

			var instructions = genome.ParseToList(this, parent, length.Value);

			return new Context(passes.Value, rotationStep.Value, translation.Value, instructions);
		}
	}
}