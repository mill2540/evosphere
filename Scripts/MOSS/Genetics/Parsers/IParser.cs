﻿using System.Collections.Generic;

namespace MOSS.Genetics.Parsers
{
	/// <summary>
	/// A Parser is used to transform a flat enumerator of genetic data into some
	/// other data structure.
	/// </summary>
	/// <typeparam name="G">The type of data held in the enumerator</typeparam>
	/// <typeparam name="T">The type of data returned by the parser</typeparam>
	public interface IParser<G, T> where T : class
	{
		T Parse(IParser<G, T> parent, IEnumerator<G> genome);
	}
}