﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MOSS.Genetics.Parsers
{
	/// <summary>
	/// A collection of simple utilities for writing parsers.
	/// </summary>
	public static class ParserUtilities
	{
		/// <summary>
		/// Attempts to read length values genome.
		/// </summary>
		/// <typeparam name="G">The type held in the genome</typeparam>
		/// <param name="genome">The genome to read from</param>
		/// <param name="length">The number of values to read</param>
		/// <returns>
		/// an array of length values if genome had at least length values, and null otherwise.
		/// </returns>
		public static G[] ReadN<G>(this IEnumerator<G> genome, int length)
		{
			var results = new G[length];

			for (var i = 0; i < length; ++i)

			{
				if (!genome.MoveNext()) return null;
				results[i] = genome.Current;
			}

			return results;
		}

		public static G? Read<G>(this IEnumerator<G> genome) where G : struct
		{
			if (genome.MoveNext())
			{
				return genome.Current;
			}
			return null;
		}

		public static List<T> ParseToList<G, T>(this IEnumerator<G> genome, IParser<G, T> parent, IParser<G, T> parser, int maxLength = -1) where T : class
		{
			var results = new List<T>();
			if (maxLength > 0)
			{
				results.Capacity = maxLength;
			}

			while (maxLength < 0 || results.Count <= maxLength)
			{
				var item = parser.Parse(parent, genome);
				if (item == null) break;
				results.Add(item);
			}

			return results;
		}

		public static float Remap(this byte value, float min, float max)
		{
			var fvalue = (float)value;
			return (fvalue / byte.MaxValue) * (max - min) + min;
		}

		public static float? Remap(this byte? value, float min, float max)
		{
			if (value != null)
			{
				return ((byte)value).Remap(min, max);
			}
			return null;
		}

		public static float[] Remap(this byte[] value, float min, float max)
		{
			return value != null ? value.Select((byte arg) => arg.Remap(min, max)).ToArray() : null;
		}

		public static float Snap(this float value, float stepSize)
		{
			return Mathf.Round(value / stepSize) * stepSize;
		}

		public static float? Snap(this float? value, float stepSize)
		{
			if (value != null)
			{
				return Mathf.Round((float)value / stepSize) * stepSize;
			}
			return null;
		}

		public static float[] Snap(this float[] value, float stepSize)
		{
			return value != null ? value.Select((arg) => arg.Snap(stepSize)).ToArray() : null;
		}

		public static Vector3[] ToVectors(this float[] value)
		{
			if (value == null || value.Length % 3 != 0) return null;
		
			var vectors = new Vector3[value.Length / 3];
			for (uint i = 0; i < vectors.Length; ++i)
			{
				var j = i * 3;
				vectors[i] = new Vector3(value[j], value[j + 1], value[j + 2]);
			}
			return vectors;
		}

		public static T? UnwrapHead<T>(this T[] value) where T : struct
		{
			if (value == null || value.Length == 0) return null;
			return value[0];
		}

		public static Vector3? ReadVec(this IEnumerator<byte> value)
		{
			var coors = value.ReadN(3);
			if (coors != null)
			{
				return new Vector3((float)coors[0], (float)coors[1], (float)coors[2]);
			}
			return null;
		}

	}
}