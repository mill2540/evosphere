﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace MOSS.Genetics.Parsers
{
    public class Pattern<G, T> : IParser<G, T> where T : class
    {
        private int length;
        private Dictionary<G[], IParser<G, T>> cases = new Dictionary<G[], IParser<G, T>>(new ArrayEqualityComparer());

        public Pattern(int length)
        {
            this.length = length;
        }

        public Pattern<G, T> AddCase(G[] pattern, IParser<G, T> target)
        {
            if (pattern.Length != length)
            {
                /// TODO: make custom exception
                throw new Exception("Pattern Length does not match matcher length");
            }

            cases.Add(pattern, target);

            return this;
        }

        public T Parse(IParser<G, T> parent, IEnumerator<G> genome)
        {
            var patternBuffer = new G[length];

            // Read in the pattern. If the genome is not long enough, we will exit here.
            for (uint i = 0; i < length; ++i)
            {
                if (!genome.MoveNext()) return null;
                patternBuffer[i] = genome.Current;
            }

            // Search for the pattern
            while (!cases.ContainsKey(patternBuffer))
            {
                // Make sure we are not at the end of the genome yet. If we are, then
                // there was no match
                if (!genome.MoveNext()) return null;

                // Shift the array to the left and read in the next item from the genome
                Array.Copy(patternBuffer, 1, patternBuffer, 0, length - 1);
                patternBuffer[length - 1] = genome.Current;
            }

            // We found the pattern, so match against it
            return cases[patternBuffer].Parse(this, genome);
        }

        private class ArrayEqualityComparer : IEqualityComparer<G[]>
        {
            public bool Equals(G[] x, G[] y)
            {
                if (x.Length != y.Length) return false;

                return !x.Where((t, i) => !t.Equals(y[i])).Any();
            }

            public int GetHashCode(G[] obj)
            {
                // TODO: better algorithm?
                var hash = 23;
                for (var i = 0; i < obj.Length; ++i)
                {
                    hash = hash * 43 * i + obj[i].GetHashCode();
                }

                return hash;
            }
        }
    }
}