﻿using System;
using System.Collections.Generic;

namespace MOSS.Genetics.Parsers
{
    public class Range<G, T> : IParser<G, T> where T : class where G : IComparable<G>
    {
        private List<KeyValuePair<G, IParser<G, T>>> cases = new List<KeyValuePair<G, IParser<G, T>>>();
        private bool sorted = true;

        public Range<G, T> AddCase(G start, IParser<G, T> target)
        {
            cases.Add(new KeyValuePair<G, IParser<G, T>>(start, target));
            sorted = false;

            return this;
        }

        private void UpdateCache()
        {
            if (!sorted)
            {
                cases.Sort((KeyValuePair<G, IParser<G, T>> a, KeyValuePair<G, IParser<G, T>> b) => b.Key.CompareTo(a.Key));
                sorted = true;
            }
        }

        public T Parse(IParser<G, T> parent, IEnumerator<G> genome)
        {
            UpdateCache();
            genome.MoveNext();
            if (!genome.MoveNext()) return null;

            foreach (var c in cases)
            {
                var loc = c.Key.CompareTo(genome.Current);

                if (loc < 0)
                {
                    return c.Value.Parse(parent, genome);
                }
            }
            return null;
        }
    }
}