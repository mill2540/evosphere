using System.Collections;
using System.Collections.Generic;
using MOSS.MossBody;
using UnityEngine;

public interface IMossGenomeMutator
{
    void Mutate(MossGenome genome);

    IMossGenomeMutator Clone();
}

public interface IMossGenomeMutatorFactory
{
    IMossGenomeMutator Build();
}

public class MossGenomeMutator : IMossGenomeMutator
{
    public void Mutate(MossGenome mossGenome)
    {
        const float pChangeCodon = 0.05f;
        var genome = new List<byte>(mossGenome.Genome);
        for (var i = 0; i < genome.Count; ++i)
        {
            if (Random.value < pChangeCodon)
            {
                genome[i] = (byte) (Random.value * 255);
            }
        }

        mossGenome.Genome = genome.ToArray();
    }

    public IMossGenomeMutator Clone()
    {
        return new MossGenomeMutator();
    }
}

[Register]
public class MossGenomeMutatorFactory : IMossGenomeMutatorFactory
{
    public IMossGenomeMutator Build()
    {
        return new MossGenomeMutator();
    }
}


[Register]
public class MossFactory : IComponentFactory
{
    [Config.Value] public IMossGenomeFactory Genome;
    [Config.Value] public IMossGenomeMutatorFactory Mutator;

    public AbstractOrganismComponent Build(GameObject target)
    {
        return target.AddComponent<Moss>().Init(Genome.Build(), Mutator.Build());
    }
}

/// <summary>
/// Moss body class. Handles interfacing with the genome, as well as
/// construction, destruction and serialization of bodies.
/// </summary>
public class Moss : OrganismComponent<Moss>
{
    public bool FixConnections { get; set; }

    private GameObject head;
    private MossGenome genome;
    private IMossGenomeMutator mutator;

    public Moss Init(MossGenome genome, IMossGenomeMutator mutator)
    {
        this.genome = genome;
        this.mutator = mutator;

        return this;
    }

    public override void Inherit(Moss parent)
    {
        if (parent == null) return;

        Init(parent.genome.Replicate(), parent.mutator.Clone());
        mutator.Mutate(genome);
    }

    /// <summary>
    /// Steps the children
    /// <code>
    /// StateControllers
    /// </code>
    /// by dt
    /// </summary>
    /// <param name="dt">The timestep</param>
    public void UpdateBlocks(float dt)
    {
        var children = GetComponentsInChildren<StateController>();

        foreach (var child in children)
        {
            child.UpdateState(dt);
        }

        foreach (var child in children)
        {
            child.FinializeState();
        }
    }

    /// <summary>
    /// Iterates the children by iteration iterations.
    /// </summary>
    /// <param name="dt">The total time step.</param>
    /// <param name="iterations">The number of iterations.</param>
    public void Iterate(float dt, int iterations)
    {
        for (var i = iterations; i > 0; --i)
        {
            UpdateBlocks(dt / iterations);
        }
    }

    /// <summary>
    /// Running this command disables or distroys any children which have become
    /// disconnected from the organism. A block is considered disconnected when
    /// is it no longer joined to the organism by a face.
    /// </summary>
    /// <param name="distroyGameObject">
    /// If set to <c>true</c>, then games objects are destroyed instead of being disabled.
    /// </param>
    /// <param name="trim"></param>
    public void Trim(bool distroyGameObject = true, bool trim = true)
    {
        var visited = new HashSet<GameObject>();
        var toVisit = new Queue<GameObject>();

        toVisit.Enqueue(head.gameObject);

        while (toVisit.Count > 0)
        {
            var current = toVisit.Dequeue();
            visited.Add(current);

            var meta = current.GetComponentInParent<BlockMeta>();
            var neighbors = meta.Neighbors;

            foreach (var neighbor in neighbors)
            {
                if (!visited.Contains(neighbor))
                {
                    toVisit.Enqueue(neighbor);
                }
            }
        }

        // TODO: Does this function have ANY side effects when trim is false?
        if (trim)
        {
            foreach (var meta in GetComponentsInChildren<BlockMeta>())
            {
                if (!visited.Contains(meta.gameObject))
                {
                    if (distroyGameObject)
                    {
                        Destroy(meta.gameObject);
                    }
                    else
                    {
                        foreach (var stateController in meta.GetComponentsInChildren<StateController>())
                        {
                            Destroy(stateController);
                        }
                        Destroy(meta);
                    }
                }
            }
        }
    }

    public override void UpdateComponent(float dt, float[] state)
    {
        foreach (var face in GetComponentsInChildren<FaceController>())
        {
            if (face.target == null)
            {
                // Destroy(face.gameObject);
                face.GetComponent<Collider>().enabled = false;
            }
        }

        //TODO: make this tuneable
        Iterate(Time.deltaTime, 100);


        if (FixConnections) Trim();
        FixConnections = false;
    }

    public override void Pause(bool paused)
    {
        PauseRigidbody.Pause(this, paused);
    }

    public override void PrepInspection()
    {
        foreach (var face in GetComponentsInChildren<FaceController>())
        {
            face.GetComponent<Renderer>().enabled = true;
            Destroy(face);
        }
        foreach (var stateController in GetComponentsInChildren<StateController>()) Destroy(stateController);
        foreach (var block in GetComponentsInChildren<BlockMeta>())
        {
            block.gameObject.AddComponent<BlockMarker>();
            Destroy(block);
        }
        Destroy(this);

        gameObject.AddComponent<MossInspectable>();
    }

    public override IEnumerator Build()
    {
        head = genome.InitInterpreter(gameObject);
        yield return new WaitForFixedUpdate();

        // run until we get the end of the genome.
        while (!genome.InterpretStep(gameObject))
        {
            yield return new WaitForFixedUpdate();
        }
        yield return new WaitForFixedUpdate();
        Trim();
        yield return new WaitForFixedUpdate();

        //
        // Calculates the bounding box around the whole organism
        //

        // build bounds collider for the parent object
        var bc = gameObject.AddComponent<BoxCollider>();
        gameObject.AddComponent<Rigidbody>();
        GetComponent<Rigidbody>().isKinematic = true;

        var bounds = new Bounds {center = gameObject.transform.position};

        // make sure it conains all children of this organism
        foreach (var childCollider in GetComponentsInChildren<Collider>())
        {
            if (childCollider != bc)
                bounds.Encapsulate(childCollider.bounds);
        }

        bc.center = gameObject.transform.InverseTransformPoint(bounds.center);
        bc.size = bounds.size + new Vector3(0.1f, 0.1f, 0.1f);

        yield return new WaitForFixedUpdate();

        //
        // Lower the object to the ground, so that it is only just touching whatever we are using for terrain
        // Also, wakeup & enable all the components of the bot
        //

        RaycastHit info;
        GetComponent<Rigidbody>().SweepTest(Vector3.down, out info);

        transform.position += Vector3.down * info.distance + Vector3.up * 0.1f;

        // clean up
        Destroy(GetComponent<Collider>());
        Destroy(GetComponent<Rigidbody>());

        foreach (var face in GetComponentsInChildren<FaceController>())
        {
            foreach (var renderer in face.GetComponentsInChildren<Renderer>())
            {
                renderer.enabled = false;
            }
        }

        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
        yield return new WaitForFixedUpdate();
    }

    public override void StartRunning()
    {
        // enable physics
        foreach (var rb in GetComponentsInChildren<Rigidbody>())
        {
            rb.SetDensity(1f);
            rb.isKinematic = false;
            rb.WakeUp();
        }
    }
}