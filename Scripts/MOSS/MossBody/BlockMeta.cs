﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace MOSS.MossBody
{
    /// <summary>
    /// BlockMeta provides additional information and utilites about MOSS blocks to
    /// the organism.
    /// </summary>
    public class BlockMeta : MonoBehaviour
    {
        public int Rank;
        public string PrefabName;

        //        private bool pruneing = true;

        /// <summary>
        /// Collect all the neighbors of this block which are currently connected by
        /// a face.
        /// </summary>
        /// <value>The neighbors of this block</value>
        public List<GameObject> Neighbors
        {
            get
            {
                return (from fc in GetComponentsInChildren<FaceController>()
                        where fc.Connected
                        select fc.OtherBlock.gameObject).ToList();
            }
        }

        /// <summary>
        /// Gets the state of a particular type of state controller attached to this block.
        /// </summary>
        /// <returns>The state controller corisponding to the given type of state</returns>
        /// <param name="type">the type of state to retrieve.</param>
        public StateController GetState(StateController.Type type)
        {
            var cs = GetComponentsInChildren<StateController>();
            foreach (var s in cs)
            {
                if (s.type == type)
                {
                    return s;
                }
            }
            Debug.LogError("A " + gameObject.name + " Block is attempting to access a " + type +
                           " state, but this block has no such state. Maybe you forgot to attach it?");
            return null;
        }

        /// <summary>
        /// Set up the block initially. Will cause all rigid bodies to become
        /// kinematic and will disable rendering for all faces.
        /// </summary>
        private void Awake()
        {
            foreach (var rigidbody in GetComponentsInChildren<Rigidbody>())
            {
                rigidbody.isKinematic = true;
            }

            foreach (var face in GetComponentsInChildren<FaceController>())
            {
                face.gameObject.GetComponent<Renderer>().enabled = true;
            }
        }

        /// <summary>
        /// Attempts to handle broken joints, indicating that a face has broken.
        /// NOTE: may not actually achive anything.
        /// </summary>
        /// <param name="breakforce">unused, required from Unity API.</param>
        private void OnJointBreak(float breakforce)
        {
            foreach (var face in GetComponentsInChildren<FaceController>())
            {
                face.Disconnect(false);
            }
            GetComponentInParent<Moss>().Trim();
        }
    }
}