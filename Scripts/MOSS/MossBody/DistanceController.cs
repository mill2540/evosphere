﻿using UnityEngine;

namespace MOSS.MossBody
{
	public class DistanceController : MonoBehaviour {

		private StateController powerState;
		private StateController dataState;

		public float middleDistance = 50;

		// Use this for initialization
		private void Start ()
		{
			powerState = GetComponent<BlockMeta> ().GetState (StateController.Type.Power);
			dataState = GetComponent<BlockMeta> ().GetState (StateController.Type.Data);

			Debug.Log (powerState);
			Debug.Log (dataState);
		}

		// Update is called once per frame
		private void Update ()
		{
			var forward = transform.TransformDirection (Vector3.up);
			RaycastHit hit;

			if (Physics.Raycast (transform.position, forward, out hit)) {
//			Debug.Log (hit.distance);
//			Debug.DrawRay (transform.position, hit.point, Color.black, 10);
				dataState.InitalValue = (hit.distance - middleDistance) / middleDistance;
			}
		}
	}
}
