﻿using UnityEngine;

namespace MOSS.MossBody
{
    /// <summary>
    /// Controls Moss block faces. Mostly manages connecting and disconnecting.
    /// </summary>
    public class FaceController : MonoBehaviour
    {
        /// <summary>
        /// The type of information this face transmits.
        /// </summary>
        public StateController.Type Type;

        /// <summary>
        /// The face controller which this ///
        /// </summary>
        private StateController parent;

        /// <summary>
        /// The block which this face is a part of.
        /// </summary>
        public BlockMeta Block { get; private set; }

        /// <summary>
        /// The amount of information this face transmits.
        /// </summary>
        public float coef = 1;

        /// <summary>
        /// The face to which this face is connected.
        /// </summary>
        public FaceController target;

        /// <summary>
        /// The other block to which this face is connected.
        /// </summary>
        public BlockMeta OtherBlock { get; private set; }

        /// <summary>
        /// The joint connecting the two faces.
        /// </summary>
        private FixedJoint joint;

        /// <summary>
        /// If the face is connected to another face.
        /// </summary>
        public bool Connected { get; private set; }

        /// <summary>
        /// If it will accept connections.
        /// </summary>
        private bool allowConnections = true;


        /// <summary>
        /// Connect to the other face.
        /// </summary>
        /// <param name="other">The face to connect to</param>
        private void Connect(FaceController other)
        {
            // Only connect if we are not currently connected and we are accepting connections
            if (Connected || !allowConnections) return;
            
            
            // we are connected now
            Connected = true;

            // make sure that things are properly setup
            // TODO: this may not be nessesary
            Init();
            other.Init();

            // intialize the connections.
            OtherBlock = other.Block;
            target = other;

            // get the closest rigidbodies
            var rigidbody = GetComponentInParent<Rigidbody>();
            var otherRigidBody = other.GetComponentInParent<Rigidbody>();

            // If there is not already a joint between these two faces, make one
            if (joint == null && rigidbody != null && otherRigidBody != null)
            {
                joint = rigidbody.gameObject.AddComponent<FixedJoint>();
                // TODO: export to settings
                joint.breakForce = 400;
                joint.breakTorque = 600;
                joint.connectedBody = otherRigidBody;

                // inform the other face of the new connection
                other.joint = joint;
            }

            // tell the other to connect to us.
            other.Connect(this);
        }

        /// <summary>
        /// Gets the face being passed through this face from the connected block.
        /// </summary>
        /// <returns>
        /// The value of this face. takes
        /// <code>
        /// coef
        /// </code>
        /// into account
        /// </returns>
        public float GetValue()
        {
            if (target != null && target.parent != null)
            {
                return coef * target.parent.Value;
            }
            return 0;
        }

        /// <summary>
        /// Disconnects from the given block.
        /// </summary>
        /// <param name="fixConnections">
        /// If set to <c>true</c>, tell our parent block to remove disconnected blocks.
        /// </param>
        public void Disconnect(bool fixConnections)
        {
            if (Connected && joint == null)
            {
                Connected = false;
                allowConnections = false;
                OtherBlock = null;

                var tmp = target;
                target = null;
                if (tmp != null)
                {
                    tmp.Disconnect(fixConnections);
                }
            }
            if (fixConnections)
                GetComponentInParent<Moss>().FixConnections = true;
        }

        private void OnTriggerEnter(Collider otherCollider)
        {
            
            var other = otherCollider.GetComponent<FaceController>();
            if (other != null) Connect(other);
        }

        private void OnTriggerExit(Collider otherCollider)
        {
            if (otherCollider.GetComponent<FaceController>() == target)
            {
                Disconnect(true);
            }
        }

        private void GetParent()
        {
            if (parent != null || Type == StateController.Type.None) return;

            parent = GetComponentInParent<BlockMeta>().GetState(Type);
            parent.Faces.Add(this);
        }

        private void GetMeta()
        {
            if (Block == null)
            {
                Block = GetComponentInParent<BlockMeta>();
            }
        }

        private void Init()
        {
            GetMeta();
            GetParent();
        }

        private void Awake()
        {
            GetComponent<Collider>().enabled = false;
            GetComponent<Collider>().isTrigger = true;
            
            // Init ();
            Connected = false;

            switch (Type)
            {
                case StateController.Type.All:
                    gameObject.GetComponent<Renderer>().material.color = new Color32(0, 204, 255, 255);
                    break;
                case StateController.Type.Power:
                    gameObject.GetComponent<Renderer>().material.color = new Color32(0, 255, 153, 255);
                    break;
                case StateController.Type.None:
                    gameObject.GetComponent<Renderer>().material.color = Color.white;
                    break;
                case StateController.Type.Data:
                    gameObject.GetComponent<Renderer>().material.color = new Color32(204, 51, 0, 255);
                    break;
            }
        }

        private void Update()
        {
            //TODO: is this really nessesary?
            if (Connected && joint == null)
            {
                Disconnect(true);
            }
        }
    }
}