using System.Collections.Generic;
using System.Linq;
using Config;
using MOSS.Genetics;
using MOSS.Genetics.Instructions;
using MOSS.Genetics.Parsers;
using MOSS.MossBody;
using UnityEngine;

public static class MossDefaults
{
    /// The min (x, y, z) corrdinate that the genome can code for.
    public const float MinSize = -1;

    /// The max (x, y, z) corrdinate that the genome can code for.
    public const float MaxSize = 1;

    /// The start codon. Two of these means the start of a gene
    /// NOTE: using 42 here because I am pretty sure that the ByteGenome class is
    /// biased for markov brains, which use this.
    public const byte StartCodon1 = 42;

    /// This codon means we are in a block gene
    public const byte StartCodon2 = 200;

    public const int VectorWidth = 3;
    public const int BlockWidth = 1 + 2 * VectorWidth;
    public const int ContextEnterWidth = 2 + VectorWidth + VectorWidth;


    public static Vector3 SnapTranslation(float x, float y, float z, float oldMin, float oldMax)
    {
        return new Vector3(
            Mathf.Round(Remap(x, oldMin, oldMax, MinSize, MaxSize)),
            Mathf.Round(Remap(y, oldMin, oldMax, MinSize, MaxSize)),
            Mathf.Round(Remap(z, oldMin, oldMax, MinSize, MaxSize))
        );
    }

    /// <summary>
    /// Utility to remap a value from one range to another.
    /// </summary>
    /// <param name="value">Value.</param>
    /// <param name="oldMin">Old minimum.</param>
    /// <param name="oldMax">Old max.</param>
    /// <param name="newMin">New minimum.</param>
    /// <param name="newMax">New max.</param>
    public static float Remap(float value,
        float oldMin,
        float oldMax,
        float newMin,
        float newMax)
    {
        var oldRangeWidth = oldMax - oldMin;
        var newRangeWidth = newMax - newMin;
        return ((value - oldMin) / oldRangeWidth) * newRangeWidth + newMin;
    }


    public static Vector3 SnapTranslation(Vector3 orig, float oldMin, float oldMax)
    {
        return SnapTranslation(orig.x, orig.y, orig.z, oldMin, oldMax);
    }
}

public interface IMossGenomeFactory
{
    MossGenome Build();
}

[Register]
public class ConfigBlockSpec
{
    [Value] public string Prefab;
    [Value] public int? Limit;

    internal BlockSpec ToBlockSpec()
    {
        return new BlockSpec(this);
    }
}

internal class BlockSpec
{
    private readonly ConfigBlockSpec initial;

    public string Prefab
    {
        get { return initial.Prefab; }
    }

    public int? Limit;

    public BlockSpec(ConfigBlockSpec initial)
    {
        this.initial = initial;
        Limit = initial.Limit;
    }

    public BlockSpec Reset()
    {
        return new BlockSpec(initial);
    }
}

[Register]
public class MossGenomeFactory : IMossGenomeFactory
{
    [Value] public int Length = 512;

    [Value] public ConfigBlockSpec[] Prefabs =
    {
        new ConfigBlockSpec {Prefab = "BasePrefab", Limit = 4},
        new ConfigBlockSpec {Prefab = "WheelMotorPrefab", Limit = 2},
        new ConfigBlockSpec {Prefab = "ShortBracePrefab", Limit = 3},
        new ConfigBlockSpec {Prefab = "LongBracePrefab", Limit = 2}
    };

    [Value] public ConfigBlockSpec HeadPrefab = new ConfigBlockSpec {Prefab = "PowerblockPrefab"};
    [Value] public byte[] Start = {MossDefaults.StartCodon1, MossDefaults.StartCodon2};

    public MossGenome Build()
    {
        var genomeData = new byte[Length];

        for (var i = 0; i < Length; i++)
        {
            genomeData[i] = (byte) Random.Range(0, 256);
        }

        var step = Mathf.Max(MossDefaults.BlockWidth, MossDefaults.ContextEnterWidth) + 1;
        var minStep = step;
        var maxStep = step * 2;

        // Seed the genome with some intial data
        for (var i = 0; i < Length - Start.Length; i += Random.Range(minStep, maxStep))
        {
            if (!(Random.Range(0, 1) < 0.75)) continue;

            foreach (var codon in Start)
            {
                genomeData[i++] = codon;
            }
        }

        return new MossGenome(genomeData, Prefabs.Select(spec => spec.ToBlockSpec()).ToArray(),
            HeadPrefab.ToBlockSpec(), Start);
    }
}

/// <summary>
/// Moss body genome class. Responsible for storing and interpreting the
/// bytestring which makes up the genome.
/// </summary>
public class MossGenome
{
    public byte[] Genome;

    /// <summary>
    /// The from which the genome draws to build the genome.
    /// </summary>
    /// TODO: load this from the config.
    private readonly BlockSpec[] blockSpecs;

    private BlockSpec headSpec;

    private readonly byte[] start;

    private int rank;

    public int NextRank
    {
        get { return rank++; }
    }

    /// <summary>
    /// TODO: move to config The max number of times that contexts should be
    /// allowed to run.
    /// </summary>
    public int MaxPasses = 3;

    public Context Context { get; private set; }

    /// <summary>
    /// The children this genome has created.
    /// </summary>
    public List<BlockMeta> Children { private set; get; }


    internal MossGenome(byte[] genome, BlockSpec[] blockSpecs, BlockSpec headSpec, byte[] start)
    {
        Genome = genome;
        this.blockSpecs = blockSpecs;
        this.headSpec = headSpec;
        this.start = start;
    }

    /// <summary>
    /// Utility to snap values in the range 0-256 to one of {0, 90, 180, 270}
    /// </summary>
    /// <returns>The snaped Euler rotation vector.</returns>
    /// <param name="x">The x coordinate.</param>
    /// <param name="y">The y coordinate.</param>
    /// <param name="z">The z coordinate.</param>
    public static Vector3 SnapRotation(byte[] b)
    {
        return new Vector3(
            (Mathf.Round(b[0] / 64f) * 90) % 360,
            (Mathf.Round(b[1] / 64f) * 90) % 360,
            (Mathf.Round(b[2] / 64f) * 90) % 360
        );
    }

    public GameObject InitInterpreter(GameObject parent)
    {
        Children = new List<BlockMeta>();

        var parser = new Pattern<byte, ContextInstruction>(start.Length)
            .AddCase(start, new Range<byte, ContextInstruction>().AddCase(16, new BlockParser())
                .AddCase(0, new ContextParser(20, MaxPasses)));

        using (var genomeEnumerable = (Genome as IEnumerable<byte>).GetEnumerator())
        {
            var sequence = genomeEnumerable.ParseToList(parser, parser);

            Context = new Context(1, Vector3.zero, Vector3.zero, sequence);

            return AddChild(parent, headSpec);
        }
    }

    internal BlockSpec GetBlockSpec(byte blockID)
    {
        return blockSpecs[blockID % blockSpecs.Length];
    }

    public GameObject AddChild(GameObject parent, byte blockID, Vector3 offset = new Vector3(),
        Quaternion rotation = new Quaternion(), bool check = false)
    {
        var spec = GetBlockSpec(blockID);
        return AddChild(parent, spec, offset, rotation, check);
    }

    internal GameObject AddChild(GameObject parent,
        BlockSpec spec,
        Vector3 offset = new Vector3(),
        Quaternion rotation = new Quaternion(),
        bool check = false)
    {
        if (spec.Limit != null && spec.Limit <= 0) return null;

        var blockObject = Object.Instantiate(Resources.Load(spec.Prefab)) as GameObject;

        // setup the block
        blockObject.transform.parent = parent.transform;
        blockObject.transform.localPosition = offset;
        blockObject.transform.localRotation = rotation;

        if (check)
        {
            // CHECK FOR COLLISIONS
            //
            // make this block just a bit smaller, so that it will not register
            // collisions with blocks that are just touching it.
            // TODO: there might be a better way to handle this?
            var scale = blockObject.transform.localScale;
            blockObject.transform.localScale = new Vector3(0.95f, 0.95f, 0.95f);

            Children.RemoveAll(b => b == null);

            // Check all the other blocks in this organism
            if ((from other in Children
                where other.gameObject != blockObject
                from otherCollider in other.GetComponentsInChildren<Collider>()
                where otherCollider.GetComponent<FaceController>() == null
                from ourCollider in blockObject.GetComponentsInChildren<Collider>()
                where ourCollider.GetComponent<FaceController>() == null
                where (ourCollider.enabled && otherCollider.enabled) &&
                      (ourCollider.bounds.Intersects(otherCollider.bounds) ||
                       otherCollider.bounds.Intersects(ourCollider.bounds))
                select otherCollider).Any())
            {
                Object.Destroy(blockObject);
                return null;
            }
            // reset the scale
            blockObject.transform.localScale = scale;
        }
        if (spec.Limit != null) --spec.Limit;

        var block = blockObject.GetComponent<BlockMeta>();
        block.Rank = NextRank;
        block.PrefabName = spec.Prefab;
        Children.Add(block);

        foreach (var face in block.GetComponentsInChildren<FaceController>())
        {
            face.GetComponent<Collider>().enabled = true;
        }

        return blockObject;
    }

    private void RemoveChild(GameObject child)
    {
        var block = child.GetComponent<BlockMeta>();
        if (block == null) return;
        Children.Remove(block);
    }

    /// <summary>
    /// Creates a new body attached to the given parent.
    /// </summary>
    /// Note that the generated body will be unprocessed, so more post processing
    /// will need to happen in the organism.
    /// <param name="parent">The parent object for this body.</param>
    /// <param name="direction">The direction to interpret in (so we can have reversible genomes)</param>
    public bool InterpretStep(GameObject parent,
        Instruction<byte>.Direction direction = Instruction<byte>.Direction.Forward)
    {
        return Context.Run(direction, this, parent);
    }

    public MossGenome Replicate()
    {
        return new MossGenome(Genome, blockSpecs.Select(spec => spec.Reset()).ToArray(), headSpec.Reset(), start);
    }
}