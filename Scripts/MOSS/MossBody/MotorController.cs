﻿using UnityEngine;

namespace MOSS.MossBody
{
	public class MotorController : MonoBehaviour
	{

		private StateController powerState;
		private StateController dataState;
		private HingeJoint motorJoint;

		// Use this for initialization
		private void Start ()
		{
			powerState = GetComponent<BlockMeta> ().GetState (StateController.Type.Power);
			dataState = GetComponent<BlockMeta> ().GetState (StateController.Type.Data);

			motorJoint = GetComponentInChildren<HingeJoint> ();
		}
	
		// Update is called once per frame
		private void Update ()
		{
			if (powerState != null && dataState != null) {
			
				var power = powerState.Value;

				var motor = motorJoint.motor;
				motor.force = 1000;
				motorJoint.useMotor = true;

				if (power > 0) {
					motor.targetVelocity = 360 * dataState.Value;
				} else {
					motor.targetVelocity = 0;
				}
				motorJoint.motor = motor;
			} else {
				var motor = motorJoint.motor;
				motor.force = 0;
				motor.targetVelocity = 0;
				motorJoint.useMotor = false;
			}
		}
	}
}
