﻿using System.Collections.Generic;
using UnityEngine;

namespace MOSS.MossBody
{
	public class StateController : MonoBehaviour
	{
		public enum Type
		{
			Power,
			Data,
			All,
			None
		}


		public float InitalValue = 0;
		public bool IsFixed = false;

		private float lastValue;
		public float value;
		private float dvalue;

		public Type type;
		public float Value { get { return lastValue; } set { this.value = value; }}

		public List<FaceController> Faces = new List<FaceController>();

		private void Start ()
		{
			lastValue = value = InitalValue;
			dvalue = InitalValue;
		}

		public void FinializeState() {
			lastValue = value;
			dvalue = 0;
		}

		public void UpdateState (float dt)
		{
			// Don't do anything if the values are fixed
			if (IsFixed) return;
			
			var anyConnected = false;
			foreach (var face in Faces) {
				if (face.Connected && Mathf.Abs(face.GetValue()) > Mathf.Abs(lastValue)) {
					dvalue += face.GetValue () - lastValue;
					anyConnected = true;
				}
			}

			if(anyConnected) value += dvalue * dt;
			else value = 0;
		}
		
		/// <summary>
		/// Describes the rules which define what types of faces can be connected
		/// </summary>
		/// <param name="other">The target to test against</param>
		/// <returns>True if the two types of data are compatable</returns>
		public bool CanOutput (StateController other)
		{
			if (type == Type.None || other == null || other.type == Type.None)
				return false;

			if (other.Equals (this) || other.IsFixed)
				return false;

			if (type == Type.All || other.type == Type.All)
				return true;

			if (type == Type.Power)
				return (other.type == Type.Power || other.type == Type.Data);

			return type == other.type;
		}

	}
}
