﻿using System.Linq;
using GUI;
using UnityEngine;

public class BlockMarker : MonoBehaviour
{
    private Vector3? initialPosition;

    public Vector3 Offset
    {
        set
        {
            if (initialPosition == null)
            {
                initialPosition = transform.localPosition;
            }
            transform.localPosition = initialPosition.Value + value;
        }
    }
}

public class MossInspectable : Inspectable
{
    private const float DefaultScale = 1;
    private const float ExplodedScale = 0.8f;
    private const float ScaleRate = 0.1f;

    private Vector3 initialGlobalScale;
    private float targetScale = DefaultScale;
    private float currentScale = DefaultScale;
    
    private const string ExplodedView = "Exploded";

    private void Awake()
    {
        initialGlobalScale = transform.localScale;
    }

    public override string Name
    {
        get { return "Moss"; }
    }

    public override string[] Views
    {
        get { return new[] {ExplodedView}; }
    }

    private void EnterExplode()
    {
        targetScale = ExplodedScale;
    }

    protected override void EnterView(string view)
    {
        if (view == ExplodedView) EnterExplode();
    }

    public override void Reset()
    {
        targetScale = DefaultScale;
    }

    public void Update()
    {
        var blocks = GetComponentsInChildren<BlockMarker>();
        var center = blocks.Aggregate(Vector3.zero, (current, block) => current + block.transform.localPosition);

        currentScale += (targetScale - currentScale) * ScaleRate;
        foreach (var block in GetComponentsInChildren<BlockMarker>())
        {
            var offset = (center - block.transform.localPosition) * (currentScale - 1);

            block.Offset = offset;
        }
//        transform.localScale = initialGlobalScale / currentScale;
    }
}