using System.Collections;
using UnityEngine;

public static class NeuralNetworkGenomeFactory
{
    public static NeuralNetworkGenome Build(int inputs, int[] layers, int outputs)
    {
        int length;
        if (layers.Length != 0)
        {
            length = (inputs + 1) * layers[0] + (layers[layers.Length - 1] + 1) * outputs;
        }
        else
        {
            length = (inputs + 1) * outputs;
        }

        for (var i = 1; i < layers.Length; ++i)
        {
            length += (layers[i - 1] + 1) * layers[i];
        }

        var weights = new float[length];
        for (var i = 0; i < length; ++i)
        {
            weights[i] = (2 * UnityEngine.Random.value - 1);
        }

        return new NeuralNetworkGenome(weights);
    }
}

public interface INeuralNetworkGenomeMutator
{
    void Mutate(NeuralNetworkGenome genome);

    INeuralNetworkGenomeMutator Clone();
}

public class DefaultNeuralNetworkMutator : INeuralNetworkGenomeMutator
{
    private float pChangeWeight;

    public DefaultNeuralNetworkMutator(float pChangeWeight)
    {
        this.pChangeWeight = pChangeWeight;
    }

    public void Mutate(NeuralNetworkGenome genome)
    {
        for (var i = 0; i < genome.weights.Length; ++i)
        {
            if (UnityEngine.Random.value < pChangeWeight)
            {
                genome.weights[i] = (2 * UnityEngine.Random.value - 1);
            }
        }
    }

    public INeuralNetworkGenomeMutator Clone()
    {
        return this;
    }
}

public interface INeuralNetworkGenomeMutatorFactory
{
    INeuralNetworkGenomeMutator Build();
}

[Register]
[Doc("Simple mutator for acting on neural network brains")]
public class DefaultNeuralNetworkMutatorFactory : INeuralNetworkGenomeMutatorFactory
{
    [Config.Value] [Doc("The probability that a given weight in changed int the brain")] public float pChangeWeight;

    public INeuralNetworkGenomeMutator Build()
    {
        return new DefaultNeuralNetworkMutator(pChangeWeight);
    }
}

public class NeuralNetworkGenome
{
    public float[] weights;

    public NeuralNetworkGenome(float[] weights)
    {
        this.weights = weights;
    }

    public NeuralNetworkGenome Replicate()
    {
        return new NeuralNetworkGenome((float[])this.weights.Clone());
    }
}

// [Register]
// [Doc("The probability that a given weight in changed int the brain")]
// public class NeuralNetworkBrainFactory : IComponentFactory
// {
//     [Config.Value]
//     [Doc(
//         "The inputs to the brain, or the first (visible) layer. Each element in the array should be an address in the organisms state buffer.")]
//     public int[] inputs;

//     [Config.Value]
//     [Doc("Specifies the topology of the hidden layers in the brain.")]
//     public int[] hidden;

//     [Config.Value]
//     [Doc(
//         "The outputs from the brain, or the last (visible) layer. Each element in the array should be an address in the organisms state buffer.")]
//     public int[] outputs;

//     [Config.Value]
//     [Doc("The mutator being used to mutate this brain each generation")]
//     public INeuralNetworkGenomeMutatorFactory mutator;

//     public AbstractOrganismComponent Build(UnityEngine.GameObject target)
//     {
//         var nnb = target.AddComponent<NeuralNetworkBrain>();

//         nnb.Init(inputs, hidden, outputs, NeuralNetworkGenomeFactory.Build(inputs.Length, hidden, outputs.Length),
//             mutator.Build());

//         return nnb;
//     }
// }

[Register]
[Doc("The probability that a given weight in changed int the brain")]
public class NeuralNetworkBrain : OrganismComponent<NeuralNetworkBrain>
{
    private int[] inputs;

    private int[] hidden;

    private int[] outputs;

    private NeuralNetworkGenome genome;

    private INeuralNetworkGenomeMutator mutator;

    private float[][] states;

    [Deserializer]
    public void Init(int[] inputs, int[] hidden, int[] outputs, INeuralNetworkGenomeMutatorFactory mutator)
    {
        Init(inputs, hidden, outputs, NeuralNetworkGenomeFactory.Build(inputs.Length, hidden, outputs.Length), mutator.Build());
    }

    public void Init(int[] inputs, int[] hidden, int[] outputs, NeuralNetworkGenome genome, INeuralNetworkGenomeMutator mutator)
    {
        this.inputs = inputs;
        this.hidden = hidden;
        this.outputs = outputs;

        this.genome = genome;
        this.mutator = mutator;

        this.states = new float[2 + hidden.Length][];
        this.states[0] = new float[inputs.Length];
        this.states[this.states.Length - 1] = new float[outputs.Length];

        for (var i = 0; i < hidden.Length; ++i)
        {
            this.states[i + 1] = new float[hidden[i]];
        }
    }

    public override void UpdateComponent(float dt, float[] state)
    {
        for (var i = 0; i < inputs.Length; ++i)
        {
            states[0][i] = state[inputs[i]];
        }

        var weight = 0;
        for (var layer = 1; layer < states.Length; ++layer)
        {
            for (var i = 0; i < states[layer].Length; ++i)
            {
                var input = genome.weights[weight++];
                for (var j = 0; j < states[layer - 1].Length; ++j)
                {
                    input += states[layer - 1][j] * genome.weights[weight++];
                }

                states[layer][i] = ActivationFunction(input);
            }
        }

        for (var i = 0; i < outputs.Length; ++i)
        {
            state[outputs[i]] = states[states.Length - 1][i];
        }
    }

    public override void Pause(bool paused)
    {
    }

    public override void PrepInspection()
    {
        Destroy(this);
    }

    private static float ActivationFunction(float input)
    {
        return 1 - 2 / (1 + Mathf.Exp(100 * input));
    }

    public override void Inherit(NeuralNetworkBrain parent)
    {
        if (parent == null)
        {
            // Assume that the factory set us up correctly
            return;
        }

        var genome = parent.genome.Replicate();
        var mutator = parent.mutator.Clone();
        mutator.Mutate(genome);

        Init(parent.inputs, parent.hidden, parent.outputs, genome, mutator);
    }
}