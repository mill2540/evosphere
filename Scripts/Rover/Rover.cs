using System.Collections;
using UnityEngine;

[Register]
[Doc("A simple two wheeled rover")]
public class Rover : OrganismComponent<Rover>
{
    public int LeftDrive = 0;
    public int RightDrive = 1;
    public int LeftSpeed = 2;
    public int RightSpeed = 3;
    private RoverController rover;

    [Deserializer]
    public void Load(
        [Doc("Which index in the state buffer to take the target speed of the left motor")]
        int LeftDrive = 0,

        [Doc("Which index in the state buffer to take the target speed of the right motor")]
        int RightDrive = 1,

        [Doc("Which index in the state buffer to send the actual speed of the left motor to")]
        int LeftSpeed = 2,

        [Doc("Which index in the state buffer to send the actual speed of the right motor to")]
        int RightSpeed = 3)
    {
        this.LeftDrive = LeftDrive;
        this.RightDrive = RightDrive;
        this.LeftSpeed = LeftSpeed;
        this.RightSpeed = RightSpeed;
    }

    public override IEnumerator Build()
    {
        rover = Instantiate(Resources.Load<RoverController>("RoverPrefab"));
        rover.transform.SetParent(transform, false);

        yield break;
    }

    public override void Kill()
    {
        // Nothing to do here
    }

    public override void UpdateComponent(float dt, float[] state)
    {
        rover.LeftDrive = state[LeftDrive];
        rover.RightDrive = state[RightDrive];

        state[LeftSpeed] = rover.LeftSpeed;
        state[RightSpeed] = rover.RightSpeed;
    }

    public override void Pause(bool paused)
    {
        PauseRigidbody.Pause(this, paused);
    }

    public override void PrepInspection()
    {
        Destroy(this);
    }

    public override void Inherit(Rover parent)
    {
        // Also nothing do do
    }
}