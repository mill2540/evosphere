﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoverBumper : MonoBehaviour
{
    [SerializeField]
    private Color whenActive;
    
    [SerializeField]
    private Color whenInactive;

    private void Awake()
    {
        GetComponent<Renderer>().material.color = whenActive;
    }

    private void OnTriggerEnter(Collider other)
    {
        GetComponent<Renderer>().material.color = whenActive;
    }
    
    private void OnTriggerExit(Collider other)
    {
        GetComponent<Renderer>().material.color = whenInactive;
    }
}