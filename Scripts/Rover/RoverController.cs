﻿using UnityEngine;

public class RoverController : MonoBehaviour
{
    [SerializeField] private float targetVelocity;

    [SerializeField] private float force;

    [SerializeField] private HingeJoint left;

    [SerializeField] private HingeJoint right;

    public float LeftDrive
    {
        get { return left.motor.targetVelocity / targetVelocity; }

        set
        {
            var motor = left.motor;

            motor.targetVelocity = targetVelocity * Constrain(value);

            left.motor = motor;
        }
    }

    public float LeftSpeed
    {
        get { return Constrain(left.velocity / targetVelocity); }
    }


    public float RightDrive
    {
        get { return right.motor.targetVelocity / targetVelocity; }

        set
        {
            var motor = right.motor;

            motor.targetVelocity = targetVelocity * Constrain(value);

            right.motor = motor;
        }
    }
    
    public float RightSpeed
    {
        get { return Constrain(right.velocity / targetVelocity); }
    }


    private static float Constrain(float value)
    {
        return Mathf.Min(Mathf.Max(value, -1), 1);
    }

    private void InitMotor(HingeJoint joint)
    {
        var motor = joint.motor;
        motor.force = force;
        joint.motor = motor;
        joint.useMotor = true;
    }

    private void Awake()
    {
        InitMotor(left);
        InitMotor(right);
    }
}