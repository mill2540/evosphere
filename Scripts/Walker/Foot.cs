﻿using UnityEngine;

namespace Walker
{

    public class Foot : MonoBehaviour
    {
        public bool Contact { get; private set; }
    
        private void OnCollisionEnter(Collision other)
        {
            Contact = true;
        }

        private void OnCollisionExit(Collision other)
        {
            Contact = false;
        }
    }
}