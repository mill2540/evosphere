﻿using System.Collections.Generic;
using UnityEngine;

namespace Walker
{
    [Register]
    public class LegConnections
    {
        [Config.Value] public readonly int UpperTension;
        [Config.Value] public readonly int LowerTension;
        [Config.Value] public readonly int Foot;
    }


    public class Leg : MonoBehaviour
    {
        private LegConnections connections;

        public void Init(LegConnections connections)
        {
            this.connections = connections;
        }
        
        [SerializeField] private HingeJoint upperTensionHinge;
        [SerializeField] private HingeJoint lowerTensionHinge;
        [SerializeField] private Foot foot;

        public void UpdateLeg(IList<float> state)
        {
            var upperTension = Mathf.Max(Mathf.Min(state[connections.UpperTension], 1), -1) * 45;
            var lowerTension = Mathf.Max(Mathf.Min(state[connections.LowerTension], 1), -1) * 45;

            var upperSpring = upperTensionHinge.spring;
            upperSpring.targetPosition = upperTension;
            upperTensionHinge.spring = upperSpring;
            
            var lowerSpring = lowerTensionHinge.spring;
            lowerSpring.targetPosition = lowerTension;
            lowerTensionHinge.spring = lowerSpring;

            state[connections.Foot] = foot.Contact ? 1 : -1;
        }
    }
}