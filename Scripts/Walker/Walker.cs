﻿using System.Collections;
using UnityEngine;


namespace Walker
{
    [Register]
    public class Walker : OrganismComponent<Walker>
    {
        private WalkerController controller;
        private LegConnections frontRight, frontLeft, backRight, backLeft;

        public void Init(
            [Config.Value]
            LegConnections frontRight,
            [Config.Value]
            LegConnections frontLeft,
            [Config.Value]
            LegConnections backRight,
            [Config.Value]
            LegConnections backLeft)
        {
            this.frontRight = frontRight;
            this.frontLeft = frontLeft;
            this.backRight = backRight;
            this.backLeft = backLeft;
        }

        public override IEnumerator Build()
        {
            var walkerObject = (GameObject)Object.Instantiate(Resources.Load("Walker"));
            controller = walkerObject.GetComponent<WalkerController>();
            controller.Init(frontRight, frontLeft, backRight, backLeft);

            walkerObject.transform.SetParent(transform, false);

            return base.Build();
        }

        public override void UpdateComponent(float dt, float[] state)
        {
            controller.UpdateComponent(dt, state);
        }

        public override void Pause(bool paused)
        {
            PauseRigidbody.Pause(this, paused);
        }

        public override void PrepInspection()
        {
            Destroy(this);
        }

        public override void Inherit(Walker parent)
        {
            if (parent != null)
            {
                Init(parent.frontRight, parent.frontLeft, parent.backRight, parent.backLeft);
            }
        }
    }
}