﻿using UnityEngine;

namespace Walker
{
    public class WalkerController : MonoBehaviour
    {
        [SerializeField] public Leg FrontRight;
        [SerializeField] public Leg FrontLeft;
        [SerializeField] public Leg BackRight;
        [SerializeField] public Leg BackLeft;

        public void Init(LegConnections frontRight, LegConnections frontLeft,
            LegConnections backRight, LegConnections backLeft)
        {
            FrontRight.Init(frontRight);
            FrontLeft.Init(frontLeft);
            BackRight.Init(backRight);
            BackLeft.Init(backLeft);
        }


        public void UpdateComponent(float dt, float[] state)
        {
            FrontRight.UpdateLeg(state);
            FrontLeft.UpdateLeg(state);
            BackRight.UpdateLeg(state);
            BackLeft.UpdateLeg(state);
        }
    }
}