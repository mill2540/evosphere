import { Export, Hierarchy, LazyHierarchy } from "./Hierarchy";
import {
    ArrayInfo,
    ConstructableInfo,
    Constructor,
    ConstructorParameter,
    EnumInfo,
    RegisteredInfo,
    RegisteredMember,
    Type,
} from "./Type";
import {
    ArrayValue,
    ConstructedValue,
    EnumValue,
    RegisteredValue,
    SerialArrayValue,
    SerialConstructedValue,
    SerialEnumValue,
    SerialRegisteredValue,
    SerialStringValue,
    SerialValue,
    StringValue,
    Value,
} from "./Value";

export interface IParameter {
    name: string;
    type: string;
    optional: boolean;
}

export interface Member {
    type: string;
    documentation: string;
}

export interface IUntiyTypeMeta {
    name: string;
    documentation: string;
    shortName: string;
    deserializerTarget: string;
    isStringable: boolean;
    isConstructable: boolean;
    isRegistered: boolean;
    isArray: boolean;
    isMap: boolean;
    isEnum: boolean;
    accepts: string[];
    arrayInfo?: { element: string };
    mapInfo?: { key: string; value: string };
    enumInfo?: { variants: string[] };
    registeredInfo?: { members: { [key: string]: Member } };
    constructedInfo?: { constructors: IParameter[][] };
}

export type OnBackendConnectedCallback = (backend: Backend) => void;

export interface UnityCallResult {
    Name: string;
    Value: any;
    Error: any;
}

export interface UnityResponse {
    Id: string;
    Responses: UnityCallResult[];
}

export interface QueryReplyOk {
    kind: "ok";
    value: any;
    src: string;
}

export interface QueryReplyError {
    kind: "error";
    error: any;
    src: string;
}

export type QueryReply = QueryReplyOk | QueryReplyError;
export const QueryReply = {
    ok(src: string, value: any): QueryReplyOk {
        return { src, value, kind: "ok" };
    },
    err(src: string, error: any): QueryReplyError {
        return { src, error, kind: "error" };
    },
    unwrap(reply: QueryReply) {
        switch (reply.kind) {
            case "error":
                throw new Error(`${reply.src}: ${reply.error}`);
            case "ok":
                return reply.value;
        }
    },
    fromCallResult(call: UnityCallResult) {
        if (call.Error) {
            return QueryReply.err(call.Name, call.Error);
        } else {
            return QueryReply.ok(call.Name, call.Value);
        }
    },
};

export type UnityEvent = (id: string) => void;

export interface IConnection {
    callTarget: string;
}

export abstract class Backend {
    // The type cache. This is an implementation detail, and holds info about
    // types that have already been requested from the Unity side. const types:
    // {[key: string]: Type} = {};
    private types: { [key: string]: Type } = {};
    private connection: IConnection | undefined;

    public connect(): Promise<IConnection> {
        return new Promise<IConnection>((resolve, reject) => {
            if (this.connection === undefined) {
                this.connectImpl()
                    .then((connection) => {
                        this.connection = connection;
                        resolve(connection);
                    })
                    .catch(reject);
            } else {
                resolve(this.connection);
            }
        });
    }

    public query(query: string, args: any[]): Promise<QueryReply[]> {
        return this.connect().then(() => this.queryImpl(query, args));
    }

    public subscribe(target: string, listener: UnityEvent): Promise<void> {
        return this.connect().then(() => this.subscribeImpl(target, listener));
    }

    public async callObject(hierarchy: Hierarchy | string, exported: Export) {}

    public async call(name: string, args: any[]): Promise<QueryReply> {
        const { callTarget } = await this.connect();
        const reply = await this.query(`${callTarget}.${name}`, args);
        return reply[0];
    }

    public async getNativeTypeName(typeName: string): Promise<string> {
        return this.call("GetNativeTypeName", [typeName]).then(
            QueryReply.unwrap
        );
    }

    public async getTypeInfo(typeName: string): Promise<IUntiyTypeMeta> {
        return this.call("GetTypeInfo", [typeName]).then(QueryReply.unwrap);
    }

    public async jsonToYaml(value: Value): Promise<string> {
        return this.call("JsonToYaml", [
            JSON.stringify(value.serialize()),
        ]).then(QueryReply.unwrap);
    }

    public async yamlToJson(yaml: string): Promise<Value> {
        const serialValue: SerialValue = await this.call("YamlToJson", [
            yaml,
        ]).then(QueryReply.unwrap);
        return this.deserializeSerialValue(serialValue);
    }

    public async loadExperimentFromJson(value: Value): Promise<string> {
        return await this.call("LoadExperimentFromJson", [
            JSON.stringify(value.serialize()),
        ]).then(QueryReply.unwrap);
    }

    public getHierarchy(target: string) {
        return new LazyHierarchy(this, target);
    }

    /**
     * This is the primary interface to the C# type system. Note that it is
     * asyncronous, and the first time it is called for a given type,
     * it may take an arbrtary amount of time to return a value.
     * @param  {string}        typeName The name of the type that we want
     * information about. Note that this is *not* the human readable type, but is
     * the assembly dependent full name of the type in C#.
     * @return {Promise<Type>}          A Promise that the requested type will be
     * returned
     */
    public async getType(typeName: string): Promise<Type> {
        // If the type is in the type cache, then don't send a request to unity
        if (typeName in this.types) {
            return this.types[typeName];
        }

        // This is where the talking to unity happens. We are given the same
        // representation of the type as Unity uses.
        const metaObj = await this.getTypeInfo(typeName);

        let kind: Type["kind"] = "interface";
        if (metaObj.isArray) {
            kind = "array";
        }
        if (metaObj.isConstructable) {
            kind = "constructor";
        }
        if (metaObj.isEnum) {
            kind = "enum";
        }
        if (metaObj.isRegistered) {
            kind = "registered";
        }
        if (metaObj.isStringable) {
            kind = "string";
        }
        if (metaObj.isMap) {
            kind = "map";
        }

        // Construct the type object using the type meta we got from unity
        const type = new Type(
            typeName,
            metaObj.documentation,
            metaObj.shortName,
            kind
        );

        // Insert the type into the cache. Note that this MUST happen before
        // loading the information about the children of this type, as that could
        // cause an infinite loop if the type is cyclic
        // (eg, struct Foo { Foo child; })
        this.types[typeName] = type;

        // Switches handle translating the rest of the type. Mostly complex because
        // children types have to be loaded recursively.
        if (metaObj.isEnum) {
            type.info = new EnumInfo(type.name, metaObj.enumInfo.variants);
        } else if (metaObj.arrayInfo) {
            type.info = new ArrayInfo(
                type.name,
                await this.getType(metaObj.arrayInfo.element)
            );
        } else if (metaObj.registeredInfo) {
            // Loads all the children of this object.
            const members: { [key: string]: RegisteredMember } = {};
            for (const memberName of Object.keys(
                metaObj.registeredInfo.members
            )) {
                const member = metaObj.registeredInfo.members[memberName];
                members[memberName] = new RegisteredMember(
                    await this.getType(member.type),
                    member.documentation
                );
            }
            type.info = new RegisteredInfo(type.name, members);
        } else if (metaObj.isConstructable) {
            // Loads the types of the parameters of each available constructor
            const constructors: Constructor[] = [];

            for (const [
                index,
                constructor,
            ] of metaObj.constructedInfo.constructors.entries()) {
                const parameters: ConstructorParameter[] = [];

                for (const parameter of constructor) {
                    parameters.push(
                        new ConstructorParameter(
                            parameter.name,
                            await this.getType(parameter.type),
                            parameter.optional
                        )
                    );
                }

                constructors.push(new Constructor(index, parameters));
            }
            type.info = new ConstructableInfo(type.name, constructors);
        }

        type.accepts = await Promise.all(
            metaObj.accepts.map((t) => this.getType(t))
        );

        type.deserializerTarget = await this.getType(
            metaObj.deserializerTarget
        );

        return type;
    }

    // Converts a SerialValue into a Value
    public async deserializeSerialValue(value: SerialValue): Promise<Value> {
        // Load the type using its name
        const type = await this.getType(value.type);

        switch (type.kind) {
            case "array":
                const arrayValue = value as SerialArrayValue;
                const values = await Promise.all(
                    arrayValue.values.map((serial) =>
                        this.deserializeSerialValue(serial)
                    )
                );
                return new ArrayValue(type, values);
            case "constructor":
                const info = type.info as ConstructableInfo;

                const constructorValue = value as SerialConstructedValue;
                const parameters: Value[] = await Promise.all(
                    constructorValue.parameters.map((serial) =>
                        this.deserializeSerialValue(serial)
                    )
                );

                const length = parameters.length;
                const selectedConstructor = info.constructors.find(
                    (constructor) => {
                        return constructor.parameters.every(
                            (parameter, i, arr) => {
                                return (
                                    parameter.optional ||
                                    (i < length &&
                                        parameter.type.isAssignableFrom(
                                            parameters[i].type
                                        ))
                                );
                            }
                        );
                    }
                );

                return new ConstructedValue(
                    type,
                    selectedConstructor,
                    parameters
                );
            case "enum":
                const enumValue = value as SerialEnumValue;
                return new EnumValue(type, enumValue.variant);
            case "registered":
                const registeredValue = value as SerialRegisteredValue;
                const members: { [key: string]: Value } = {};

                for (const member of Object.keys(registeredValue.members)) {
                    members[member] = await this.deserializeSerialValue(
                        registeredValue.members[member]
                    );
                }

                return new RegisteredValue(type, members);
            case "string":
                const stringValue = value as SerialStringValue;
                return new StringValue(type, stringValue.value);
        }
    }

    protected abstract connectImpl(): Promise<IConnection>;

    protected abstract queryImpl(
        query: string,
        args: any[]
    ): Promise<QueryReply[]>;

    protected abstract subscribeImpl(
        target: string,
        listener: UnityEvent
    ): Promise<void>;
}
