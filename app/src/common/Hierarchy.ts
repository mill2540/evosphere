import { Backend, QueryReply } from "./Backend";
import { Lazy } from "./Lazy";
import { LazyType, Type } from "./Type";
import { Value } from "./Value";

export class Export {
    constructor(
        private readonly backend: Backend,
        private readonly hierarchyId: string,
        public readonly name: string,
        public readonly parameters: LazyType[],
        public readonly returnType: LazyType
    ) {}

    public async call(...args: Value[]) {
        console.log(args);
        const parameters = await Promise.all(
            this.parameters.map((parameter) => parameter.value())
        );
        console.log(parameters);

        for (let i = 0; i < Math.max(args.length, parameters.length); i++) {
            if (i >= args.length) {
                throw new Error("Too many arguments");
            }
            if (i >= parameters.length) {
                throw new Error("Too few arguments");
            }

            const parameter = parameters[i];
            const arg = args[i];

            if (!parameter.isAssignableFrom(arg.type)) {
                throw new Error(
                    `Cannot use argument of type ${
                        arg.type.name
                    } for argument of type ${parameter.name}`
                );
            }
        }

        return this.backend
            .query(
                `${this.hierarchyId}.${this.name}`,
                args.map((arg) => arg.serialize())
            )
            .then((responses) => {
                console.log(responses);
                return responses[0];
            })
            .then(QueryReply.unwrap);
    }
}

export class Hierarchy {
    constructor(
        private readonly backend: Backend,
        public readonly id: string,
        public readonly name: string,
        public readonly type: Type,
        public readonly children: ReadonlyArray<LazyHierarchy>,
        public readonly exports: ReadonlyArray<Export>
    ) {}
}

export class LazyHierarchy extends Lazy<Hierarchy> {
    constructor(private backend: Backend, public readonly target: string) {
        super();
    }

    protected async get(): Promise<Hierarchy> {
        const { target, backend } = this;

        const replies = await backend.query(`${target}.GetHierarchy`, []);

        if (replies.length !== 1) {
            throw new Error(`Error getting hierarcy for ${target}`);
        }
        const untypedHierarchy = QueryReply.unwrap(replies[0]);

        const children = (untypedHierarchy.Children as string[]).map(
            (child) => {
                return new LazyHierarchy(backend, child);
            }
        );
        const exports = untypedHierarchy.Exports.map((exported: any) => {
            return new Export(
                backend,
                untypedHierarchy.Id,
                exported.Name,
                exported.Parameters.map(
                    (parameter: any) => new LazyType(backend, parameter)
                ),
                new LazyType(backend, exported.ReturnType)
            );
        });

        return new Hierarchy(
            this.backend,
            untypedHierarchy.Id,
            untypedHierarchy.Name,
            await backend.getType(untypedHierarchy.Type),
            children,
            exports
        );
    }
}
