import { Backend, IConnection, QueryReply, UnityEvent } from "./Backend";

// Unity interface
interface IGameInstance {
    SendMessage(object: string, functionName: string, args: string): void;
}

interface CallResult {
    Name: string;
    Value: any;
    Error: any;
}

interface UnityResponse {
    Id: string;
    Responses: [CallResult];
}

type PendingReturn = (reply: QueryReply[]) => void;

type InitListener = (connection: IConnection) => void;

// declare const UnityLoader: any;

export class JsBackend extends Backend {
    private onInitialized: InitListener[] = [];
    private interfaceTarget?: string = undefined;
    private pendingReturns = new Map<string, PendingReturn>();
    private listeners = new Map<string, UnityEvent>();
    private gameInstance: IGameInstance | undefined = undefined;

    public handleUnityEvent(subscriber: string, queryable: string) {
        if (this.listeners.has(subscriber)) {
            const listener = this.listeners.get(subscriber);
            this.listeners.delete(subscriber);

            if (listener) {
                listener(queryable);
            }
        }
    }

    protected connectImpl(): Promise<IConnection> {
        // Create unity call backs. These functions are called by the embedded unity
        // app to return from function calls
        (window as any).WebGLUnityResponse = (responseJson: string) => {
            const { Id, Responses }: UnityResponse = JSON.parse(responseJson);

            const handler = this.pendingReturns.get(Id);
            this.pendingReturns.delete(Id);

            handler(Responses.map(QueryReply.fromCallResult));
        };

        (window as any).WebGLUnityReady = (gameObject: string) => {
            this.interfaceTarget = gameObject;
            for (const listener of this.onInitialized) {
                listener({ callTarget: "*:WebGLInterface" });
            }
        };

        (window as any).WebGLUnityEvent = (event: string) => {
            const { subscriber, queryable } = JSON.parse(event);
            this.handleUnityEvent(subscriber, queryable);
        };

        return new Promise((resolve) => {
            if (this.interfaceTarget) {
                resolve({ callTarget: "*:WebGLInterface" });
                return;
            }

            const unityScript = document.createElement("script");
            unityScript.onload = () => {
                this.gameInstance = (window as any).UnityLoader.instantiate(
                    "gameContainer",
                    "./unity-web/Build/unity-web.json",
                );
            };
            unityScript.onerror = (error) => {
                console.log(error);
            };
            unityScript.src = "./unity-web/Build/UnityLoader.js";

            document.head.appendChild(unityScript);

            this.onInitialized.push(resolve);
        });
    }

    protected async queryImpl(
        query: string,
        args: any[],
    ): Promise<QueryReply[]> {
        await this.connect();

        return new Promise<QueryReply[]>((resolve) => {
            const id = (Math.random() * Number.MAX_SAFE_INTEGER).toFixed();

            this.pendingReturns.set(id, resolve);

            const request = { Arguments: args, Id: id, Query: query };
            this.gameInstance.SendMessage(
                this.interfaceTarget,
                "ExternalHandleWebGLRequest",
                JSON.stringify(request),
            );
        });
    }
    protected async subscribeImpl(
        target: string,
        listener: UnityEvent,
    ): Promise<void> {
        const id = (Math.random() * Number.MAX_SAFE_INTEGER).toFixed();

        this.listeners.set(id, listener);

        await this.call("Subscribe", [target, id]);
    }
}
