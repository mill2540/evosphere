export abstract class Lazy<T> {
    public get id() {
        return this.idValue;
    }

    private idValue: number;

    private valueCache: T | undefined;

    public invalidate() {
        this.valueCache = undefined;
    }

    public async value(): Promise<T> {
        if (this.valueCache === undefined) {
            const gotten = await this.get();
            this.valueCache = gotten;
            return this.valueCache;
        } else {
            return this.valueCache;
        }
    }

    protected abstract async get(): Promise<T>;
}
