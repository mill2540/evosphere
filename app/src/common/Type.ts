import { Backend } from "./Backend";
import { Lazy } from "./Lazy";
import {
    ArrayValue,
    ConstructedValue,
    EnumValue,
    RegisteredValue,
    StringValue,
    Value,
} from "./Value";

// These classes define abstracts which are generated from the C# typesystem
// TODO: implement docs from unity
export class ConstructorParameter {
    constructor(
        public readonly name: string,
        public readonly type: Type,
        public readonly optional: boolean,
    ) {}
}

export class Constructor {
    constructor(
        public readonly index: number,
        public readonly parameters: ConstructorParameter[],
    ) {}
}

export class ConstructableInfo {
    constructor(
        public readonly name: string,
        public readonly constructors: Constructor[],
    ) {}
}

export class RegisteredMember {
    constructor(public type: Type, public documentation: string) {}
}

export class RegisteredInfo {
    constructor(
        public readonly name: string,
        public readonly members: { [key: string]: RegisteredMember },
    ) {}
}

export class ArrayInfo {
    constructor(
        public readonly name: string,
        public readonly innerType: Type,
    ) {}
}

export class EnumInfo {
    constructor(
        public readonly name: string,
        public readonly variants: string[],
    ) {}
}

// TypeInfo represents information about a C# type. It splits into subtypes
// depeding on what sort of type it is representing.
export type TypeInfo =
    | RegisteredInfo
    | ArrayInfo
    | ConstructableInfo
    | EnumInfo;

// This is the most useful thing here. Generally, it is what the API gives you
// when you ast form information about a C# type,
export class Type {
    public info: TypeInfo;
    /**
     * We also have a list of all the types which can be assigned to an object
     * of this type.
     *
     * For example, if we have
     * interface IFoo {}
     *
     * class Bar : IFoo {}
     *
     * then IFoo.accepts = { IFoo, Bar }
     * and Bar.accepts = { Bar }
     *
     * This should probably be used mostly through isAssignableFrom
     */
    public accepts: ReadonlyArray<Type>;
    public deserializerTarget: Type;

    constructor(
        public readonly name: string,
        public readonly documentation: string,
        public readonly shortName: string,
        public readonly kind: Value["kind"] | "map" | "interface",
    ) {}

    /**
     * Returns true if and only if this type can be asssigned a value of the
     * target type
     * @param  {Type}    target the type that we want to check against
     * @return {boolean}
     */
    public isAssignableFrom(target: Type): boolean {
        return this.accepts.includes(target);
    }

    public value() {
        switch (this.kind) {
            case "array":
                return ArrayValue.create(this);
            case "constructor":
                return ConstructedValue.create(this);
            case "enum":
                return EnumValue.create(this);
            case "registered":
                return RegisteredValue.create(this);
            case "string":
                return StringValue.create(this);
        }
    }
}

export class LazyType extends Lazy<Type> {
    constructor(private backend: Backend, public readonly name: string) {
        super();
    }

    public get() {
        return this.backend.getType(this.name);
    }
}
