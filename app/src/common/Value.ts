import {
    ConstructableInfo,
    Constructor,
    EnumInfo,
    RegisteredInfo,
    Type,
} from "./Type";

// There are two things here: SerialValues and Values.
//
// The difference is that SerialValues do not contain type information, where as
// Values do. This is nessesary because it is not possible turn recursive
// objects into JSON / YAML, and the Type objects are inherently recursive.

export type Value =
    | RegisteredValue
    | ArrayValue
    | StringValue
    | ConstructedValue
    | EnumValue;

// This is needed so that we have a nice value to give to react.
// Basically, we want to make sure that every value we create gets a unique id attached to it.
let VALUE_ID_COUNTER = 0;

export type SerialValue =
    | SerialRegisteredValue
    | SerialArrayValue
    | SerialStringValue
    | SerialConstructedValue
    | SerialEnumValue;

export interface SerialEnumValue {
    type: string;
    variant: string;
}

export class EnumValue {
    public static create(type: Type) {
        const info = type.info as EnumInfo;
        return new EnumValue(type, info.variants[0]);
    }

    public readonly kind: "enum" = "enum";

    constructor(
        public readonly type: Type,
        public readonly variant: string,
        public readonly id: number = VALUE_ID_COUNTER++,
    ) {}

    public serialize(): SerialEnumValue {
        return { type: this.type.name, variant: this.variant };
    }

    public isGood(): boolean {
        return true;
    }
}

export interface SerialRegisteredValue {
    type: string;
    members: { [key: string]: SerialValue };
}

export class RegisteredValue {
    public static create(type: Type) {
        const info = type.info as RegisteredInfo;
        const members: { [key: string]: Value } = {};

        return new RegisteredValue(
            type,
            Object.keys(info.members).reduce((obj, name) => {
                obj[name] = undefined;
                return obj;
            }, members),
        );
    }

    public readonly kind: "registered" = "registered";

    constructor(
        public readonly type: Type,
        public readonly members: Readonly<{ [key: string]: Readonly<Value> }>,
        public readonly id: number = VALUE_ID_COUNTER++,
    ) {}

    public serialize(): SerialRegisteredValue {
        const newMembers: { [key: string]: SerialValue } = {};
        return {
            members: Object.keys(this.members).reduce((obj, memberName) => {
                const member = this.members[memberName];
                obj[memberName] =
                    member !== undefined ? member.serialize() : undefined;
                return obj;
            }, newMembers),
            type: this.type.name,
        };
    }

    public isGood(): boolean {
        return Object.values(this.members).every(
            (member) => member !== undefined && member.isGood(),
        );
    }

    public set(name: string, value?: Value) {
        return new RegisteredValue(this.type, {
            ...this.members,
            [name]: value,
        });
    }
}

export interface SerialArrayValue {
    type: string;
    values: SerialValue[];
}

export class ArrayValue {
    public static create(type: Type) {
        return new ArrayValue(type, []);
    }

    public readonly kind: "array" = "array";

    constructor(
        public readonly type: Type,
        public readonly values: ReadonlyArray<Value>,
        public readonly id: number = VALUE_ID_COUNTER++,
    ) {}

    public serialize(): SerialArrayValue {
        return {
            type: this.type.name,
            values: this.values.map(
                (value) =>
                    value !== undefined ? value.serialize() : undefined,
            ),
        };
    }

    public isGood(): boolean {
        return this.values.every(
            (value) => value !== undefined && value.isGood(),
        );
    }

    public insert(idx: number, value?: Value) {
        const values = [...this.values];
        values.splice(idx, 0, value);
        return new ArrayValue(this.type, values);
    }

    public set(idx: number, value?: Value) {
        const values = [...this.values];
        values[idx] = value;
        return new ArrayValue(this.type, values);
    }

    public delete(idx: number) {
        const values = [...this.values];
        values.splice(idx, 1);
        return new ArrayValue(this.type, values);
    }
}

export interface SerialStringValue {
    type: string;
    value: string;
}

export class StringValue {
    public static create(type: Type) {
        return new StringValue(type, "");
    }

    public readonly kind: "string" = "string";

    constructor(
        public readonly type: Type,
        public readonly value: string,
        public readonly id: number = VALUE_ID_COUNTER++,
    ) {}

    public serialize(): SerialStringValue {
        return { type: this.type.name, value: this.value };
    }

    public isGood(): boolean {
        return true;
    }

    public set(value: string) {
        return new StringValue(this.type, value);
    }
}

export interface SerialConstructedValue {
    type: string;
    parameters: SerialValue[];
}

export class ConstructedValue {
    public static create(type: Type): Value {
        return ConstructedValue.changeConstructor(undefined, type, 0);
    }

    public static changeConstructor(
        value: ConstructedValue | undefined,
        assumedType: Type,
        index: number,
    ) {
        // the value has changed if:
        // the current value is undefined OR the selected constructor has changed
        const changed =
            value !== undefined ? value.constructor.index !== index : true;

        if (changed) {
            // Extract the type of the value,
            // defaulting to the assumed type
            const { type } = value || { type: assumedType };
            // Extract the newly selected constructor
            const info = type.info as ConstructableInfo;
            const constructor = info.constructors[index];

            return new ConstructedValue(
                type,
                constructor,
                Array(constructor.parameters.length).fill(undefined),
            ); // Generate some new parameters
        }
        // Nothing changed, don't update the value
        return value;
    }

    public readonly kind: "constructor" = "constructor";

    constructor(
        public readonly type: Type,
        public readonly constructor: Constructor,
        public readonly parameters: ReadonlyArray<Value>,
        public readonly id: number = VALUE_ID_COUNTER++,
    ) {}

    public serialize(): SerialConstructedValue {
        return {
            parameters: this.parameters.map(
                (parameter) =>
                    parameter !== undefined ? parameter.serialize() : undefined,
            ),
            type: this.type.name,
        };
    }

    public isGood(): boolean {
        return this.parameters.every(
            (parameter) => parameter !== undefined && parameter.isGood(),
        );
    }

    public set(idx: number, value?: Value) {
        const parameters = [...this.parameters];
        parameters[idx] = value;
        return new ConstructedValue(this.type, this.constructor, parameters);
    }
}
