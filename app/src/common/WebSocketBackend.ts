import {
    Backend,
    IConnection,
    QueryReply,
    UnityEvent,
    UnityResponse,
} from "./Backend";

type State = "pre-init" | "init" | "post-init";
interface Reply<D = any, E = any> {
    resolve: (data: D) => void;
    reject: (error: E) => void;
}

export class WebSocketBackend extends Backend {
    private state: State = "pre-init";
    private onInit: Array<Reply<IConnection, Event>> = [];
    private onQuery: Map<string, Reply<QueryReply[]>> = new Map();

    private ws?: WebSocket = undefined;

    protected connectImpl(): Promise<IConnection> {
        return new Promise((resolve, reject) => {
            this.onInit.push({ resolve, reject });
            if (this.ws === undefined) {
                this.ws = new WebSocket("ws://localhost:4646");
                this.ws.onopen = () => {
                    this.ws.onmessage = this.onMessage;
                };
                this.ws.onerror = (ev) => {
                    this.onInit.forEach((init) => init.reject(ev));
                };
            }
        });
    }

    protected async queryImpl(
        query: string,
        args: any[],
    ): Promise<QueryReply[]> {
        await this.connect();

        return new Promise<QueryReply[]>((resolve, reject) => {
            const id = (Math.random() * Number.MAX_SAFE_INTEGER).toFixed();
            this.onQuery.set(id, { resolve, reject });

            const request = { Arguments: args, Id: id, Query: query };
            this.ws.send(JSON.stringify(request));
        });
    }

    protected async subscribeImpl(
        target: string,
        listener: UnityEvent,
    ): Promise<void> {
        throw new Error("subscribe is not supported in http yet.");
    }
    private readonly onMessage = (ev: MessageEvent) => {
        switch (this.state) {
            case "pre-init":
                this.state = "init";
                const connection: IConnection = JSON.parse(ev.data);
                this.onInit.forEach(({ resolve }) => resolve(connection));
                this.state = "post-init";
                return;
            case "post-init":
                const { Id, Responses }: UnityResponse = JSON.parse(ev.data);

                const reply = this.onQuery.get(Id);
                this.onQuery.delete(Id);

                reply.resolve(Responses.map(QueryReply.fromCallResult));

                return;
            case "init":
                // Throw out all messages we recive here, for now.
                // Really, they should be stored and then dispatched once we hit post-init.
                return;
        }
    }
}
