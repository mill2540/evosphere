import {
    ArrayInfo,
    ConstructableInfo,
    RegisteredInfo,
    Type,
} from "../common/Type";
import {
    ArrayValue,
    ConstructedValue,
    RegisteredValue,
    Value,
} from "../common/Value";

/**
 * An element in a value path
 */
export interface ValueSelector {
    /**
     * Gets the type of the selected element given it's containing type. For
     * instance, if we are selecting the first element of an array, then this
     * should return the type of components in that array. On the other hand, if
     * we are selecting a member of a class, then this should return the type of
     * that member.
     *
     * @param type the type to query into
     * @returns(the child type this path element selects, or undefined if this
     * type element cannot select into the given parent type)
     */
    getTypeFrom(type: Type): Type;

    /**
     * Similar to above, but gets child values from values instead of child types
     * from types.
     */
    getFrom(value: Value): Value;
    /**
     * Checks to see if this selection matches the given value.
     */
    matches(value: Value): boolean;

    /**
     * A human readable representation of this path element.
     */
    repr(): string;
}

/**
 * Selects a member of a class, such as Foo::bar or foo.bar.
 */
export class RegisteredMemberSelector implements ValueSelector {
    constructor(public name: string) {}

    public getTypeFrom(type: Type): Type {
        // check that the given type is indeed a class with members
        if (type.info instanceof RegisteredInfo) {
            // If so, then extract the type for this member
            return type.info.members[this.name].type;
        }

        // Otherwise, return undefined.
        return undefined;
    }

    public getFrom(value: Value): Value {
        if (value instanceof RegisteredValue) {
            // Get the value of the member this path element selects
            return value.members[this.name];
        }

        return undefined;
    }

    public matches(value: Value): boolean {
        // This only matches the given value if the given value is a class with a
        // member having our name.
        return value instanceof RegisteredValue && this.name in value.members;
    }

    public repr(): string {
        return `.${this.name}`;
    }
}

/**
 * Selects the given index in an array, such as foo[10].
 */
export class ArrayElementSelector implements ValueSelector {
    constructor(public index: number) {}

    public getTypeFrom(type: Type): Type {
        if (type.info instanceof ArrayInfo) {
            // Get the inner type this array contains.
            return type.info.innerType;
        }
        return undefined;
    }

    public getFrom(value: Value): Value {
        if (value instanceof ArrayValue) {
            // Get the value at our index
            return value.values[this.index];
        }
        return undefined;
    }

    public matches(value: Value): boolean {
        // We match the given value if it is an array and our index could be an
        // element in the array.
        return value instanceof ArrayValue && this.index < value.values.length;
    }

    public repr(): string {
        return `[${this.index}]`;
    }
}

/**
 * Selects parameters to constructors. For example, new Foo(bar).
 */
export class ConstructorParameterSelector implements ValueSelector {
    /**
     * Creates a new constructor parameter selector
     * @param constructor The index of the constructor this selector selects into.
     * @param index The index of the parameter.
     */
    constructor(public constructor: number, public index: number) {}

    public getTypeFrom(type: Type): Type {
        if (type.info instanceof ConstructableInfo) {
            // Get the constructor containing this parameter, and get this parameter
            // from that constructor.
            // TODO: add more error checking here.
            return type.info.constructors[this.constructor].parameters[
                this.index
            ].type;
        }
        return undefined;
    }

    public getFrom(value: Value): Value {
        if (value instanceof ConstructedValue) {
            // We don't need to know which constructor is being used here, because of
            // how values hold constructed values.
            return value.parameters[this.index];
        }
        return undefined;
    }

    public matches(value: Value): boolean {
        // If this index of this constructor parameter is plausible, then we have a
        // match
        return (
            value instanceof ConstructedValue &&
            this.index < value.parameters.length
        );
    }

    public repr(): string {
        return `(..., ${this.index}, ...)`;
    }
}

/**
 * A path for selecting values and types. Contains a list of ValueSelectors,
 * which are applied in sequence to descend into values and types.
 */
export class ValuePath {
    /**
     * Attempt to descend into an array, but only if this path starts with an
     * array AND the start selects the given index.
     */
    public static popFromArray(path: ValuePath, index: number) {
        return ValuePath.popIf(
            path,
            (element) =>
                element instanceof ArrayElementSelector &&
                element.index === index,
        );
    }

    /**
     * Attempt to descend into an constructor parameter selector, but only if the
     * head selector selects the given index.
     */
    public static popFromConstructor(path: ValuePath, index: number) {
        return ValuePath.popIf(
            path,
            (element) =>
                element instanceof ConstructorParameterSelector &&
                element.index === index,
        );
    }

    /**
     * Attempt to descend into an registered member selector, but only if this
     * selector selects the given member.
     */
    public static popFromRegistered(path: ValuePath, name: string) {
        return ValuePath.popIf(
            path,
            (element) =>
                element instanceof RegisteredMemberSelector &&
                element.name === name,
        );
    }

    public static isAnyParent(path: ValuePath, value: Value) {
        return path !== undefined && path.isAnyParent(value);
    }

    public static isDirectParent(path: ValuePath, value: Value) {
        return path !== undefined && path.isDirectParent(value);
    }

    public static isAtEnd(path: ValuePath, value: Value) {
        return (
            path !== undefined &&
            (path.path.length === 0 ||
                (path.path.length === 1 && path.path[0].matches(value)))
        );
    }

    /**
     * UI elements generally use this to check if they have been selected. If
     * this returns true, then it means that all selectors have already been
     * popped off this path, which can only happen if the value held by that UI
     * element has been selected.
     */
    public static isTerminal(path: ValuePath | undefined) {
        return path !== undefined && path.terimal;
    }

    /**
     * If this path selects the given value, or is a parent of the selected value.
     */
    public static contains(path: ValuePath | undefined, value: Value) {
        return path !== undefined && (path.terimal || path.isAnyParent(value));
    }

    /**
     * Pops the head off a path. If the head does not match the given filter,
     * return undefined.
     * @param path The path to pop from
     * @param filter Only pop if this filter returns true
     */
    private static popIf(
        path: ValuePath,
        filter: (path: ValueSelector) => boolean,
    ) {
        if (path === undefined || path.path.length === 0) {
            return undefined;
        }
        const [head, ...tail] = path.path;

        if (filter(head)) {
            return new ValuePath(...tail);
        }
        return undefined;
    }

    public readonly path: ReadonlyArray<ValueSelector>;

    constructor(...path: ValueSelector[]) {
        this.path = path;
    }

    public get terimal() {
        return this.path.length === 0;
    }

    public get parent() {
        if (this.terimal) {
            return this;
        } else {
            return new ValuePath(...this.path.slice(0, -1));
        }
    }

    /**
     * Adds a parent to this path. Used to step up the value tree when a UI
     * element is selected.
     */
    public prependParents(...parents: ValueSelector[]): ValuePath {
        return new ValuePath(...parents, ...this.path);
    }

    /**
     * The selected value is in an array.
     */
    public inArray(at: number) {
        return this.prependParents(new ArrayElementSelector(at));
    }

    public inConstructor(constructor: number, parameter: number) {
        return this.prependParents(
            new ConstructorParameterSelector(constructor, parameter),
        );
    }

    public inRegistered(name: string) {
        return this.prependParents(new RegisteredMemberSelector(name));
    }

    /**
     * A child value is selected.
     */
    public appendChildren(...children: ValueSelector[]): ValuePath {
        return new ValuePath(...this.path, ...children);
    }

    /**
     * Extracts the selected value from this value.
     */
    public select(value: Value): Value {
        let current = value;
        for (const elem of this.path) {
            current = elem.getFrom(current);
            if (current === undefined) {
                return current;
            }
        }
        return current;
    }

    /**
     * Extracts the selected value from this value, as well as from the given
     * type. If it turns out that the value is undefined (and thus has no type),
     * the expected value is used instead.
     */
    public get(
        root: Value | undefined,
        expected: Type,
    ): { value: Value | undefined; expected: Type } {
        let value = root;
        let parent = value;
        for (const elem of this.path) {
            parent = value;
            value = elem.getFrom(value);
            if (value === undefined) {
                expected = undefined;
                if (parent !== undefined) {
                    expected = elem.getTypeFrom(parent.type);
                }
                return { value, expected };
            }
        }
        return { value, expected };
    }

    /**
     * Checks if this is the immediate parent of the selected value. For
     * instance, if this path selects foo.bar, then foo would be the direct
     * parent of foo.bar.
     */
    public isDirectParent(value: Value): boolean {
        return this.path.length === 1 && this.path[0].matches(value);
    }

    /**
     * Checks is this is any parent of the selected value. For example, if the
     * selected value is foo.bar.bazz, then foo and bar are both parents of
     * foo.bar.bazz
     */
    public isAnyParent(value: Value): boolean {
        return this.path.length >= 1 && this.path[0].matches(value);
    }

    /**
     * Descend one level into the selection path, but only if the value matches
     * the start of the path.
     */
    public tryPop(value: Value): [ValueSelector, ValuePath] {
        // If this path is not terminal, and it matches the given value
        if (this.path.length > 0 && this.path[0].matches(value)) {
            // split it into the head selector and the remaining path
            return [this.path[0], new ValuePath(...this.path.slice(1))];
        }
        return [undefined, new ValuePath()];
    }

    /**
     * Descend one level into the selection path.
     */
    public getHeadTail(): [ValueSelector, ValuePath] {
        const [head, ...tail] = this.path;
        return [head, new ValuePath(...tail)];
    }

    /**
     * Replace the selected child value of root with the given value.
     */
    public replace(root: Value, newValue: Value): Value {
        // If root is the selected value, replace it
        if (this.path.length === 0) {
            return newValue;
        }

        // descend into the head tail
        const [head, tail] = this.getHeadTail();

        if (head instanceof RegisteredMemberSelector) {
            if (root instanceof RegisteredValue) {
                // replaces the given member with the new value. The new value
                // is gotten by recursively descending into the value tree,
                // following the selection path.

                return new RegisteredValue(root.type, {
                    ...root.members,
                    [head.name]: tail.replace(
                        root.members[head.name],
                        newValue,
                    ),
                });
            } else {
                throw new Error(
                    `Expected RegisteredValue with member ${
                        head.name
                    } but got ${typeof root}`,
                );
            }
        }

        if (head instanceof ArrayElementSelector) {
            if (root instanceof ArrayValue && head.index < root.values.length) {
                // replaces the value at the given index with the new value

                const values = [...root.values];
                values[head.index] = tail.replace(
                    root.values[head.index],
                    newValue,
                );

                return new ArrayValue(root.type, values);
            } else {
                throw new Error(
                    `Expected ArrayValue with at least ${1 +
                        head.index} elements but got ${root.type.shortName}`,
                );
            }
        }

        if (head instanceof ConstructorParameterSelector) {
            if (
                root instanceof ConstructedValue &&
                head.index < root.parameters.length
            ) {
                // Replaces the parameter at the given index with the new value

                const parameters = [...root.parameters];
                parameters[head.index] = tail.replace(
                    root.parameters[head.index],
                    newValue,
                );

                return new ConstructedValue(
                    root.type,
                    root.constructor,
                    parameters,
                );
            } else {
                throw new Error(
                    `Expected ArrayValue with at least ${1 +
                        head.index} parameters but got ${typeof root}`,
                );
            }
        }

        throw new Error(`Invalid path`);
    }

    public repr() {
        return "Experiment" + this.path.map((entry) => entry.repr()).join("");
    }
}
