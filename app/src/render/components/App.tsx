import * as React from "react";
import { hot } from "react-hot-loader";

import { CssBaseline, withStyles } from "@material-ui/core";

import { Menus } from "./Menus";
import { Styles, styles } from "./styles";
import { Unity } from "./Unity";
import { Views } from "./Views";

const ColdApp = withStyles(styles)(((state: Styles) => {
    const { classes } = state;

    return (
        <div className={classes.root}>
            <CssBaseline />
            <Menus />
            <main className={classes.content}>
                <div className={classes.toolbar} />
                <Views />
            </main>
            <Unity />
        </div>
    );
}) as React.SFC);

export const App = hot(module)(ColdApp);
