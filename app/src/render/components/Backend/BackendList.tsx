import * as React from "react";

import { List, ListItem, ListItemText, withStyles } from "@material-ui/core";

import { JsBackend } from "../../../common/JsBackend";
import { WebSocketBackend } from "../../../common/WebSocketBackend";

import { Backend } from "../../../common/Backend";
import { styles, Styles } from "../styles";

const BACKENDS = [
    {
        description: [
            "The unity instance built into this application.",
            "This does not need a connection to any external resources & should just work",
        ],
        id: "local",
        text: "Local",
        value: () => new JsBackend(),
    },
    {
        description: ["Connect to a remote unity instance"],
        id: "ws",
        text: "WebSocket",
        value: () => new WebSocketBackend(),
    },
];

export type OnBackend = (backend: Backend) => void;

/**
 * State information of the App Loading
 */
interface IBackendChooserProps extends Styles {
    backend: Backend | undefined;
    onBackend: OnBackend;
}

export const BackendList = withStyles(styles)((({
    backend,
    onBackend,
}: IBackendChooserProps) => {
    return (
        <List>
            {BACKENDS.map(({ text, value, id, description }) => (
                <ListItem key={id} button onClick={() => onBackend(value())}>
                    <ListItemText
                        primary={text}
                        secondary={description.join(" ")}
                    />
                </ListItem>
            ))}
        </List>
    );
}) as React.SFC<IBackendChooserProps>);
