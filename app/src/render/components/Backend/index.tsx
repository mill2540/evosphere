import * as React from "react";

import {
    Button,
    Card,
    CardActions,
    CardContent,
    CircularProgress,
    Fade,
    Typography,
    withStyles,
} from "@material-ui/core";

import { Backend } from "../../../common/Backend";
import { styles, Styles } from "../styles";
import { BackendList } from "./BackendList";

export type OnCancel = () => void;
export type OnBackendReady = (backend: Backend) => void;

/**
 * State information of the App Loading
 */
interface IBackendChooserProps extends Styles {
    backend?: Backend;
    onCancel?: OnCancel;
    onBackendReady: OnBackendReady;
}
interface IBackendChooserState {
    choosenBackend?: Backend;
    waitingForBackend: boolean;
}
export const BackendChooserCard = withStyles(styles)(
    class extends React.Component<IBackendChooserProps, IBackendChooserState> {
        constructor(props: IBackendChooserProps) {
            super(props);

            this.state = {
                choosenBackend: props.backend,
                waitingForBackend: false,
            };
        }

        public render() {
            const { classes, onCancel } = this.props;
            const { choosenBackend, waitingForBackend } = this.state;

            // const content = waitingForBackend ? (
            //     <React.Fragment>
            //         <Typography variant="h6">Loading Unity...</Typography>
            //         <br />
            //         <LinearProgress />
            //     </React.Fragment>
            // ) : (
            //     <React.Fragment>
            //         <Typography variant="h6">Select Backend</Typography>
            //         <BackendList
            //             backend={choosenBackend}
            //             onBackend={this.onBackend}
            //         />
            //     </React.Fragment>
            // );
            const content = (
                <React.Fragment>
                    <Fade
                        in={choosenBackend !== undefined && waitingForBackend}
                    >
                        <div className={classes.loadingSection}>
                            <Typography variant="h6">
                                Loading Unity...
                            </Typography>
                            <CircularProgress />
                        </div>
                    </Fade>
                    <Fade
                        in={choosenBackend === undefined && !waitingForBackend}
                    >
                        <div className={classes.loadingSection}>
                            <Typography variant="h6">Select Backend</Typography>
                            <BackendList
                                backend={choosenBackend}
                                onBackend={this.onBackend}
                            />
                        </div>
                    </Fade>
                </React.Fragment>
            );

            return (
                <Card>
                    <CardContent className={classes.loadingContent}>
                        {content}
                    </CardContent>
                    {onCancel && (
                        <CardActions>
                            <Button
                                size="small"
                                onClick={onCancel}
                                color="primary"
                            >
                                Cancel
                            </Button>
                        </CardActions>
                    )}
                </Card>
            );
        }

        private onBackend = (choosenBackend: Backend) => {
            this.setState({
                choosenBackend,
                waitingForBackend: true,
            });
            choosenBackend
                .connect()
                .then(() => {
                    this.setState({ waitingForBackend: false });
                    this.props.onBackendReady(choosenBackend);
                })
                .catch(() => {
                    this.setState({
                        choosenBackend: this.props.backend,
                        waitingForBackend: false,
                    });
                });
        }
    },
);
