import * as React from "react";

import { withStyles } from "@material-ui/core";
import { LazyType, Type } from "../../../common/Type";
import { styles } from "../styles";

interface ILazyTypeViewProps {
    lazy_type: LazyType;
}
interface ILazyTypeViewState {
    type?: Type;
}

export const ExportView = withStyles(styles)(
    class LazyTypeView extends React.Component<
        ILazyTypeViewProps,
        ILazyTypeViewState
    > {
        constructor(props: ILazyTypeViewProps) {
            super(props);

            this.state = {};
        }
    },
);
