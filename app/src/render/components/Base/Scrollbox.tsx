import * as React from "react";

import { withStyles } from "@material-ui/core";

import { Styles, styles } from "../styles";

interface ScrollboxProps extends Styles {
    children: React.ReactNode;
    padded?: boolean;
}

export const Scrollbox = withStyles(styles)(
    ({ classes, padded, children }: ScrollboxProps) => {
        return <div className={classes.scrollbox}> {children}</div>;
    },
);
