import * as React from "react";

import { Paper, withStyles } from "@material-ui/core";
import { styles, Styles } from "../styles";

export const Tile = withStyles(styles)(
    ({ classes, children }: Styles & { children?: React.ReactNode }) => {
        return <Paper className={classes.paperRoot}>{children}</Paper>;
    }
);
