import * as React from "react";

import { Paper, withStyles } from "@material-ui/core";
import { styles, Styles } from "../styles";

interface Props extends Styles {
    readonly children?: React.ReactNode;
}

export const TitleBox = withStyles(styles)(((props: Props) => {
    const { classes, children } = props;

    return <Paper className={classes.paperRoot}>{children}</Paper>;
}) as React.SFC);
