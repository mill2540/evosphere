import * as React from "react";

import { Typography } from "@material-ui/core";
import { Type } from "../../../common/Type";

interface ITypeViewProps {
    value: Type;
}

export function TypeName({ value }: ITypeViewProps) {
    return <Typography>{value.shortName}</Typography>;
}
