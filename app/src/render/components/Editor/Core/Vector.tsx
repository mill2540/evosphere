import * as React from "react";

import { Input, TextField, Typography, withStyles } from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import MaskedInput, { maskArray, MaskedInputProps } from "react-text-mask";

import { InputBaseComponentProps } from "@material-ui/core/InputBase";
import { ConstructedValue } from "../../../../common/Value";
import { styles, Styles } from "../../styles";
import { editor } from "../editors";
import { IEditorProps } from "../types";

const SEGMENT_START = /\+|-|[0-9]/;
const SEGMENT_CENTER = /[0-9]/;

function vectorMask(degree: number, start = "<", end = ">", sep = ",") {
    const maskStart = start.split("");
    const maskEnd = end.split("");
    const maskSep = sep.split("");

    return function maskFunction(value: string = ""): maskArray {
        const numberSegments: maskArray[] = value
            .split(sep)
            .map((coor) => coor.trim().split(""))
            .map((segment) => {
                segment = segment.filter((c, i) => {
                    const expected = i === 0 ? SEGMENT_START : SEGMENT_CENTER;
                    return c.match(expected);
                });

                if (segment.length === 0) {
                    return [SEGMENT_START];
                } else {
                    return [...segment, SEGMENT_CENTER];
                }
            });

        const segments: maskArray = [];
        for (let idx = 0; idx < degree; idx++) {
            if (idx < numberSegments.length) {
                segments.push(...numberSegments[idx]);
            } else {
                segments.push(SEGMENT_START);
            }

            if (idx < degree - 1) {
                segments.push(...maskSep);
            }
        }

        return [...maskStart, ...segments, ...maskEnd];
    };
}

function VectorInput({
    inputRef,
    ...props
}: MaskedInputProps & InputBaseComponentProps) {
    return (
        <MaskedInput {...props} ref={inputRef} mask={vectorMask(2)} showMask />
    );
}

export const VectorEditor = editor(
    "Vector, Assembly-CSharp, Version=0.0.0.0, Culture=neutral, PublicKeyToken=null",
    "inline"
)(
    withStyles(styles)(
        ({
            classes,
            value,
            onValueChanged,
        }: IEditorProps<ConstructedValue> & Styles) => {
            return <Input inputComponent={VectorInput} />;
        }
    )
);
