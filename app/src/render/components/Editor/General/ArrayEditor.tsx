import * as React from "react";

import {
    IconButton,
    List,
    ListItem,
    Paper,
    withStyles,
} from "@material-ui/core";

import AddIcon from "@material-ui/icons/Add";
import { ValueWell } from "..";
import { ArrayInfo } from "../../../../common/Type";
import { ArrayValue, Value } from "../../../../common/Value";
import { ArrayElementSelector, ValuePath } from "../../../ValuePath";
import { Tile } from "../../Base/Tile";
import { Styles, styles } from "../../styles";
import { kindEditor } from "../editors";
import { IEditorProps } from "../types";

interface IArrayEditorState {
    expandedMember?: string;
}

export const ArrayEditor = kindEditor("array", "block")(
    class extends React.Component<IEditorProps<ArrayValue>, IArrayEditorState> {
        constructor(props: IEditorProps<ArrayValue>) {
            super(props);

            this.state = {};
        }

        public render() {
            const { path, value, onValueChanged } = this.props;
            const info = value.type.info as ArrayInfo;

            const AddItem = (key: string, idx: number) => (
                <ListItem key={key}>
                    <Tile>
                        <IconButton
                            onClick={() => {
                                onValueChanged(value.insert(idx));
                            }}
                        >
                            <AddIcon />
                        </IconButton>
                    </Tile>
                </ListItem>
            );

            return (
                <List>
                    {AddItem("add-first-item", 0)}
                    {value.values.map((item, idx) => (
                        <React.Fragment
                            key={
                                item !== undefined
                                    ? `id-${item.id}`
                                    : `idx-${idx}`
                            }
                        >
                            <ListItem>
                                <ValueWell
                                    value={item}
                                    path={path.appendChildren(
                                        new ArrayElementSelector(idx)
                                    )}
                                    expectedType={info.innerType}
                                    onValueChanged={(newItemValue) => {
                                        onValueChanged(
                                            value.set(idx, newItemValue)
                                        );
                                    }}
                                />
                            </ListItem>
                            {AddItem(`add-${idx}`, idx + 1)}
                        </React.Fragment>
                    ))}
                </List>
            );
        }
    }
);
