import * as React from "react";

import {
    ExpansionPanel,
    ExpansionPanelDetails,
    ExpansionPanelSummary,
    Grid,
    Paper,
    Typography,
    withStyles,
} from "@material-ui/core";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import { ValueWell } from "..";
import { Type } from "../../../../common/Type";
import { Value } from "../../../../common/Value";
import { ValuePath } from "../../../ValuePath";
import { Styles, styles } from "../../styles";

export interface IMember {
    title: string;
    documentation?: string;
    expectedType: Type;
    value?: Value;
    path: ValuePath;
    onValueChanged: (value?: Value) => void;
}

interface IMemberEditorProps extends IMember {
    expanded: boolean;
    onExpand: () => void;
}

export function MemberWithDocsEditor({
    title,
    documentation,
    expanded,
    onExpand,
    ...props
}: IMemberEditorProps) {
    return (
        <Grid item>
            <ExpansionPanel expanded={expanded} onChange={onExpand}>
                <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
                    <Typography variant="h6">{title}</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        spacing={16}
                    >
                        <Grid item xs>
                            <Typography variant="caption">
                                {documentation}
                            </Typography>
                        </Grid>
                        <Grid item xs>
                            <ValueWell {...props} />
                        </Grid>
                    </Grid>
                </ExpansionPanelDetails>
            </ExpansionPanel>
        </Grid>
    );
}

export const MemberWithoutDocsEditor = withStyles(styles)(
    ({ title, ...props }: IMemberEditorProps & Styles) => {
        return (
            <Grid item>
                <Paper className={props.classes.paperRoot}>
                    <Grid
                        container
                        direction="row"
                        justify="center"
                        alignItems="center"
                        spacing={16}
                    >
                        <Grid item xs>
                            <Typography variant="h6">{title}</Typography>
                        </Grid>
                        <Grid item xs>
                            <ValueWell {...props} />
                        </Grid>
                    </Grid>
                </Paper>
            </Grid>
        );
    }
);

function MemberEditor(props: IMemberEditorProps) {
    if (props.documentation) {
        return <MemberWithDocsEditor {...props} />;
    } else {
        return <MemberWithoutDocsEditor {...props} />;
    }
}

interface IAssociativeEditorProps {
    items: ReadonlyArray<
        IMember & {
            key: string | number;
        }
    >;
}

interface IAssociativeEditorState {
    expandedMember?: string | number;
}

export class AssociativeEditor extends React.Component<
    IAssociativeEditorProps,
    IAssociativeEditorState
> {
    constructor(props: IAssociativeEditorProps) {
        super(props);

        this.state = {};
    }

    public render() {
        const { items } = this.props;
        let { expandedMember } = this.state;

        if (expandedMember === undefined && items.length > 0) {
            expandedMember = items[0].key;
        }

        return (
            <Grid container direction="column" spacing={16}>
                {items.map(({ key, ...member }) => {
                    return (
                        <MemberEditor
                            key={key}
                            expanded={expandedMember === key}
                            onExpand={this.handleExpand(key)}
                            {...member}
                        />
                    );
                })}
            </Grid>
        );
    }

    private handleExpand = (expandedMember: string | number) => () => {
        if (this.state.expandedMember !== expandedMember) {
            this.setState({ expandedMember });
        } else {
            this.setState({ expandedMember: undefined });
        }
    };
}
