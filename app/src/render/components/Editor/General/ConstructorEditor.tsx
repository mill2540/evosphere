import * as React from "react";

import {
    Button,
    Collapse,
    Grid,
    List,
    ListItem,
    ListItemText,
    Paper,
    Tooltip,
    withStyles,
} from "@material-ui/core";

import { ConstructableInfo } from "../../../../common/Type";
import { ConstructedValue, Value } from "../../../../common/Value";
import { ConstructorParameterSelector } from "../../../ValuePath";
import { styles, Styles } from "../../styles";
import { kindEditor } from "../editors";
import { IEditorProps } from "../types";
import { AssociativeEditor } from "./AssociativeEditor";

function Constructor({
    path,
    value,
    onValueChanged,
}: IEditorProps<ConstructedValue>) {
    return (
        <AssociativeEditor
            items={value.parameters.map((parameter, idx) => ({
                documentation: "",
                expectedType: value.constructor.parameters[idx].type,
                key: value.constructor.parameters[idx].name,
                onValueChanged: (newValue?: Value) => {
                    onValueChanged(value.set(idx, newValue));
                },
                path: path.appendChildren(
                    new ConstructorParameterSelector(
                        value.constructor.index,
                        idx
                    )
                ),
                title: value.constructor.parameters[idx].name,
                value: parameter,
            }))}
        />
    );
}

interface IConstructorChooserProps {
    expanded: boolean;
}

const ConstructorChooser = withStyles(styles)(
    class extends React.Component<
        IEditorProps<ConstructedValue> & Styles,
        IConstructorChooserProps
    > {
        constructor(props: IEditorProps<ConstructedValue> & Styles) {
            super(props);

            this.state = { expanded: false };
        }

        public render() {
            const { classes, value, onValueChanged } = this.props;
            const { expanded } = this.state;

            const info = value.type.info as ConstructableInfo;

            return (
                <Paper className={classes.paperRoot}>
                    <Grid
                        container
                        direction="column"
                        spacing={16}
                        alignItems="stretch"
                    >
                        <Grid item className={classes.flexAlignSelfCenter}>
                            <Tooltip
                                title={[
                                    "Warning. Changing the selected constructor will delete all",
                                    "values you have configured for this setting",
                                ].join(" ")}
                            >
                                <Button
                                    disabled={expanded}
                                    color="secondary"
                                    onClick={() => {
                                        this.setState({
                                            expanded: true,
                                        });
                                    }}
                                >
                                    Select a different constructor
                                </Button>
                            </Tooltip>
                        </Grid>
                        <Collapse in={expanded} unmountOnExit mountOnEnter>
                            <Grid item>
                                <List>
                                    {info.constructors.map((constructor) => (
                                        <ListItem
                                            key={constructor.index}
                                            button
                                            selected={
                                                value.constructor.index ===
                                                constructor.index
                                            }
                                            onClick={() => {
                                                this.setState({
                                                    expanded: false,
                                                });
                                                onValueChanged(
                                                    ConstructedValue.changeConstructor(
                                                        value,
                                                        value.type,
                                                        constructor.index
                                                    )
                                                );
                                            }}
                                        >
                                            <ListItemText
                                                primary={
                                                    "(" +
                                                    constructor.parameters
                                                        .map(
                                                            (parameter) =>
                                                                parameter.name
                                                        )
                                                        .join(", ") +
                                                    ")"
                                                }
                                            />
                                        </ListItem>
                                    ))}
                                </List>
                            </Grid>
                        </Collapse>
                    </Grid>
                </Paper>
            );
        }
    }
);

export const ConstructorEditor = kindEditor("constructor", "block")(
    withStyles(styles)(({ ...props }: IEditorProps<ConstructedValue>) => {
        return (
            <Grid container direction="column" spacing={16}>
                <Grid item>
                    <ConstructorChooser {...props} />
                </Grid>
                <Grid item>
                    <Constructor {...props} />
                </Grid>
            </Grid>
        );
    })
);
