import * as React from "react";

import { TextField, withStyles } from "@material-ui/core";

import { StringValue } from "../../../../common/Value";
import { Tile } from "../../Base/Tile";
import { styles, Styles } from "../../styles";
import { kindEditor } from "../editors";
import { IEditorProps } from "../types";

export const StringEditor = kindEditor("string", "inline")(
    withStyles(styles)(
        ({
            classes,
            value,
            onValueChanged,
        }: IEditorProps<StringValue> & Styles) => {
            return (
                <TextField
                    className={classes.input}
                    variant="standard"
                    value={value.value}
                    onChange={(event) => {
                        onValueChanged(value.set(event.target.value));
                    }}
                />
            );
        }
    )
);
