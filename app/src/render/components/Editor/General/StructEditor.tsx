import * as React from "react";

import { withStyles } from "@material-ui/core";

import { RegisteredInfo } from "../../../../common/Type";
import { RegisteredValue, Value } from "../../../../common/Value";
import { RegisteredMemberSelector, ValuePath } from "../../../ValuePath";
import { kindEditor } from "../editors";
import { IEditorProps } from "../types";
import { AssociativeEditor } from "./AssociativeEditor";

interface IRegisteredEditorState {
    expandedMember?: string;
}

class StructEditorImpl extends React.Component<
    IEditorProps<RegisteredValue>,
    IRegisteredEditorState
> {
    constructor(props: IEditorProps<RegisteredValue>) {
        super(props);

        this.state = {};
    }

    public render() {
        const { path, value, onValueChanged } = this.props;

        const info = value.type.info as RegisteredInfo;

        return (
            <AssociativeEditor
                items={Object.entries(info.members).map(([name, member]) => ({
                    documentation: member.documentation,
                    expectedType: member.type,
                    key: name,
                    onValueChanged: (newMemberValue?: Value) => {
                        onValueChanged(value.set(name, newMemberValue));
                    },
                    path: path.appendChildren(
                        new RegisteredMemberSelector(name)
                    ),
                    title: name,
                    value: value.members[name],
                }))}
            />
        );
    }
}

export const StructEditor = kindEditor("registered", "block")(StructEditorImpl);
