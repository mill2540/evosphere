import { Type } from "../../../common/Type";
import { IEditorProps } from "./types";

export type TypePredicate = string | string[] | ((type: Type) => boolean);

export type Editor = React.ComponentType<IEditorProps> & {
    evosphereTypePredicate: TypePredicate;
    evosphereEditorSize: "inline" | "block";
};

export class Editors {
    public static editorFor(
        type: Type,
        size?: "inline" | "block"
    ): React.ComponentType<IEditorProps> {
        const matches = this.EDITORS.filter(
            ({ evosphereTypePredicate: predicate, evosphereEditorSize }) =>
                (size === undefined || size === evosphereEditorSize) &&
                (typeof predicate === "string"
                    ? predicate === type.name
                    : Array.isArray(predicate)
                        ? predicate.indexOf(type.name) !== -1
                        : predicate(type))
        );

        if (matches.length === 0) {
            return this.DEFAULT_EDITOR;
        } else if (matches.length === 1) {
            return matches[0];
        } else {
            // Exact matches get priority
            const exactMatches = matches.filter(
                ({ evosphereTypePredicate }) =>
                    typeof evosphereTypePredicate === "string"
            );
            const fuzzyMatches = matches.filter(
                ({ evosphereTypePredicate }) =>
                    typeof evosphereTypePredicate !== "string"
            );

            if (exactMatches.length === 0) {
                if (fuzzyMatches.length === 0) {
                    return undefined;
                } else if (fuzzyMatches.length === 1) {
                    return fuzzyMatches[0];
                } else {
                    return undefined;
                }
            } else if (exactMatches.length === 1) {
                return exactMatches[0];
            } else {
                return undefined;
            }
        }
    }

    public static append(editors: { [key: string]: Editor }) {
        this.EDITORS = [...this.EDITORS, ...Object.values(editors)];
    }

    private static EDITORS: Editor[] = [];

    private static readonly DEFAULT_EDITOR: React.ComponentType<
        IEditorProps
    > = undefined;
}

export function kindPredicate(kind: Type["kind"]) {
    return (type: Type) => type.kind === kind;
}

export function editor(
    predicate: TypePredicate,
    size: Editor["evosphereEditorSize"]
) {
    return (e: React.ComponentType<IEditorProps>): Editor => {
        const erased = e as Editor;
        erased.evosphereTypePredicate = predicate;
        erased.evosphereEditorSize = size;

        return erased as Editor;
    };
}

export function kindEditor(
    kind: Type["kind"],
    size: Editor["evosphereEditorSize"]
) {
    return editor(kindPredicate(kind), size);
}

import * as General from "./General";
Editors.append(General);

import * as Core from "./Core";
Editors.append(Core);
