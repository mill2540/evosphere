import * as React from "react";

import {
    Card,
    CardActionArea,
    CardContent,
    CardHeader,
    Divider,
    Grid,
    IconButton,
    Paper,
    Typography,
    withStyles,
} from "@material-ui/core";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import DeleteIcon from "@material-ui/icons/Delete";

import { ValuePath } from "../../ValuePath";
import { styles, Styles } from "../styles";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IAppState } from "../../store";
import * as actions from "../../store/views/actions";
import { IEditView } from "../../store/views/types";
import { Tile } from "../Base/Tile";
import { TitleBox } from "../Base/TitleBox";
import { TypeSelector } from "../Util/TypeSelector";
import { Editors } from "./editors";
import { StringEditor } from "./General/StringEditor";
import { IBaseValueWellProps, IEditorProps } from "./types";

function mapStateToProps(state: IAppState) {
    return { selection: (state.view as IEditView).selection };
}
function mapDispatchToProps(dispatch: Dispatch) {
    return {
        onSelectionChanged: (selection: ValuePath) => {
            dispatch(actions.edit({ selection }));
        },
    };
}

export const ValueWell = connect(
    mapStateToProps,
    mapDispatchToProps
)(
    withStyles(styles)(
        class extends React.Component<IBaseValueWellProps & Styles> {
            constructor(props: IBaseValueWellProps & Styles) {
                super(props);
            }

            public componentDidMount() {
                const { expectedType, value, onValueChanged } = this.props;

                if (
                    expectedType.accepts.length === 1 &&
                    value === undefined &&
                    expectedType.accepts[0].kind !== "interface"
                ) {
                    onValueChanged(expectedType.accepts[0].value());
                }
            }

            public render() {
                const {
                    classes,
                    value,
                    onValueChanged,
                    expectedType,
                    onSelectionChanged,
                    path,
                } = this.props;
                const type =
                    value !== undefined
                        ? value.type.deserializerTarget
                        : expectedType.deserializerTarget;

                const title = type.shortName;
                const documentation = type.documentation;

                const deleteButton = (
                    <IconButton
                        color="secondary"
                        disabled={value === undefined}
                        onClick={() => {
                            onValueChanged(undefined);
                        }}
                    >
                        <DeleteIcon />
                    </IconButton>
                );

                let editor;
                if (value !== undefined) {
                    editor = Editors.editorFor(value.type, "inline");
                }

                const content =
                    editor !== undefined ? (
                        <CardContent>
                            {React.createElement(editor, {
                                onValueChanged,
                                path,
                                value,
                            })}
                        </CardContent>
                    ) : (
                        <CardActionArea
                            onClick={() => {
                                onSelectionChanged(path);
                            }}
                        >
                            <CardContent>Edit</CardContent>
                        </CardActionArea>
                    );

                return (
                    <Card className={classes.valueWellCard}>
                        <CardHeader
                            title={title}
                            subheader={documentation}
                            action={deleteButton}
                        />
                        {content}
                    </Card>
                );
            }
        }
    )
);

const ValueEditorContent = (props: IBaseValueWellProps) => {
    const { expectedType, onValueChanged } = props;

    if (props.value !== undefined) {
        return React.createElement(
            Editors.editorFor(props.value.type),
            props as IEditorProps
        );
    } else {
        return (
            <TypeSelector
                autoselect
                types={expectedType.accepts}
                onSelect={(type) => {
                    onValueChanged(type.value());
                }}
            />
        );
    }
};

export interface IValueEditorProps extends IBaseValueWellProps {
    title: string;
    documentation: string;
}

export const ValueEditor = connect(
    mapStateToProps,
    mapDispatchToProps
)(
    withStyles(styles)((props: IValueEditorProps & Styles) => {
        const { classes, title, documentation, ...content } = props;

        return (
            <Grid container direction="column" spacing={16}>
                <Grid item>
                    <TitleBox>
                        <Grid container direction="column" spacing={8}>
                            <Grid item className={classes.flexRow}>
                                <Typography variant="h6">
                                    <IconButton
                                        onClick={() => {
                                            content.onSelectionChanged(
                                                content.path.parent
                                            );
                                        }}
                                    >
                                        <ArrowUpwardIcon fontSize="small" />
                                    </IconButton>
                                    {title}
                                </Typography>
                            </Grid>
                            <Divider />
                            <Grid item>
                                <Typography variant="caption">
                                    {documentation}
                                </Typography>
                            </Grid>
                        </Grid>
                    </TitleBox>
                </Grid>

                <Grid item xs="auto">
                    <ValueEditorContent {...content} />
                </Grid>
            </Grid>
        );
    })
);
