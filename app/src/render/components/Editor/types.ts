import * as React from "react";

import { Type } from "../../../common/Type";
import { Value } from "../../../common/Value";
import { ValuePath } from "../../ValuePath";
import { Styles } from "../styles";

export interface IEditorProps<V extends Value = Value> {
    path: ValuePath;
    value: V;
    onValueChanged: (value?: Value) => void;
}

export interface IBaseValueWellProps {
    expectedType: Type;
    value?: Value;
    selection: ValuePath;
    path: ValuePath;
    onSelectionChanged: (selection: ValuePath) => void;
    onValueChanged: (value?: Value) => void;
}
