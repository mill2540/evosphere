import * as React from "react";

import {
    List,
    ListItem,
    ListItemIcon,
    ListItemText,
    withStyles,
} from "@material-ui/core";

import { Styles, styles } from "../styles";
import { IMenuItem, IMenuSegment } from "./types";

import { store } from "../..";

interface IDrawerMenuItemProps extends Styles, IMenuItem {}

export const DrawerMenuItem = withStyles(styles)(((
    props: IDrawerMenuItemProps,
) => {
    const { title, description, icon, onSelected, disabled } = props;

    return (
        <ListItem
            button
            disabled={disabled || onSelected === undefined}
            onClick={() => {
                if (onSelected) {
                    onSelected(store.dispatch);
                }
            }}
        >
            {icon && <ListItemIcon>{icon}</ListItemIcon>}
            <ListItemText
                primary={title}
                secondary={description && description.join(" ")}
            />
        </ListItem>
    );
}) as React.SFC<IDrawerMenuItemProps>);

interface IDrawerMenuSegmentProps extends Styles, IMenuSegment {}

export const DrawerMenuSegment = withStyles(styles)(((
    props: IDrawerMenuSegmentProps,
) => {
    const { items } = props;

    return (
        <List>
            {items.map((item) => (
                <DrawerMenuItem key={item.id} {...item} />
            ))}
        </List>
    );
}) as React.SFC<IDrawerMenuSegmentProps>);
