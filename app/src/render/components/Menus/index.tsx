import * as React from "react";

import {
    AppBar,
    Divider,
    Drawer,
    IconButton,
    Slide,
    Toolbar,
    Typography,
    withStyles,
} from "@material-ui/core";
import ExpandMore from "@material-ui/icons/ExpandLess";
import MenuIcon from "@material-ui/icons/Menu";

import { connect } from "react-redux";
import { IAppState } from "../../store";
import { Styles, styles } from "../styles";
import { DrawerMenuSegment } from "./DrawerMenuSegment";
import { IAppMenus } from "./types";
import { calculateAppMenu } from "./views";

interface IMenusProps extends Styles, IAppMenus {}

interface IMenusState {
    showDrawer: boolean;
}

function mapStoreToProps(store: IAppState) {
    return {
        ...calculateAppMenu(store.view),
    };
}

export const Menus = connect(mapStoreToProps)(
    withStyles(styles)(
        class extends React.Component<IMenusProps, IMenusState> {
            constructor(props: IMenusProps) {
                super(props);
                this.state = { showDrawer: false };
            }

            public render() {
                const { classes, bar, drawer } = this.props;
                const { showDrawer } = this.state;

                return (
                    <React.Fragment>
                        <AppBar className={classes.appBar} position="fixed">
                            <Toolbar>
                                <Slide
                                    direction="right"
                                    in={drawer !== undefined}
                                    mountOnEnter
                                    unmountOnExit
                                >
                                    <IconButton
                                        className={classes.toggleDrawerButton}
                                        onClick={() => {
                                            this.setState({
                                                showDrawer: !showDrawer,
                                            });
                                        }}
                                    >
                                        {showDrawer ? (
                                            <ExpandMore />
                                        ) : (
                                            <MenuIcon />
                                        )}
                                    </IconButton>
                                </Slide>

                                <Typography color="inherit" variant="h6">
                                    {bar.title}
                                </Typography>
                                {bar.description && (
                                    <Typography
                                        color="inherit"
                                        variant="subtitle1"
                                    >
                                        {bar.description}
                                    </Typography>
                                )}
                            </Toolbar>
                        </AppBar>
                        <Slide
                            direction="right"
                            in={drawer !== undefined && showDrawer}
                            mountOnEnter
                            unmountOnExit
                        >
                            <nav className={classes.appDrawerContainer}>
                                <Drawer
                                    classes={{
                                        docked: classes.drawerRoot,
                                        paper: classes.drawerPaper,
                                    }}
                                    variant="permanent"
                                >
                                    <div className={classes.toolbar} />
                                    {drawer &&
                                        ("begin" in drawer ? (
                                            <React.Fragment>
                                                {drawer.begin.map(
                                                    (menu, idx, array) => (
                                                        <React.Fragment
                                                            key={menu.id}
                                                        >
                                                            <DrawerMenuSegment
                                                                {...menu}
                                                            />
                                                            {(idx <
                                                                array.length -
                                                                    1 ||
                                                                drawer.end) && (
                                                                <Divider />
                                                            )}
                                                        </React.Fragment>
                                                    ),
                                                )}
                                                <div
                                                    className={
                                                        classes.drawerSpacer
                                                    }
                                                />
                                                {drawer.end && (
                                                    <DrawerMenuSegment
                                                        {...drawer.end}
                                                    />
                                                )}
                                            </React.Fragment>
                                        ) : (
                                            drawer()
                                        ))}
                                </Drawer>
                            </nav>
                        </Slide>
                        )
                    </React.Fragment>
                );
            }
        },
    ),
);
