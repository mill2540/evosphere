import { ListItemIconProps } from "@material-ui/core/ListItemIcon";
import { Dispatch } from "redux";

export type MenuItemOnSelected = (dispatch: Dispatch) => void;
export interface IMenuItem {
    id: string;
    title: string;
    description?: string[];
    onSelected?: MenuItemOnSelected;
    icon?: React.ReactElement<ListItemIconProps>;
    disabled?: boolean;
}

export interface IMenuSegment {
    items: IMenuItem[];
}

export interface IMultiMenuSegement extends IMenuSegment {
    id: string;
}

export interface IAppMenus {
    drawer?:
        | {
              begin: IMultiMenuSegement[];
              end?: IMenuSegment;
          }
        | (() => JSX.Element);
    bar: {
        title: string;
        description?: string;
        middle?: IMenuSegment;
        end?: IMenuSegment;
    };
}
