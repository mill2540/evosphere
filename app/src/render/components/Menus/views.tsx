import { store } from "../..";
import { LazyHierarchy } from "../../../common/Hierarchy";
import * as actions from "../../store/views/actions";
import { IEditView, ViewState } from "../../store/views/types";
import { IAppMenus, IMenuItem } from "./types";

const CHANGE_BACKEND: IMenuItem = {
    description: [
        "Selects a new backend to use.",
        "Generally, you should not need to change this, unless you want to connect",
        "to an instance of EvoSphere running remotely",
    ],
    id: "change-backend",
    onSelected: () => {
        store.dispatch(actions.startup());
    },
    title: "Change Backend",
};

const NEW: IMenuItem = {
    description: ["Create a new evosphere experiment"],
    id: "new-experiment",
    onSelected: () => {
        // store.dispatch(actions.)
    },
    title: "New",
};

const SAVE: IMenuItem = {
    description: ["Save the current experiment"],
    id: "save-experiment",
    onSelected: () => {
        // store.dispatch(actions.)
    },
    title: "Save",
};

const OPEN: IMenuItem = {
    description: ["Open a new experiment"],
    id: "open-experiment",
    onSelected: () => {
        store.dispatch(actions.edit({ showOpen: true }));
    },
    title: "Open",
};

const RUN = (edit: IEditView): IMenuItem => ({
    description: ["Runs the current experiment"],
    disabled: store.getState().value.root === undefined,
    id: "run-experiment",
    onSelected: () => {
        const root = store.getState().value.root;
        if (root !== undefined) {
            edit.backend
                .loadExperimentFromJson(root)
                .then((id) => new LazyHierarchy(edit.backend, id))
                .then((experiment) => {
                    store.dispatch(
                        actions.experiment({
                            backend: edit.backend,
                            experiment,
                            kind: "experiment",
                        }),
                    );
                })
                .catch((err) => console.error(err));
        }
    },
    title: "Run",
});

export function calculateAppMenu(view: ViewState): IAppMenus {
    switch (view.kind) {
        case "startup":
            return {
                bar: { title: "EvoSphere" },
            };
        case "edit":
            return {
                bar: { title: view.title },
                drawer: {
                    begin: [
                        {
                            id: "io-menu",
                            items: [NEW, OPEN, SAVE],
                        },
                        {
                            id: "control-menu",
                            items: [RUN(view)],
                        },
                    ],
                    end: {
                        items: [CHANGE_BACKEND],
                    },
                },
            };
        case "experiment":
            return {
                bar: {
                    title: "Running",
                },
            };
    }
}
