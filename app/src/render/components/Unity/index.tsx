import * as React from "react";

import { withStyles } from "@material-ui/core";
import { styles, Styles } from "../styles";

export const Unity = withStyles(styles)(((state: Styles) => {
    const { classes } = state;

    return <div id="gameContainer" className={classes.unity} />;
}) as React.SFC);
