import * as React from "react";

import { Lazy } from "../../../common/Lazy";
import { IViewProps } from "./View";

interface ILazyCtlProps<T, L> {
    readonly lazy: L;
    readonly noAutoload?: boolean;

    readonly onPreload?: (lazy: L) => void;
    readonly preload: React.ComponentType<{ lazy: L }>;

    readonly onLoading?: (lazy: L) => void;
    readonly loading: React.ComponentType<{ lazy: L }>;

    readonly onError?: (message: any, lazy: L) => void;
    readonly error: React.ComponentType<{ message: any; lazy: L }>;

    readonly onReady?: (value: T) => void;
    readonly ready: React.ComponentType<IViewProps<T>>;
}
interface ILazyCtlState<T> {
    readonly status:
        | { readonly name: "preload" }
        | { readonly name: "loading" }
        | { readonly name: "error"; readonly message: any }
        | { readonly name: "ready"; readonly value: T };
}

export class LazyCtl<T, L extends Lazy<T> = Lazy<T>> extends React.Component<
    ILazyCtlProps<T, L>,
    ILazyCtlState<T>
> {
    constructor(props: ILazyCtlProps<T, L>) {
        super(props);

        this.state = { status: { name: "preload" } };
    }

    private get autoload() {
        return this.state.status.name === "preload" && !this.props.noAutoload;
    }

    public init() {
        if (this.autoload) {
            this.loading();

            this.props.lazy
                .value()
                .then(this.ready)
                .catch(this.error);
        }
    }

    public clear(invalidate: boolean = true) {
        const { lazy, onPreload } = this.props;
        if (onPreload !== undefined) {
            onPreload(lazy);
        }

        if (invalidate) {
            lazy.invalidate();
        }

        this.setState({ status: { name: "preload" } });
    }

    public render() {
        const { lazy } = this.props;
        const { status } = this.state;

        switch (status.name) {
            case "error":
                return React.createElement(this.props.error, {
                    lazy,
                    message: status.message,
                });
            case "loading":
                return React.createElement(this.props.loading, {
                    lazy,
                });
            case "preload":
                return React.createElement(this.props.preload, {
                    lazy,
                });
            case "ready":
                return React.createElement(this.props.ready, {
                    value: status.value,
                });
            default:
                throw new Error("Unreachable");
        }
    }

    public componentDidMount() {
        this.init();
    }

    public componentDidUpdate() {
        this.init();
    }

    private error = (message: any) => {
        const { lazy, onError } = this.props;

        if (onError !== undefined) {
            onError(message, lazy);
        }

        this.setState({ status: { name: "error", message } });
    }

    private loading = () => {
        const { lazy, onLoading } = this.props;

        if (onLoading !== undefined) {
            onLoading(lazy);
        }

        this.setState({ status: { name: "loading" } });
    }

    private ready = (value: T) => {
        const { onReady } = this.props;

        if (onReady !== undefined) {
            onReady(value);
        }

        this.setState({ status: { name: "ready", value } });
    }
}
