import * as React from "react";

import { ListItem, ListItemText, Typography } from "@material-ui/core";

interface ILoadingProps {
    title?: string;
    subtitle?: string;
}

export function LoadingListItem({ title, subtitle }: ILoadingProps) {
    if (title === undefined) {
        title = "Loading...";
    }

    return (
        <div>
            <Typography variant="h6">{title}</Typography>
            {subtitle && (
                <Typography variant="subtitle2">{subtitle}</Typography>
            )}
        </div>
    );
}
