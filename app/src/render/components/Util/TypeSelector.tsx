import * as React from "react";

import {
    Button,
    List,
    ListItem,
    ListItemSecondaryAction,
    ListItemText,
    Typography,
    withStyles,
} from "@material-ui/core";

import { Type } from "../../../common/Type";
import { Styles, styles } from "../styles";

interface ITypeSelectorProps extends Styles {
    autoselect?: boolean;
    selectedType?: Type;
    types: ReadonlyArray<Type>;
    onSelect: (type: Type) => void;
}

interface ITypeSelectorState {
    expandedType?: Type;
}

export const TypeSelector = withStyles(styles)(
    class extends React.Component<ITypeSelectorProps, ITypeSelectorState> {
        constructor(props: ITypeSelectorProps) {
            super(props);

            this.state = { expandedType: props.selectedType };
        }

        public componentDidMount() {
            const { autoselect, selectedType, types, onSelect } = this.props;
            if (
                autoselect &&
                types.length === 1 &&
                selectedType === undefined
            ) {
                onSelect(types[0]);
            }
        }

        public render() {
            const { selectedType, types, onSelect } = this.props;

            return (
                <List>
                    {types.map((type) => (
                        <ListItem key={type.name}>
                            <ListItemText>
                                <Typography variant="h6">
                                    {type.shortName}
                                </Typography>
                                <Typography variant="caption">
                                    {type.documentation}
                                </Typography>
                            </ListItemText>
                            <ListItemSecondaryAction>
                                <Button
                                    size="small"
                                    color="primary"
                                    disabled={selectedType === type}
                                    onClick={() => onSelect(type)}
                                >
                                    Use
                                </Button>
                            </ListItemSecondaryAction>
                        </ListItem>
                    ))}
                </List>
            );
        }
    },
);
