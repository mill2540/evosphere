import * as React from "react";

import {
    Button,
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    withStyles,
} from "@material-ui/core";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IAppState } from "../../../store";

import { IEditView } from "../../../store/views/types";
import { Styles, styles } from "../../styles";

import { Backend } from "../../../../common/Backend";
import { Value } from "../../../../common/Value";
import * as valueActions from "../../../store/value/actions";
import * as viewActions from "../../../store/views/actions";
import { ValuePath } from "../../../ValuePath";

interface IOpenProps extends Styles {
    backend: Backend;
    show: boolean;
    onOpen: (root: Value) => void;
    onCancel: () => void;
}
interface IOpenState {
    file: undefined | File;
}

function mapStateToProps(state: IAppState) {
    const view = state.view as IEditView;
    return {
        backend: view.backend,
        show: view.showOpen,
    };
}

function mapDisptachToProps(dispatch: Dispatch) {
    return {
        onCancel: () => {
            dispatch(viewActions.edit({ showOpen: false }));
        },
        onOpen: (root: Value) => {
            dispatch(
                viewActions.edit({
                    selection: new ValuePath(),
                    showOpen: false,
                }),
            );
            dispatch(
                valueActions.update({
                    path: new ValuePath(),
                    update: root,
                }),
            );
        },
    };
}

export const Open = connect(
    mapStateToProps,
    mapDisptachToProps,
)(
    withStyles(styles)(
        class extends React.Component<IOpenProps, IOpenState> {
            constructor(props: IOpenProps) {
                super(props);
                this.state = { file: undefined };
            }

            public render() {
                const { show, onOpen, onCancel, backend } = this.props;
                const { file } = this.state;

                return (
                    <Dialog open={show}>
                        <DialogTitle>Open</DialogTitle>
                        <DialogContent>
                            <DialogContentText>
                                Select a file to open
                            </DialogContentText>

                            <input
                                type="file"
                                onChange={(e) => {
                                    if (e.target.files && e.target.files[0]) {
                                        this.setState({
                                            file: e.target.files[0],
                                        });
                                    }
                                }}
                            />
                        </DialogContent>
                        <DialogActions>
                            <Button
                                color="primary"
                                onClick={() => {
                                    this.setState({ file: undefined });
                                    onCancel();
                                }}
                            >
                                Cancel
                            </Button>
                            <Button
                                color="primary"
                                disabled={file === undefined}
                                onClick={() => {
                                    const reader = new FileReader();

                                    reader.onload = () => {
                                        backend
                                            .yamlToJson(reader.result as string)
                                            .then((value) => {
                                                onOpen(value);
                                            });
                                    };

                                    reader.readAsText(file);

                                    this.setState({ file: undefined });
                                }}
                            >
                                Open
                            </Button>
                        </DialogActions>
                    </Dialog>
                );
            }
        },
    ),
);
