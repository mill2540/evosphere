import * as React from "react";

import { Grid, Typography, withStyles } from "@material-ui/core";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { IAppState } from "../../../store";

import { IEditView } from "../../../store/views/types";
import { ValuePath } from "../../../ValuePath";
import { Styles, styles } from "../../styles";

import { Type } from "../../../../common/Type";
import { Value } from "../../../../common/Value";
import * as valueActions from "../../../store/value/actions";
import * as viewActions from "../../../store/views/actions";
import { ValueEditor } from "../../Editor";
import { Open } from "./Open";

interface IEditProps extends Styles {
    type: Type;
    value?: Value;
    selection: ValuePath;
    showOpen: boolean;
    onValueChanged: (path: ValuePath, value: Value) => void;
}

function mapStateToProps(state: IAppState) {
    const view = state.view as IEditView;
    const { value, expected } = view.selection.get(
        state.value.root,
        view.experimentType
    );
    return {
        ...view,
        selection: view.selection,
        type: expected,
        value,
    };
}

function mapDisptachToProps(dispatch: Dispatch) {
    return {
        onValueChanged: (path: ValuePath, update: Value) => {
            dispatch(valueActions.update({ path, update }));
        },
    };
}

export const Edit = connect(
    mapStateToProps,
    mapDisptachToProps
)(
    withStyles(styles)(((props: IEditProps) => {
        const { type, value, onValueChanged, selection } = props;

        const title =
            value !== undefined ? value.type.shortName : type.shortName;
        const documentation =
            value !== undefined ? value.type.documentation : type.documentation;

        return (
            <React.Fragment>
                <Grid container direction="row" spacing={16}>
                    <Grid item xs>
                        <Typography variant="h6">TODO: TreeView</Typography>
                    </Grid>

                    <Grid item xs={6}>
                        <ValueEditor
                            path={selection}
                            title={title}
                            documentation={documentation}
                            value={value}
                            expectedType={type}
                            onValueChanged={(newValue) => {
                                onValueChanged(selection, newValue);
                            }}
                        />
                    </Grid>
                    <Grid item xs />
                </Grid>
                <Open />
            </React.Fragment>
        );
    }) as React.SFC)
);
