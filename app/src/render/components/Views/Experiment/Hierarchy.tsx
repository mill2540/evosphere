import * as React from "react";

import {
    Button,
    IconButton,
    Table,
    TableBody,
    TableCell,
    TableRow,
    Tooltip,
    Typography,
    withStyles,
} from "@material-ui/core";

import RefreshIcon from "@material-ui/icons/Refresh";

import { Export, Hierarchy, LazyHierarchy } from "../../../../common/Hierarchy";
import { Value } from "../../../../common/Value";
import { Editors } from "../../Editor/editors";
import { styles } from "../../styles";

interface IExportedViewProps {
    exported: Export;
}

interface IExportedViewState {
    refreshing: boolean;
    value: object | undefined;
}

export const ExportView = withStyles(styles)(
    class ExportViewComponent extends React.Component<
        IExportedViewProps,
        IExportedViewState
    > {
        constructor(props: IExportedViewProps) {
            super(props);
            this.state = { value: undefined, refreshing: false };
        }

        public render() {
            const { exported } = this.props;
            const { value, refreshing } = this.state;

            const action =
                exported.parameters.length === 0 ? (
                    <IconButton onClick={this.refresh}>
                        <RefreshIcon />
                    </IconButton>
                ) : (
                    <Typography color="error">
                        Arguments are not supported
                    </Typography>
                );

            let valueView;
            if (value !== undefined) {
                valueView = JSON.stringify(value);
            }

            return (
                <React.Fragment>
                    <TableCell> {exported.name} </TableCell>
                    <TableCell> {action} </TableCell>
                    <TableCell>
                        {value !== undefined
                            ? valueView
                            : refreshing
                                ? "loading..."
                                : "needs refresh"}
                    </TableCell>
                </React.Fragment>
            );
        }

        private refresh = () => {
            this.setState({ refreshing: true });
            this.props.exported
                .call()
                .then((value) => this.setState({ value, refreshing: false }))
                .catch((err) => {
                    this.setState({ value: undefined, refreshing: false });
                    console.log(err);
                });
        }
    }
);

interface IHierarchyLinkProps {
    target: Hierarchy | LazyHierarchy;
}

const HierarchyLink: React.SFC<IHierarchyLinkProps> = ({
    target,
}: IHierarchyLinkProps) => {
    const [title, subtitle] =
        target instanceof LazyHierarchy
            ? [target.id, undefined]
            : [target.name, target.id];

    const btn = <Button>{title}</Button>;
    return subtitle ? <Tooltip title={subtitle}>{btn}</Tooltip> : btn;
};

interface IHierarchyViewProps {
    value: Hierarchy;
}

export function HierarchyView({ value }: IHierarchyViewProps) {
    return (
        <React.Fragment>
            <HierarchyLink target={value} />
            <Table>
                <TableBody>
                    {value.exports.map((exported) => (
                        <TableRow
                            key={`${exported.name}${exported.parameters.map(
                                (lazy) => lazy.name
                            )}${exported.returnType.name}`}
                        >
                            <ExportView exported={exported} />
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </React.Fragment>
    );
}
