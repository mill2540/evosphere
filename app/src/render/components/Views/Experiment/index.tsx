import * as React from "react";

import { Typography, withStyles } from "@material-ui/core";

import { connect } from "react-redux";
import { LazyHierarchy } from "../../../../common/Hierarchy";
import { IAppState } from "../../../store";
import { IExperimentView } from "../../../store/views/types";
import { Styles, styles } from "../../styles";
import { LazyCtl } from "../../Util/LazyCtl";
import { HierarchyView } from "./Hierarchy";

interface IExperimentProps extends Styles, IExperimentView {}

function mapStateToProps(state: IAppState): Partial<IExperimentProps> {
    const view = state.view as IExperimentView;
    return {
        ...view,
    };
}

function mapDisptachToProps() {
    return {};
}

export const Experiment = connect(
    mapStateToProps,
    mapDisptachToProps
)(
    withStyles(styles)(((props: IExperimentProps) => {
        const { experiment } = props;

        const loading = ({ lazy }: { lazy: LazyHierarchy }): any => (
            <React.Fragment>
                <Typography variant="h6">Loading...</Typography>
                <Typography variant="subtitle2">{lazy.id}</Typography>
            </React.Fragment>
        );
        const error = ({
            message,
            lazy,
        }: {
            message: any;
            lazy: LazyHierarchy;
        }): any => (
            <React.Fragment>
                <Typography variant="h6">{`${message}`}</Typography>
                <Typography variant="subtitle2">{lazy.id}</Typography>
            </React.Fragment>
        );

        return (
            <LazyCtl
                lazy={experiment}
                preload={loading}
                loading={loading}
                error={error}
                ready={HierarchyView}
            />
        );
    }) as React.SFC)
);
