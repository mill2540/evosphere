import * as React from "react";

import { withStyles } from "@material-ui/core";

import { connect } from "react-redux";
import { Dispatch } from "redux";
import { Backend, QueryReply } from "../../../common/Backend";
import { LazyHierarchy } from "../../../common/Hierarchy";
import { Type } from "../../../common/Type";
import * as actions from "../../store/views/actions";
import { ValuePath } from "../../ValuePath";
import { BackendChooserCard, OnBackendReady } from "../Backend";
import { Styles, styles } from "../styles";

export type Ready = { backend: Backend } & (
    | { experimentType: Type }
    | { experiment: LazyHierarchy });

export type OnReady = (ready: Ready) => void;

interface IStartupProps extends Styles {
    onReady: OnReady;
}

function mapStateToProps() {
    return {};
}

function mapDisptachToProps(dispatch: Dispatch) {
    return {
        onReady: (ready: Ready) => {
            if ("experimentType" in ready) {
                dispatch(
                    actions.edit({
                        kind: "edit",
                        selection: new ValuePath(),
                        showOpen: false,
                        title: "Untitled",
                        ...ready,
                    }),
                );
            } else {
                dispatch(
                    actions.experiment({
                        kind: "experiment",
                        ...ready,
                    }),
                );
            }
        },
    };
}

export const Startup = connect(
    mapStateToProps,
    mapDisptachToProps,
)(
    withStyles(styles)(((props: IStartupProps) => {
        const { onReady } = props;

        return (
            <BackendChooserCard
                onBackendReady={(backend) => {
                    backend
                        .call("ExperimentId", [])
                        .then(QueryReply.unwrap)
                        .then((experimentId: string) => {
                            if (experimentId !== "") {
                                onReady({
                                    backend,
                                    experiment: backend.getHierarchy(
                                        experimentId,
                                    ),
                                });
                            } else {
                                return backend
                                    .getType("IExperimentFactory")
                                    .then((experimentType) => {
                                        onReady({
                                            backend,
                                            experimentType,
                                        });
                                    })
                                    .catch((error) => {
                                        console.log(error);
                                    });
                            }
                        });
                }}
            />
        );
    }) as React.SFC),
);
