import * as React from "react";

import { Grid, withStyles } from "@material-ui/core";

import { connect } from "react-redux";
import { IAppState } from "../../store";
import { ViewState } from "../../store/views/types";
import { Styles, styles } from "../styles";
import { Edit } from "./Edit";
import { Experiment } from "./Experiment";
import { Startup } from "./Startup";

interface IViewsProps extends Styles {
    view: ViewState;
}

const VIEWS = {
    edit: () => <Edit />,
    experiment: () => <Experiment />,
    startup: () => <Startup />,
};

export const Views = connect((state: IAppState) => ({
    view: state.view,
}))(
    withStyles(styles)(
        class extends React.Component<IViewsProps> {
            public render() {
                const { classes, view } = this.props;

                return (
                    <div className={classes.views}>{VIEWS[view.kind]()}</div>
                );
            }
        },
    ),
);
