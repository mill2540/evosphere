import { createStyles, Theme, WithStyles, withStyles } from "@material-ui/core";

const MIN_DRAWER_WIDTH = 240;

export const styles = (theme: Theme) =>
    createStyles({
        appBar: {
            zIndex: theme.zIndex.drawer + 1,
        },
        appDrawerContainer: {
            display: "flex",
            minHeight: "100%",
            zIndex: theme.zIndex.drawer,
        },
        center: {
            alignItems: "center",
            display: "flex",
            justifyContent: "center",
            minHeight: "100%",
            minWidth: "100%",
        },
        content: {
            alignItems: "center",
            backgroundColor: theme.palette.background.default,
            display: "flex",
            flexDirection: "column",
            justifyContent: "flex-start",
            maxHeight: "100vh",
            maxWidth: "100%",
            minHeight: "100vh",
            overflow: "auto",
            position: "fixed",
            width: "100%",
            // zIndex: theme.zIndex.drawer - 1,
        },
        drawerPaper: {
            display: "flex",
            flexDirection: "column",
            maxWidth: "15%",
            minHeight: "100%",
            minWidth: MIN_DRAWER_WIDTH,
            position: "relative",
        },
        drawerRoot: {
            width: 0,
        },
        drawerSpacer: {
            flexGrow: 999,
        },
        loadingContent: {
            display: "grid",
        },
        loadingSection: {
            alignItems: "center",
            display: "flex",
            flexDirection: "column",
            gridColumn: 1,
            gridRow: 1,
            justifyContent: "center",
        },
        root: {
            display: "flex",
            flexGrow: 1,
            height: "100vh",
            overflow: "hidden",
            position: "relative",
            width: "100vw",
            zIndex: 1,
        },
        scrollbox: {
            overflow: "auto",
        },
        toggleDrawerButton: {
            marginLeft: -12,
            marginRight: 20,
        },
        toolbar: theme.mixins.toolbar,
        paperRoot: {
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            ...theme.mixins.gutters(),
            paddingBottom: theme.spacing.unit * 2,
            paddingTop: theme.spacing.unit * 2,
            width: "100%",
        },
        unity: {
            height: "100%",
            left: 0,
            padding: theme.spacing.unit * 3,
            position: "fixed",
            right: 0,
            width: "100%",
            zIndex: -1,
        },
        flexRow: {
            display: "flex",
            flexDirection: "row",
            flexWrap: "nowrap",
            justifyItems: "center",
        },
        flexAlignSelfBegin: {
            alignSelf: "flex-begin",
        },
        flexAlignSelfEnd: {
            alignSelf: "flex-end",
        },
        flexAlignSelfCenter: {
            alignSelf: "center",
        },
        input: {
            width: "100%",
        },
        valueWellPaper: {
            display: "flex",
            ...theme.mixins.gutters(),
            paddingBottom: theme.spacing.unit * 2,
            paddingTop: theme.spacing.unit * 2,
        },
        valueWellCard: {
            width: "100%",
        },
        sublist: {
            paddingLeft: theme.spacing.unit * 4,
        },
        views: {
            maxHeight: "100%",
            minHeight: "100%",
            minWidth: "100vw",
            ...theme.mixins.gutters(),
        },
    });

export type Styles = WithStyles<typeof styles>;
