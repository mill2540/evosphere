import * as React from "react";
import * as ReactDOM from "react-dom";
import { Provider } from "react-redux";
import { createStore } from "redux";
import { App } from "./components/App";
import { reducer } from "./store";

export const store = createStore(
    reducer,
    // TODO: disable if not dev?
    (window as any).__REDUX_DEVTOOLS_EXTENSION__ &&
        (window as any).__REDUX_DEVTOOLS_EXTENSION__(),
);

const app = document.createElement("div");
document.body.appendChild(app);

ReactDOM.render(
    <Provider store={store}>
        <App />
    </Provider>,
    app,
);
