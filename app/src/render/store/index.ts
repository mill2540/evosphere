import { combineReducers } from "redux";

import { value } from "./value/reducer";
import { IValueState } from "./value/types";
import { view } from "./views/reducer";
import { ViewState } from "./views/types";

export interface IAppState {
    view: ViewState;
    value: IValueState;
}

export const reducer = combineReducers<IAppState>({
    value,
    view,
});
