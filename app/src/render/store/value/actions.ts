import { createAction } from "typesafe-actions";
import { Value } from "../../../common/Value";
import { ValuePath } from "../../ValuePath";

export const update = createAction("@evosphere/value/update", (resolve) => {
    return (payload: { path: ValuePath; update?: Value }) => resolve(payload);
});
