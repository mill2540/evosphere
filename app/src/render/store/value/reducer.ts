import { getType } from "typesafe-actions";
import * as actions from "./actions";
import { IValueState, ValueAction } from "./types";

export function value(
    state: IValueState = {},
    action: ValueAction,
): IValueState {
    switch (action.type) {
        case getType(actions.update):
            const { path, update } = action.payload;
            const { root } = state;

            const newState = { ...state, root: path.replace(root, update) };
            return newState;
        default:
            return state;
    }
}
