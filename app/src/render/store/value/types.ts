import { ActionType } from "typesafe-actions";

import { Value } from "../../../common/Value";
import * as actions from "./actions";

export type ValueAction = ActionType<typeof actions>;

export interface IValueState {
    root?: Value;
}
