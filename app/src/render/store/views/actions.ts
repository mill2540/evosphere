import { createAction } from "typesafe-actions";
import {
    IEditView,
    IEditViewData,
    IExperimentView,
    IExperimentViewData,
} from "./types";

export const edit = createAction("@evosphere/views/edit", (resolve) => {
    return (state: Partial<IEditViewData> | IEditView) => resolve(state);
});

export const startup = createAction("@evosphere/views/startup", (resolve) => {
    return () => resolve();
});

export const experiment = createAction(
    "@evosphere/views/experiment",
    (resolve) => {
        return (state: Partial<IExperimentViewData> | IExperimentView) =>
            resolve(state);
    },
);
