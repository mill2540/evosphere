import { getType } from "typesafe-actions";
import * as actions from "./actions";
import { IExperimentView, UiAction, ViewState } from "./types";

export function view(
    state: ViewState = ViewState.STARTUP,
    action: UiAction,
): ViewState {
    switch (action.type) {
        case getType(actions.edit): {
            const newState =
                "kind" in action.payload
                    ? action.payload
                    : state.kind === "edit"
                        ? { ...state, ...action.payload }
                        : undefined;

            if (newState === undefined) {
                throw new Error("Invalid State");
            }

            return newState;
        }
        case getType(actions.startup): {
            return ViewState.STARTUP;
        }
        case getType(actions.experiment): {
            const newState =
                "kind" in action.payload
                    ? action.payload
                    : state.kind === "experiment"
                        ? { ...state, ...action.payload }
                        : undefined;

            if (newState === undefined) {
                throw new Error("Invalid State");
            }

            return newState;
        }
        default:
            return state;
    }
}
