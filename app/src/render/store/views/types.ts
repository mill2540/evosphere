import { ActionType } from "typesafe-actions";

import * as actions from "./actions";

import { Backend } from "../../../common/Backend";
import { LazyHierarchy } from "../../../common/Hierarchy";
import { Type } from "../../../common/Type";
import { ValuePath } from "../../ValuePath";

export type UiAction = ActionType<typeof actions>;

export interface IStartupView {
    kind: "startup";
}

export interface IEditViewData {
    title: string;
    selection: ValuePath;
    showOpen: boolean;
    backend: Backend;
    experimentType: Type;
}

export interface IEditView extends IEditViewData {
    kind: "edit";
}

export interface IExperimentViewData {
    backend: Backend;
    experiment: LazyHierarchy;
}

export interface IExperimentView extends IExperimentViewData {
    kind: "experiment";
}

export type ViewState = IStartupView | IEditView | IExperimentView;
export const ViewState = {
    STARTUP: { kind: "startup" } as IStartupView,
    // edit(state: IEditView): IEditView;
    edit(state: IEditViewData): IEditView {
        return { kind: "edit", ...state };
    },
    experiment(state: IExperimentView): IExperimentView {
        return { kind: "experiment", ...state };
    },
};
