import * as ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";
import * as HtmlWebpackPlugin from "html-webpack-plugin";
import * as path from "path";
import {
    Configuration,
    HotModuleReplacementPlugin,
    NamedModulesPlugin,
} from "webpack";
import "webpack-dev-server";

const DIST = path.resolve(__dirname, "..", "dist");

const config: Configuration = {
    devServer: {
        compress: true,
        contentBase: [DIST],
        hot: true,
    },
    devtool: "source-map",
    entry: [
        "@babel/polyfill",
        path.resolve(__dirname, "src", "render", "index.tsx"),
    ],
    mode: "development",
    module: {
        rules: [
            {
                exclude: /node_modules/,
                loader: "babel-loader",
                test: /\.(t|j)sx?$/,
                options: {
                    babelrc: false,
                    cacheDirectory: true,
                    plugins: [
                        "@babel/proposal-class-properties",
                        "@babel/proposal-object-rest-spread",
                        "react-hot-loader/babel",
                    ],
                    presets: [
                        "@babel/preset-env",
                        "@babel/preset-typescript",
                        "@babel/preset-react",
                    ],
                },
            },
            // Better source map loading
            {
                enforce: "pre",
                exclude: path.resolve(__dirname, "node_modules"),
                loader: "source-map-loader",
                test: /\.js$/,
            },
        ],
    },
    output: {
        filename: "[name].[hash].js",
        path: DIST,
    },
    plugins: [
        new HtmlWebpackPlugin({ title: "EvoSphere" }),
        new ForkTsCheckerWebpackPlugin(),
        new HotModuleReplacementPlugin(),
    ],
    resolve: { extensions: [".ts", ".tsx", ".js", ".jsx"] },
    target: "web",
};

export default config;
