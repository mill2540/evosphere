#!/usr/bin/env node

const fs = require("fs");
const os = require("os");
const path = require("path");
const { spawn } = require("child_process");

function run_script(script, args) {
    const options = { shell: process.platform === "win32" };

    const proc = spawn(script, args, options);

    return new Promise((resolve, reject) => {
        let stdout = "";
        let stderr = "";

        proc.stdout.on("data", (data) => (stdout += data));
        proc.stderr.on("data", (data) => (stderr += data));

        proc.on("close", (code) => {
            resolve([code, stdout.trim(), stderr.trim()]);
        });

        proc.on("error", () => reject());
    });
}

async function get_unity_path() {
    if (process.platform === "win32") {
        return "unity";
    }
    if (process.platform === "darwin") {
        return "/Applications/Unity/Unity.app/Contents/MacOS/Unity";
    }

    const names = ["unity-editor-beta", "unity-editor", "Unity"];
    for (const name of names) {
        const [error, stdout] = await run_script("which", [name]);
        if (error === 0) {
            return stdout;
        }
    }
}

async function create_unity_logfile() {
    const tmpdir = os.tmpdir();
    const tmp = path.join(tmpdir, "unitylog.txt");
    return new Promise((resolve, reject) => {
        fs.open(tmp, "w", (err) => {
            if (err) {
                reject(err);
            } else {
                resolve(tmp);
            }
        });
    });
}

async function call_unity(projectPath, executeMethod, dest) {
    const unityExec = await get_unity_path();
    const logfile = await create_unity_logfile();

    // https://stackoverflow.com/questions/11225001/reading-a-file-in-real-time-using-node-js

    const logState = { build_in_progress: true, output_echoed_position: 0 };
    let buildInProgress = true;
    function echo_log(fd, ls) {
        if (!ls.build_in_progress) {
            return;
        }

        const stat = fs.fstatSync(fd);
        if (stat.size <= ls.output_echoed_position) {
            setTimeout(() => echo_log(fd, ls), 10);
        } else {
            const size = stat.size - ls.output_echoed_position;
            fs.read(
                fd,
                new Buffer(size),
                0,
                size,
                ls.output_echoed_position,
                (error, len, data) => {
                    ls.output_echoed_position += len;

                    process.stdout.write(data, () => {
                        setTimeout(() => echo_log(fd, ls), 10);
                    });
                },
            );
        }
    }

    fs.open(logfile, "r", (err, fd) => echo_log(fd, logState));

    const cmd = spawn(
        unityExec,
        [
            "-quit",
            "-batchmode",
            "-executeMethod",
            executeMethod,
            "-path",
            dest,
            "-projectPath",
            projectPath,
            "-logFile",
            logfile,
        ],
        { shell: process.platform === "win32" },
    );

    return new Promise((resolve, reject) => {
        cmd.on("exit", () => {
            logState.build_in_progress = false;
            resolve();
        });
    });
}

function file_exists(fpath) {
    return new Promise((resolve) => {
        fs.exists(fpath, resolve);
    });
}

function get_stats(fpath) {
    return new Promise((resolve, reject) => {
        fs.stat(fpath, (err, stats) => {
            if (err) {
                reject(err);
            } else {
                resolve(stats);
            }
        });
    });
}

function readdir(fpath) {
    return new Promise((resolve, reject) => {
        fs.readdir(fpath, (err, files) => {
            if (err) {
                reject(err);
            } else {
                resolve(files);
            }
        });
    });
}

async function get_last_modified(fpath) {
    let last_modified = 0;
    let paths = [fpath];

    while (paths.length !== 0) {
        const subpath = paths.pop();
        const stats = await get_stats(subpath);
        if (stats.isDirectory()) {
            const path = require("path");
            const children = await readdir(subpath);
            paths = paths.concat(
                children.map((child) => path.join(subpath, child)),
            );
        } else {
            last_modified = Math.max(stats.mtimeMs, last_modified);
        }
    }
    return last_modified;
}

async function needs_rebuild(project, dist) {
    const dist_exists = await file_exists(dist);
    if (!dist_exists) {
        return true;
    }

    const project_mtime = await get_last_modified(project);
    const dist_mtime = await get_last_modified(dist);

    return project_mtime > dist_mtime;
}

const [target] = process.argv.slice(2);
const engine_path = path.join(process.cwd(), "engine");
const dist_path = path.join(process.cwd(), "dist");
const assets_path = path.join(engine_path, "Assets");

if (target === "native") {
    const name = `native-${process.platform}-${process.arch}`;
    const native_dist_path = path.join(dist_path, name);
    const native_dist_bin = path.join(native_dist_path, name);

    needs_rebuild(assets_path, native_dist_path).then((needed) => {
        if (needed) {
            call_unity(engine_path, "NativeBuild.Build", native_dist_bin);
        } else {
            console.log("Native build build is up to date");
        }
    });
} else if (target === "web") {
    const web_dist_path = path.join(dist_path, "unity-web");
    needs_rebuild(assets_path, web_dist_path).then((needed) => {
        if (needed) {
            call_unity(engine_path, "WebBuild.Build", web_dist_path);
        } else {
            console.log("Web build is up to date");
        }
    });
} else if (target) {
    console.log(
        `"${target} is not a supported command. Please specify one of "web" or "native"`,
    );
} else if (!target || target === "-h") {
    console.log(`Please specify one of "web" or "native"`);
}
