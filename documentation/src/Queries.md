# Queries

One of the most important parts of the EvoSphere engine is the query engine. It is what allows the GUI to have things like a live experiment hierarchy for ANY kind of experiment that anyone can define. In principal, it can even be used to auto generate a website from a running simulation which can be queried live.

## How queries are used

The query system is implemented mostly via the `Queryable` base class. All components of an EvoSphere experiment should have this class as one of their parents. If an object is descended from `Queryable` then its members can be exposed with the `[Expose]` attribute, which marks them as accessible through the query interface.

## Example Queries

* `**/Bob`: Ask the experiment for all objects named Bob
* `**/Bob:Robot`: Ask the experiment for all objects named Bob of type Robot
* `**:Robot`: Ask the experiment for all objects type Robot
* `*/*`: Ask for the first child of the experiment
* `**`: Ask for everything. You don't want this.
* `#uuid`: Ask for an element by its id. You should use this whenever you can, as it is an O(1) operation.
* `**/#uuid`: This is _not_ a valid query, because if you know the uuid of an object you would always just ask for it that way. 
* However, `#uuid/**` is legal and asks for all the children of `#uuid` 

