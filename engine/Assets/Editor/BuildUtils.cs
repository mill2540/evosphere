﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public static class BuildUtils
{

    public static bool GetFlag(string flag)
    {
        return System.Environment.GetCommandLineArgs().Contains(flag);
    }

    public static string GetArg(string name)
    {
        var args = System.Environment.GetCommandLineArgs().SkipWhile(arg => !arg.Equals(name)).GetEnumerator();
        if (args.MoveNext() && args.MoveNext()) return args.Current;
        return null;
    }
}
