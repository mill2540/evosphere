﻿using UnityEditor;
using UnityEngine;

public static class NativeBuild
{
    [MenuItem("EvoSphere/Build Native Target")]
    public static void Build()
    {
        var headless = BuildUtils.GetFlag("-headless");
        Build(headless);
    }

    [MenuItem("EvoSphere/Build Native Target (Headless)")]
    public static void BuildHeadless()
    {
        Build(true);
    }

    public static void Build(bool headless)
    {
        var destPath = BuildUtils.GetArg("-destPath");
        if (destPath == null)
        {
            destPath = EditorUtility.SaveFilePanel("Select build folder", "native-build", "native-build", "");
        }

        Debug.Log(string.Format("Native Build: {0}", destPath));

        BuildTarget target = BuildTarget.StandaloneWindows64;
        if (SystemInfo.operatingSystemFamily == OperatingSystemFamily.Linux)
        {
            target = BuildTarget.StandaloneLinux64;
        }

        BuildOptions options = BuildOptions.StrictMode;
        if (headless) options |= BuildOptions.EnableHeadlessMode;

        BuildPipeline.BuildPlayer(
            new [] { "Assets/Boot.unity" },
            destPath,
            target,
            options
        );
    }
}