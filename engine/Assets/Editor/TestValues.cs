﻿// using UnityEngine;
// using UnityEditor;
// using System.Linq;
// using Newtonsoft.Json.Linq;
// using YamlDotNet.RepresentationModel;
// using NUnit.Framework;

// public struct Struct
// {
//     public string StructField { get; private set; }
//     public string field2;

//     public Struct(string structField) { StructField = structField; field2 = "filed2"; }
//     public Struct(string structField, string field2) { StructField = structField; this.field2 = field2; }

//     public override string ToString()
//     {
//         return string.Format("[Struct: StructField={0}]", StructField);
//     }
// }

// [Register]
// public class MiniClass
// {
//     [Config.Value]
//     public string myValue;
// }

// [Register]
// public class HelloWorld
// {
//     [Config.Value]
//     public string hello;

//     [Config.Value]
//     public MiniClass mini;

//     [Config.Value]
//     public int[] positions;

//     [Config.Value]
//     public int[][] positions2d;

//     [Config.Value]
//     public Struct testStruct = new Struct("tiny!");

//     [Config.Value]
//     public Vector3 unity;

//     public override string ToString()
//     {
//         return string.Format("{{hello=\"{0}\", positions={1}, positions2d={2}, testStruct={3}, unity={4}}}",
//                              hello,
//                              ArrayToString(positions),
//                              ArrayToString(positions2d),
//                              testStruct, unity);
//     }

//     public static string ArrayToString<T>(T[] array)
//     {
//         if (array == null) return "[]";
//         return "[" + string.Join(", ", array.Select(pos => pos.ToString()).ToArray()) + "]";
//     }

//     public static string ArrayToString<T>(T[][] array)
//     {
//         if (array == null) return "[]";
//         return "[" + string.Join(", ", array.Select(pos => ArrayToString(pos)).ToArray()) + "]";
//     }
// }

// [Register]
// public class HelloWorldSubClass : HelloWorld
// {
//     [Config.Value]
//     public string subParameter;
// }

// public class TestValues
// {

//     [Test]
//     public void TestObjectFromYaml()
//     {
//         var repr = Values
//             .ValueFactory
//             .FromYaml(typeof(HelloWorld), Config.Yaml.LoadFromString(@"!HelloWorld
//     hello: Hello"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld().ToString());
//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello"
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromYamlWithUnityType()
//     {
//         var repr = Values
//             .ValueFactory
//             .FromYaml(typeof(HelloWorld), Config.Yaml.LoadFromString(@"!HelloWorld
//     hello: Hello
//     unity: [1, 2]"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld().ToString());
//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello",
//             unity = new Vector3(1, 2)
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromYamlWithArray()
//     {
//         var repr = Values
//             .ValueFactory
//             .FromYaml(typeof(HelloWorld), Config.Yaml.LoadFromString(@"!HelloWorld
//     hello: Hello
//     positions: [1, 2]"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld().ToString());
//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello",
//             positions = new[] { 1, 2 }
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromYamlWithArray2d()
//     {
//         var repr = Values
//             .ValueFactory
//             .FromYaml(typeof(HelloWorld), Config.Yaml.LoadFromString(@"!HelloWorld
//     hello: Hello
//     positions2d: [[1, 2], [3]]"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld().ToString());
//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello",
//             positions2d = new[] { new[] { 1, 2 }, new[] { 3 } }
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromYamlWithConstructor()
//     {
//         var repr = Values
//             .ValueFactory
//             .FromYaml(typeof(HelloWorld), Config.Yaml.LoadFromString(@"!HelloWorld
//     hello: Hello
//     testStruct: [hello]"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld().ToString());
//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello",
//             testStruct = new Struct("hello")
//         }.ToString());
//     }

//     [Test]
//     public void TestEmptyObjectFromJson()
//     {
//         var repr = Values.ValueFactory.FromJson(JObject.Parse(@"{
// 			type: 'HelloWorld',
// 			members: { }
// 		}"));

//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld().ToString());
//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello"
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromJsonWithStringValueMember()
//     {
//         var repr = Values.ValueFactory.FromJson(JObject.Parse(@"{
// 			type: 'HelloWorld',
// 			members: {
// 				hello: {
// 					type: 'System.String',
// 					value: 'Hello'
// 				}
// 			}
// 		}"));

//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello"
//         }.ToString());

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             hello = "Hello2"
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromJsonWithEmptyArrayMember()
//     {
//         var repr = Values.ValueFactory.FromJson(JObject.Parse(@"{
// 			type: 'HelloWorld',
// 			members: {
// 				positions: {
// 					type: 'System.Int32[]',
// 					values: []
// 				}
// 			}
// 		}"));

//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions = new int[0]
//         }.ToString());

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions = new[] { 1 }
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromJsonWithArrayMember()
//     {
//         var repr = Values.ValueFactory.FromJson(JObject.Parse(@"{
// 			type: 'HelloWorld',
// 			members: {
// 				positions: {
// 					type: 'System.Int32[]',
// 					values: [{
// 						type: 'System.Int32',
// 						value: 1
// 					}]
// 				}
// 			}
// 		}"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions = new int[0]
//         }.ToString());

//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions = new[] { 1 }
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromJsonWithConstructor()
//     {
//         var repr = Values.ValueFactory.FromJson(JObject.Parse(@"{
// 			type: 'HelloWorld',
// 			members: {
// 				testStruct: {
// 					type: 'Struct',
// 					parameters: [{
// 						type: 'System.String',
// 						value: 'Hello World'
// 					}]
// 				}
// 			}
// 		}"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions = new int[0]
//         }.ToString());

//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             testStruct = new Struct("Hello World")
//         }.ToString());
//     }

//     [Test]
//     public void TestObjectFromJsonWithArrayMember2d()
//     {
//         var repr = Values.ValueFactory.FromJson(JObject.Parse(@"{
// 			type: 'HelloWorld',
// 			members: {
// 				positions2d: {
// 					type: 'System.Int32[][]',
// 					values: [{
// 						type: 'System.Int32[]',
// 						values: [{
// 							type: 'System.Int32',
// 							value: 1
// 						}, {
// 							type: 'System.Int32',
// 							value: 2
// 						}]
// 					}]
// 				}
// 			}
// 		}"));

//         Assert.AreNotEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions2d = new int[0][]
//         }.ToString());

//         Assert.AreEqual(repr.AsObject().ToString(), new HelloWorld
//         {
//             positions2d = new[] { new[] { 1, 2 } }
//         }.ToString());
//     }
// }