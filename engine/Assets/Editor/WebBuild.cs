﻿using UnityEngine;
using UnityEditor;

public class WebBuild
{

    [MenuItem("EvoSphere/Build Web Target")]
    public static void Build()
    {
        var path = BuildUtils.GetArg("-path");
        if (path == null)
        {
            path = EditorUtility.SaveFolderPanel("Select build folder", "web-build", "web-build");
        }

        Debug.Log(string.Format("Web Build: {0}", path));

        BuildPipeline.BuildPlayer(
                    new[] { "Assets/MainScene.unity" },
                    path,
                    BuildTarget.WebGL,
                    BuildOptions.None
                );
    }
}
