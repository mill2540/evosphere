using System.Collections.Generic;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public class AllArchivist : Archivist
    {
        private readonly Archivist[] archivists;

        [Expose]
        public AllArchivist(params Archivist[] archivists) { this.archivists = archivists; }

        public void OnShutdown(Experiment experiment, List<Organism> population)
        {
            foreach (var archivist in archivists)
            {
                archivist.OnShutdown(experiment, population);
            }
        }

        public bool PostEvaluate(Experiment experiment, List<Organism> population)
        {
            bool shouldContinue = false;
            foreach (var archivist in archivists)
            {
                shouldContinue = archivist.PostEvaluate(experiment, population) || shouldContinue;
            }

            return shouldContinue;
        }

        public void PreEvaulate(Experiment experiment, List<Organism> population)
        {
            foreach (var archivist in archivists)
            {
                archivist.PreEvaulate(experiment, population);
            }
        }
    }
}