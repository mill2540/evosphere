using System;
using System.Collections.Generic;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{

    public class AvidaBrain : OrganismComponent
    {
        public enum Instructions
        {
            PushReg = 0x01,
            PopReg = 0x02,
            Op = 0x03,
            Jmp = 0x04,
            JmpIf = 0x05
        }

        private List<Int32> hiddenRegisters;
        private List<byte> instructions;
        private List<Int32> stack;
        private int execPointer;
        private int speed;

        private void InitRegs(int length)
        {
            hiddenRegisters = new List<Int32>(length);
            for (int i = 0; i < length; i++)
            {
                hiddenRegisters.Add(0);
            }
        }

        private Int32 GetRegister(byte index, float[] state)
        {
            if (index < state.Length) { return FromFloat(state[index]); }
            index -= (byte) state.Length;
            if (index < hiddenRegisters.Count) { return hiddenRegisters[index]; }

            return 0;
        }

        private static float FromInt(Int32 value) => BitConverter.ToSingle(BitConverter.GetBytes(value), 0);
        private static int FromFloat(float value) => BitConverter.ToInt32(BitConverter.GetBytes(value), 0);

        private void SetRegister(byte index, Int32 value, float[] state)
        {
            if (index < state.Length)
            {
                state[index] = FromInt(value);
                return;
            }
            index -= (byte) state.Length;
            if (index < hiddenRegisters.Count)
            {
                hiddenRegisters[index] = value;
                return;
            }
        }

        private void Push(Int32 value)
        {
            stack.Add(value);
        }

        private Int32 Pop()
        {
            if (stack.Count == 0)
            {
                return 0;
            }
            else
            {
                var value = stack[stack.Count - 1];
                stack.RemoveAt(stack.Count - 1);
                return value;
            }
        }

        private byte ReadInstruction()
        {
            var instruction = instructions[execPointer];
            ++execPointer;
            if (execPointer >= instructions.Count)
            {
                execPointer = 0;
            }
            return instruction;
        }

        private void Exec(float[] state)
        {
            var instruction = ReadInstruction();

            switch (instruction)
            {
                case 0xFF:
                    break;
            }
        }

        public override void EvoSphereUpdate(float[] state)
        {
            for (int i = 0; i < speed; ++i)
            {
                Exec(state);
            }
        }

        public override void Inherit(List<OrganismComponent> parents)
        {
            if (instructions == null)
            {
                var parent = (AvidaBrain) parents[0];
                instructions = new List<byte>(parent.instructions);

                InitRegs(parent.hiddenRegisters.Count);

                // TODO: Mutate
            }

            stack = new List<int>();
        }
    }
}