using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public class ColorModifierComponent : OrganismComponent
    {
        [SerializeField]
        float hue, saturation, value;

        float mutationSize;

        [MonoBehaviourMethodInitializer]
        public void Init(float mutationSize = 0.0f)
        {
            Init(
                UnityEngine.Random.value,
                UnityEngine.Random.value,
                UnityEngine.Random.value,
                mutationSize);
        }

        [MonoBehaviourMethodInitializer]
        public void Init(float hue, float saturation, float value, float mutationSize = 0.0f)
        {
            this.hue = hue;
            this.saturation = saturation;
            this.value = value;
            this.mutationSize = mutationSize;
        }

        public override void Inherit(List<OrganismComponent> parents)
        {
            if (parents.Count == 0)
            {
                Init();
            }
            else
            {

                foreach (var parent in parents.Cast<ColorModifierComponent>())
                {
                    hue += parent.hue;
                    saturation += parent.saturation;
                    value += parent.value;
                }

                hue /= parents.Count;
                saturation /= parents.Count;
                value /= parents.Count;

                hue += UnityEngine.Random.value * mutationSize - mutationSize * 0.1f;

                hue = hue % 1;
                if (hue < 0) hue = 1 + hue;
            }

        }

        public override IEnumerator Born()
        {
            // Find everyone who has already been born, and assign the color to them
            foreach (var renderer in GetComponentsInChildren<MeshRenderer>())
            {
                renderer.material.color = Color.HSVToRGB(hue, saturation, value);
            }
            yield break;
        }

    }
}