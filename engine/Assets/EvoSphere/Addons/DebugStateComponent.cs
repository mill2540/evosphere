using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public class DebugStateComponent : OrganismComponent
    {

        private GameObject plane;

        [MonoBehaviourMethodInitializer]
        public void Init() { }

        public override void Inherit(List<OrganismComponent> parents) { }

        public override void EvoSphereUpdate(float[] state)
        {
            Debug.Log($"[{string.Join(", ", state)}]");
        }

    }
}