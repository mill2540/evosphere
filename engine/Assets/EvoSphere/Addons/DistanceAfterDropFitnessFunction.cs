using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public class DistanceAfterDropFitnessFunction : FitnessFunction
    {
        public string dataName;

        [Expose]
        public DistanceAfterDropFitnessFunction(string dataName = "DistanceAfterDrop")
        {
            this.dataName = dataName;
        }

        private Vector3? maybeStart;

        public void PostBorn(Organism organism) { maybeStart = organism.GetBounds()?.center; }

        public void PostEvaluate(Organism organism)
        {
            if (maybeStart is Vector3 start && organism.GetBounds() is Bounds end)
            {
                var distance = (end.center - start).magnitude;
                organism.Data.Set(dataName, distance);
            }
        }

        public void PostUpdate(Organism organism, float[] state) { }

        public void PreUpdate(Organism organism, float[] state) { }
    }
}