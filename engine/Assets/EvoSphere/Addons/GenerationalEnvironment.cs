using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public interface EnvironmentSpace
    {
        IEnumerator Reset();
        IEnumerator Place(Organism organism);
    }

    public class GenerationalEnvironment : Coenvironment
    {
        public EnvironmentSpace space;
        public int size;
        public float generationDuration;
        public Factory<Organism> organismFactory;
        public Factory<FitnessFunction> fitnessFunctionFactory;

        [MonoBehaviourMethodInitializer]
        public void CreateGenerationalEnvironment(
            EnvironmentSpace space,
            Data organism,
            Data fitness,
            int size = 10,
            float generationDuration = 10)
        {
            this.space = space;

            this.organismFactory = new Factory<Organism>(organism);
            this.fitnessFunctionFactory = new Factory<FitnessFunction>(fitness);
            this.size = size;
            this.generationDuration = generationDuration;
        }

        public override IEnumerator Evaluate(List<Organism> population)
        {
            Physics.autoSimulation = false;

            // Make sure that the space is clear before using it for anything
            yield return StartCoroutine(space.Reset());

            // Allow each organism to construct it's self in turn
            foreach (var organism in population)
            {
                organism.gameObject.transform.SetParent(gameObject.transform, false);
                organism.gameObject.transform.position = new Vector3(1E4f, 1E4f, 1E4f);

                organism.FitnessFunction = fitnessFunctionFactory.New();

                // The organism now runs through it's born sequence
                var e = organism.Born();
                if (e != null)
                    while (e.MoveNext()) { yield return e.Current; }

                // Place the organism into this space
                var e2 = space.Place(organism);
                if (e2 != null)
                    while (e2.MoveNext()) { yield return e2.Current; }

                organism.FitnessFunction.PostBorn(organism);
            }

            Physics.autoSimulation = true;

            var startTime = Time.time;
            var currentTime = Time.time;
            while (currentTime - startTime <= generationDuration)
            {
                foreach (var organism in population)
                {
                    organism.EvoSphereUpdate();
                }

                yield return null;
                currentTime = Time.time;
            }
        }

        public override List<Organism> NewPopulation()
        {
            var population = new List<Organism>(size);
            while (population.Count < size)
            {
                var o = organismFactory.New(new GameObject());

                o.Inherit();
                population.Add(o);
            }

            return population;
        }
    }
}