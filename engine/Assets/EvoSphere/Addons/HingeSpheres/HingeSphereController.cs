using UnityEngine;

namespace EvoSphere.Addons
{
    public class HingeSphereController : MonoBehaviour
    {
        public byte InputForward { get; private set; }
        public byte InputBackward { get; private set; }
        public byte OutputTouchingSelf { get; private set; }
        public byte OutputTouchingOther { get; private set; }

        private HingeSpheres owner;
        private HingeJoint joint;
        public bool IsTouchingSelf { get; private set; }
        public bool IsTouchingOther { get; private set; }

        public HingeSphereController Init(
            byte inputForward,
            byte inputBackward,
            byte outputTouchingSelf,
            byte outputTouchingOther,
            HingeSpheres owner,
            HingeJoint joint)
        {
            this.InputForward = inputForward;
            this.InputBackward = inputBackward;
            this.OutputTouchingSelf = outputTouchingSelf;
            this.OutputTouchingOther = outputTouchingOther;

            this.owner = owner;
            this.joint = joint;

            return this;
        }

        public void SetDrive(float speed, float force)
        {
            if (joint != null)
            {
                var motor = joint.motor;
                motor.force = force;
                motor.targetVelocity = speed;
                joint.motor = motor;
            }
        }

        void OnCollisionEnter(Collision collision)
        {
            var hsc = collision.gameObject.GetComponent<HingeSphereController>();
            if (hsc != null && hsc.owner == owner)
            {
                IsTouchingSelf = true;
            }
            else
            {
                IsTouchingOther = true;
            }
        }

        void OnCollisionExit(Collision collision)
        {
            var hsc = collision.gameObject.GetComponent<HingeSphereController>();
            if (hsc != null && hsc.owner == owner)
            {
                IsTouchingSelf = false;
            }
            else
            {
                IsTouchingOther = false;
            }
        }
    }
}