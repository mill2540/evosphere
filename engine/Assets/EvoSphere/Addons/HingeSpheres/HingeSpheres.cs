using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{

    public class HingeSpheres : OrganismComponent
    {
        // [SIZE] [HX] [HY] [HZ] [HW] [IF] [IB] [OO] [OS]
        private byte[] genome;
        private static readonly int GENE_SIZE = 9;

        private float minSize;
        private float maxSize;
        private float siteMutationProbability;
        private float driveForce;
        private float maxSpeed;

        private List<HingeSphereController> hscs = new List<HingeSphereController>();

        [MonoBehaviourMethodInitializer]
        public void Init(
            int initialLength = 5,
            float minSize = 0.1f,
            float maxSize = 2,
            float driveForce = 100,
            float maxSpeed = 100,
            float siteMutationProbability = 0.05f)
        {
            Init(new byte[initialLength * GENE_SIZE], minSize, maxSize, driveForce, maxSpeed, siteMutationProbability);
        }

        [MonoBehaviourMethodInitializer]
        public void Init(
            byte[] genome,
            float minSize,
            float maxSize,
            float driveForce,
            float maxSpeed,
            float siteMutationProbability)
        {
            this.genome = genome;
            this.minSize = minSize;
            this.maxSize = maxSize;
            this.driveForce = driveForce;
            this.maxSpeed = maxSpeed;
            this.siteMutationProbability = siteMutationProbability;
        }

        public override void Inherit(List<OrganismComponent> parents)
        {
            if (parents.Count == 1)
            {
                var parent = (HingeSpheres) parents[0];

                Init(parent.genome.Length / GENE_SIZE, parent.minSize, parent.maxSize, parent.siteMutationProbability);

                for (int i = 0; i < genome.Length; i++)
                {
                    if (UnityEngine.Random.value < siteMutationProbability)
                    {
                        genome[i] = (byte) (UnityEngine.Random.value * byte.MaxValue);
                    }
                    else
                    {
                        genome[i] = parent.genome[i];
                    }
                }
            }
            else
            {
                for (int i = 0; i < genome.Length; i++)
                {
                    genome[i] = (byte) (UnityEngine.Random.value * byte.MaxValue);
                }
            }
        }

        public override IEnumerator Born()
        {
            float? lastSize = null;
            Rigidbody lastRB = null;
            Transform lastTransform = null;
            for (int i = 0; i < genome.Length / GENE_SIZE; i++)
            {
                float size = (genome[i * GENE_SIZE + 0] / (float) byte.MaxValue) * (maxSize - minSize) + minSize;
                float hx = (genome[i * GENE_SIZE + 1] / (float) byte.MaxValue) * 2 - 1;
                float hy = (genome[i * GENE_SIZE + 2] / (float) byte.MaxValue) * 2 - 1;
                float hz = (genome[i * GENE_SIZE + 3] / (float) byte.MaxValue) * 2 - 1;
                float hw = (genome[i * GENE_SIZE + 4] / (float) byte.MaxValue) * 2 - 1;

                var inputF = genome[i * GENE_SIZE + 5];
                var inputB = genome[i * GENE_SIZE + 6];
                var outputS = genome[i * GENE_SIZE + 7];
                var outputO = genome[i * GENE_SIZE + 8];

                var current = GameObject.CreatePrimitive(PrimitiveType.Sphere);

                var rigidBody = current.AddComponent<Rigidbody>();

                current.transform.SetParent(transform, false);
                current.transform.localScale *= size;

                HingeJoint joint = null;
                if (lastTransform != null)
                {
                    float actualLastSize = (float) lastSize;
                    current.transform.position = lastTransform.position +
                        Vector3.forward * (actualLastSize + size + 0.05f) / 2;

                    Physics.SyncTransforms();

                    joint = current.AddComponent<HingeJoint>();
                    joint.connectedBody = lastRB;
                    joint.anchor = Vector3.back * 0.5f;
                    joint.enableCollision = true;
                    joint.useMotor = true;

                    joint.axis = new Quaternion(hx, hy, hz, hw) * Vector3.forward;
                }

                hscs.Add(current
                    .AddComponent<HingeSphereController>()
                    .Init(inputF, inputB, outputS, outputO, this, joint)
                );

                lastRB = rigidBody;
                lastSize = size;
                lastTransform = current.transform;
            }

            return null;
        }

        public override void EvoSphereUpdate(float[] state)
        {
            foreach (var hsc in hscs)
            {
                var inputForward = (int) ((hsc.InputForward / (float) byte.MaxValue) * state.Length);
                var inputBackward = (int) ((hsc.InputBackward / (float) byte.MaxValue) * state.Length);
                var outputSelf = (int) ((hsc.OutputTouchingSelf / (float) byte.MaxValue) * state.Length);
                var outputOther = (int) ((hsc.OutputTouchingOther / (float) byte.MaxValue) * state.Length);

                var forward = Mathf.Max(0, Mathf.Min(1, state[inputForward]));
                var backward = Mathf.Max(0, Mathf.Min(1, state[inputBackward]));

                hsc.SetDrive((forward - backward) * maxSpeed, driveForce);

                state[outputSelf] = hsc.IsTouchingSelf ? 1 : 0;
                state[outputOther] = hsc.IsTouchingOther ? 1 : 0;
            }
        }
    }
}