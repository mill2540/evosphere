using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using Newtonsoft.Json;
using Newtonsoft.Json.Bson;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace EvoSphere.Addons
{
    public struct PathFile
    {
        private static readonly Regex TEMPLATE_REGEX = new Regex(@"\<\w+\>", RegexOptions.Compiled);

        public string pathTemplate;

        [Expose]
        public PathFile(string path)
        {
            this.pathTemplate = path;
        }

        public void Write(
            Experiment experiment,
            Archivist archivist,
            string data)
        {
            MatchEvaluator templater = (Match match) =>
            {
                string name = match.Value.Substring(1, match.Value.Length - 2);

                if (name == "EXP_ID") return experiment.id.ToString();
                else if (name == "EXP_NAME") return experiment.experimentName;
                else
                {
                    return Universe.Instance
                        .Find(name, null, archivist.GetType())
                        .InvokeArgs(archivist).ToString();
                }
            };

            var actualPath = TEMPLATE_REGEX.Replace(pathTemplate, templater);

            actualPath = Path.GetFullPath(actualPath);

            var directory = Path.GetDirectoryName(actualPath);
            Directory.CreateDirectory(directory);

            using(var file = new StreamWriter(actualPath))
            {
                file.Write(data);
                file.Flush();
            }
        }
    }

    public enum JsonFormat
    {
        Json,
        PrettyJson,
        Bson,
    }

    public struct JsonFile
    {
        private PathFile file;
        private JsonFormat format;

        [Expose]
        public JsonFile(string path, JsonFormat format = JsonFormat.Json)
        {
            this.file = new PathFile(path);
            this.format = format;
        }

        public void Write(
            Experiment experiment,
            Archivist archivist,
            JToken json)
        {

            switch (format)
            {
                case JsonFormat.Json:
                    file.Write(experiment, archivist, json.ToString(Formatting.None));
                    break;
                case JsonFormat.PrettyJson:
                    file.Write(experiment, archivist, json.ToString(Formatting.Indented));
                    break;
                case JsonFormat.Bson:
                    using(var stream = new MemoryStream())
                    {
                        using(var bsonWriter = new BsonWriter(stream))
                        {
                            json.WriteTo(bsonWriter);
                        }
                        file.Write(experiment, archivist, Convert.ToBase64String(stream.ToArray()));
                    }
                    break;
            }
        }
    }

    [Expose]
    public class LODArchivist : Archivist
    {
        public JsonFile file;

        private void BuildLOD(
            OrganismData organism,
            HashSet<Guid> saved,
            List<OrganismData> lod)
        {
            if (!saved.Contains(organism.id))
            {
                lod.Add(organism);
                foreach (var parent in organism.Parents)
                {
                    BuildLOD(parent, saved, lod);
                }
            }
        }

        public void OnShutdown(Experiment experiment, List<Organism> population)
        {
            var known = new HashSet<Guid>(population.Select(organism => organism.Data.id));
            var unsaved = new Stack<OrganismData>(population.Select(organism => organism.Data));

            var archive = new JObject();
            while (unsaved.Count > 0)
            {
                var data = unsaved.Pop();
                archive.Add(data.id.ToString(), data.SaveJson());

                foreach (var parent in data.Parents)
                {
                    if (!known.Contains(parent.id))
                    {
                        known.Add(parent.id);
                        unsaved.Push(parent);
                    }
                }
            }

            file.Write(experiment, this, archive);
        }

        public bool PostEvaluate(Experiment experiment, List<Organism> population) => false;

        public void PreEvaulate(Experiment experiment, List<Organism> population) { }
    }
}