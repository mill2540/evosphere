using System;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    /// http://adamilab.msu.edu/markov-network-brains/
    public class MarkovBrain : OrganismComponent
    {
        [Expose]
        private int maxIO;

        [SerializeField]
        [Expose]
        private List<byte> genome;

        [SerializeField]
        private bool[] hiddenNodes;

        private float one;
        private float zero;

        private List<Gate> gates;

        [Expose]
        public class MutationSettings
        {
            public float probabilityOfSiteMutation = 0.01f;
            public float probabilityOfDeletionMutation = 0.2f;
            public float probabilityOfDuplicationMutation = 0.2f;
            public int minGenomeSize = 1000;
            public int maxGenomeSize = 20000;

            public int minDeletionLength = 256;
            public int maxDeletionLength = 512;
            public int minInsertionLength = 256;
            public int maxInsertionLength = 512;

            public int GetRandomGenomeSize()
            {
                return Mathf.FloorToInt(UnityEngine.Random.value * (maxGenomeSize - minGenomeSize) + minGenomeSize);
            }
        }

        private MutationSettings mutationSettings;

        struct Gate
        {
            int[] inputIds;
            int[] outputIds;

            float[, ] table;

            public Gate(int[] inputIds, int[] outputIds, float[, ] table)
            {
                this.inputIds = inputIds;
                this.outputIds = outputIds;
                this.table = table;
            }

            public void Exec(bool[] inputNodes, bool[] outputNodes)
            {
                int rowNumber = 0;

                for (int i = 0; i < inputIds.Length; i++)
                {
                    var inputIdx = Mathf.FloorToInt(inputNodes.Length * inputIds[i] / 256f);
                    if (inputNodes[inputIdx])
                    {
                        rowNumber |= 1 << i;
                    }
                }

                int columnNumber = 0;
                float p = UnityEngine.Random.value;
                for (; columnNumber < 1 << outputIds.Length; ++columnNumber)
                {
                    p -= table[rowNumber, columnNumber];
                    if (p <= 0) { break; }
                }

                for (int i = 0; i < outputIds.Length; ++i)
                {
                    var outputIdx = Mathf.FloorToInt(outputNodes.Length * outputIds[i] / 256f);
                    outputNodes[outputIdx] = outputNodes[outputIdx] || (columnNumber & (1 << i)) != 0;
                }

            }
        }

        private static byte RandomCodon()
        {
            return (byte) ((UnityEngine.Random.value * (byte.MaxValue - byte.MinValue)) + byte.MinValue);
        }

        [MonoBehaviourMethodInitializer]
        public void Init(
            int? genomeLength = null,
            int numberOfHiddenNodes = 4,
            int maxIO = 4,
            float one = 1.0f,
            float zero = 0.0f,
            MutationSettings mutationSettings = null)
        {
            mutationSettings = mutationSettings ?? new MutationSettings();

            var actualGenomeLength = genomeLength ?? mutationSettings.GetRandomGenomeSize();

            List<byte> genome = new List<byte>(actualGenomeLength);

            for (int i = 0; i < actualGenomeLength; i++)
            {
                genome.Add(RandomCodon());
            }

            Init(genome, numberOfHiddenNodes, maxIO, one, zero, mutationSettings);
        }

        [MonoBehaviourMethodInitializer]
        public void Init(
            List<byte> genome,
            int numberOfHiddenNodes = 4,
            int maxIO = 4,
            float one = 1.0f,
            float zero = 0.0f,
            MutationSettings mutationSettings = null)
        {
            this.genome = genome;
            this.maxIO = maxIO;
            this.one = one;
            this.zero = zero;
            this.hiddenNodes = new bool[numberOfHiddenNodes];
            this.mutationSettings = mutationSettings ?? new MutationSettings();
        }

        private void TranslateGenome()
        {
            int position = 0;
            int gatePosition = 0;

            Func<byte> readNextGateCodon = () =>
            {
                if (gatePosition >= genome.Count) { gatePosition = 0; }
                var codon = genome[gatePosition];
                ++gatePosition;

                return codon;
            };

            Func<int, int[]> readGateIO = (int n) =>
            {
                int[] io = new int[n];
                for (int i = 0; i < maxIO; i++)
                {
                    if (i < n)
                    {
                        io[i] = readNextGateCodon();
                    }
                }

                return io;
            };

            Func<Gate> readGate = () =>
            {
                gatePosition = position;

                var nIn = Mathf.FloorToInt(maxIO * readNextGateCodon() / 255f);
                var nOut = Mathf.FloorToInt(maxIO * readNextGateCodon() / 255f);

                var inputs = readGateIO(nIn);
                var outputs = readGateIO(nOut);

                int rows = 1 << nIn;
                int columns = 1 << nOut;
                var table = new float[rows, columns];

                for (int i = 0; i < rows; i++)
                {
                    var row = new byte[columns];
                    float total = 0;
                    for (int o = 0; o < columns; ++o)
                    {
                        var data = readNextGateCodon();
                        row[o] = data;
                        total += 1 + data;
                    }

                    for (int o = 0; o < columns; ++o)
                    {
                        table[i, o] = (1 + row[o]) / total;
                    }
                }

                return new Gate(inputs, outputs, table);
            };

            Func<Gate?> readNextGate = () =>
            {
                for (; position < genome.Count - 1; ++position)
                {
                    if (genome[position] == 42 && genome[position + 1] == 213)
                    {
                        // Read in the 213 start byte
                        ++position;
                        return readGate();
                    }
                }

                return null;
            };

            while (readNextGate() is Gate gate)
            {
                gates.Add(gate);
            }
        }

        public override void Inherit(List<OrganismComponent> parents)
        {
            if (parents.Count != 0)
            {
                var parent = (MarkovBrain) parents[0];
                var mutationSettings = parent.mutationSettings;

                var genome = new List<byte>(parent.genome.Count);

                foreach (var codon in parent.genome)
                {
                    var mutatedCodon = codon;
                    if (UnityEngine.Random.value <= mutationSettings.probabilityOfSiteMutation)
                    {
                        mutatedCodon = RandomCodon();
                    }
                    genome.Add(mutatedCodon);
                }

                if (genome.Count > mutationSettings.minGenomeSize &&
                    UnityEngine.Random.value <= mutationSettings.probabilityOfDeletionMutation)
                {
                    var length = (int) (
                        UnityEngine.Random.value * (mutationSettings.maxDeletionLength - mutationSettings.minDeletionLength) +
                        mutationSettings.minDeletionLength);

                    if (length < genome.Count)
                    {
                        var start = (int) (UnityEngine.Random.value * genome.Count);
                        var end = length + start;

                        if (end >= genome.Count)
                        {
                            var endLength = genome.Count - start;
                            var startLength = length - endLength;

                            genome.RemoveRange(start, endLength);
                            genome.RemoveRange(0, startLength);
                        }
                        else
                        {
                            genome.RemoveRange(start, length);
                        }
                    }

                }

                if (genome.Count < mutationSettings.maxGenomeSize &&
                    UnityEngine.Random.value <= mutationSettings.probabilityOfDuplicationMutation)
                {
                    var length = (int) (
                        UnityEngine.Random.value * (mutationSettings.maxInsertionLength - mutationSettings.minInsertionLength) +
                        mutationSettings.minInsertionLength);

                    var srcStart = (int) (UnityEngine.Random.value * genome.Count);
                    var srcEnd = srcStart + length;
                    var dest = (int) (UnityEngine.Random.value * genome.Count);

                    List<byte> data;
                    if (srcEnd >= genome.Count)
                    {
                        var endLength = genome.Count - srcStart;
                        var startLength = length - endLength;

                        data = genome.GetRange(srcStart, endLength);
                        data.AddRange(genome.GetRange(0, startLength));
                    }
                    else
                    {
                        data = genome.GetRange(srcStart, length);
                    }

                    genome.InsertRange(dest, data);
                }

                Init(genome, parent.hiddenNodes.Length, parent.maxIO, parent.one, parent.zero, mutationSettings);
            }
            else if (genome == null)
            {
                Init();
            }

            gates = new List<Gate>();
            TranslateGenome();
        }

        public override void EvoSphereUpdate(float[] state)
        {
            var inputs = new bool[state.Length + hiddenNodes.Length];
            var outputs = new bool[state.Length + hiddenNodes.Length];

            float threshold = (one - zero) / 2;

            for (int i = 0; i < state.Length; i++)
            {
                inputs[i] = !float.IsNaN(state[i]) || float.IsPositiveInfinity(state[i]) || state[i] > threshold;
            }

            Array.Copy(hiddenNodes, 0, inputs, state.Length, hiddenNodes.Length);

            foreach (var gate in gates)
            {
                gate.Exec(inputs, outputs);
            }

            Array.Copy(outputs, state.Length, hiddenNodes, 0, hiddenNodes.Length);

            for (int i = 0; i < state.Length; i++)
            {
                state[i] = outputs[i] ? one : zero;
            }
        }
    }
}