using System;
using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{

    public class Marvin : OrganismComponent
    {
        private GameObject marvinBody;
        private MarvinBodyController marvinBodyController;
        private byte[] genome = new byte[6];

        private float force;
        private float maxSpeed;

        private float siteMutationProbability;

        [MonoBehaviourMethodInitializer]
        public void Init(
            float force = 10,
            float maxSpeed = 25,
            float siteMutationProbability = 0.05f)
        {
            this.force = force;
            this.maxSpeed = maxSpeed;
            this.siteMutationProbability = siteMutationProbability;
        }

        public override void Inherit(List<OrganismComponent> parents)
        {
            if (parents.Count == 1)
            {
                var parent = (Marvin) parents[0];

                force = parent.force;
                maxSpeed = parent.maxSpeed;

                for (int i = 0; i < genome.Length; ++i)
                {
                    if (UnityEngine.Random.value < siteMutationProbability)
                    {
                        genome[i] = (byte) (UnityEngine.Random.value * byte.MaxValue);
                    }
                    else
                    {
                        genome[i] = parent.genome[i];
                    }
                }
            }
            else
            {
                for (int i = 0; i < genome.Length; ++i)
                {
                    genome[i] = (byte) (UnityEngine.Random.value * byte.MaxValue);
                }
            }

        }

        public override IEnumerator Born()
        {
            marvinBody = Instantiate(Resources.Load<GameObject>("MarvinPrefab"));
            marvinBodyController = marvinBody.GetComponent<MarvinBodyController>();
            marvinBody.transform.SetParent(transform, false);

            marvinBodyController.SetLeft(maxSpeed, force);
            marvinBodyController.SetRight(maxSpeed, force);

            return null;
        }

        private int MapCodonToState(int idx, int states)
        {
            return (int) ((genome[idx] / (float) Byte.MaxValue) * states);
        }

        private float GetState(float[] state, int idx)
        {
            return state[MapCodonToState(idx, state.Length)];
        }

        private void SetState(float[] state, int idx, float value)
        {
            state[MapCodonToState(idx, state.Length)] = value;
        }

        public override void EvoSphereUpdate(float[] state)
        {
            float leftDriveForward = Mathf.Max(Mathf.Min(GetState(state, 0), 1), 0);
            float leftDriveBackward = Mathf.Max(Mathf.Min(GetState(state, 1), 1), 0);

            float rightDriveForward = Mathf.Max(Mathf.Min(GetState(state, 2), 1), 0);
            float rightDriveBackward = Mathf.Max(Mathf.Min(GetState(state, 3), 1), 0);

            marvinBodyController.SetLeft((leftDriveForward - leftDriveBackward) * maxSpeed, force);
            marvinBodyController.SetRight((rightDriveForward - rightDriveBackward) * maxSpeed, force);

            if (marvinBodyController.IsFrontActivated) { SetState(state, 4, 1); }
            else { SetState(state, 4, 0); }

            if (marvinBodyController.IsBackActivated) { SetState(state, 5, 1); }
            else { SetState(state, 5, 0); }
        }

        public void OnDestroy()
        {
            Destroy(marvinBody);
        }
    }
}