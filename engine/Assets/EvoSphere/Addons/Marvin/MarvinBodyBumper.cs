using UnityEngine;

namespace EvoSphere.Addons
{
    public class MarvinBodyBumper : MonoBehaviour
    {
        public bool IsActivated { get; private set; }

        public void OnTriggerEnter() { IsActivated = true; }
        public void OnTriggerExit() { IsActivated = false; }
    }
}