using UnityEngine;

namespace EvoSphere.Addons
{
    public class MarvinBodyController : MonoBehaviour
    {
        [SerializeField]
        private HingeJoint right;

        [SerializeField]
        private HingeJoint left;

        [SerializeField]
        private MarvinBodyBumper front;

        [SerializeField]
        private MarvinBodyBumper back;

        public void Awake()
        {
            left.useMotor = true;
            right.useMotor = true;
        }

        public void SetRight(float speed, float force)
        {
            var rm = right.motor;
            rm.targetVelocity = speed;
            rm.force = force;
            right.motor = rm;
        }

        public void SetLeft(float speed, float force)
        {
            var lm = left.motor;
            lm.targetVelocity = speed;
            lm.force = force;
            left.motor = lm;
        }

        public bool IsFrontActivated => front.IsActivated;
        public bool IsBackActivated => back.IsActivated;
    }
}