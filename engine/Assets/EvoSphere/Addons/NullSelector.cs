using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;

[Expose]
public class NullSelector : Selector
{
    public List<Organism> Select(List<Organism> organisms)
    {
        return organisms.Select(organism => Organism.Replicate(organism)).ToList();
    }
}