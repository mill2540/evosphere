﻿using System;
using System.Collections;
using System.Collections.Generic;
using EvoSphere.Addons;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

public class PlaneSpace : MonoBehaviour, EnvironmentSpace
{
    public GameObject planePrefab;
    private GameObject plane;

    private Vector2 padding;
    private float verticalOffset;
    private float cameraStartingHeight;
    private Vector2Int grid;

    private List<Vector3> cells = new List<Vector3>();

    [MonoBehaviourMethodInitializer]
    public void CreatePlaneSpace(
        int gridX = 100,
        int gridZ = 100,
        float verticalOffset = 1,
        float cameraStartingHeight = 2,
        Vector2? padding = null)
    {
        if (padding == null)
        {
            this.padding = new Vector3(1, 1);
        }

        this.verticalOffset = verticalOffset;
        this.cameraStartingHeight = cameraStartingHeight;

        this.grid = new Vector2Int(gridX, gridZ);
    }

    public IEnumerator Reset()
    {
        Destroy(this.plane);
        while (this.plane != null)
        {
            yield return null;
        }

        planePrefab = Resources.Load<GameObject>("PlaneSpace");
        this.plane = Instantiate(planePrefab);
        plane.transform.SetParent(gameObject.transform, true);

        // Generate list of cells we can place stuff in
        // Make sure that the colliders have time to get in place
        yield return null;

        cells.Clear();

        var terrain = this.plane.GetComponent<TerrainCollider>().bounds;

        var padding = new Vector3(
            Mathf.Min(this.padding.x, terrain.extents.x),
            Mathf.Min(this.padding.y, terrain.extents.y)
        );

        var grid = new Vector2Int(
            Mathf.Abs(terrain.extents.x) >= 1E-5 ? this.grid.x : 1,
            Mathf.Abs(terrain.extents.z) >= 1E-5 ? this.grid.y : 1
        );

        var size = new Vector2(
            terrain.size.x - padding.x * 2,
            terrain.size.z - padding.y * 2
        );

        var delta = new Vector2(
            grid.x > 1 ? size.x / grid.x : 0,
            grid.y > 1 ? size.y / grid.y : 0
        );

        var start = new Vector3(
            terrain.min.x + padding.x,
            terrain.max.y + verticalOffset,
            terrain.min.z + padding.y
        );

        var offsets = start;

        for (int x = 0; x < grid.x; ++x)
        {
            for (int z = 0; z < grid.y; ++z)
            {
                cells.Add(offsets);
                offsets.z += delta.y;
            }
            offsets.z = start.z;
            offsets.x += delta.x;
        }

        var center = terrain.center;
        cells.Sort(
            (x, y) => (y - center).sqrMagnitude.CompareTo((x - center).sqrMagnitude)
        );

        var camera = Boot.Instance.Camera;

        if (camera != null)
        {
            center.y += cameraStartingHeight;
            camera.transform.position = center + Vector3.back * 3;
        }
    }

    private static void DrawBounds(Bounds bounds, Color color, float duration)
    {
        var c000 = bounds.min;
        var c111 = bounds.max;

        var c001 = new Vector3(c000.x, c000.y, c111.z);
        var c010 = new Vector3(c000.x, c111.y, c000.z);
        var c011 = new Vector3(c000.x, c111.y, c111.z);
        var c100 = new Vector3(c111.x, c000.y, c000.z);
        var c101 = new Vector3(c111.x, c000.y, c111.z);
        var c110 = new Vector3(c111.x, c111.y, c000.z);

        Debug.DrawLine(c000, c001, color, duration, false);
        Debug.DrawLine(c000, c010, color, duration, false);
        Debug.DrawLine(c000, c100, color, duration, false);

        Debug.DrawLine(c111, c110, color, duration, false);
        Debug.DrawLine(c111, c101, color, duration, false);
        Debug.DrawLine(c111, c011, color, duration, false);

        Debug.DrawLine(c110, c010, color, duration, false);
        Debug.DrawLine(c110, c100, color, duration, false);

        Debug.DrawLine(c101, c100, color, duration, false);
        Debug.DrawLine(c101, c001, color, duration, false);

        Debug.DrawLine(c011, c010, color, duration, false);
        Debug.DrawLine(c011, c001, color, duration, false);
    }

    public IEnumerator Place(Organism organism)
    {
        // Make sure that the physics system has updated all transformation information
        Physics.SyncTransforms();

        // Make sure that a physics frame has passed since the organism was born
        var maybeBounds = organism.GetBounds();

        // If the organism has bounds, then place it.
        // Otherwise, we assume that it has no embodied components,
        // and doesn't need to exist physically
        if (maybeBounds is Bounds bounds)
        {
            for (int i = cells.Count - 1; i >= 0; --i)
            {
                var position = cells[i];
                position.y += bounds.extents.y;

                if (!Physics.CheckBox(
                        position,
                        bounds.extents,
                        Quaternion.identity,
                        Physics.DefaultRaycastLayers,
                        QueryTriggerInteraction.Ignore))
                {
                    var offset = bounds.center - organism.transform.position;

                    // DrawBounds(new Bounds(position, bounds.size * 0.9f), Color.blue, 100);

                    organism.transform.position = position - offset;
                    // DrawBounds((Bounds) organism.GetBounds(), Color.red, 100);
                    yield break;
                }

                cells.RemoveAt(i);
            }

            throw new Exception("Cannot place organism. All places are filled");
        }

    }
}