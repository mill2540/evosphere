using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace EvoSphere.Addons
{

    [Expose]
    public class PostEvaluateArchivist : Archivist
    {
        private int cycle = 0;
        [Expose]
        public int Cycle { get => cycle; }

        public JsonFile file;

        public void OnShutdown(Experiment experiment, List<Organism> population) { }

        public bool PostEvaluate(Experiment experiment, List<Organism> population)
        {
            ++cycle;
            var data = new JArray();
            foreach (var organism in population)
            {
                data.Add(organism.Data.SaveJson());
            }

            file.Write(experiment, this, data);

            return false;
        }

        public void PreEvaulate(Experiment experiment, List<Organism> population) { }
    }
}