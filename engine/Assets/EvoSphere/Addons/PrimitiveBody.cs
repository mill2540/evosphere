﻿using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

public class PrimitiveBody : OrganismComponent
{
    private PrimitiveType primitive;
    private Vector3 forceStrength;
    private GameObject primitiveObject;

    [MonoBehaviourMethodInitializer]
    public void Init(Vector3 forceStrength, PrimitiveType primitive = PrimitiveType.Capsule)
    {
        this.primitive = primitive;
        this.forceStrength = forceStrength;
    }

    public override IEnumerator Born()
    {
        primitiveObject = GameObject.CreatePrimitive(primitive);
        // primitiveObject.AddComponent<Collider>();
        primitiveObject.AddComponent<Rigidbody>();
        primitiveObject.transform.SetParent(gameObject.transform, false);

        yield break;
    }

    public override void EvoSphereUpdate(float[] state)
    {
        var rb = primitiveObject.GetComponent<Rigidbody>();
        if (rb != null)
        {
            rb.AddRelativeForce(new Vector3(state[0] * forceStrength.x, state[1] * forceStrength.y, state[2] * forceStrength.z));
        }
    }

    public void OnDestory()
    {
        Destroy(primitiveObject);
    }

    public override void Inherit(List<OrganismComponent> parents)
    {
        if (parents.Count > 0)
        {
            var parent = (PrimitiveBody) parents[0];
            primitive = parent.primitive;
            forceStrength = parent.forceStrength;
        }
    }
}