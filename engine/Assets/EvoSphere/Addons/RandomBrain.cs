using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public class RandomBrain : OrganismComponent
    {

        private float min, max;

        [MonoBehaviourMethodInitializer]
        public void Init(float min = 0, float max = 1) { this.min = min; this.max = max; }

        public override void Inherit(List<OrganismComponent> parents)
        {
            if (parents.Count > 0)
            {
                min = parents.Cast<RandomBrain>().Max(parent => parent.min);
                max = parents.Cast<RandomBrain>().Min(parent => parent.max);

                if (max < min)
                {
                    var tmp = min;
                    max = min;
                    min = tmp;
                }
            }
        }

        public override void EvoSphereUpdate(float[] state)
        {
            for (int i = 0; i < state.Length; i++)
            {
                state[i] = Random.value * (max - min) + min;
            }
        }

    }
}