using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    public class RoulettSelector : Selector
    {
        private string fitness;

        [Expose]
        public RoulettSelector(string fitness)
        {
            this.fitness = fitness;
        }

        private Organism Roulett(float minFitness, float maxFitness, List<Organism> organisms)
        {
            if (Mathf.Abs(maxFitness - minFitness) > 0.00001)
            {

                var p = UnityEngine.Random.value;
                foreach (var organism in organisms)
                {
                    var value = (float) organism.Data.Get(fitness);
                    p -= (value - minFitness) / (maxFitness - minFitness);

                    if (p < 0) { return organism; }
                }

                throw new System.Exception("Failed to select organism with roulett");
            }
            else
            {
                // max & min fitness are baiscally the same, and there may be problems
                // with floating point math, so just pick evenly
                return organisms[(int) (UnityEngine.Random.value * organisms.Count)];
            }
        }

        public List<Organism> Select(List<Organism> organisms)
        {
            var minFitness = organisms.Min(organism => (float) organism.Data.Get(fitness));
            var maxFitness = organisms.Max(organism => (float) organism.Data.Get(fitness));

            var children = new List<Organism>();
            while (children.Count < organisms.Count)
            {
                var parent = Roulett(minFitness, maxFitness, organisms);

                children.Add(Organism.Replicate(parent));
            }

            return children;
        }
    }
}