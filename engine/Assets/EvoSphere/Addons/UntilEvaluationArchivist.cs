using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using EvoSphere.Core.Reflection;
using EvoSphere.Engine;
using UnityEngine;

namespace EvoSphere.Addons
{
    /// A Archivist that will run the experiment until the specified number of evaluation loops have completed
    [Expose]
    public class UntilEvaluationArchivist : Archivist
    {
        private int counter = 0;
        [Expose]
        public int Cycle { get => counter; }

        public int until = 10;

        public void OnShutdown(Experiment experiment, List<Organism> population) { }

        public bool PostEvaluate(Experiment experiment, List<Organism> population) => ++counter <= until;

        public void PreEvaulate(Experiment experiment, List<Organism> population) { }
    }
}