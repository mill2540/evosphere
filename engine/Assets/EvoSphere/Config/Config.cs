using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace EvoSphere.Core.Config
{

    public class Factory<T>
    {
        public readonly Data Data;

        [Expose]
        public Factory(Data data)
        {
            this.Data = data;
        }

        public T New(UnityEngine.GameObject target = null)
        {
            return (T) Data.Invoke(typeof(T), target);
        }
    }

    public static class DataExt
    {

        private static object InvokeString(this StringData stringData, Type hint, GameObject target)
        {
            if (ReflectionUtils.WebSafeIsAssignableFrom(hint, typeof(string)))
            {
                return stringData.Value;
            }
            else if (hint.IsEnum)
            {
                return Enum.Parse(hint, stringData.Value, false);
            }
            else
            {
                string error;
                bool isNoArgs;
                var callable = stringData.SelectStringCallable(hint, out error, out isNoArgs);

                if (callable != null)
                {
                    if (isNoArgs)
                    {
                        return CallUtilities.InvokeCall(
                            callable,
                            new List<Data>(), new List<KeyValuePair<string, Data>>(),
                            Invoke,
                            target);
                    }
                    else
                    {
                        return CallUtilities.InvokeCall(
                            callable,
                            new List<Data>() { stringData }, new List<KeyValuePair<string, Data>>(),
                            Invoke,
                            target);
                    }
                }
                else
                {
                    throw new Exception(error);
                }
            }
        }

        private static object InvokeCall(this CallData callData, Callable callable, GameObject target)
        {
            return CallUtilities.InvokeCall(
                callable,
                callData.Args, callData.Kwargs,
                Invoke, target);
        }

        private static Callable SelectCallCallable(this CallData callData, Type hint, out string error)
        {
            return SelectCallable(
                callData?.id,
                callData.CallableName, hint,
                callData.Args, callData.Kwargs,
                out error
            );
        }

        private static Callable SelectStringCallable(this StringData stringData, Type hint, out string error, out bool isNoArgs)
        {
            string noargsError;
            int noargsBadness;
            var noargsCallable = CallUtilities.SelectCallable(
                stringData.Value,
                hint,
                new List<Data>(),
                new List<KeyValuePair<string, Data>>(),
                TypeCheckData,
                out noargsBadness,
                out noargsError
            );

            string convertError;
            int convertBadness;
            var convertCallable = CallUtilities.SelectCallable(
                null,
                hint,
                new List<Data>() { stringData.ToStrict() },
                new List<KeyValuePair<string, Data>>(),
                TypeCheckData,
                out convertBadness,
                out convertError
            );

            if (noargsError != null)
            {
                noargsError = $"no argument-less constructor exists because {noargsError}";
            }
            if (convertError != null)
            {
                convertError = $"cannot convert from string because {convertError}";
            }

            if (noargsCallable == null && convertCallable == null)
            {
                error = $"cannot create {ReflectionUtils.GetNiceName(hint)} from {stringData.Value}";
                isNoArgs = false;

                if (noargsError != null || convertCallable != null) error += " because ";

                if (noargsError != null) error += noargsError;

                if (noargsError != null && noargsCallable != null) error += " and because ";
                if (noargsCallable != null) error += noargsError;

                return null;
            }
            else if (noargsCallable != null && convertCallable != null)
            {
                if (noargsBadness < convertBadness)
                {
                    error = null;
                    isNoArgs = true;
                    return noargsCallable;
                }
                else if (convertBadness < noargsBadness)
                {
                    error = null;
                    isNoArgs = false;
                    return convertCallable;
                }
                else
                {
                    isNoArgs = false;
                    error = $"cannot create {ReflectionUtils.GetNiceName(hint)} from {stringData.Value} because two canidates were found: {noargsCallable.Signature} and {convertCallable.Signature}";
                    return null;
                }

            }
            else
            {
                error = null;
                isNoArgs = noargsCallable != null;
                return noargsCallable ?? convertCallable;
            }
        }

        private static Type TypeCheckData(this Data data, Type hint, out string error)
        {
            var dtype = data.TypeCheckImpl(hint, out error);

            if (error == null && dtype != null && !ReflectionUtils.WebSafeIsAssignableFrom(hint, dtype))
            {
                error = $"cannot create {ReflectionUtils.GetNiceName(hint)} from {ReflectionUtils.GetNiceName(dtype)}";
                return null;
            }
            else
            {
                return dtype;
            }
        }

        private static Type TypeCheckImpl(this Data data, Type hint, out string error)
        {
            if (hint != null && ReflectionUtils.WebSafeIsAssignableFrom(hint, typeof(Data)))
            {
                error = null;
                return typeof(Data);
            }
            else if (data is CallData callData)
            {
                return callData.SelectCallCallable(hint, out error)?.Signature.ReturnType;
            }
            else if (data is StrictStringData strictStringData)
            {
                error = null;
                return typeof(string);
            }
            else if (data is StringData stringData)
            {
                if (hint != null && ReflectionUtils.WebSafeIsAssignableFrom(hint, typeof(string)))
                {
                    error = null;
                    return typeof(string);
                }
                else if (hint != null && hint.IsEnum)
                {
                    if (Enum.IsDefined(hint, stringData.Value))
                    {
                        error = null;
                        return hint;
                    }
                    else
                    {
                        error = $"expected one of {string.Join(", ", Enum.GetNames(hint))}";
                        return null;
                    }
                }
                else
                {
                    bool isNoArgs;
                    return stringData.SelectStringCallable(hint, out error, out isNoArgs)?.Signature.ReturnType;
                }
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        private static readonly Dictionary<Guid, Callable> callableCache = new Dictionary<Guid, Callable>();

        public static Callable SelectCallable(
            Guid? maybeId,
            string name,
            Type hint,
            IEnumerable<Data> args,
            IEnumerable<KeyValuePair<string, Data>> kwargs,
            out string errors)
        {
            if (maybeId is Guid id)
            {
                Callable callable;
                if (callableCache.TryGetValue(id, out callable))
                {
                    errors = null;
                    return callableCache[id];
                }
                else
                {
                    callable = CallUtilities.SelectCallable(
                        name, hint,
                        args, kwargs,
                        TypeCheckData,
                        out errors);

                    if (errors == null) { callableCache.Add(id, callable); }
                    return callable;
                }
            }
            else
            {
                return CallUtilities.SelectCallable(
                    name, hint,
                    args, kwargs,
                    TypeCheckData,
                    out errors);
            }
        }

        private static object InvokeCall(this CallData callData, Type hint, GameObject target)
        {
            string errors;
            var callable = SelectCallable(
                callData?.id,
                callData.CallableName, hint,
                callData.Args, callData.Kwargs,
                out errors);

            if (callable != null)
            {
                return callData.InvokeCall(callable, target);
            }
            else
            {
                throw new Exception(errors);
            }
        }

        public static object Invoke(this Data data, Type hint, GameObject target = null)
        {
            if (data == null)
            {
                return null;
            }
            else if (ReflectionUtils.WebSafeIsAssignableFrom(hint, typeof(Data)))
            {
                return data;
            }
            else if (data is CallData callData)
            {
                return callData.InvokeCall(hint, target);
            }
            else if (data is StringData stringData)
            {
                return stringData.InvokeString(hint, target);
            }
            else
            {
                throw new NotImplementedException();
            }
        }

        public static T Invoke<T>(this Data data, GameObject target = null)
        {
            return (T) data.Invoke(typeof(T), target);
        }
    }
}