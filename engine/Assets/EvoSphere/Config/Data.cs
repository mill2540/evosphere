using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace EvoSphere.Core.Config
{
    public abstract class Data
    {
        // For caching reasons, we assign an id to the data
        public readonly Guid id = Guid.NewGuid();

        internal abstract void SaveImpl(TextWriter writer, string indent, int indentLevel);

        public static implicit operator Data(string value)
        {
            if (value == null) { return null; }
            else { return new StringData(value); }
        }

        public static implicit operator Data(int value)
        {
            return new StringData(value.ToString());
        }

        public static implicit operator Data(bool value)
        {
            return new StringData(value.ToString());
        }

        public static implicit operator Data(float value)
        {
            return new StringData(value.ToString());
        }

        internal static void StringWriterHelper(TextWriter writer, string value)
        {
            bool needsQuotes =
                value == "" ||
                value == "null" ||
                DataIO.SCALAR_TERMINALS
                .Where(terminal => terminal != -1)
                .Any(terminal => value.Contains((char) terminal));

            if (needsQuotes)
            {
                // Escape special characters
                var escaped = value
                    .Replace("\\", "\\\\")
                    .Replace("\n", "\\n")
                    .Replace("\r", "\\r")
                    .Replace("\t", "\\t")
                    .Replace("\"", "\\\"");

                writer.Write("\"");
                writer.Write(escaped);
                writer.Write("\"");
            }
            else { writer.Write(value); }
        }

        public string ToString(string indent) => this.Save(indent);
        public override string ToString() => this.ToString(null);
    }

    public class StrictStringData : StringData
    {
        public StrictStringData(string value) : base(value) { }
        internal override void SaveImpl(TextWriter writer, string indent, int indentLevel)
        {
            throw new NotImplementedException();
        }

        public override string ToString() => $"string({Value})";
    }

    public class StringData : Data
    {
        public readonly string Value;

        public StringData(string value) { Value = value; }

        internal override void SaveImpl(TextWriter writer, string indent, int indentLevel)
        {
            StringWriterHelper(writer, Value);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return Value.Equals(((StringData) obj).Value);
        }

        public override int GetHashCode()
        {
            return Value.GetHashCode();
        }

        public StrictStringData ToStrict() { return new StrictStringData(Value); }

        public override string ToString() => $"String({this.Save()})";
    }

    public class CallData : Data, IEnumerable<KeyValuePair<string, Data>>
    {
        public readonly string CallableName;

        private List<Data> args;
        public IReadOnlyList<Data> Args { get => args; }
        private Dictionary<string, Data> kwargs;
        public IReadOnlyDictionary<string, Data> Kwargs { get => kwargs; }

        public CallData(string name = null,
            IEnumerable<Data> args = null,
            IEnumerable<KeyValuePair<string, Data>> kwargs = null)
        {
            CallableName = name;
            if (args == null) this.args = new List<Data>();
            else this.args = new List<Data>(args);

            if (kwargs == null) this.kwargs = new Dictionary<string, Data>();
            else this.kwargs = kwargs.ToDictionary(entry => entry.Key, entry => entry.Value);
        }

        public void Add(Data arg) => args.Add(arg);

        public void Add(string name, Data value)
        {
            if (name == null) { args.Add(value); }
            else { kwargs.Add(name, value); }
        }

        internal override void SaveImpl(TextWriter writer, string indent, int indentLevel)
        {
            indentLevel += 1;
            string indentString = "";
            if (indent != null)
            {
                for (int i = 0; i < indentLevel; i++)
                {
                    indentString += indent;
                }
            }

            if (CallableName != null) StringWriterHelper(writer, CallableName);

            if (args.Count > 0 || kwargs.Count == 0)
            {
                writer.Write(DataIO.START_ARGS);

                for (int i = 0; i < args.Count; i++)
                {
                    if (indent != null)
                    {
                        writer.WriteLine();
                        writer.Write(indentString);
                    }

                    args[i].SaveImpl(writer, indent, indentLevel);
                    if (i < args.Count - 1)
                    {
                        writer.Write(DataIO.ARG_SEP);
                    }
                }

                writer.Write(DataIO.END_ARGS);
            }

            if (kwargs.Count > 0)
            {
                writer.Write(DataIO.START_KWARGS);

                int count = 0;
                foreach (var kwarg in kwargs)
                {
                    if (indent != null)
                    {
                        writer.WriteLine();
                        writer.Write(indentString);
                    }

                    StringWriterHelper(writer, kwarg.Key);
                    writer.Write(DataIO.EQUALS);

                    kwarg.Value.SaveImpl(writer, indent, indentLevel);

                    if (indent != null) { writer.Write(indent); }

                    if (count < kwargs.Count - 1)
                    {
                        writer.Write(DataIO.ARG_SEP);
                    }
                    ++count;
                }

                writer.Write(DataIO.END_KWARGS);
            }

        }

        public IEnumerator<KeyValuePair<string, Data>> GetEnumerator() => (IEnumerator<KeyValuePair<string, Data>>) args.Select(
            arg => new KeyValuePair<string, Data>(null, arg)).Concat(kwargs);

        IEnumerator IEnumerable.GetEnumerator() => args.Select(
            arg => new KeyValuePair<string, Data>(null, arg)).Concat(kwargs).GetEnumerator();

        public override bool Equals(object obj)
        {
            if (obj != null && obj is CallData other)
            {
                if (CallableName != null && !CallableName.Equals(other.CallableName)) { return false; }
                if (CallableName == null && other.CallableName != null) { return false; }

                if (!other.args.SequenceEqual(args)) { return false; }
                if (other.kwargs.Count != kwargs.Count) { return false; }

                foreach (var entry in kwargs)
                {
                    Data otherValue;
                    if (!other.kwargs.TryGetValue(entry.Key, out otherValue) || !otherValue.Equals(entry.Value))
                    {
                        return false;
                    }
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public override int GetHashCode()
        {
            // TODO: roll our own hashcode
            return Tuple.Create(CallableName, args, kwargs).GetHashCode();
        }
    }

    public static class DataIO
    {
        public static void Save(this Data data, TextWriter writer, string indent = null)
        {
            if (data == null) { writer.Write("null"); }
            else { data.SaveImpl(writer, indent, 0); }
        }

        public static string Save(this Data data, string indent = null)
        {
            using(var writer = new StringWriter())
            {
                data.Save(writer, indent);
                return writer.ToString();
            }
        }

        internal static readonly char START_ARGS = '[';
        internal static readonly char END_ARGS = ']';

        internal static readonly char START_KWARGS = '{';
        internal static readonly char END_KWARGS = '}';

        internal static readonly char ARG_SEP = ',';

        internal static readonly char EQUALS = '=';

        // private static readonly char MINUS = '-';
        // private static readonly char PLUS = '+';
        // private static readonly char DOT = '.';

        internal static readonly char QUOTE = '"';

        internal static readonly int[] WHITESPACE = { ' ', '\t', '\n', '\r' };
        internal static readonly int[] SCALAR_TERMINALS = {
            ' ',
            '\t',
            '\n',
            '\r',
            START_ARGS,
            END_ARGS,
            START_KWARGS,
            END_KWARGS,
            QUOTE,
            ARG_SEP,
            EQUALS,
            -1
        };

        private static Exception UnexpectedCharacter(TextReader reader, string expected)
        {
            return UnexpectedCharacter(reader.Peek(), expected);
        }

        private static Exception UnexpectedCharacter(int current, string expected)
        {
            if (current == -1)
            {
                return new Exception($"Expected {expected} but got end of input");
            }
            else
            {
                return new Exception($"Expected {expected} but got '{(char)current}' (ascii={current})");
            }
        }

        private static string ReadUntil(TextReader reader, int[] terminals)
        {
            string data = "";
            while (!terminals.Contains(reader.Peek())) { data += (char) reader.Read(); }
            return data;
        }

        private static bool TryReadQuoteText(TextReader reader, out string value)
        {
            if (reader.Peek() == QUOTE)
            {
                reader.Read(); // Consume the quote

                bool escaped = false;
                value = "";

                while (true)
                {
                    var current = reader.Read();

                    if (escaped)
                    {
                        if (current == 'n') value += '\n';
                        else if (current == 't') value += '\t';
                        else if (current == 'r') value += '\r';
                        else if (current == '\\') value += '\\';
                        else if (current == '"') value += '"';
                        else UnexpectedCharacter(current, "one of n, t, r, \\, \"");
                        escaped = false;
                    }
                    else
                    {
                        if (current == -1) throw new Exception("Unexpeced end of data");
                        else if (current == QUOTE) break;
                        else if (current == '\\') escaped = true;
                        else value += (char) current;
                    }
                }

                return true;
            }
            else
            {
                value = null;
                return false;
            }
        }

        private static bool TryReadQuoteString(TextReader reader, out Data data)
        {
            string value;
            if (TryReadQuoteText(reader, out value))
            {
                data = new StringData(value);
                return true;
            }
            else
            {
                data = null;
                return false;
            }
        }

        private static bool TryReadText(TextReader reader, out string text)
        {
            text = ReadUntil(reader, SCALAR_TERMINALS);
            if (text == "")
            {
                text = null;
                return false;
            }
            return true;
        }

        private static bool Consume(TextReader reader, params int[] consume)
        {
            if (consume.Contains(reader.Peek()))
            {
                reader.Read();
                return true;
            }
            else
            {
                return false;
            }
        }

        private static void ConsumeMany(TextReader reader, params int[] consume)
        {
            while (Consume(reader, consume)) { }
        }

        private static void ConsumeWhitespace(TextReader reader)
        {
            ConsumeMany(reader, WHITESPACE);
        }

        private static bool StartsWithPeeking(TextReader reader, params int[] needle)
        {
            ConsumeWhitespace(reader);

            int current = reader.Peek();
            if (needle.Contains(current))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        private static bool StartsWith(TextReader reader, params int[] needle)
        {
            if (StartsWithPeeking(reader, needle))
            {
                reader.Read();
                return true;
            }
            else { return false; }
        }

        private static void Require(TextReader reader, params int[] required)
        {
            if (!StartsWith(reader, required))
            {
                throw UnexpectedCharacter(reader.Peek(), string.Join(", ", required.Select(c => $"'{(char) c}'")));
            }
        }

        private static void RequirePeeking(TextReader reader, params int[] required)
        {
            if (!StartsWithPeeking(reader, required))
            {
                throw UnexpectedCharacter(reader.Peek(), string.Join(", ", required.Select(c => $"'{(char) c}'")));
            }
        }

        private static List<T> ReadList<T>(TextReader reader, int start, int end, Func<TextReader, T> read)
        {
            if (StartsWith(reader, start))
            {
                List<T> items = new List<T>();
                // Read in the sequence arguments
                while (!StartsWithPeeking(reader, end, -1))
                {
                    items.Add(read(reader));

                    RequirePeeking(reader, ARG_SEP, end, -1);
                    if (reader.Peek() == ARG_SEP) { reader.Read(); }
                }

                // Consume the end marker 
                Require(reader, end);

                return items;
            }

            return null;
        }

        private static KeyValuePair<string, Data> ReadKwarg(TextReader reader)
        {
            string kwargName;
            if (!TryReadQuoteText(reader, out kwargName) && !TryReadText(reader, out kwargName))
            {
                // We were not able to read any name  
                throw UnexpectedCharacter(reader, "argument name");
            }

            Require(reader, EQUALS);

            var node = ReadNode(reader);

            return new KeyValuePair<string, Data>(kwargName, node);
        }

        private static bool TryReadCallArgs(string name, TextReader reader, out Data data)
        {
            var start = reader.Peek();
            if (StartsWithPeeking(reader, START_ARGS, START_KWARGS))
            {
                var args = ReadList(reader, START_ARGS, END_ARGS, ReadNode);
                var kwargs = ReadList(reader, START_KWARGS, END_KWARGS, ReadKwarg);

                data = new CallData(name,
                    args : args,
                    kwargs : kwargs);
                return true;
            }
            else
            {
                data = null;
                return false;
            }
        }

        private static Data ReadNode(TextReader reader)
        {
            Data data;
            ConsumeWhitespace(reader);

            // Is this an unnamed a callable?
            if (TryReadCallArgs(null, reader, out data)) { return data; }

            // Try to read in any unquoted scalar or callable name:
            string text;
            if (TryReadText(reader, out text) || TryReadQuoteText(reader, out text))
            {
                // Consume any whitespace between the callable name and the args
                ConsumeWhitespace(reader);

                // Is this a named callable?
                if (TryReadCallArgs(text, reader, out data)) { return data; }
                else if (text == "null") return null;
                else if (text != null) return new StringData(text);
                else throw UnexpectedCharacter(reader, "scalar or call arguments");
            }
            else
            {
                throw UnexpectedCharacter(reader, "scalar or callable name");
            }
        }

        public static Data Load(TextReader reader)
        {
            var node = ReadNode(reader);
            Require(reader, SCALAR_TERMINALS);

            return node;
        }

        public static Data Load(string value)
        {
            using(var sr = new StringReader(value))
            {
                return Load(sr);
            }
        }
    }
}