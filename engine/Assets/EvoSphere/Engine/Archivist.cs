using System.Collections.Generic;

namespace EvoSphere.Engine
{
    public interface Archivist
    {
        /// The experiment should call this before the environment is evaluated.
        /// Note that organisms may be null
        void PreEvaulate(Experiment experiment, List<Organism> population);

        /// The experiment should call this after the environment is evaluated.
        /// If this returns false, then the experiment may terminate.
        /// Note that the experiment is not guaranteed to terminate when returns false, 
        // but will not terminate except due to errors otherwise
        bool PostEvaluate(Experiment experiment, List<Organism> population);

        /// Called once after the experiment has terminated, possibly because of errors
        void OnShutdown(Experiment experiment, List<Organism> population);
    }
}