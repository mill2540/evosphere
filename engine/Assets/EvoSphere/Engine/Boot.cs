﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine
{
    [System.Serializable]
    public class MultipleBootInstancesException : System.Exception
    {
        public MultipleBootInstancesException() { }
        public MultipleBootInstancesException(string message) : base(message) { }
        public MultipleBootInstancesException(string message, System.Exception inner) : base(message, inner) { }
        protected MultipleBootInstancesException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    /// The core behavior responsible for actually starting up the EvoSphere runtime 
    public class Boot : MonoBehaviour
    {
        public readonly Guid RunId = Guid.NewGuid();
        public static Boot Instance { get; private set; }

        [SerializeField]
        private Camera bootCamera;
        public Camera Camera { get => bootCamera; }

        // The root experiment for the whole simulation
        public Experiment experiment;
        private GameObject experimentGameObject;

#if UNITY_EDITOR
        [SerializeField]
        private string settingsFileDefault;
#endif

        void HandleLog(string logString, string stackTrace, LogType type)
        {
            if (type == LogType.Exception && experiment != null)
            {
                experiment.EmergencyShutdown();
            }
        }

        public void Start()
        {
            Application.logMessageReceivedThreaded += HandleLog;

            if (Instance == null) { Instance = this; }
            else
            {
                throw new MultipleBootInstancesException("An Instance of Boot already exists in this scene");
            }

            Debug.Log("Starting boot ...");

            // First, we need to get a startup configuration from somewhere.
            string settingsFile = System.Environment.GetEnvironmentVariable("EVOSPHERE_CONFIG")?.Trim();

#if UNITY_EDITOR
            if (settingsFile == null || settingsFile == "")
            {
                Debug.Log("\tPrompting for settings file...");
                // If we are in the editor, show a popup asking for a settings file
                settingsFile = UnityEditor.EditorUtility.OpenFilePanel("Select Settings File", "", "evosphere");
                Debug.Log($"\t... Got {settingsFile}");
            }
#endif

            if (settingsFile == null || settingsFile == "")
            {
                Debug.LogError("No settings file selected");
                Application.Quit();
                Debug.Break();
            }
            else if (!File.Exists(settingsFile))
            {
                Debug.LogError($"No such file as \"{settingsFile}\" exists");
                Application.Quit();
                Debug.Break();
            }
            else
            {
                using(var reader = new StreamReader(settingsFile))
                {
                    try
                    {
                        var data = DataIO.Load(reader);

                        Universe.Instance.Scan();

                        Universe.Instance.Scan(typeof(Vector2), forceExpose : true);
                        Universe.Instance.Scan(typeof(Vector2Int), forceExpose : true);
                        Universe.Instance.Scan(typeof(Vector3), forceExpose : true);
                        Universe.Instance.Scan(typeof(Vector3Int), forceExpose : true);
                        Universe.Instance.Scan(typeof(Vector4), forceExpose : true);

                        Universe.Instance.Scan(typeof(System.Convert), forceExpose : true);

                        experimentGameObject = new GameObject();

                        experiment = data.Invoke<Experiment>(experimentGameObject);
                    }
                    catch (System.Exception ex)
                    {
                        Debug.Log($"Error loading {settingsFile}");
                        Debug.LogException(ex);
                        Debug.LogError(ex.StackTrace);
                        Application.Quit();
                        Debug.Break();
                    }

                }
            }
        }

        public void Update()
        {
            if (experiment == null && experimentGameObject != null)
            {
                Application.Quit();
                Destroy(experimentGameObject);
            }
        }

        public void OnDestroy()
        {
            Application.logMessageReceivedThreaded -= HandleLog;
        }

    }
}