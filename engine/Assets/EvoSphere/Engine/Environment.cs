using System;
using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine
{
    public abstract class Environment : MonoBehaviour
    {
        /// This must call Organism.Born for every organism in the population
        /// And then it should start calling Organism.UpdateOrganism for every organism in the population.
        /// If this function reassigns population, then it is responsible for cleaning up any members of 
        /// the population who are leaked. Generally, this should only nessesary if population != null 
        /// and you do population = y, which most populations do not need to do.
        /// It can be assumed that there will never be a case where 
        /// multiple evaluations are in flight at once
        public abstract void StartEvaluate(ref List<Organism> population);

        /// This will be used by the experiment to determine when the environment has finished running.
        /// It should be true when the evaluation is complete
        public abstract bool IsEvaluateFinished { get; }
    }

    public abstract class Coenvironment : Environment
    {
        private bool isEvaluateFinished = false;
        public override bool IsEvaluateFinished { get => isEvaluateFinished; }

        public override void StartEvaluate(ref List<Organism> population)
        {
            if (population == null)
            {
                population = NewPopulation();
            }

            if (population != null)
            {
                isEvaluateFinished = false;
                var evaluate = Evaluate(population);
                if (evaluate != null) { StartCoroutine(DoEvaluate(evaluate)); }
                else { isEvaluateFinished = true; }
            }
        }

        private IEnumerator DoEvaluate(IEnumerator enumerator)
        {
            while (enumerator.MoveNext()) { yield return enumerator.Current; }
            isEvaluateFinished = true;
        }

        /// Create a new population when we are not given one.
        /// Note that it is valid for this to be called multiple times
        /// in one experiment
        public abstract List<Organism> NewPopulation();

        /// Evaluate a population we have been given.
        /// population will never be null.
        /// Any exceptions that occure here will kill the app and trigger shutdown
        public abstract IEnumerator Evaluate(List<Organism> population);
    }
}