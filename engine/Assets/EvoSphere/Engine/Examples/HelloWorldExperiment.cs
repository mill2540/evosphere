using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine.Examples
{
    /// The core behavior responsible for actually starting up the EvoSphere runtime
    [Expose]
    public class HelloWorldExperiment : MonoBehaviour
    {
        void Start()
        {
            Debug.Log("Hello World!");
        }
    }
}