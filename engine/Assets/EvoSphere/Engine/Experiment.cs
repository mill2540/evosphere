using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine
{
    /// The core behavior responsible for actually starting up the EvoSphere runtime
    public class Experiment : MonoBehaviour
    {

        public readonly Guid id = Guid.NewGuid();

        public enum State
        {
            READ_TO_EVALUATE,
            EVALUATING,
            CLEANING,
            SHUTDOWN
        }

        [Expose]
        public State state = State.READ_TO_EVALUATE;

        private List<Organism> population = null;
        private List<Organism> nextPopulation = null;

        public string experimentName;
        public Environment environment;
        public Selector selector;
        public Archivist archivist;

        [MonoBehaviourMethodInitializer]
        public void CreateExperiment(Environment environment, Selector selector, string name = null, Archivist archivist = null)
        {
            this.experimentName = name;
            this.environment = environment;
            this.selector = selector;
            this.archivist = archivist;
        }

        private void EnterEvaluating()
        {
            if (environment != null)
            {
                if (archivist != null)
                {
                    archivist.PreEvaulate(this, population);
                }
                environment.StartEvaluate(ref population);
                state = State.EVALUATING;
            }
            else
            {
                // If we don't have a population, 
                // there's not much point in doing anything
                state = State.SHUTDOWN;
            }
        }

        private void Evaluate()
        {
            if (environment.IsEvaluateFinished)
            {
                if (population == null)
                {
                    state = State.SHUTDOWN;
                }
                else
                {
                    foreach (var organism in population)
                    {
                        organism.FitnessFunction.PostEvaluate(organism);
                    }

                    foreach (var organism in population) { organism.Data.Freeze(); }

                    if (archivist == null || archivist.PostEvaluate(this, population))
                    {
                        if (selector != null)
                        {
                            nextPopulation = selector.Select(population);
                        }

                        // Start the cleaning process

                        // Destory everyone in the old population
                        foreach (var organism in population)
                        {
                            // TODO: only destory the organism, so we can use an  object pool
                            Destroy(organism.gameObject);
                        }

                        state = State.CLEANING;
                    }
                    else
                    {
                        state = State.SHUTDOWN;
                    }
                }
            }
        }

        public void EmergencyShutdown()
        {
            Shutdown();
            Application.Quit();
            Debug.Break();
        }

        private void Cleaning()
        {
            // Wait until every member of the population is destroyed
            if (population != null && population.All(organism => organism == null))
            {
                population = nextPopulation;
                nextPopulation = null;

                state = State.READ_TO_EVALUATE;
            }
        }

        private void Shutdown()
        {
            if (archivist != null) { archivist.OnShutdown(this, population); }

            if (environment != null)
            {
                Destroy(environment);
            }

            if (population != null)
            {
                foreach (var organism in population)
                {
                    Destroy(organism.gameObject);
                }
            }
        }

        public void Update()
        {
            while (true)
            {
                switch (state)
                {
                    case State.READ_TO_EVALUATE:
                        EnterEvaluating();
                        // We want to be able to go straight 
                        // to evaluate or shutdown
                        break;
                    case State.EVALUATING:
                        Evaluate();
                        // If we are not finished evaluating now,
                        // we want to wait at least one frame
                        return;
                    case State.CLEANING:
                        Cleaning();
                        // simular to evaluate
                        break;
                    case State.SHUTDOWN:
                        Shutdown();
                        Destroy(this);
                        // We always exit right now
                        return;
                }
            }
        }

    }
}