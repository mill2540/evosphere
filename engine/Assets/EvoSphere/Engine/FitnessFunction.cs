namespace EvoSphere.Engine
{
    public interface FitnessFunction
    {
        void PostBorn(Organism organism);

        void PreUpdate(Organism organism, float[] state);
        void PostUpdate(Organism organism, float[] state);

        void PostEvaluate(Organism organism);
    }
}