using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using Newtonsoft.Json.Linq;
using UnityEngine;

namespace EvoSphere.Engine
{
    public class OrganismData
    {
        private bool frozen = false;
        private JObject frozenCache = null;
        public readonly Guid id = Guid.NewGuid();

        private readonly List<OrganismData> parents;
        public IReadOnlyList<OrganismData> Parents { get => parents; }
        private readonly Dictionary<string, object> data = new Dictionary<string, object>();

        public OrganismData(List<OrganismData> parents)
        {
            this.parents = parents;
        }

        public object Get(string key) { return data[key]; }
        public bool TryGet(string key, out object value)
        {
            return data.TryGetValue(key, out value);
        }

        public void Set(string key, object value)
        {
            if (frozen) throw new Exception("Setting Frozen data it not allowed");
            data[key] = value;
        }

        private JObject CalculateJson()
        {
            var jsonData = new JObject();

            foreach (var entry in data)
            {
                jsonData[entry.Key] = entry.Value.ToString();
            }

            return new JObject()
            {
                {
                    "id",
                    id.ToString()
                },
                {
                    "parentIds",
                    new JArray(parents.Select(parent => new JValue(parent.id)))
                },
                {
                    "data",
                    jsonData
                }
            };
        }

        public void Freeze()
        {
            this.frozen = true;
        }

        [Expose]
        public JObject SaveJson()
        {
            if (frozen)
            {
                if (frozenCache == null)
                {
                    frozenCache = CalculateJson();
                }

                return frozenCache;
            }
            else
            {
                return CalculateJson();
            }
        }
    }

    public class Organism : MonoBehaviour
    {
        private OrganismData data;
        [Expose]
        public OrganismData Data { get => data; }

        [SerializeField]
        private float[] states;
        public List<OrganismComponent> Components
        {
            get;
            private set;
        }

        public FitnessFunction FitnessFunction { get; set; }

        [MonoBehaviourMethodInitializer]
        public void Init(int states, params Data[] components)
        {
            this.states = new float[states];

            Components = new List<OrganismComponent>();

            foreach (var component in components)
            {
                Components.Add(component.Invoke<OrganismComponent>(gameObject));
            }
        }

        public static Organism Replicate(List<Organism> parents)
        {
            var organism = new GameObject().AddComponent<Organism>();
            organism.Inherit(parents);
            return organism;
        }

        public static Organism Replicate(params Organism[] parents)
        {
            var organism = new GameObject().AddComponent<Organism>();
            organism.Inherit(parents);
            return organism;
        }

        public void Inherit(params Organism[] parents) { Inherit(parents.ToList()); }

        public void Inherit(List<Organism> parents)
        {
            this.data = new OrganismData(parents?.Select(parent => parent.Data)?.ToList() ?? new List<OrganismData>());

            List<Type> componentTypes = null;

            // This organism gets the largest state buffer of any parent
            if (parents.Count > 0)
            {
                this.states = new float[parents.Max(parent => parent.states.Length)];

                foreach (var parent in parents)
                {
                    if (componentTypes == null)
                    {
                        componentTypes = parent.Components.Select(component => component.GetType()).ToList();
                    }
                    else if (componentTypes.Count != parent.Components.Count)
                    {
                        throw new Exception("Organism's cannot inherit from parents with different numbers of components");
                    }

                    for (int i = 0; i < componentTypes.Count; i++)
                    {
                        if (componentTypes[i] != parent.Components[i].GetType())
                        {
                            throw new Exception("Organism's cannot inherit from parents with different types of components");
                        }
                    }
                }

                Components = new List<OrganismComponent>();

                for (int i = 0; i < componentTypes.Count; i++)
                {
                    // This assumes that all the parents of this organism have the same 
                    // OrganismComponents in the same order as this Organism's OrganismComponents.
                    // If that is not true, it *may* result in errors as this
                    // Organism's OrganismComponents attempt to do their inheritance,
                    // because they will be given parents of a different type then themselves

                    // TODO: this may cause issues when there are populations of heterogenous organisms,
                    // ie, organisms with different OrganismComponents. Could be an issue for experiments
                    // where different types of organisms compete in the same population
                    var component = (OrganismComponent) gameObject.AddComponent(componentTypes[i]);
                    component.Inherit(parents.Select(parent => parent.Components[i]).ToList());

                    Components.Add(component);
                }
            }
            else
            {
                foreach (var component in Components)
                {
                    component.Inherit(new List<OrganismComponent>());
                }
            }
        }

        public IEnumerator Born()
        {
            foreach (var component in Components)
            {
                var e = component.Born();
                if (e != null)
                {
                    while (e.MoveNext()) { yield return e.Current; }
                }
            }
        }

        public void EvoSphereUpdate()
        {
            FitnessFunction.PreUpdate(this, states);

            foreach (var component in Components)
            {
                component.EvoSphereUpdate(states);
            }

            FitnessFunction.PostUpdate(this, states);
        }

        public void EvoSphereFixedUpdate()
        {
            foreach (var component in Components)
            {
                component.EvoSphereFixedUpdate(states);
            }
        }

        private void OnDestroy()
        {
            foreach (var component in Components)
            {
                Destroy(component);
            }

            if (this.data != null)
            {
                this.data.Freeze();
            }
        }

        public Bounds? GetBounds()
        {
            return Components.Select(component => component.GetBounds()).Aggregate(
                (Bounds?) null,
                (acc, maybeBounds) =>
                {
                    if (acc is Bounds accBounds)
                    {
                        if (maybeBounds is Bounds bounds)
                        {
                            accBounds.Encapsulate(bounds);
                        }

                        return accBounds;
                    }
                    else if (maybeBounds is Bounds bounds)
                    {
                        return bounds;
                    }
                    else
                    {
                        return null;
                    }
                });
        }
    }
}