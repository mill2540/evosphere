using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine
{
    /// The core behavior responsible for actually starting up the EvoSphere runtime 
    public abstract class OrganismComponent : MonoBehaviour
    {
        public Organism Organism
        {
            get => gameObject.GetComponent<Organism>();
        }

        public abstract void Inherit(List<OrganismComponent> parents);

        public virtual IEnumerator Born() { yield break; }

        public virtual void EvoSphereUpdate(float[] state) { }
        public virtual void EvoSphereFixedUpdate(float[] state) { }

        public virtual Bounds? GetBounds()
        {
            return GetComponentsInChildren<Collider>().Select(collider => collider.bounds).Aggregate(
                (Bounds?) null,
                (acc, bounds) =>
                {
                    if (acc is Bounds accBounds)
                    {
                        accBounds.Encapsulate(bounds);
                        return accBounds;
                    }
                    else
                    {
                        return bounds;
                    }
                });
        }
    }
}