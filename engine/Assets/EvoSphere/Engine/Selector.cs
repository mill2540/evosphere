using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine
{
    public interface Selector
    {
        List<Organism> Select(List<Organism> organisms);
    }
}