using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using UnityEngine;

namespace EvoSphere.Engine.Utilities
{

    internal class ObjectPoolMember<T> : MonoBehaviour where T : MonoBehaviour
    {
        internal ObjectPool<T> owner;
        internal T behavior;

        internal void Free()
        {
            if (behavior != null)
            {
                MonoBehaviour.Destroy(behavior);
                behavior = null;
            }
        }

        public bool IsFree { get => behavior == null; }
        public bool IsUsed { get => behavior != null; }

        public bool IsOwnedBy(ObjectPool<T> op) => op == owner;

        public bool CannotBeOwnedBy(ObjectPool<T> canidate) => owner != null && canidate != owner;
    }

    public class ObjectPool<T> where T : MonoBehaviour
    {

        private List<ObjectPoolMember<T>> Free { get; }
        private List<ObjectPoolMember<T>> Used { get; }

        public delegate T CreateBehaviorAction(GameObject go);

        public ObjectPool()
        {
            Free = new List<ObjectPoolMember<T>>();
            Used = new List<ObjectPoolMember<T>>();
        }

        private void AttachAndFree(ObjectPoolMember<T> member)
        {
            if (member != null)
            {
                member.owner = this;
                member.Free();

                Free.Add(member);
            }
        }

        /// Create a new object and add it to the free pool
        private void NewFreeObject(GameObject go = null)
        {
            if (go == null)
            {
                go = new GameObject();
            }

            var marker = go.GetComponent<ObjectPoolMember<T>>();

            if (marker == null || marker.CannotBeOwnedBy(this))
            {
                marker = go.AddComponent<ObjectPoolMember<T>>();
            }

            AttachAndFree(marker);
        }

        private bool TryPopFree(out ObjectPoolMember<T> member)
        {
            if (Free.Count == 0)
            {
                member = null;
                return false;
            }
            else
            {
                member = Free[Free.Count - 1];
                Free.RemoveAt(Free.Count - 1);

                return true;
            }
        }

        /// Scan through the objects who currently might be in use, 
        // and if any can be safely resused, add them to the free pool
        public void FreeUnusedUsed()
        {
            // Scan through all the items in the used pool
            for (int i = Used.Count - 1; i >= 0; --i)
            {
                var used = Used[i];
                // If we find someone who can be reused, then remove it from 
                // the used pool and add its object to the free pool 
                if (used.IsFree)
                {
                    Used.RemoveAt(i);
                    AttachAndFree(used);
                }
            }
        }

        public void EnsureCapacity(int capacity, bool freeUnusedUsed = true)
        {
            if (freeUnusedUsed || Free.Count < capacity)
            {
                FreeUnusedUsed();
            }

            while (Free.Count < capacity)
            {
                NewFreeObject();
            }
        }

        public T Create(CreateBehaviorAction createBehavior, bool freeUnusedUsed = true)
        {
            return CreateAll(1, createBehavior, freeUnusedUsed).First();
        }

        public IEnumerable<T> CreateAll(int count, CreateBehaviorAction createBehavior, bool freeUnusedUsed = true)
        {
            if (freeUnusedUsed)
            {
                FreeUnusedUsed();
            }

            // Loop untill we have enough objects
            while (count > 0)
            {
                // Get an object out of the pool
                ObjectPoolMember<T> free = null;
                if (TryPopFree(out free))
                {
                    if (free != null)
                    {
                        --count;
                        var behavior = createBehavior(free.gameObject);

                        free.behavior = behavior;

                        yield return behavior;
                    }
                }
                else
                {
                    // If we couldn't get anything, create a new one
                    NewFreeObject();
                }
            }
        }

        private bool IsOwnerOf(GameObject go)
        {
            return go.GetComponent<ObjectPoolMember<T>>()?.IsOwnedBy(this) ?? false;
        }

        /// The object pool should start managing this gameObject
        public void Manage(GameObject go, T behavior = null)
        {
            if (!IsOwnerOf(go))
            {
                var member = go.AddComponent<ObjectPoolMember<T>>();
                member.owner = this;
                member.behavior = behavior ?? go.GetComponent<T>();
            }
        }

        /// The object pool should stop managing this gameobject
        public void Forget(GameObject go)
        {
            var member = go.GetComponent<ObjectPoolMember<T>>();
            if (member != null && member.IsOwnedBy(this))
            {
                Free.Remove(member);
                Used.Remove(member);

                MonoBehaviour.Destroy(member);
            }
        }
    }
}