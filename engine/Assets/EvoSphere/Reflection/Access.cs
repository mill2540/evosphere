﻿namespace EvoSphere.Core.Reflection
{
    public enum Access
    {
        Get,
        Set
    }
}