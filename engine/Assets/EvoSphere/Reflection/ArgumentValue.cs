using System;
using System.Linq;

namespace EvoSphere.Core.Reflection
{

    public struct ArgumentValue<V>
    {
        public readonly string Name;
        public readonly int Position;
        public readonly V Value;
        public readonly Type Type;

        public ArgumentValue(string name, int position, V value, Type type)
        {
            this.Name = name;
            this.Position = position;
            this.Value = value;
            this.Type = type;
        }

        public ArgumentValue<R> Map<R>(Func<V, R> map) =>
        new ArgumentValue<R>(Name, Position, map(Value), Type);

        public override string ToString() => $"{ReflectionUtils.GetNiceName(Type)} {Name} = {Value}";
    }
}