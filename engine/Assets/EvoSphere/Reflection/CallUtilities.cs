using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{
    public static class CallUtilities
    {
        public delegate Type TypeChecker<T>(T value, Type hint, out string errors);
        public delegate object ValueOf<T>(T value, Type hint, GameObject target);

        public static bool TryVisitSignature<T>(Signature signature,
            IEnumerable<T> argsIn,
            IEnumerable<KeyValuePair<string, T>> kwargsIn,
            Action<Parameter, int, T> argMatch,
            Action<Parameter, int, object> optionalArg,
            Action<Parameter, int, Type, T[]> paramsArgs,
            out string error)
        {
            var args = new Queue<T>(argsIn);
            var kwargs = kwargsIn.ToDictionary(entry => entry.Key, entry => entry.Value);

            var parameters = signature.Parameters;
            var paramsParameter = signature.ParamsParameter;

            var argumentValues = new List<ArgumentValue<object>>();

            int position = 0;
            foreach (var parameter in parameters)
            {
                T kwarg;
                if (kwargs.TryGetValue(parameter.Name, out kwarg))
                {
                    argMatch(parameter, position, kwarg);
                    kwargs.Remove(parameter.Name);
                }
                else if (args.Count != 0)
                {
                    argMatch(parameter, position, args.Dequeue());
                }
                else if (parameter.IsOptional)
                {
                    if (parameter.Default != null)
                    {
                        optionalArg(parameter, position, parameter.Default.Value.DefaultValue);
                    }
                }
                else
                {
                    error = $"parameter not bound: {parameter}";
                    return false;
                }
                ++position;
            }

            if (paramsParameter != null)
            {
                T kwarg;
                if (kwargs.TryGetValue(paramsParameter.Name, out kwarg))
                {
                    argMatch(paramsParameter, position, kwarg);
                    kwargs.Remove(paramsParameter.Name);
                }
                else
                {
                    var arrayType = paramsParameter.Type.GetElementType();
                    paramsArgs(paramsParameter, position, arrayType, args.ToArray());
                    args.Clear();
                }
            }

            if (kwargs.Count > 0)
            {
                error = $"too many arguments for {signature}";
                return false;
            }
            else if (args.Count > 0)
            {
                error = $"too many arguments for {signature}";
                return false;
            }
            else
            {
                error = null;
                return true;
            }
        }

        public static int TypeCheckCall<T>(
            Signature signature,
            IEnumerable<T> args,
            IEnumerable<KeyValuePair<string, T>> kwargs,
            TypeChecker<T> typeChecker,
            out string error)
        {
            var argumentErrors = new List<string>();

            // UnityEngine.Debug.Log($"\tTypeCheckCall {signature}");

            int badness = 0;

            bool success = TryVisitSignature(
                signature,
                args,
                kwargs,
                (parameter, position, data) =>
                {
                    string parameterError;
                    var argType = typeChecker(data, parameter.Type, out parameterError);

                    if (parameterError != null)
                    {
                        argumentErrors.Add($"in {parameter.Name}: {parameterError}");
                    }

                    if (argType == null || parameter.Type != argType)
                    {
                        ++badness;
                    }
                },
                (parameter, position, defaultValue) =>
                {
                    // There cannot be type errors here
                    ++badness;
                },
                (parameter, position, elementType, elementData) =>
                {
                    for (int i = 0; i < elementData.Length; i++)
                    {
                        var element = elementData[i];

                        string parameterError;
                        var elemType = typeChecker(element, elementType, out parameterError);

                        if (parameterError != null)
                        {
                            argumentErrors.Add($"in {parameter.Name}[{i}]: {parameterError}");
                        }

                        if (elemType == null || parameter.Type == elemType)
                        {
                            ++badness;
                        }
                    }
                },
                out error);

            if (argumentErrors.Count > 0)
            {
                error = $"in {signature}: {string.Join("\n", argumentErrors)}";

                return int.MaxValue;
            }
            else if (success)
            {
                error = null;
                return badness;
            }
            else { return int.MaxValue; }
        }

        private class SelectedCanidate
        {
            public Callable canidate;
            public int badness;
        }

        public static Callable SelectCallable<T>(
            string name,
            Type hint,
            IEnumerable<T> argsIn,
            IEnumerable<KeyValuePair<string, T>> kwargsIn,
            TypeChecker<T> typeChecker,
            out string error)
        {
            int badness;
            return SelectCallable<T>(name, hint, argsIn, kwargsIn, typeChecker, out badness, out error);
        }

        public static Callable SelectCallable<T>(
            string name,
            Type hint,
            IEnumerable<T> argsIn,
            IEnumerable<KeyValuePair<string, T>> kwargsIn,
            TypeChecker<T> typeChecker,
            out int badness,
            out string error)
        {
            if (name == null && hint == null)
            {
                error = null;
                badness = int.MaxValue;
                return null;
            }

            var args = argsIn.ToList();
            var kwargs = kwargsIn.ToList();

            var failures = new List<KeyValuePair<Callable, string>>();
            var successes = new List<SelectedCanidate>();

            foreach (var canidate in Universe.Instance.FindAll(name, hint))
            {
                string canidateErrors;
                var canidateBadness = TypeCheckCall(
                    canidate.Signature,
                    args, kwargs, typeChecker,
                    out canidateErrors);

                if (canidateErrors == null)
                {
                    successes.Add(new SelectedCanidate() { canidate = canidate, badness = canidateBadness });
                }
                else
                {
                    failures.Add(new KeyValuePair<Callable, string>(canidate, canidateErrors));
                }
            }

            if (successes.Count >= 1)
            {
                successes.Sort((x, y) => x.badness.CompareTo(y.badness));

                if (successes.Count == 1 || successes[0].badness < successes[1].badness)
                {
                    error = null;
                    badness = successes[0].badness;
                    return successes[0].canidate;
                }
                else
                {
                    List<string> idents = new List<string>();
                    if (hint != null)
                    {
                        idents.Add("return type " + ReflectionUtils.GetNiceName(hint));
                    }
                    if (name != null)
                    {
                        idents.Add("name " + name);
                    }

                    var champions = (IEnumerable<SelectedCanidate>) successes
                        .GroupBy(x => x.badness)
                        .FirstOrDefault() ?? new List<SelectedCanidate>();

                    var argsStr = string.Join(", ", args.Select(arg => arg.ToString())
                        .Concat(kwargs.Select(entry => $"{entry.Key} = {entry.Value}")));

                    error = $"found multiple possible callables with {string.Join(" and ", idents)} for ({argsStr})\n" +
                        string.Join("\n", champions.Select(canidate => $"\t[{canidate.badness}] " + canidate.canidate.Signature.ToString()));

                    badness = int.MaxValue;
                    return null;
                }
            }
            else
            {
                List<string> idents = new List<string>();
                if (hint != null)
                {
                    idents.Add("return type " + ReflectionUtils.GetNiceName(hint));

                }
                if (name != null)
                {
                    idents.Add("name " + name);
                }

                var argsStr = string.Join(", ", args.Select(arg => arg.ToString())
                    .Concat(kwargs.Select(entry => $"{entry.Key} = {entry.Value}")));

                error = $"found no possible callables with {string.Join(" and ", idents)} for ({argsStr}):\n" +
                    string.Join("\n", failures.Select(entry => $"\tcannot use {entry.Key.Signature} because {entry.Value}"));

                badness = int.MaxValue;
                return null;
            }
        }

        public static object InvokeCall<T>(
            Callable callable,
            IEnumerable<T> argsIn,
            IEnumerable<KeyValuePair<string, T>> kwargsIn,
            ValueOf<T> valueOf,
            GameObject target)
        {
            var argumentValues = new List<ArgumentValue<object>>();

            Nullable<ArgumentValue<object>> maybeParamsArgument = null;
            Array paramsValue;

            string error;
            var success = TryVisitSignature(
                callable.Signature,
                argsIn,
                kwargsIn,
                (parameter, position, data) =>
                {
                    argumentValues.Add(parameter.CreateArgumentValue(valueOf(data, parameter.Type, target), position));
                },
                (parameter, position, defaultValue) =>
                {
                    argumentValues.Add(parameter.CreateArgumentValue(defaultValue, position));
                },
                (parameter, position, elementType, elementData) =>
                {
                    paramsValue = Array.CreateInstance(elementType, elementData.Length);
                    for (int i = 0; i < elementData.Length; i++)
                    {
                        paramsValue.SetValue(valueOf(elementData[i], elementType, target), i);
                    }

                    maybeParamsArgument = parameter.CreateArgumentValue((object) paramsValue, position);
                },
                out error);

            if (success)
            {
                if (maybeParamsArgument is ArgumentValue<object> paramsArgument)
                {
                    argumentValues.Add(paramsArgument);
                }

                return callable.UncheckedInvoke(target, argumentValues.ToArray());
            }
            else
            {
                throw new Exception(error);
            }
        }

        public static object Invoke(
            this Callable callable,
            IEnumerable<object> argsIn,
            IEnumerable<KeyValuePair<string, object>> kwargsIn,
            GameObject target)
        {
            return InvokeCall(
                callable,
                argsIn,
                kwargsIn,
                (v, hint, go) => v,
                target);
        }

        public static object InvokeArgs(
            this Callable callable,
            params object[] args)
        {
            return Invoke(callable, args, new KeyValuePair<string, object>[0], null);
        }

    }
}