using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{

    public abstract partial class Callable
    {
        public Guid Id { get; }

        public abstract Signature Signature { get; }

        protected Callable()
        {
            this.Id = Guid.NewGuid();
        }

        public abstract object UncheckedInvoke(GameObject target, ArgumentValue<object>[] args);

        public override string ToString()
        {
            return string.Format("[{0}] {1}", Id, Signature);
        }
    }
}