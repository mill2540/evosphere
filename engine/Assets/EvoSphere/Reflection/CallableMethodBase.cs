using System;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{
    [System.Serializable]
    public class ExpectedInstanceException : System.Exception
    {
        public ExpectedInstanceException() { }
        public ExpectedInstanceException(string message) : base(message) { }
        public ExpectedInstanceException(string message, System.Exception inner) : base(message, inner) { }
        protected ExpectedInstanceException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    public abstract class CallableMethodLike : Callable
    {

        private readonly Type InstanceType;

        public CallableMethodLike(Type instanceType) { this.InstanceType = instanceType; }

        public override object UncheckedInvoke(GameObject target, ArgumentValue<object>[] args)
        {
            if (args.Length > 0 &&
                InstanceType != null)
            {
                if (args[0].Value != null && InstanceType.IsAssignableFrom(args[0].Value.GetType()))
                {
                    return UncheckedMethodInvoke(args[0].Value, args.Skip(1).ToArray());
                }
                else { throw new ExpectedInstanceException(); }
            }
            else { return UncheckedMethodInvoke(null, args); }
        }

        public abstract object UncheckedMethodInvoke(object instance, ArgumentValue<object>[] args);
    }

    public class CallableMethodInfo : CallableMethodLike
    {
        public MethodInfo MethodInfo { get; }

        public override Signature Signature { get; }

        public CallableMethodInfo(
            MethodInfo method,
            string[] names) : base(method.IsStatic ? null : method.DeclaringType)
        {
            this.MethodInfo = method;
            this.Signature = Signature.Method(method, names : names);
        }

        public override object UncheckedMethodInvoke(object instance, ArgumentValue<object>[] args) =>
            MethodInfo.Invoke(instance, args.Select(arg => arg.Value).ToArray());
    }
}