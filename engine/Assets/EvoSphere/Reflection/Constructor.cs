using System.Linq;
using System.Reflection;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{
    public sealed class Constructor : Callable
    {
        public ConstructorInfo Info { get; }

        public override Signature Signature { get; }

        public Constructor(ConstructorInfo info, params string[] names) : base()
        {
            this.Info = info;
            this.Signature = Signature.Method(info, names : names);
        }

        public override object UncheckedInvoke(GameObject target, ArgumentValue<object>[] args) =>
            Info.Invoke(args.Select(value => value.Value).ToArray());
    }
}