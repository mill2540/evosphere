using System;
using System.Collections.Generic;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    [AttributeUsage(AttributeTargets.Field |
        AttributeTargets.Property |
        AttributeTargets.Method |
        AttributeTargets.Constructor |
        AttributeTargets.Class |
        AttributeTargets.Struct)]
    public class ExposeAttribute : Attribute
    {
        public string[] Names { get; }

        public ExposeAttribute(params string[] names)
        {
            Names = names;
        }

    }

    public class ExposedInfo
    {
        public readonly MemberInfo Info;
        public readonly string[] Names;
        public Callable Callable;

        public ExposedInfo(MemberInfo info, string[] names, Callable callable = null)
        {
            this.Info = info;
            this.Names = names ?? new string[0];
            this.Callable = callable;
        }

        public static ExposedInfo GetExposed(MemberInfo info, bool forceExpose)
        {
            if (info.IsDefined(typeof(ExposeAttribute), false))
            {
                var attribute = info.GetCustomAttribute(typeof(ExposeAttribute), false) as ExposeAttribute;
                return new ExposedInfo(info, attribute.Names);
            }
            else if (forceExpose)
            {
                // Handle black-listed methods
                if (info is MethodInfo method)
                {
                    if (method.Name == "Equals" &&
                        !method.IsStatic && !method.IsConstructor &&
                        method.ReturnType.IsEquivalentTo(typeof(bool)) &&
                        method.GetParameters().Length == 1 &&
                        method.GetParameters() [0].ParameterType.IsEquivalentTo(typeof(object))) { return null; }
                    else if (method.Name == "GetHashCode" &&
                        !method.IsStatic && !method.IsConstructor &&
                        method.GetParameters().Length == 0 &&
                        method.ReturnType.IsEquivalentTo(typeof(int))) { return null; }
                    else if (method.Name == "GetType" &&
                        !method.IsStatic && !method.IsConstructor &&
                        method.GetParameters().Length == 0 &&
                        method.ReturnType.IsEquivalentTo(typeof(Type))) { return null; }
                    else if (method.Name == "ToString" &&
                        !method.IsStatic && !method.IsConstructor &&
                        method.GetParameters().Length == 0 &&
                        method.ReturnType.IsEquivalentTo(typeof(String))) { return null; }
                }

                // If we got past the blacklist, then we are done, and we continue normally
                return new ExposedInfo(info, null);
            }
            else { return null; }
        }
    }
}