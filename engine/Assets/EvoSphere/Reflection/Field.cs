using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    public sealed class Field : CallableMethodLike
    {

        public Access Access { get; }
        public FieldInfo FieldInfo { get; }

        public override Signature Signature { get; }

        public Field(Access access, FieldInfo info, params string[] names) : base(info.IsStatic ? null : info.DeclaringType)
        {
            this.Access = access;
            this.FieldInfo = info;
            this.Signature = Signature.MethodLike(
                declaringType: info.DeclaringType,
                isMethod: !info.IsStatic,
                names : new string[] { info.Name }.Concat(names).ToList(),
                returnType : access == Access.Set ? typeof(void) : info.FieldType,
                preParameters : access == Access.Set ?
                new List<Parameter>()
                {
                    new Parameter("value",
                        info.FieldType)
                } :
                null);
        }

        public override object UncheckedMethodInvoke(object instance, ArgumentValue<object>[] args)
        {
            if (Access == Access.Get)
            {
                return FieldInfo.GetValue(instance);
            }
            else
            {
                FieldInfo.SetValue(instance, args[0].Value);
                return null;
            }
        }
    }
}