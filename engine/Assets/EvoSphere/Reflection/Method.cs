using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    public sealed class Method : CallableMethodInfo
    {

        public Method(MethodInfo method, params string[] names) : base(method, names) { }

    }

}