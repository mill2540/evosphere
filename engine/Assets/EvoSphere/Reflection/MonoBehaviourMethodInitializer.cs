using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{
    public class MonoBehaviourMethodInitalizer : Callable
    {
        public readonly Type Type;
        public override Signature Signature { get; }

        private readonly MethodInfo Method;

        public MonoBehaviourMethodInitalizer(Type type, MethodInfo method, params string[] names) : base()
        {
            this.Type = type;
            this.Method = method;
            this.Signature = Signature.Method(
                method,
                isMarkedAsMonoBehaviourMethodInitializer : true,
                names : names);
        }

        public override object UncheckedInvoke(GameObject target, ArgumentValue<object>[] args)
        {
            if (target == null)
            {
                throw new ArgumentNullException("MonoBehaviors cannot be initialized with out a GameObject");
            }
            var instance = target.AddComponent(Signature.ReturnType);
            Method.Invoke(instance, args.Select(argumentValue => argumentValue.Value).ToArray());

            return instance;
        }
    }
}