using System;
using System.Collections.Generic;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    [AttributeUsage(AttributeTargets.Method)]
    public class MonoBehaviourMethodInitializerAttribute : Attribute
    {
        public string[] Names { get; }

        public MonoBehaviourMethodInitializerAttribute(params string[] names)
        {
            Names = names;
        }

    }
}