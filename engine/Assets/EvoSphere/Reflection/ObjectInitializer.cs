﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{
    [System.Serializable]
    public class ExpectedDefaultConstructorException : System.Exception
    {
        public ExpectedDefaultConstructorException() { }
        public ExpectedDefaultConstructorException(string message) : base(message) { }
        public ExpectedDefaultConstructorException(string message, System.Exception inner) : base(message, inner) { }
        protected ExpectedDefaultConstructorException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    public class ObjectInitializer : Callable
    {
        private interface Initializer
        {
            string Name { get; }
            void SetValue(object target, object value);
        }

        private class PropertyInitializer : Initializer
        {
            public MethodInfo Setter;

            public string Name => Setter.Name;

            public void SetValue(object target, object value) => Setter.Invoke(target, new object[] { value });
        }

        private class FieldInitializer : Initializer
        {
            public FieldInfo Field;

            public string Name =>
                Field.Name;

            public void SetValue(object target, object value) => Field.SetValue(target, value);
        }

        public readonly Type Type;
        public override Signature Signature { get; }

        private readonly ConstructorInfo Constructor;
        private readonly Dictionary<string, Initializer> initializers;

        public ObjectInitializer(Type type, ConstructorInfo constructor, params string[] names) : base()
        {
            this.Type = type;

            Constructor = constructor;

            // Collect all the parameters
            List<Parameter> parameters = new List<Parameter>();
            initializers = new Dictionary<string, Initializer>();
            foreach (var member in Type.GetMembers())
            {
                if (member is FieldInfo field)
                {
                    if (!field.IsInitOnly)
                    {
                        parameters.Add(new Parameter(field.Name, field.FieldType, true, null));
                        initializers.Add(field.Name, new FieldInitializer { Field = field });
                    }
                }
                else if (member is PropertyInfo property)
                {
                    if (property.CanWrite)
                    {
                        parameters.Add(new Parameter(property.Name, property.PropertyType, true, null));
                        initializers.Add(property.Name, new PropertyInitializer { Setter = property.GetSetMethod() });
                    }
                }
            }

            var namesList = new List<string>(names);
            namesList.Add(type.Name);

            Signature = new Signature(
                isMethod: false,
                names: namesList,
                returnType: Type,
                parameters: parameters,
                paramsParameter: null);
        }

        public override object UncheckedInvoke(GameObject target, ArgumentValue<object>[] args)
        {
            object instance;
            if (Signature.ReturnType.IsSubclassOf(typeof(ScriptableObject)))
            {
                instance = ScriptableObject.CreateInstance(Signature.ReturnType);
            }
            else if (Signature.ReturnType.IsSubclassOf(typeof(Component)))
            {

                if (target == null)
                {
                    throw new ArgumentNullException("MonoBehaviors cannot be initialized with out a GameObject");
                }

                instance = target.AddComponent(Signature.ReturnType);
            }
            else
            {
                instance = Constructor.Invoke(new object[0]);
            }

            foreach (var arg in args)
            {
                initializers[arg.Name].SetValue(instance, arg.Value);
            }

            return instance;
        }
    }
}