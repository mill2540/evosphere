using System;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    public class Parameter
    {
        public struct DefaultStore
        {
            public readonly object DefaultValue;
            public DefaultStore(object value) { this.DefaultValue = value; }
        }

        public string Name { get; }
        public Type Type { get; }
        public bool IsOptional { get; }
        public DefaultStore? Default { get; }

        public Parameter(string name, Type type, bool isOptional, DefaultStore? d)
        {
            this.Name = name;
            this.Type = type;
            this.IsOptional = isOptional;
            this.Default = d;
        }

        public Parameter(string name, Type type, object d) : this(name, type, true, new DefaultStore(d)) { }

        public Parameter(string name, Type type) : this(name, type, false, null) { }

        public Parameter(ParameterInfo parameter) : this(
            parameter.Name,
            parameter.ParameterType,
            parameter.IsOptional,
            parameter.HasDefaultValue ?
            (DefaultStore?) new DefaultStore(parameter.DefaultValue) :
            null) { }

        public override string ToString()
        {
            return string.Format("{0} {1}{2}",
                ReflectionUtils.GetNiceName(Type),
                Name,
                IsOptional ? string.Format("={0}", Default?.DefaultValue?.ToString() ?? "[unknown]") : "");
        }

        public ArgumentValue<V> CreateArgumentValue<V>(V value, int position) => new ArgumentValue<V>(Name, position, value, Type);

    }
}