using System.Linq;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    public sealed class Property : CallableMethodInfo
    {
        public Access Access { get; }
        public PropertyInfo PropertyInfo { get; }

        public Property(Access access, PropertyInfo propertyInfo, params string[] names) : base(
            access == Access.Set ?
            propertyInfo.GetSetMethod() :
            propertyInfo.GetGetMethod(),
            new string[] { propertyInfo.Name }.Concat(names).ToArray())
        {
            this.Access = access;
            this.PropertyInfo = propertyInfo;
        }

    };

}