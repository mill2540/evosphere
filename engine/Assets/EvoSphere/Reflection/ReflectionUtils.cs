﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    /// <summary>
    /// Utility class for reflections
    /// </summary>
    public static class ReflectionUtils
    {

        public static string GetNiceName(Type type)
        {
            if (type == null)
            {
                return "NULL";
            }
            else if (type.IsArray)
            {
                return $"{GetNiceName(type.GetElementType())}[]";
            }
            else if (type.IsGenericType)
            {
                var name = type.Name.Split('`') [0];

                return $"{name}<{string.Join(", ", type.GetGenericArguments().Select(arg => GetNiceName(arg)))}>";
            }
            else
            {
                return type.Name;
            }
        }

        public static bool IsParams(ParameterInfo info)
        {
            return info.IsDefined(typeof(ParamArrayAttribute), false);
        }

        public static bool AllowsNull(Type type)
        {
            if (!type.IsValueType) { return true; }
            else { return Nullable.GetUnderlyingType(type) != null; }
        }

        public static bool DoesTypeMatchGenericConstraints(Type type, Type genericArg)
        {
            if (!genericArg.IsGenericParameter) throw new ArgumentException("Not a generic argument");

            var attrs = genericArg.GenericParameterAttributes;

            if ((attrs & GenericParameterAttributes.DefaultConstructorConstraint) != 0 &&
                type.GetConstructor(Type.EmptyTypes) == null)
            {
                return false;
            }

            if ((attrs & GenericParameterAttributes.ReferenceTypeConstraint) != 0 &&
                !(type.IsClass || Nullable.GetUnderlyingType(type) != null))
            {
                return false;
            }

            if ((attrs & GenericParameterAttributes.NotNullableValueTypeConstraint) != 0 && !type.IsValueType)
            {
                return false;
            }

            var arguments = genericArg.GetGenericParameterConstraints();

            return arguments.All((argument) => argument.IsAssignableFrom(type));
        }

        public static IEnumerable<Type> FindTypesAcceptedByGenericArgument(Type genericArgument, Assembly assembly = null)
        {
            return FindTypes((type) => DoesTypeMatchGenericConstraints(type, genericArgument), assembly);
        }

        public static IEnumerable<Type> FindTypes(Func<Type, bool> predicate, Assembly assembly = null)
        {
            if (assembly == null) assembly = Assembly.GetExecutingAssembly();
            return assembly.GetTypes().Where(predicate);
        }

        /// <summary>
        /// Scans all the types in an assembly for ones which are decorated with the given attribute.
        /// </summary>
        /// <returns>An enumerator of types with the given attribute</returns>
        /// <param name="assembly">The assembly to scan</param>
        /// <param name="attributes">The attributes to scan for</param>
        /// <param name="inherit">Whether types which are decendents of types with the given attribute should be included.</param>
        public static IEnumerable<Type> FindTypesWithAttributes(Assembly assembly, Type[] attributes, bool inherit = true)
        {
            return FindTypes(type => attributes.Any(attribute => type.IsDefined(attribute, inherit)), assembly);
        }

        public static IEnumerable<MemberInfo> FindMembersWithAttributes(this Type parent, Type[] attributes,
            bool inherit = false)
        {
            return parent.GetMembers(BindingFlags.Public |
                    BindingFlags.NonPublic |
                    BindingFlags.Instance)
                .Where(member => attributes.Any(attribute => member.IsDefined(attribute, inherit)));
        }

        /// <summary>
        /// Finds all the types in the current assembly which have the given attribute.
        /// </summary>
        /// <returns>The types with the given attribute</returns>
        /// <param name="attributes">Attribute.</param>
        /// <param name="inherit">If set to <c>true</c> inherit.</param>
        public static IEnumerable<Type> FindTypesWithAttributes(Type[] attributes, bool inherit = true)
        {
            return FindTypesWithAttributes(Assembly.GetExecutingAssembly(), attributes, inherit);
        }

        public static Type GetGenericInterface(this Type type, Type genericInterface)
        {
            return type.GetInterfaces().FirstOrDefault(iface =>
                iface.IsGenericType && iface.GetGenericTypeDefinition() == genericInterface);
        }

        public struct MapDesc
        {
            public Type Key { get; private set; }
            public Type Value { get; private set; }

            public MapDesc(Type key, Type value) : this()
            {
                Key = key;
                Value = value;
            }
        }

        public static MapDesc GetMapDesc(Type type)
        {
            // Assume that we want to load any object by default.
            var keyType = typeof(object);
            var valueType = typeof(object);

            // Try and get better types, if we can
            var idictGeneric = type.GetGenericInterface(typeof(IDictionary<,>));
            if (idictGeneric == null) return new MapDesc(keyType, valueType);

            var genericArgs = idictGeneric.GetGenericArguments();
            keyType = genericArgs[0];
            valueType = genericArgs[1];

            return new MapDesc(keyType, valueType);
        }

        /// <summary>
        /// Custom implementation of IsAssignableFrom.
        /// </summary>
        /// This exists because when compiled to webgl, the system IsAssignableFrom will fail in some cases. 
        /// In particular, it will fail to correctly handle covariance and contravariance in interfaces.
        /// For example:
        /// <code>
        /// interface Iface&lt; out T&gt; {}
        /// 
        /// class Base {}
        /// class Derived : Base {}
        /// 
        /// Foo&lt;Derived&gt; derived = ...;
        /// Foo&lt;Base&gt; base = derived;
        /// </code>
        /// 
        /// always compiles, but 
        /// <code>typeof(Iface&lt;Base&gt;).IsAssignable(typeof(Iface&ltDerived&gt;))</code> 
        /// will fail, which can be important for some implementations of factories.
        /// 
        /// <param name="type"></param>
        /// <param name="other"></param>
        /// <returns></returns>
        public static bool WebSafeIsAssignableFrom(this Type type, Type other)
        {
            // If the normal IsAssignableFrom works, then we are golden
            if (type.IsAssignableFrom(other)) return true;

            // However, if the type to check is a generic interface...
            if (type.IsInterface && type.IsGenericType)
            {
                var genericType = type.GetGenericTypeDefinition();
                // Look through all the interfaces that the other type has
                foreach (var iface in other.GetInterfaces())
                {
                    // If the other interface is not generic, or it does not have the same generic type definition as us, then skip it
                    if (!iface.IsGenericType ||
                        type.GetGenericTypeDefinition() != iface.GetGenericTypeDefinition()) continue;

                    // These describe the generic arguments (in particular, if they are covariant or contravariant)
                    var genericArgs = genericType.GetGenericArguments();
                    // Extract the actual arguments from the type and the other type.
                    var expectedArgs = type.GetGenericArguments();
                    var gotArgs = iface.GetGenericArguments();

                    // For each argumetn
                    for (var i = 0; i < genericType.GetGenericArguments().Length; ++i)
                    {
                        var expected = expectedArgs[i];
                        var got = gotArgs[i];

                        var contravariant = (genericArgs[i].GenericParameterAttributes &
                            GenericParameterAttributes.Contravariant) != 0;
                        var covariant = (genericArgs[i].GenericParameterAttributes &
                            GenericParameterAttributes.Covariant) != 0;

                        if (!contravariant && !covariant && expected != got) return false;
                        if (covariant && !expected.WebSafeIsAssignableFrom(got)) return false;
                        if (contravariant && !got.WebSafeIsAssignableFrom(expected)) return false;
                    }

                    return true;
                }
                return false;
            }

            return false;
        }

        public static Type GetListElement(Type type)
        {
            if (type.IsArray)
            {
                return type.GetElementType();
            }

            if (typeof(IList).IsAssignableFrom(type))
            {
                var elementType = typeof(object);

                var ilistGeneric = type.GetGenericInterface(typeof(IList<>));
                if (ilistGeneric != null)
                {
                    var genericArgs = ilistGeneric.GetGenericArguments();
                    elementType = genericArgs[0];
                }

                return elementType;
            }

            return null;
        }
    }
}