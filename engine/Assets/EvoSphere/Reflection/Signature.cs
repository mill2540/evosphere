using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    public class Signature
    {
        public bool IsMethod { get; }
        public bool FirstArgumentIsGameObject { get; }

        public List<string> Names { get; }
        public Type ReturnType { get; }
        public List<Parameter> Parameters { get; }
        public Parameter ParamsParameter { get; }

        public int ParametersCount { get { return Parameters.Count + (ParamsParameter != null ? 1 : 0); } }

        public Signature(
            bool isMethod,
            List<string> names,
            Type returnType,
            List<Parameter> parameters,
            Parameter paramsParameter,
            bool firstArgumentIsGameObject = false)
        {
            this.IsMethod = isMethod;
            this.Names = names;
            this.ReturnType = returnType;

            this.Parameters = parameters;

            this.ParamsParameter = paramsParameter;
            this.FirstArgumentIsGameObject = firstArgumentIsGameObject;

        }

        public static Signature MethodLike(
            Type declaringType,
            bool isMethod,
            List<string> names,
            Type returnType,
            IEnumerable<Parameter> preParameters = null,
            IEnumerable<ParameterInfo> parametersInfo = null,
            bool firstArgumentIsGameObject = false)
        {
            var parameters = new List<Parameter>();

            if (isMethod)
            {
                parameters.Add(new Parameter("this", declaringType, false, null));
            }

            if (preParameters != null) { parameters.AddRange(preParameters); }

            Parameter paramsParameter;
            if (parametersInfo != null)
            {
                parameters.AddRange(
                    parametersInfo
                    .Where(parameter => !ReflectionUtils.IsParams(parameter))
                    .Select(parameter => new Parameter(parameter)));

                var paramsParameterInfo = parametersInfo.LastOrDefault();

                if (paramsParameterInfo != null &&
                    ReflectionUtils.IsParams(paramsParameterInfo))
                {
                    paramsParameter = new Parameter(paramsParameterInfo);
                }
                else { paramsParameter = null; }
            }
            else { paramsParameter = null; }

            return new Signature(isMethod, names, returnType, parameters, paramsParameter, firstArgumentIsGameObject);
        }

        public static Signature Method(MethodBase method,
            bool isMarkedAsMonoBehaviourMethodInitializer = false,
            bool firstArgumentIsGameObject = false,
            string[] names = null)
        {
            var parametersInfo = method.GetParameters();
            var parameters = new List<Parameter>();

            // I don't even want to handle static constructors at all
            if (method.IsStatic && method.IsConstructor) { throw new UnsupportedMethodException(method); }
            bool isMethod = !method.IsStatic && !method.IsConstructor;

            var namesList = new List<string>() { method.Name };

            if (names != null) { namesList.AddRange(names); }

            Type ReturnType;
            if (method.IsConstructor || isMarkedAsMonoBehaviourMethodInitializer)
            {
                ReturnType = method.DeclaringType;
                namesList.Add(ReturnType.Name);
            }
            else
            {
                ReturnType = (method as MethodInfo).ReturnType;
            }

            // TODO: Need to handle static monobehavior factories

            return Signature.MethodLike(
                method.DeclaringType,
                isMethod && !isMarkedAsMonoBehaviourMethodInitializer,
                namesList,
                ReturnType,
                parametersInfo : method.GetParameters(),
                firstArgumentIsGameObject : firstArgumentIsGameObject);
        }

        public bool Matches(string name = null, Type ret = null)
        {
            return (name == null || Names.Any(canidate => canidate.Equals(name))) &&
                (ret == null || ReflectionUtils.WebSafeIsAssignableFrom(ret, ReturnType));
        }

        public override string ToString()
        {
            return string.Format(
                "{0} [{1}]({2}{3})",
                ReflectionUtils.GetNiceName(ReturnType),
                string.Join(", ", Names),
                string.Join(", ", Parameters.Select(parameter => parameter.ToString())),
                ParamsParameter != null ? string.Format(", params {0}", ParamsParameter) : "");
        }
    }

}