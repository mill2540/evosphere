using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using UnityEngine;

namespace EvoSphere.Core.Reflection
{

    [System.Serializable]
    public class ScanException : System.Exception
    {
        public ScanException() { }
        public ScanException(string message) : base(message) { }
        public ScanException(string message, System.Exception inner) : base(message, inner) { }
        protected ScanException(
            System.Runtime.Serialization.SerializationInfo info,
            System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }

    public class Universe
    {
        private List<Assembly> scannedAssemblies = new List<Assembly>();
        private List<Type> scannedTypes = new List<Type>();

        private List<Callable> callables = new List<Callable>();
        private Dictionary<Guid, Callable> guidLookup = new Dictionary<Guid, Callable>();

        private static IEnumerable<ExposedInfo> GetAllExposed(IEnumerable<MemberInfo> enumerable, bool forceExpose)
        {
            return enumerable.Select((info) => ExposedInfo.GetExposed(info, forceExpose)).Where(value => value != null);
        }

        private static Universe instance;
        public static Universe Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Universe();
                }

                return instance;
            }
        }

        public void Scan(Assembly start = null, bool forceExpose = false)
        {
            // See https://stackoverflow.com/a/10253634/8055998 
            // for the basis for this code

            if (start == null)
            {
                start = Assembly.GetCallingAssembly();
            }

            if (scannedAssemblies.Contains(start))
            {
                return;
            }

            var assemblies = new List<Assembly>();
            assemblies.Add(start);
            scannedAssemblies.Add(start);

            int currentAssembly = 0;
            while (currentAssembly < assemblies.Count)
            {
                var assembly = assemblies[currentAssembly];

                // Add all the the referenced assemblies to the queue, and making sure that we never add
                // something that we have already scanned, or are planning to scan
                foreach (var referenced in assembly.GetReferencedAssemblies())
                {
                    if (scannedAssemblies.Any(o => AssemblyName.ReferenceMatchesDefinition(referenced, o.GetName())))
                    {
                        continue;
                    }
                    else
                    {
                        var referencedAssembly = Assembly.Load(referenced.FullName);
                        if (!referencedAssembly.FullName.Equals(referenced.FullName))
                        {
                            UnityEngine.Debug.LogWarning("Not loading " + referenced.FullName + " because C# attempted to load " +
                                referencedAssembly.FullName + " instead. This should not cause issues unless you need access to things in " +
                                referenced.FullName);
                        }
                        else
                        {
                            scannedAssemblies.Add(referencedAssembly);
                            assemblies.Add(referencedAssembly);
                        }

                    }
                }

                ScanAssembly(assembly, forceExpose);

                // Move to the next assembly in the stack
                currentAssembly += 1;
            }
        }

        private void ScanAssembly(Assembly assembly, bool forceExpose)
        {
            var results = new List<Callable>();

            try
            {
                var exportedTypes = assembly.ExportedTypes;

                foreach (var type in exportedTypes)
                {
                    Scan(ref results, type, forceExpose);
                }
            }
            catch (TypeLoadException) { }
            catch (ReflectionTypeLoadException) { }

            foreach (var found in results)
            {
                Insert(found);
            }
        }

        public void Scan(Type type, bool forceExpose = false)
        {
            if (scannedTypes.Contains(type))
            {
                return;
            }

            scannedTypes.Add(type);
            var results = new List<Callable>();

            Scan(ref results, type, forceExpose);

            foreach (var found in results)
            {
                Insert(found);
            }
        }

        private void Scan(ref List<Callable> results, Type type, bool forceExpose)
        {
            // create an object initializer
            if (forceExpose || type.IsDefined(typeof(ExposeAttribute), false))
            {
                var constructor = type.GetConstructor(new Type[0]);
                if (constructor != null || type.IsSubclassOf(typeof(ScriptableObject)) || type.IsSubclassOf(typeof(Component)))
                {
                    var exposed = type.GetCustomAttribute(typeof(ExposeAttribute), false) as ExposeAttribute;

                    results.Add(new ObjectInitializer(type, constructor, exposed?.Names ?? new string[0]));
                }
            }

            var constructors = type.GetConstructors();
            var properties = type.GetProperties();
            var fields = type.GetFields();
            var methods = type.GetMethods();

            if (type.IsSubclassOf(typeof(MonoBehaviour)))
            {

                foreach (var method in methods)
                {
                    if (method.IsDefined(typeof(MonoBehaviourMethodInitializerAttribute)))
                    {
                        var attr = (MonoBehaviourMethodInitializerAttribute) method.GetCustomAttribute(typeof(MonoBehaviourMethodInitializerAttribute), false);
                        results.Add(new MonoBehaviourMethodInitalizer(type, method, attr.Names));
                    }
                }
            }

            foreach (var exposed in
                    GetAllExposed(
                        constructors.Cast<MemberInfo>()
                        .Concat(properties)
                        .Concat(fields)
                        .Concat(methods),
                        forceExpose))
            {
                ScanExposed(ref results, exposed);
            }
        }

        private void ScanExposed(ref List<Callable> results, ExposedInfo exposed)
        {
            if (exposed.Callable != null) { Insert(exposed.Callable); }
            else if (exposed.Info is FieldInfo field)
            {
                results.Add(new Field(Access.Get, field, exposed.Names));
                results.Add(new Field(Access.Set, field, exposed.Names));
            }
            else if (exposed.Info is ConstructorInfo constructor)
            {
                results.Add(new Constructor(constructor, exposed.Names));
            }
            else if (exposed.Info is MethodInfo method)
            {
                results.Add(new Method(method, exposed.Names));
            }
            else if (exposed.Info is PropertyInfo property)
            {
                if (property.GetGetMethod() != null) { results.Add(new Property(Access.Get, property, exposed.Names)); }
                if (property.GetSetMethod() != null) { results.Add(new Property(Access.Set, property, exposed.Names)); }
            }
            else
            {
                throw new Exception(string.Format("Unexpected info type {0} when scanning", exposed.Info.GetType().Name));
            }
        }

        private void Insert(Callable callable)
        {
            callables.Add(callable);
            guidLookup.Add(callable.Id, callable);
            // TODO: add some caching
        }

        public Callable GetById(string id)
        {
            return GetById(Guid.Parse(id));
        }

        public Callable GetById(Guid id)
        {
            return guidLookup[id];
        }

        public bool TryGetById(string id, out Callable callable)
        {
            return TryGetById(Guid.Parse(id), out callable);
        }

        public bool TryGetById(Guid id, out Callable callable)
        {
            return guidLookup.TryGetValue(id, out callable);
        }

        public void Debug()
        {
            foreach (var callable in callables)
            {
                UnityEngine.Debug.Log(callable);
            }
        }

        public IEnumerable<Callable> FindAll(string name = null, Type ret = null)
        {
            return this.callables
                .Where(callable => callable.Signature.Matches(name, ret));
        }

        private Type TypeChk(Type o, Type hint, out string error)
        {
            if (o == null)
            {
                if (ReflectionUtils.AllowsNull(hint))
                {
                    error = null;
                    return hint;
                }
                else
                {
                    error = $"cannot assign null to argument of type {ReflectionUtils.GetNiceName(hint)}";
                    return null;
                }
            }
            else
            {
                if (ReflectionUtils.WebSafeIsAssignableFrom(hint, o))
                {
                    error = null;
                    return o.GetType();
                }
                else
                {
                    error = $"cannot assign value of type {ReflectionUtils.GetNiceName(o)} to argument of type {ReflectionUtils.GetNiceName(hint)}";
                    return null;
                }
            }
        }

        public Callable Find(
            string name, Type ret,
            IEnumerable<Type> args,
            IEnumerable<KeyValuePair<string, Type>> kwargs)
        {
            string error;
            var callable = CallUtilities.SelectCallable(
                name, ret,
                args, kwargs,
                TypeChk,
                out error
            );

            if (error != null)
            {
                throw new Exception(error);
            }
            else
            {
                return callable;
            }
        }

        public Callable Find(
            string name, Type ret,
            params Type[] args)
        {
            return Find(name, ret, args, new KeyValuePair<string, Type>[0]);
        }
    }
}