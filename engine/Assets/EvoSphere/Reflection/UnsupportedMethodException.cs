using System;
using System.Reflection;

namespace EvoSphere.Core.Reflection
{
    public class UnsupportedMethodException : Exception
    {
        public UnsupportedMethodException(MethodBase method) : base(
            string.Format("Cannot create Callable from {0}", method)) { }
    }

    // public static class Callable
    // {
    //     public IEnumerable<CallableBase> CollectCallables(Type type)
    //     {

    //     }
    // }
}