mergeInto(LibraryManager.library, {
  WebGLUnityReady: function(name) {
    window.WebGLUnityReady(Pointer_stringify(name));
  },

  WebGLUnityEvent: function(event) {
    window.WebGLUnityEvent(Pointer_stringify(event));
  },

  WebGLUnityResponse: function(response) {
    window.WebGLUnityResponse(Pointer_stringify(response));
  }
});
