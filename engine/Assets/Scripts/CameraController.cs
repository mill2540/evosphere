﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{

    private bool rotate = false;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.RightShift) || Input.GetKeyDown(KeyCode.LeftShift))
        {
            rotate = true;
        }
        if (Input.GetKeyUp(KeyCode.RightShift) || Input.GetKeyUp(KeyCode.LeftShift))
        {
            rotate = false;
        }

        if (rotate)
        {
            if (Input.GetKeyDown("w")) transform.Rotate(Vector3.right, 15, Space.World);
            if (Input.GetKeyDown("s")) transform.Rotate(Vector3.right, -15, Space.World);

            if (Input.GetKeyDown("a")) transform.Rotate(Vector3.up, 15, Space.World);
            if (Input.GetKeyDown("d")) transform.Rotate(Vector3.up, -15, Space.World);

            if (Input.GetKeyDown("q")) transform.Rotate(Vector3.forward, 15, Space.World);
            if (Input.GetKeyDown("e")) transform.Rotate(Vector3.forward, -15, Space.World);
        }
        else
        {
            if (Input.GetKeyDown("w")) transform.position += Vector3.forward;
            if (Input.GetKeyDown("s")) transform.position += Vector3.back;

            if (Input.GetKeyDown("a")) transform.position += Vector3.left;
            if (Input.GetKeyDown("d")) transform.position += Vector3.right;

            if (Input.GetKeyDown("q")) transform.position += Vector3.up;
            if (Input.GetKeyDown("e")) transform.position += Vector3.down;
        }

    }
}