﻿// using System.Collections;
// using System.Collections.Generic;
// using System.Reflection;
// using EvoSphere.Core.Reflection;
// using NUnit.Framework;
// using UnityEngine;
// using UnityEngine.TestTools;

// namespace Tests
// {
//     public class TestCallable
//     {

//         public class TestType
//         {
//             [Expose]
//             public void Foo() { }

//             [Expose]
//             public static void StaticFoo() { }

//             [Expose]
//             public static void Overload() { }

//             [Expose]
//             public static void Overload(int x) { }

//             [Expose]
//             public void InstanceOverload() { }

//             [Expose]
//             public void InstanceOverload(int x) { }

//             [Expose]
//             public static int Double(int x) { return 2 * x; }

//             [Expose]
//             public static string DoublePlusLength(int x, params object[] args)
//             {
//                 if (args == null) { return "ARGS IS NULL " + x.ToString(); }
//                 else
//                 {
//                     return (2 * x + args.Length).ToString();
//                 }
//             }

//             [Expose]
//             public int RwValue { get; set; }

//             [Expose]
//             public static int StaticRwValue { get; set; }

//             [Expose]
//             public int RValue { get; }

//             [Expose]
//             public static int StaticfValue;

//             [Expose]
//             public int fValue;

//             [Expose]
//             public TestType(int rw = 1, int r = 2, int f = 3) { RwValue = rw; RValue = r; fValue = f; }

//             public override string ToString()
//             {
//                 return string.Format("RwValue = {0} RValue = {1} fValue = {2}", RwValue, RValue, fValue);
//             }

//             public override bool Equals(object obj)
//             {
//                 if (obj == null) { return false; }
//                 else
//                 {
//                     var other = obj as TestType;
//                     return (obj != null) &&
//                         RwValue == other.RwValue &&
//                         RValue == other.RValue &&
//                         fValue == other.fValue;
//                 }
//             }
//         }

//         public class TestType2
//         {
//             [Expose]
//             public TestType2(int rw = 1, int r = 2, int f = 3) { }

//             [Expose]
//             public static TestType2 Create(string str = null) { return null; }
//         }

//         [Test]
//         public void TestInstanceBinding()
//         {
//             var universe = new Universe();
//             var instance = new TestType();

//             Assert.Throws(typeof(NoMatchingCallableException),
//                 () => universe.Bind("Foo"),
//                 "Since the assembly has not been scanned, we don't expect to see anything yet");

//             universe.Scan();

//             Assert.DoesNotThrow(() => universe.Bind("Foo",
//                     kwargs : new Dictionary<string, object>() { { "this", instance } }),
//                 "The assembly has been scanned, and we tried to bind to Foo correctly, so this should work");

//             Assert.Throws(typeof(NoMatchingCallableException),
//                 () => universe.Bind("Bar",
//                     kwargs : new Dictionary<string, object>() { { "this", instance } }),
//                 "Since Bar does not exist on Exposed, this should fail");
//         }

//         [Test]
//         public void TestStaticBinding()
//         {
//             var universe = new Universe();

//             Assert.Throws(typeof(NoMatchingCallableException),
//                 () => universe.Bind("StaticFoo"),
//                 "Since the assembly has not been scanned, we don't expect to see anything yet");

//             universe.Scan();

//             Assert.DoesNotThrow(() => universe.Bind("StaticFoo"),
//                 "The assembly has been scanned, and we tried to bind to Foo correctly, so this should work");

//             Assert.Throws(typeof(NoMatchingCallableException),
//                 () => universe.Bind("StaticBar"),
//                 "Since Bar does not exist on Exposed, this should fail");
//         }

//         [Test]
//         public void TestStaticOverloading()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             Assert.DoesNotThrow(() => universe.Bind("Overload"),
//                 "This should bind to Overload()");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("Overload",
//                     kwargs : new Dictionary<string, object>() { { "x", 5 } }),
//                 "This should bind to Overload(int x)");

//             Assert.Throws(
//                 typeof(NoMatchingCallableException),
//                 () => universe.Bind("Overload",
//                     kwargs : new Dictionary<string, object>() { { "x", 5 }, { "y", 5 } }),
//                 "This should bind nothing because Overload(int, int) does not exist");
//         }

//         [Test]
//         public void TestInstanceOverloading()
//         {
//             var universe = new Universe();
//             var instance = new TestType();
//             universe.Scan();

//             Assert.DoesNotThrow(
//                 () => universe.Bind("InstanceOverload",
//                     kwargs : new Dictionary<string, object>()
//                     { { "this", instance }
//                     }),
//                 "This should bind to InstanceOverload(this)");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("InstanceOverload",
//                     kwargs : new Dictionary<string, object>()
//                     { { "this", instance }, { "x", 5 }
//                     }),
//                 "This should bind to InstanceOverload(this, int x)");

//             Assert.Throws(
//                 typeof(NoMatchingCallableException),
//                 () => universe.Bind("InstanceOverload", kwargs : new Dictionary<string, object>()
//                 { { "this", instance }, { "x", 5 }, { "y", 5 }
//                 }),
//                 "This should bind nothing because InstanceOverload(this, int, int) does not exist");
//         }

//         [Test]
//         public void TestStaticOverloadingArgs()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             Assert.DoesNotThrow(
//                 () => universe.Bind("Overload"),
//                 "This should bind to Overload()");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("Overload",
//                     args : new object[] { 10 }),
//                 "This should bind to Overload(int x)");

//             Assert.Throws(
//                 typeof(NoMatchingCallableException),
//                 () => universe.Bind("Overload",
//                     args : new object[] { 10, 11 }),
//                 "This should bind nothing because Overload(int, int) does not exist");
//         }

//         [Test]
//         public void TestInstanceOverloadingArgs()
//         {
//             var universe = new Universe();
//             var instance = new TestType();
//             universe.Scan();

//             Assert.DoesNotThrow(
//                 () => universe.Bind("InstanceOverload", args : new object[] { instance }),
//                 "This should bind to InstanceOverload(this)");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("InstanceOverload", args : new object[] { instance, 10 }),
//                 "This should bind to InstanceOverload(this, int x)");

//             Assert.Throws(
//                 typeof(NoMatchingCallableException),
//                 () => universe.Bind("InstanceOverload", args : new object[] { instance, 10, 11 }),
//                 "This should bind nothing because InstanceOverload(this, int, int) does not exist");
//         }

//         [Test]
//         public void TestCalling()
//         {
//             var universe = new Universe();
//             var instance = new TestType();
//             universe.Scan();

//             // Make sure that our sanity check is working
//             Assert.AreEqual(TestType.Double(4), 8);

//             Assert.AreEqual(
//                 universe.Call("Double", args : new object[] { 4 }),
//                 TestType.Double(4));
//         }

//         [Test]
//         public void TestParams()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             Assert.AreEqual(TestType.DoublePlusLength(4, 5, 6), "10");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("DoublePlusLength", args : new object[] { 4, 5, 6 })
//             );

//             Assert.AreEqual(
//                 universe.Call("DoublePlusLength", args : new object[] { 4, 5, 6 }),
//                 TestType.DoublePlusLength(4, 5, 6),
//                 "We should handle params methods the same as C# does");

//             Assert.AreEqual(
//                 universe.Call("DoublePlusLength", args : new object[] { 4, new object[] { 5, 6, 7 } }),
//                 TestType.DoublePlusLength(4, new object[] { 5, 6, 7 }),
//                 "Even when passing the array explicitly");

//             Assert.AreEqual(
//                 universe.Call("DoublePlusLength", args : new object[] { 4, null }),
//                 TestType.DoublePlusLength(4, null),
//                 "Make sure that a single null in the params slot is handled correctly");

//             Assert.AreEqual(
//                 universe.Call("DoublePlusLength", args : new object[] { 4, null, null, null }),
//                 TestType.DoublePlusLength(4, null, null, null),
//                 "Make sure that multiple null's in the params slot is handled correctly");
//         }

//         [Test]
//         public void TestConstructors()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             Assert.AreEqual(universe.Call("TestType"), new TestType());

//             Assert.AreEqual(
//                 universe.Call("TestType", args : new object[] { 10 }),
//                 new TestType(10));

//             Assert.AreEqual(
//                 universe.Call("TestType", args : new object[] { 10, 11 }),
//                 new TestType(10, 11));

//             Assert.AreEqual(
//                 universe.Call("TestType", args : new object[] { 10, 11, 12 }),
//                 new TestType(10, 11, 12));
//         }

//         [Test]
//         public void TestProperties()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             var instance = new TestType();

//             Assert.DoesNotThrow(
//                 () => universe.Bind("StaticRwValue"),
//                 "Bind to the getter of StaticRwValue should work");

//             Assert.Throws(
//                 typeof(NoMatchingCallableException),
//                 () => universe.Bind("RwValue"),
//                 "Bind to the getter of RwValue should fail because RwValue is not static");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("RwValue", args : new object[] { instance }),
//                 "Bind to the getter of RwValue as RwValue(instance) should work, " +
//                 "because it is acting like a member getter function");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("RValue", args : new object[] { instance }),
//                 "Bind to the getter of RwValue as RwValue(instance) should work, " +
//                 "because it is acting like a member getter function");

//             Assert.DoesNotThrow(
//                 () => universe.Bind("RwValue", args : new object[] { instance, 10 }),
//                 "Bind to the setter of RwValue as RwValue(instance, value) should work, " +
//                 "because it is acting like a member getter function");

//             Assert.Throws(
//                 typeof(NoMatchingCallableException),
//                 () => universe.Bind("RValue", args : new object[] { instance, 10 }),
//                 "Bind to the setter of RValue as RValue(instance, value) should fail, " +
//                 "because it does not have a getter");

//             Assert.AreEqual(universe.Call("StaticRwValue"), TestType.StaticRwValue);
//             Assert.AreEqual(
//                 universe.Call("RwValue", args : new object[] { instance }),
//                 instance.RwValue);

//             Assert.AreEqual(
//                 universe.Call("RValue", args : new object[] { instance }),
//                 instance.RValue);

//             universe.Call("RwValue", args : new object[] { instance, 1234 });
//             Assert.AreEqual(instance.RwValue, 1234);
//         }

//         [Test]
//         public void TestFields()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             var instance = new TestType();

//             Assert.DoesNotThrow(() => universe.Bind("StaticfValue"));

//             Assert.DoesNotThrow(
//                 () => universe.Bind("StaticfValue", args : new object[] { 10 }));

//             Assert.Throws(typeof(NoMatchingCallableException),
//                 () => universe.Bind("fValue"));

//             Assert.DoesNotThrow(
//                 () => universe.Bind("fValue", args : new object[] { instance }));

//             Assert.DoesNotThrow(
//                 () => universe.Bind("fValue", args : new object[] { instance, 10 }));

//             Assert.AreEqual(universe.Call("StaticfValue"), TestType.StaticfValue);
//             Assert.AreEqual(
//                 universe.Call("fValue", args : new object[] { instance }),
//                 instance.fValue);

//             universe.Call("StaticfValue", args : new object[] { 1234 });
//             Assert.AreEqual(TestType.StaticfValue, 1234);
//             universe.Call("fValue", args : new object[] { instance, 1234 });
//             Assert.AreEqual(instance.fValue, 1234);
//         }

//         [Test]
//         public void TestOverloadByReturnType()
//         {
//             var universe = new Universe();
//             universe.Scan();

//             Assert.DoesNotThrow(
//                 () => universe.Bind(ret: typeof(TestType).FuzzyFilter()),
//                 "It should be possible to deduce that we want the constructor " +
//                 "for TestType just based on the return type in this case");

//             Assert.Throws(
//                 typeof(MultipleMatchingCallablesException),
//                 () => universe.Bind(ret: typeof(TestType2).FuzzyFilter()),
//                 "This should get confused between the constructor of TestType2 and TestType2.Create");

//             Assert.DoesNotThrow(
//                 () => universe.Bind(name: "TestType2", ret : typeof(TestType2).FuzzyFilter()),
//                 "By giving the name we want, we should be able to select the one we want");

//             Assert.DoesNotThrow(
//                 () => universe.Bind(
//                     ret: typeof(TestType2).FuzzyFilter(),
//                     args: new object[] { "Hello World" }),
//                 "Also, if we specify that the object must take a string as an argument, " +
//                 "then it should also be unambiguous");
//         }
//     }
// }