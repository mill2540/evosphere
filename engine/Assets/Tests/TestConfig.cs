﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestConfig
    {
        public class TestingClass
        {
            [Expose]
            int one;

            [Expose]
            int two;

            [Expose]
            public TestingClass(int one = 1, int two = 2)
            {
                this.one = one;
                this.two = two;
            }

            [Expose]
            public TestingClass(int one, int two, string value)
            {
                this.one = one;
                this.two = two * 2;
            }

            [Expose]
            public static TestingClass Create1(int one) { return new TestingClass(one: one); }

            [Expose]
            public static TestingClass Create2(int two) { return new TestingClass(two: two); }

            public override bool Equals(object obj)
            {
                if (obj is TestingClass other)
                {
                    return one == other.one && two == other.two;
                }
                return false;
            }

            public override int GetHashCode() =>
                throw new NotImplementedException();
        }

        [Expose]
        public class TestObjectInit
        {
            public int one = 1;
            public int two = 2;

            public override bool Equals(object obj)
            {
                if (obj is TestObjectInit other)
                {
                    return one == other.one && two == other.two;
                }
                return false;
            }

            public override int GetHashCode() =>
                throw new NotImplementedException();

            public override string ToString() => string.Format("TestObjectInit [ one={0}, two={1} ]", one, two);
        }

        public abstract class Base
        {
            public string value;

            public override string ToString() => value;

            public override bool Equals(object other)
            {
                if (other == null || other.GetType() != GetType()) return false;
                var otherValue = ((Base) other).value;
                return (value == null && otherValue == null) || value.Equals(otherValue);
            }

            public override int GetHashCode() => value.GetHashCode();
        }

        [Expose]
        public class A : Base { }

        [Expose]
        public class B : Base { }

        [Expose]
        public class TestFactory
        {
            public Data value;
        }

        [SetUp]
        public void InitializeUniverse()
        {
            Universe.Instance.Scan();
            // Universe.Instance.Debug();
            Universe.Instance.Scan(typeof(Convert), forceExpose : true);
        }

        [Test]
        public void TestScalars()
        {
            Assert.AreEqual(
                DataIO.Load("null").Invoke(typeof(object)),
                null,
                "Test loading null");

            Assert.AreEqual(
                DataIO.Load("\"\"").Invoke(typeof(string)),
                "",
                "Test loading the empty string");

            Assert.AreEqual(
                DataIO.Load("10").Invoke(typeof(int)),
                10,
                "Test loading integers");

            Assert.AreEqual(
                DataIO.Load("123.456").Invoke(typeof(float)),
                123.456f,
                "Test loading integers");

            Assert.AreEqual(
                DataIO.Load("/a/b/c").Invoke(typeof(string)),
                "/a/b/c",
                "Test loading paths");

            Assert.AreEqual(
                DataIO.Load(@"\a\b\c").Invoke(typeof(string)),
                @"\a\b\c",
                "Test windows paths");

            Assert.AreEqual(
                DataIO.Load("\"hello\\n\\t\\\"world\"").Invoke(typeof(string)),
                "hello\n\t\"world",
                "Test escapes");
        }

        [Test]
        public void TestCreateObjectFromAst()
        {
            Assert.AreEqual(
                new CallData().Invoke(typeof(TestingClass)),
                new TestingClass(),
                "We should be able to use the default constructor here");

            Assert.AreEqual(
                new CallData("Create1") { 10 }.Invoke(typeof(TestingClass)),
                TestingClass.Create1(10),
                "We should be able to use the Create1 method here");

            Assert.AreEqual(
                new CallData("Create2") { 20 }.Invoke(typeof(TestingClass)),
                TestingClass.Create2(20),
                "We should be able to use the Create2 method here");
        }

        [Test]
        public void TestCreateObjectInit()
        {
            Assert.AreEqual(
                new CallData() { { "one", 10 } }.Invoke(typeof(TestObjectInit)),
                new TestObjectInit { one = 10 },
                "We should be able to use the one-argument constructor here");

            Assert.AreEqual(
                new CallData() { { "one", 10 }, { "two", 20 } }.Invoke(typeof(TestObjectInit)),
                new TestObjectInit { one = 10, two = 20 },
                "We should be able to use the two-argument constructor here");
        }

        [Test]
        public void TestLoadFromString()
        {
            Assert.AreEqual(
                DataIO.Load(@"
                        {one = 10,
                        two = 20}")
                .Invoke(typeof(TestObjectInit)),
                new TestObjectInit { one = 10, two = 20 });

            Assert.AreEqual(
                DataIO.Load(@"[10, 20]").Invoke(typeof(TestingClass)),
                new TestingClass(10, 20));

            Assert.AreEqual(
                DataIO.Load(
                    @"{one = 10,two = 20}").Invoke(typeof(TestingClass)),
                new TestingClass(10, 20));

            Assert.AreEqual(
                DataIO.Load(
                    @"Create1[10]").Invoke(typeof(TestingClass)),
                TestingClass.Create1(10));

            Assert.AreEqual(
                DataIO.Load(@"Create2[10]").Invoke(typeof(TestingClass)),
                TestingClass.Create2(10));
        }

        [Test]
        public void TestFactories()
        {
            var a = new CallData("A") { "Hello World" }.Invoke(typeof(A));
            Assert.AreEqual(
                a,
                new A { value = "Hello World" }
            );

            var data = new CallData()
            {
                new CallData("A") { "Hello World" }
            };
            var inst = data.Invoke(typeof(TestFactory));
            var testFactory = (TestFactory) inst;

            Assert.AreEqual(testFactory.value.Invoke(typeof(Base)), new A { value = "Hello World" });

        }

    }
}