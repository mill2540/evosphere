﻿using System;
using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Config;
using EvoSphere.Core.Reflection;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestConfigWithUnityTypes
    {
        [Expose]
        public class TestingScriptableObject : ScriptableObject
        {
            public string scriptableData;

            // override object.Equals
            public override bool Equals(object obj)
            {
                if (obj != null && obj is TestingScriptableObject tso)
                {
                    return scriptableData.Equals(tso.scriptableData);
                }
                else
                {
                    return false;
                }

            }

            // override object.GetHashCode
            public override int GetHashCode()
            {
                return scriptableData.GetHashCode();
            }
        }

        [Expose]
        public class NormalObject
        {
            public string normalData;

            // override object.Equals
            public override bool Equals(object obj)
            {
                if (obj != null && obj is NormalObject o)
                {
                    return normalData.Equals(o.normalData);
                }
                else
                {
                    return false;
                }

            }

            // override object.GetHashCode
            public override int GetHashCode()
            {
                return normalData.GetHashCode();
            }
        }

        [Expose]
        public class TestingBehavior : MonoBehaviour
        {
            public string behaviourData;
            public TestingScriptableObject tso;
            public NormalObject no;

            // override object.Equals
            public override bool Equals(object obj)
            {
                if (obj != null && obj is TestingBehavior o)
                {
                    return behaviourData.Equals(o.behaviourData) && tso.Equals(o.tso) && no.Equals(o.no);
                }
                else
                {
                    return false;
                }
            }

            public override int GetHashCode() { throw new NotImplementedException(); }
        }

        public class TestingMethodInitializer : MonoBehaviour
        {
            public string behaviourData;

            [MonoBehaviourMethodInitializer]
            [Expose]
            public void Create(string behaviourData)
            {
                this.behaviourData = behaviourData;
            }

            [MonoBehaviourMethodInitializer]
            [Expose]
            public void CreateDefault()
            {
                this.behaviourData = "Defaulted!";
            }

            // override object.Equals
            public override bool Equals(object obj)
            {
                if (obj != null && obj is TestingMethodInitializer tmi)
                {
                    return behaviourData.Equals(tmi.behaviourData);
                }
                else
                {
                    return false;
                }

            }

            // override object.GetHashCode
            public override int GetHashCode()
            {
                return behaviourData.GetHashCode();
            }

            public override string ToString() => $"{GetType().Name}(behaviourData={behaviourData})";
        }

        public class TestingMethodInitializerDefault : MonoBehaviour
        {
            public string behaviourData;

            [MonoBehaviourMethodInitializer]
            [Expose]
            public void Create(string behaviourData, string ignoreMe)
            {
                this.behaviourData = behaviourData;
            }

            [MonoBehaviourMethodInitializer]
            [Expose]
            public void CreateDefault()
            {
                this.behaviourData = "Defaulted!";
            }

            // override object.Equals
            public override bool Equals(object obj)
            {
                if (obj != null && obj is TestingMethodInitializerDefault tmid)
                {
                    return behaviourData.Equals(tmid.behaviourData);
                }
                else
                {
                    return false;
                }

            }

            // override object.GetHashCode
            public override int GetHashCode()
            {
                return behaviourData.GetHashCode();
            }

            public override string ToString() => $"{GetType().Name}(behaviourData={behaviourData})";
        }

        [SetUp]
        public void InitializeUniverse()
        {
            Universe.Instance.Scan();
            Universe.Instance.Scan(typeof(Convert), forceExpose : true);
        }

        // A UnityTest behaves like a coroutine in Play Mode. In Edit Mode you can use
        // `yield return null;` to skip a frame.
        [UnityTest]
        public IEnumerator TestCreatingSimpleUnityTypes()
        {
            var target = new GameObject();

            var configString = "{ behaviourData = \"hello world\", tso = { scriptableData = \"Hello From ScriptableObjects\" }, no = { normalData = \"Hello From Normal Data\" } }";
            var config = DataIO.Load(configString);

            var testingBehavior = config.Invoke(typeof(TestingBehavior), target);

            var known = new GameObject();
            var knownTestingBehavior = known.AddComponent<TestingBehavior>();
            knownTestingBehavior.behaviourData = "hello world";
            knownTestingBehavior.tso = ScriptableObject.CreateInstance<TestingScriptableObject>();
            knownTestingBehavior.tso.scriptableData = "Hello From ScriptableObjects";
            knownTestingBehavior.no = new NormalObject { normalData = "Hello From Normal Data" };

            Assert.AreEqual(testingBehavior, knownTestingBehavior);

            yield return null;
        }

        [UnityTest]
        public IEnumerator TestMonoBehaviourMethodInitializers()
        {
            var target = new GameObject();

            var configString = "{ behaviourData = \"hello world\" }";
            var config = DataIO.Load(configString);

            var knownObject = new GameObject();
            var knownTestingBehavior = knownObject.AddComponent<TestingMethodInitializer>();
            knownTestingBehavior.Create("hello world");

            var canidate = config.Invoke(typeof(TestingMethodInitializer), target);
            Assert.AreEqual(canidate, knownTestingBehavior);

            yield return null;
        }

        [UnityTest]
        public IEnumerator TestMonoBehaviourMethodInitializerFromDefaultScalar()
        {
            var target = new GameObject();

            var configString = "CreateDefault";
            var config = DataIO.Load(configString);

            var knownObject = new GameObject();
            var knownTestingBehavior = knownObject.AddComponent<TestingMethodInitializerDefault>();
            knownTestingBehavior.CreateDefault();

            Assert.Throws(typeof(System.Exception), () =>
            {
                // We should have an ambiguity between CreateDefault and Create for scalars
                config.Invoke(typeof(TestingMethodInitializer), target);
            });

            var canidate = config.Invoke(typeof(TestingMethodInitializerDefault), target);;
            Assert.AreEqual(canidate, knownTestingBehavior);

            yield return null;
        }
    }
}