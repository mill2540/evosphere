﻿using System;
using System.Collections;
using System.Collections.Generic;
using EvoSphere.Core.Config;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests
{
    public class TestData
    {
        private static Tuple<CallData, string, string[]>[] Examples()
        {
            return new Tuple<CallData, string, string[]>[]
            {
                Tuple.Create(
                        new CallData("Hello"),
                        "Hello[]", new string[]
                        {
                            "Hello []",
                            "\"Hello\"  {}"
                        }),

                    Tuple.Create(
                        new CallData("Hello") { "foo", "bar" },
                        "Hello[foo,bar]", new string[]
                        {
                            "Hello [foo,    bar]",
                            "\"Hello\"  [   foo, \"bar\"]"
                        }),

                    Tuple.Create(
                        new CallData() { "foo", "bar" },
                        "[foo,bar]", new string[]
                        {
                            " [foo,    bar]",
                            "[   foo, \"bar\"]"
                        }),

                    Tuple.Create(
                        new CallData("Hello") { new CallData { "x", "y" }, "bar" },
                        "Hello[[x,y],bar]",
                        new string[] { }
                    ),

                    Tuple.Create(
                        new CallData("Hel\"lo") { new CallData { "x", "y" }, "ba,r" },
                        "\"Hel\\\"lo\"[[x,y],\"ba,r\"]", new string[] { }
                    ),

                    Tuple.Create(
                        new CallData("Hello") { { "x", "a" }, { "y", "b" } },
                        "Hello{x=a,y=b}", new string[]
                        {
                            "Hello[]{x = a, y = b}",
                            "Hello{\"x\" = a, y = b}",
                            "Hello{\"x\" = a, y = \"b\"}",
                        }
                    ),

                    Tuple.Create(
                        new CallData() { { "x", "a" }, { "y", "b" } },
                        "{x=a,y=b}", new string[]
                        {
                            "[]{x = a, y = b}",
                            "{\"x\" = a, y = b}",
                            "{\"x\" = a, y = \"b\"}",
                        }
                    ),

                    Tuple.Create(
                        new CallData("Hello") { { null, "1" }, { null, "2" }, { "x", "a" }, { "y", "b" } },
                        "Hello[1,2]{x=a,y=b}", new string[]
                        {
                            "Hello[1,2]{x = a, y = b}",
                            "Hello[1,2]{\"x\" = a, y = b}",
                            "Hello[1,2]{\"x\" = a, y = \"b\"}",
                        }
                    )
            };
        }

        // A Test behaves as an ordinary method
        [Test]
        public void TestSaveData()
        {
            foreach (var example in Examples())
            {
                Assert.AreEqual(example.Item1.Save(), example.Item2);
            }
        }

        [Test]
        public void TestLoadData()
        {
            foreach (var example in Examples())
            {
                Assert.AreEqual(
                    DataIO.Load(example.Item2),
                    example.Item1,
                    $"Loading data from {example.Item2}");

                foreach (var extra in example.Item3)
                {
                    Assert.AreEqual(
                        DataIO.Load(extra),
                        example.Item1,
                        $"Loading data from {example.Item2}");
                }
            }
        }
    }
}