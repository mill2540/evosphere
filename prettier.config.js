module.exports = {
  arrowParens: "always",
  bracketSpacing: true,
  jsxBracketSameLine: false,
  overrides: [
    {
      files: "*.ts",
      options: {
        parser: "typescript",
      },
    },
    {files: "*.tsx", options: {parser: "typescript"}},
  ],
  semi: true,
  singleQuote: false,
  tabWidth: 4,
  trailingComma: "es5",
  useTabs: false,
};
